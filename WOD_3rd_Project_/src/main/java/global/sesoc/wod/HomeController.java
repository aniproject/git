package global.sesoc.wod;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(HttpSession session) {
		return "home";
	}
	
	@RequestMapping(value = "/moveTempChanel", method = RequestMethod.GET)
	public String moveTempChanel(Locale locale, Model model) {
	
		return "Trainer/TrainerChannel2";
	}
	@RequestMapping(value = "/GoDetail", method = RequestMethod.GET)
	public String GoDetail(Locale locale, Model model) {
	
		return "User/ChannelDetail";
	}

}
