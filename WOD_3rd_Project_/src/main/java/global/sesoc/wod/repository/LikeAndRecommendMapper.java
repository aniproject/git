package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import global.sesoc.wod.dto.LikeAndRecommendDTO;

public interface LikeAndRecommendMapper {
	
	int insertLike(LikeAndRecommendDTO lar); // 좋아요에 추가
	
	int deleteLike(LikeAndRecommendDTO lar);
	
	LikeAndRecommendDTO selectLike(LikeAndRecommendDTO lar);
	
	LikeAndRecommendDTO selectLikeAndRecommendCount(int channelseq);

	ArrayList<HashMap<String, String>> selectLikeList(int userseq);
	
}
