package global.sesoc.wod.repository;

import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.User_BasicDTO;
import global.sesoc.wod.dto.User_RoleDTO;

@Repository
public class UserRepository {
	
	@Autowired
	SqlSession session;

	public User_BasicDTO selectOneUser(User_BasicDTO user) {
		// TODO Auto-generated method stub
		User_BasicDTO u = new User_BasicDTO();
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
		u = mapper.selectOneUser(user);
		}catch(Exception e){
			return u;
		}
		return u;
	}

	public boolean insertUser(User_BasicDTO user) {
		// TODO Auto-generated method stub
		boolean insertResult = false;
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
		insertResult = mapper.insertUser(user);
		}catch(Exception e){
			return insertResult;
		}
		return insertResult;
	}

	public boolean insertUserRole(User_BasicDTO u) {
		// TODO Auto-generated method stub
		boolean insertUserRoleResult = false;
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
		insertUserRoleResult = mapper.insertUserRole(u);
		}catch(Exception e){
			return insertUserRoleResult;
		}
		return insertUserRoleResult;
	}

	public User_RoleDTO selectRoleseq(int userseq) {
		// TODO Auto-generated method stub
		User_RoleDTO uRole = new User_RoleDTO();
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
		uRole = mapper.selectRoleseq(userseq);
		}catch(Exception e){
			return uRole;
		}
		return uRole;
	}

	public boolean updateUserRole(User_RoleDTO uRole) {
		// TODO Auto-generated method stub
		boolean updateUserRoleResult = false;
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
			updateUserRoleResult = mapper.updateUserRole(uRole);
		}catch(Exception e){
			return updateUserRoleResult;
		}
		return updateUserRoleResult;
		
	}

	public boolean rollbackUserRole(User_RoleDTO uRole) {
		// TODO Auto-generated method stub
		boolean rollbackUserRoleResult = false;
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
		rollbackUserRoleResult = mapper.rollbackUserRole(uRole);
		}catch(Exception e){
			return rollbackUserRoleResult;
		}
		return rollbackUserRoleResult;
	}

	public User_BasicDTO passwordCheck(User_BasicDTO user) {
		// TODO Auto-generated method stub
		User_BasicDTO passwordCheckResult = new User_BasicDTO();
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
		passwordCheckResult = mapper.passwordCheck(user);
		}catch(Exception e){
			return passwordCheckResult;
		}
		return passwordCheckResult;
	}

	public boolean updateUserBasic(User_BasicDTO user) {
		// TODO Auto-generated method stub
		boolean updateUserBasicResult = false;
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
			updateUserBasicResult = mapper.updateUserBasic(user);
		}catch(Exception e){
			return updateUserBasicResult;
		}
		return updateUserBasicResult;

	}

	public boolean deleteUserRole(int userseq) {
		// TODO Auto-generated method stub
		boolean deleteUserRoleResult = false;
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
			deleteUserRoleResult = mapper.deleteUserRole(userseq);
		}catch(Exception e){
			return deleteUserRoleResult;
		}
		return deleteUserRoleResult;
		
	}

	public boolean deleteUserBasic(int userseq) {
		// TODO Auto-generated method stub
		boolean deleteUserBasicResult = false;
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
			deleteUserBasicResult = mapper.deleteUserBasic(userseq);
		}catch(Exception e){
			e.printStackTrace();
			return deleteUserBasicResult;
		}
		return deleteUserBasicResult;
	}

	public User_BasicDTO userkeyCheck(User_BasicDTO codeCheck) {
		// TODO Auto-generated method stub
		User_BasicDTO userkeyCheckResult = new User_BasicDTO();
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
			userkeyCheckResult = mapper.userkeyCheck(codeCheck);
		}catch(Exception e){
			return userkeyCheckResult;
		}
		return userkeyCheckResult;
	}

	public boolean updateUserkey(int userseq) {
		// TODO Auto-generated method stub
		boolean updateUserkeyResult = false;
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
			updateUserkeyResult = mapper.updateUserkey(userseq);
		}catch(Exception e){
			return updateUserkeyResult;
		}
		return updateUserkeyResult;
	}

	public User_BasicDTO selectUserByMemberseq(int memberseq) {
		// TODO Auto-generated method stub
		User_BasicDTO user = new User_BasicDTO();
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
		user = mapper.selectUserByMemberseq(memberseq);
		}catch(Exception e){
			return user;
		}
		return user;
	}

	public HashMap<String, Object> chkUser(int userseq) {
		// TODO Auto-generated method stub
		HashMap<String, Object>result = new HashMap<String, Object>();
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
			result = mapper.chkUser(userseq);
		}catch(Exception e){
			return result;
		}
		return result;
	}

	public int selectAdminSeq() {
		// TODO Auto-generated method stub
		int result = 0;
		UserMapper mapper = session.getMapper(UserMapper.class);
		try{
			result = mapper.selectAdminSeq();
		}catch(Exception e){
			return result;
		}
		return result;

	}

}
