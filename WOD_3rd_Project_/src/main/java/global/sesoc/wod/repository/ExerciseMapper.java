package global.sesoc.wod.repository;

import java.util.ArrayList;

import global.sesoc.wod.dto.ExerciseDTO;
import global.sesoc.wod.dto.TrainingDTO;

public interface ExerciseMapper {

	boolean insertExercise(ExerciseDTO ex);

	ExerciseDTO selectExercise(ExerciseDTO ex);

	boolean insertTraining(TrainingDTO tr);

	ArrayList<TrainingDTO> selectTrainingList(ExerciseDTO exercise);

	ArrayList<ExerciseDTO> selectExerciseCheck(ExerciseDTO exercise);

	boolean updateExercise(ExerciseDTO ex);

	boolean deleteTraining(TrainingDTO trainingDTO);

	boolean deleteExercise(int exerciseseq);

	ArrayList<ExerciseDTO> selectExerciseList(int memberseq);

	ExerciseDTO selectOneExercise(ExerciseDTO ex);

	ExerciseDTO selectExerciseByMemberSeq(int memberseq);

	TrainingDTO selectTraining(ExerciseDTO ex);

}
