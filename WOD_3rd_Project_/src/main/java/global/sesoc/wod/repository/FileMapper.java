package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import global.sesoc.wod.dto.FileDTO;
import global.sesoc.wod.dto.TrainingDTO;

public interface FileMapper {

	public int insertApplyFile(FileDTO fvo);

	public boolean applyImgUpload(FileDTO image);

	public boolean applyResumeUpload(FileDTO resume);

	public FileDTO selectResume(int userseq);

	public boolean insertVideo(FileDTO file);

	public FileDTO selectVideoFile(int trainingseq);

	public boolean deleteVideo(TrainingDTO trainingDTO);

	public boolean insertMemberVideo(FileDTO file);

	public FileDTO selectMemberVideo(int membercommentseq);

	public boolean insertFiles(FileDTO file);

	public ArrayList<FileDTO> selectChannelFile(int channelseq);

	public boolean updateProfileFile(int userseq);

	public boolean insertFilesToChannel(FileDTO file);

	public FileDTO selectCurval();

	public boolean updaterepresentive(int lessonlistseq);

	public ArrayList<HashMap<String, String>> selectUserLessonList(int customerseq);

	public boolean deleteApplyFile(int userseq);



}
