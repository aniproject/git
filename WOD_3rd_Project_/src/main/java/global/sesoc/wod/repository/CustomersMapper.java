package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import global.sesoc.wod.dto.CustomersDTO;

public interface CustomersMapper {

	boolean insertCustomers(CustomersDTO customer);

	ArrayList<HashMap<String, String>> selectAllCustomer();

	HashMap<String, String> selectOneCustomers(int userseq);

	boolean deleteCustomers(int userseq);

	boolean updateCustomers(CustomersDTO customer); 
	
	CustomersDTO selectOneCustomer(int userseq);

	CustomersDTO selectCustomersByMemberSeq(int memberseq);

	CustomersDTO selectCustomersAll(int customerseq);
}
