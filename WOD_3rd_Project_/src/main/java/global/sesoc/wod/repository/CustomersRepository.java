package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.CustomersDTO;

@Repository
public class CustomersRepository {
	
	@Autowired
	SqlSession session;

	public boolean insertCustomers(CustomersDTO customer) {
		// TODO Auto-generated method stub
		boolean insertCustomersResult = false;
		CustomersMapper mapper = session.getMapper(CustomersMapper.class);
		try{
		insertCustomersResult = mapper.insertCustomers(customer);
		}catch(Exception e){
			return insertCustomersResult;
		}
		return insertCustomersResult;
	}

	public ArrayList<HashMap<String, String>> selectAllCustomer() {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String,String>>cList = new ArrayList<HashMap<String,String>>();
		CustomersMapper mapper = session.getMapper(CustomersMapper.class);
		try{
		cList = mapper.selectAllCustomer();
		}catch(Exception e){
			e.printStackTrace();
			return cList;
		}
		return cList;
	}

	public HashMap<String, String> selectOneCustomers(int userseq) {
		// TODO Auto-generated method stub
		HashMap<String,String> customer = new HashMap<String,String>();
		CustomersMapper mapper = session.getMapper(CustomersMapper.class);
		try{
		customer = mapper.selectOneCustomers(userseq);
		}catch(Exception e){
			e.printStackTrace();
			return customer;
		}
		return customer;
	}

	public boolean deleteCustomers(int userseq) {
		// TODO Auto-generated method stub
		boolean deleteCustomersResult = false;
		CustomersMapper mapper = session.getMapper(CustomersMapper.class);
		try{
		deleteCustomersResult = mapper.deleteCustomers(userseq);
		}catch(Exception e){
			return deleteCustomersResult;
		}
		return deleteCustomersResult;
	}
	

	public boolean updateCustomers(CustomersDTO customer) {
		// TODO Auto-generated method stub
		boolean updateCustomersResult = false;
		CustomersMapper mapper = session.getMapper(CustomersMapper.class);
		try{
		updateCustomersResult = mapper.updateCustomers(customer);
		}catch(Exception e){
			return updateCustomersResult;
		}
		return updateCustomersResult;
	}
	
	
	public CustomersDTO selectOneCustomer(int userseq) {
		// TODO Auto-generated method stub
		CustomersDTO result = new CustomersDTO();
		CustomersMapper mapper = session.getMapper(CustomersMapper.class);
		try{
			result = mapper.selectOneCustomer(userseq);
		}catch(Exception e){
			return result;
		}
		return result;
	}

	public CustomersDTO selectCustomersByMemberSeq(int memberseq) {
		// TODO Auto-generated method stub
		CustomersDTO result = new CustomersDTO();
		CustomersMapper mapper = session.getMapper(CustomersMapper.class);
		try{
			result = mapper.selectCustomersByMemberSeq(memberseq);
		}catch(Exception e){
			return result;
		}
		return result;
		
	}

	public CustomersDTO selectCustomersAll(int customerseq) {
		// TODO Auto-generated method stub
		CustomersDTO result = new CustomersDTO();
		CustomersMapper mapper = session.getMapper(CustomersMapper.class);
		try{
			result = mapper.selectCustomersAll(customerseq);
		}catch(Exception e){
			return result;
		}
		return result;
	}
		
}
