package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import global.sesoc.wod.dto.ApplyDTO;
import global.sesoc.wod.dto.CustomersDTO;
import global.sesoc.wod.dto.TrainerDTO;

public interface TrainerMapper {

	ArrayList<HashMap<String,String>> selectTrainer(String selectValue);

	TrainerDTO selectOneTrainer(int userseq);
	
	boolean insertTrainer(CustomersDTO customer);

	boolean insertApply(ApplyDTO apply);

	boolean deleteApply(int userseq);
	
	ArrayList<HashMap<String,String>> selectTrainerAndChannel(int trainerseq);
	
	int updateTrainerComment(TrainerDTO trainer);

	ArrayList<HashMap<String, String>> selectOneTrainerDetail(int userseq);

	ArrayList<HashMap<String, String>> selectTrainerMemberCount();

	TrainerDTO selectTrainerAll(int trainerseq);

	ArrayList<HashMap<String, String>> selectTrainerLessonCount();

	ArrayList<HashMap<String, String>> selectOneTrainerDetailNoLesson(int userseq);

	ArrayList<HashMap<String, String>> selectRankTrainer();

}
