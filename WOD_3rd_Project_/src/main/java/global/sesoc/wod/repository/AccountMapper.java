package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import global.sesoc.wod.dto.AccountDTO;
import global.sesoc.wod.dto.PaymentDTO;

public interface AccountMapper {
   public AccountDTO chkAccount(int userseq);
   
   public int insertAccount(int userseq);

   public int updateAccount(AccountDTO account);

   public AccountDTO chkTrainer(int trainerseq1);

public boolean insertPayment(PaymentDTO pay);

public ArrayList<PaymentDTO> selectPaymentList(PaymentDTO pay);

public ArrayList<PaymentDTO> selectAllPayment();

public int chkAdmin();

public ArrayList<HashMap<String, String>> selectTrainerPaymentList(int userseq);
}
