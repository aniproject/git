package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.LessonDTO;
import global.sesoc.wod.dto.SearchDTO;

@Repository
public class LessonRepository {

	@Autowired
	SqlSession session;
	
	public int insertTrainerNewLessonlist(LessonDTO lesson){
		int result = 0;
		LessonMapper mapper = session.getMapper(LessonMapper.class);
		try{
			result = mapper.insertTrainerNewLessonlist(lesson);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}
	
	public ArrayList<LessonDTO> selectOneTrainerLessonlist(LessonDTO lesson) {
		// TODO Auto-generated method stub
		ArrayList<LessonDTO> result = new ArrayList<LessonDTO>();
		LessonMapper mapper = session.getMapper(LessonMapper.class);
		try{
		result = mapper.selectOneTrainerLessonlist(lesson);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}
	
	
	
	public int deleteLessonlist(LessonDTO lesson) {
		// TODO Auto-generated method stub
		int result = 0;
		LessonMapper mapper = session.getMapper(LessonMapper.class);
		try{
			result = mapper.deleteLessonlist(lesson);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}
	
	
	public LessonDTO selectOneLesson(int lessonlistseq) {
		// TODO Auto-generated method stub
		LessonDTO result = new LessonDTO();
		LessonMapper mapper = session.getMapper(LessonMapper.class);
		try{
			result = mapper.selectOneLesson(lessonlistseq);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}

	public ArrayList<HashMap<String, String>> selectAllLessonList(int cSeq) {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String, String>>lList = new ArrayList<HashMap<String, String>>();
		
		LessonMapper mapper = session.getMapper(LessonMapper.class);
		try{
			lList = mapper.selectAllLessonList(cSeq);
		}catch(Exception e){
			e.printStackTrace();
			return lList;
		}
		return lList;
	
	
		
	}

	public ArrayList<HashMap<String, String>> selectSearchLessonList(SearchDTO search) {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String, String>>lList = new ArrayList<HashMap<String, String>>();
		LessonMapper mapper = session.getMapper(LessonMapper.class);
		try{
			lList = mapper.selectSearchLessonList(search);
		}catch(Exception e){
			e.printStackTrace();
			return lList;
		}
		return lList;
	}
	
	public ArrayList<LessonDTO> selectLessonlistByLessontype(LessonDTO lesson) {
	      // TODO Auto-generated method stub
	      ArrayList<LessonDTO> result = new ArrayList<LessonDTO>();
	      LessonMapper mapper = session.getMapper(LessonMapper.class);
	      try{
	      result = mapper.selectLessonlistByLessontype(lesson);
	      }catch(Exception e){
	         return result;
	      }
	      return result;
	   }
	   
	   
	   
	   public LessonDTO selectEqualLessonlist(LessonDTO lesson) {
	      // TODO Auto-generated method stub
	      LessonDTO result = new LessonDTO();
	      LessonMapper mapper = session.getMapper(LessonMapper.class);
	      try{
	         result = mapper.selectEqualLessonlist(lesson);
	      }catch(Exception e){
	         e.printStackTrace();
	         return result;
	      }
	      return result;
	   }
	   
	   
	   
	   public int checkDeleteLessonlist() {
	      // TODO Auto-generated method stub
	      int result = 0;
	      LessonMapper mapper = session.getMapper(LessonMapper.class);
	      try{
	         result = mapper.checkDeleteLessonlist();
	      }catch(Exception e){
	         e.printStackTrace();
	         return result;
	      }
	      return result;
	   }
	   
	   
	   
	   public ArrayList<LessonDTO> selectDeleteMemberseq() {
	      // TODO Auto-generated method stub
	      ArrayList<LessonDTO> result = new ArrayList<LessonDTO>();
	      LessonMapper mapper = session.getMapper(LessonMapper.class);
	      try{
	         result = mapper.selectDeleteMemberseq();
	      }catch(Exception e){
	         e.printStackTrace();
	         return result;
	      }
	      return result;
	   }

	public ArrayList<LessonDTO> selectLessonList(int userseq) {
		// TODO Auto-generated method stub
		ArrayList<LessonDTO>lList = new ArrayList<LessonDTO>();
		 LessonMapper mapper = session.getMapper(LessonMapper.class);
	     try{
	    	 lList = mapper.selectLessonList(userseq);
	     }catch(Exception e){
         e.printStackTrace();
         return lList;
      }
	     return lList;
	}

	public ArrayList<HashMap<String, String>>  selectRecommend() {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String, String>> lList = new ArrayList<HashMap<String, String>>();
				 LessonMapper mapper = session.getMapper(LessonMapper.class);
			     try{
			    	 lList = mapper.selectRecommend();
			     }catch(Exception e){
		         e.printStackTrace();
		         return lList;
		      }
			     return lList;
	}
	
	
	
	
}
