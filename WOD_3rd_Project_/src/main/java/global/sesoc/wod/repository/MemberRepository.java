package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.MemberCommentDTO;
import global.sesoc.wod.dto.MemberDTO;

@Repository
public class MemberRepository {

	@Autowired
	SqlSession session;
	
	
	public ArrayList<HashMap<String, String>> selectTrainingCustomers(int trainerseq) {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String,String>>cList = new ArrayList<HashMap<String,String>>();
		MemberMapper mapper = session.getMapper(MemberMapper.class);
		try{
		cList = mapper.selectTrainingCustomers(trainerseq);
		}catch(Exception e){
			e.printStackTrace();
			return cList;
		}
		return cList;
	}
	
	
	public int insertMember(MemberDTO member) {
		// TODO Auto-generated method stub
		int result = 0;
		MemberMapper mapper = session.getMapper(MemberMapper.class);
		try{
		result = mapper.insertMember(member);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}
	
	
	public int selectLastMemberseq() {
		// TODO Auto-generated method stub
		int result = 0;
		MemberMapper mapper = session.getMapper(MemberMapper.class);
		try{
		result = mapper.selectLastMemberseq();
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}


	public boolean insertMemberComment(MemberCommentDTO mComment) {
		// TODO Auto-generated method stub
		boolean insertMemberComment=false;
		
		MemberMapper mapper = session.getMapper(MemberMapper.class);
		try{
		insertMemberComment = mapper.insertMemberComment(mComment);
		}catch(Exception e){
			e.printStackTrace();
			return insertMemberComment;
		}
		return insertMemberComment;
	}


	public MemberCommentDTO selectMemberComment(MemberCommentDTO m) {
		// TODO Auto-generated method stub
		MemberCommentDTO result = new MemberCommentDTO();
		
		MemberMapper mapper = session.getMapper(MemberMapper.class);
		try{
			result = mapper.selectMemberComment(m);
		}catch(Exception e){
			return result;
		}
		return result;
		
		
	}


	public ArrayList<MemberCommentDTO> selectMemberCommentList(MemberCommentDTO mc) {
		// TODO Auto-generated method stub
		ArrayList<MemberCommentDTO>mcList = new ArrayList<MemberCommentDTO>();
		MemberMapper mapper = session.getMapper(MemberMapper.class);
		try{
			mcList = mapper.selectMemberCommentList(mc);
		}catch(Exception e){
			e.printStackTrace();
			return mcList;
		}
		return mcList;
		
		
	}


	public MemberCommentDTO selectOneMemberComment(int membercommnetseq) {
		// TODO Auto-generated method stub
		MemberCommentDTO result = new MemberCommentDTO();
		
		MemberMapper mapper = session.getMapper(MemberMapper.class);
		try{
			result = mapper.selectOneMemberComment(membercommnetseq);
		}catch(Exception e){
			return result;
		}
		return result;
		
	}


	public int deleteMember(int memberseq) {
	      // TODO Auto-generated method stub
	      int result = 0;
	      MemberMapper mapper = session.getMapper(MemberMapper.class);
	      try{
	      result = mapper.deleteMember(memberseq);
	      }catch(Exception e){
	         e.printStackTrace();
	         return result;
	      }
	      return result;
	   }


	public ArrayList<MemberDTO> selectMember(MemberDTO member) {
	      // TODO Auto-generated method stub
	      ArrayList<MemberDTO> result = new ArrayList<MemberDTO>();
	      MemberMapper mapper = session.getMapper(MemberMapper.class);
	      try{
	      result = mapper.selectMember(member);
	      }catch(Exception e){
	         e.printStackTrace();
	         return result;
	      }
	      return result;
	   }

	
	public int selectCountMember(int trainerseq) {
	      // TODO Auto-generated method stub
	      int result = 0;
	      MemberMapper mapper = session.getMapper(MemberMapper.class);
	      try{
	      result = mapper.selectCountMember(trainerseq);
	      }catch(Exception e){
	         e.printStackTrace();
	         return result;
	      }
	      return result;
	   }

	
}
