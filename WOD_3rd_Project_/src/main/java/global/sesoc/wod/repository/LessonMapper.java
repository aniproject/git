package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import global.sesoc.wod.dto.LessonDTO;
import global.sesoc.wod.dto.SearchDTO;

public interface LessonMapper {

	//트레이너가 자신의 강의를 새롭게 추가  - 최문석
	public int insertTrainerNewLessonlist(LessonDTO lesson);
	
	ArrayList<LessonDTO> selectOneTrainerLessonlist(LessonDTO lesson);
	
	public int deleteLessonlist(LessonDTO lesson);
	
	LessonDTO selectOneLesson(int lessonlistseq);

	public ArrayList<HashMap<String, String>> selectAllLessonList(int userseq);
	
	public ArrayList<HashMap<String, String>> selectSearchLessonList(SearchDTO search);
	
	 int checkDeleteLessonlist();
	   
	 ArrayList<LessonDTO> selectLessonlistByLessontype(LessonDTO lesson);
	   
	 LessonDTO selectEqualLessonlist(LessonDTO lesson);
	  
	 ArrayList<LessonDTO> selectDeleteMemberseq();
   
	public ArrayList<LessonDTO> selectLessonList(int userseq);

	public ArrayList<HashMap<String, String>> selectRecommend();

	



	
}
