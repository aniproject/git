package global.sesoc.wod.repository;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.ExerciseDTO;
import global.sesoc.wod.dto.TrainingDTO;

@Repository
public class ExerciseRepository {
	
	@Autowired
	SqlSession session;

	public boolean insertExercise(ExerciseDTO ex) {
		// TODO Auto-generated method stub
		boolean insertResult = false;
		ExerciseMapper mapper = session.getMapper(ExerciseMapper.class);
		try{
		insertResult = mapper.insertExercise(ex);
		}catch(Exception e){
			return insertResult;
		}
		return insertResult;
	}

	public ExerciseDTO selectExercise(ExerciseDTO ex) {
		// TODO Auto-generated method stub
		ExerciseDTO exercise = new ExerciseDTO();
		
		ExerciseMapper mapper = session.getMapper(ExerciseMapper.class);
		try{
		exercise = mapper.selectExercise(ex);
		}catch(Exception e){
			e.printStackTrace();
			return exercise;
		}
		return exercise;
	}

	public boolean insertTraining(TrainingDTO tr) {
		// TODO Auto-generated method stub
		boolean insertResult = false;
		ExerciseMapper mapper = session.getMapper(ExerciseMapper.class);
		try{
		insertResult = mapper.insertTraining(tr);
		}catch(Exception e){
			e.printStackTrace();
			return insertResult;
		}
		return insertResult;
		
	}

	public ArrayList<TrainingDTO> selectTrainingList(ExerciseDTO exercise) {
		// TODO Auto-generated method stub
		ArrayList<TrainingDTO>tList = new ArrayList<TrainingDTO>();
		
		ExerciseMapper mapper = session.getMapper(ExerciseMapper.class);
		try{
		tList = mapper.selectTrainingList(exercise);
		}catch(Exception e){
			e.printStackTrace();
			return tList;
		}
		return tList;
	}

	public ArrayList<ExerciseDTO> selectExerciseCheck(ExerciseDTO exercise) {
		// TODO Auto-generated method stub
		ArrayList<ExerciseDTO>eList = new ArrayList<ExerciseDTO>();
		ExerciseMapper mapper = session.getMapper(ExerciseMapper.class);
		try{
			eList = mapper.selectExerciseCheck(exercise);
		}catch(Exception e){
			e.printStackTrace();
			return eList;
		}
		return eList;
	}

	public boolean updateExercise(ExerciseDTO ex) {
		// TODO Auto-generated method stub
		boolean updateResult = false;
		ExerciseMapper mapper = session.getMapper(ExerciseMapper.class);
		try{
			updateResult = mapper.updateExercise(ex);
		}catch(Exception e){
			e.printStackTrace();
			return updateResult;
		}
		return updateResult;
		
	}

	public boolean deleteTraining(TrainingDTO trainingDTO) {
		// TODO Auto-generated method stub
		boolean deleteResult = false;
		ExerciseMapper mapper = session.getMapper(ExerciseMapper.class);
		try{
			deleteResult = mapper.deleteTraining(trainingDTO);
		}catch(Exception e){
			e.printStackTrace();
			return deleteResult;
		}
		return deleteResult;
		
	
	}

	public boolean deleteExercise(int exerciseseq) {
		// TODO Auto-generated method stub
		boolean deleteResult = false;
		ExerciseMapper mapper = session.getMapper(ExerciseMapper.class);
		try{
			deleteResult = mapper.deleteExercise(exerciseseq);
		}catch(Exception e){
			e.printStackTrace();
			return deleteResult;
		}
		return deleteResult;
	
	}

	public ArrayList<ExerciseDTO> selectExerciseList(int memberseq) {
		// TODO Auto-generated method stub
		ArrayList<ExerciseDTO>eList = new ArrayList<ExerciseDTO>();
		ExerciseMapper mapper = session.getMapper(ExerciseMapper.class);
		try{
			eList = mapper.selectExerciseList(memberseq);
		}catch(Exception e){
			e.printStackTrace();
			return eList;
		}
		return eList;
	}

	public ExerciseDTO selectOneExercise(ExerciseDTO ex) {
		// TODO Auto-generated method stub
		ExerciseDTO exercise = new ExerciseDTO();
		
		ExerciseMapper mapper = session.getMapper(ExerciseMapper.class);
		try{
		exercise = mapper.selectOneExercise(ex);
		}catch(Exception e){
			e.printStackTrace();
			return exercise;
		}
		return exercise;
	}

	public ExerciseDTO selectExerciseByMemberSeq(int memberseq) {
		// TODO Auto-generated method stub
		ExerciseDTO exercise = new ExerciseDTO();
		
		ExerciseMapper mapper = session.getMapper(ExerciseMapper.class);
		try{
		exercise = mapper.selectExerciseByMemberSeq(memberseq);
		}catch(Exception e){
			return exercise;
		}
		return exercise;
	}

	public TrainingDTO selectTraining(ExerciseDTO ex) {
		// TODO Auto-generated method stub
		TrainingDTO tr = new TrainingDTO();
		
		ExerciseMapper mapper = session.getMapper(ExerciseMapper.class);
		try{
		tr = mapper.selectTraining(ex);
		}catch(Exception e){
			e.printStackTrace();
			return tr;
		}
		return tr;
	}


}
