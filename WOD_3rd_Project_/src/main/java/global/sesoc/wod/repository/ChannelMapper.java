package global.sesoc.wod.repository;

import global.sesoc.wod.dto.ChannelDTO;
import global.sesoc.wod.dto.TrainerDTO;

public interface ChannelMapper {

	public ChannelDTO selectChannel(int trainerseq);

	public boolean insertChannel(TrainerDTO trainer);

	public int minusLikeAndRecommend(int channelseq);
	
	public int plusLikeAndRecommend(int channelseq);
	
}
