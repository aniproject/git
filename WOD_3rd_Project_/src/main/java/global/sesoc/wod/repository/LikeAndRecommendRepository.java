package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.LikeAndRecommendDTO;

@Repository
public class LikeAndRecommendRepository {

	@Autowired
	SqlSession session;
	
	public int insertLike(LikeAndRecommendDTO lar){
		int result = 0;
		
		LikeAndRecommendMapper mapper = session.getMapper(LikeAndRecommendMapper.class);
		try{
			result = mapper.insertLike(lar);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		
		return result;
	}
	
	public int deleteLike(LikeAndRecommendDTO lar){
		int result = 0;
		
		LikeAndRecommendMapper mapper = session.getMapper(LikeAndRecommendMapper.class);
		try{
			result = mapper.deleteLike(lar);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		
		return result;
	}
	
	
	public LikeAndRecommendDTO selectLike(LikeAndRecommendDTO lar){
		LikeAndRecommendDTO result = new LikeAndRecommendDTO();
		LikeAndRecommendMapper mapper = session.getMapper(LikeAndRecommendMapper.class);
		try{
			result = mapper.selectLike(lar);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}
	
	public LikeAndRecommendDTO selectLikeAndRecommendCount(int channelseq){
	      LikeAndRecommendDTO result = new LikeAndRecommendDTO();
	      LikeAndRecommendMapper mapper = session.getMapper(LikeAndRecommendMapper.class);
	      try{
	         result = mapper.selectLikeAndRecommendCount(channelseq);
	      }catch(Exception e){
	         e.printStackTrace();
	         return result;
	      }
	      return result;
	   }

	public ArrayList<HashMap<String, String>> selectLikeList(int userseq) {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String,String>>lrList = new ArrayList<HashMap<String,String>>();
		LikeAndRecommendMapper mapper = session.getMapper(LikeAndRecommendMapper.class);
		try{
		lrList = mapper.selectLikeList(userseq);
		}catch(Exception e){
			return lrList;
		}
		return lrList;
	}
	
	
}
