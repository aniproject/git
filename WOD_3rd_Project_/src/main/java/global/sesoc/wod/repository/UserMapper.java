package global.sesoc.wod.repository;

import java.util.HashMap;

import global.sesoc.wod.dto.User_BasicDTO;
import global.sesoc.wod.dto.User_RoleDTO;

public interface UserMapper {

	User_BasicDTO selectOneUser(User_BasicDTO user);

	boolean insertUser(User_BasicDTO user);

	boolean insertUserRole(User_BasicDTO u);

	User_RoleDTO selectRoleseq(int userseq);

	boolean updateUserRole(User_RoleDTO uRole);

	boolean rollbackUserRole(User_RoleDTO uRole);

	User_BasicDTO passwordCheck(User_BasicDTO user);

	boolean updateUserBasic(User_BasicDTO user);

	boolean deleteUserRole(int userseq);

	boolean deleteUserBasic(int userseq);

	User_BasicDTO userkeyCheck(User_BasicDTO codeCheck);

	boolean updateUserkey(int userseq);

	User_BasicDTO selectUserByMemberseq(int memberseq);

	HashMap<String, Object> chkUser(int userseq);

	int selectAdminSeq();

	 
}
