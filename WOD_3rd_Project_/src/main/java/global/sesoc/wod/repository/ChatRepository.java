package global.sesoc.wod.repository;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.ChatDTO;
import global.sesoc.wod.dto.ChatRoomDTO;

@Repository
public class ChatRepository {
	
	@Autowired
	SqlSession session;

	public boolean insertChat(ChatDTO chat) {
		// TODO Auto-generated method stub
		boolean result=false;
		
		ChatMapper mapper = session.getMapper(ChatMapper.class);
		try{
		result = mapper.insertChat(chat);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}

	public ArrayList<ChatDTO> selectChatList(ChatDTO chat) {
		// TODO Auto-generated method stub
		ArrayList<ChatDTO>cList = new ArrayList<ChatDTO>();
		ChatMapper mapper = session.getMapper(ChatMapper.class);
		try{
		cList = mapper.selectChatList(chat);
		}catch(Exception e){
			e.printStackTrace();
			return cList;
		}
		
		return cList;
	}

	public ChatRoomDTO selectChatRoom(ChatDTO chat) {
		// TODO Auto-generated method stub
		ChatRoomDTO result = new ChatRoomDTO();
		ChatMapper mapper = session.getMapper(ChatMapper.class);
		try{
		result = mapper.selectChatRoom(chat);
		}catch(Exception e){
			return result;
		}
		return result;
	}

	public boolean insertChatRoom(ChatDTO chat) {
		// TODO Auto-generated method stub
		boolean result = false;
		
		ChatMapper mapper = session.getMapper(ChatMapper.class);
		try{
		result = mapper.insertChatRoom(chat);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;

	}

	public ArrayList<ChatRoomDTO> selectChatRoomList(ChatRoomDTO cRoom) {
		// TODO Auto-generated method stub
		ArrayList<ChatRoomDTO>cList = new ArrayList<ChatRoomDTO>();
		ChatMapper mapper = session.getMapper(ChatMapper.class);
		try{
		cList = mapper.selectChatRoomList(cRoom);
		}catch(Exception e){
			e.printStackTrace();
			return cList;
		}
		return cList;
	}

	public boolean updateRecentDate(int chatroomseq) {
		// TODO Auto-generated method stub
		boolean result = false;
		
		ChatMapper mapper = session.getMapper(ChatMapper.class);
		try{
		result = mapper.updateRecentDate(chatroomseq);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
		
	}

}
