package global.sesoc.wod.repository;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.ChannelDTO;
import global.sesoc.wod.dto.TrainerDTO;

@Repository
public class ChannelRepository {

	@Autowired
	SqlSession session;
	
	
	public ChannelDTO selectChannel(int trainerseq) {
		ChannelDTO result = new ChannelDTO();
		ChannelMapper mapper = session.getMapper(ChannelMapper.class);
		try{
		result = mapper.selectChannel(trainerseq);
		}catch(Exception e){
			return result;
		}
		return result;
	}


	public boolean insertChannel(TrainerDTO trainer) {
		// TODO Auto-generated method stub
		boolean insertChannelResult = false;
		ChannelMapper mapper = session.getMapper(ChannelMapper.class);
		try{
		insertChannelResult = mapper.insertChannel(trainer);
		}catch(Exception e){
			return insertChannelResult;
		}
		return insertChannelResult;
	}
	
	
	
	public int minusLikeAndRecommend(int channelseq) {
		int result = 0;
		ChannelMapper mapper = session.getMapper(ChannelMapper.class);
		try{
		result = mapper.minusLikeAndRecommend(channelseq);
		}catch(Exception e){
			return result;
		}
		return result;
	}
	
	
	public int plusLikeAndRecommend(int channelseq) {
		int result = 0;
		ChannelMapper mapper = session.getMapper(ChannelMapper.class);
		try{
		result = mapper.plusLikeAndRecommend(channelseq);
		}catch(Exception e){
			return result;
		}
		return result;
	}
	
}
