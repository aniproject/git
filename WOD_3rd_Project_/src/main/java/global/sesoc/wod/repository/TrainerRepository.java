package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.ApplyDTO;
import global.sesoc.wod.dto.CustomersDTO;
import global.sesoc.wod.dto.TrainerDTO;

@Repository
public class TrainerRepository {
	
	@Autowired
	SqlSession session;

	public ArrayList<HashMap<String,String>> selectTrainer(String selectValue) {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String,String>> result = null;
		TrainerMapper mapper = session.getMapper(TrainerMapper.class);
		try{
			result = mapper.selectTrainer(selectValue);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}

	public TrainerDTO selectOneTrainer(int userseq) {
		// TODO Auto-generated method stub
		TrainerDTO trainer = new TrainerDTO();
		TrainerMapper mapper = session.getMapper(TrainerMapper.class);
		try{
		trainer = mapper.selectOneTrainer(userseq);
		}catch(Exception e){
			return trainer;
		}
		return trainer;
	}


	public boolean insertTrainer(CustomersDTO customer) {
		// TODO Auto-generated method stub
		System.out.println("111"+customer);
		boolean insertTrainerResult = false;
		TrainerMapper mapper = session.getMapper(TrainerMapper.class);
		try{
		insertTrainerResult = mapper.insertTrainer(customer);
		}catch(Exception e){
			e.printStackTrace();
			return insertTrainerResult;
		}
		return insertTrainerResult;
	}

	public boolean insertApply(ApplyDTO apply) {
		// TODO Auto-generated method stub
		boolean insertApplyResult = false;
		
		TrainerMapper mapper = session.getMapper(TrainerMapper.class);
		try{
		insertApplyResult = mapper.insertApply(apply);
		}catch(Exception e){
			return insertApplyResult;
		}
		return insertApplyResult;
	}

	public boolean deleteApply(int userseq) {
		// TODO Auto-generated method stub
		boolean deleteApplyResult = false;
		TrainerMapper mapper = session.getMapper(TrainerMapper.class);
		try{
		deleteApplyResult = mapper.deleteApply(userseq);
		}catch(Exception e){
			return deleteApplyResult;
		}
		return deleteApplyResult;
	}
	
	
	
	public ArrayList<HashMap<String,String>> selectTrainerAndChannel(int trainerseq) {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String,String>> result = null;
		TrainerMapper mapper = session.getMapper(TrainerMapper.class);
		try{
			result = mapper.selectTrainerAndChannel(trainerseq);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}
	
	
	public int updateTrainerComment(TrainerDTO trainer) {
		// TODO Auto-generated method stub
		int result = 0;
		TrainerMapper mapper = session.getMapper(TrainerMapper.class);
		try{
			result = mapper.updateTrainerComment(trainer);
		}catch(Exception e){
			System.out.println("111111111111111");
			e.printStackTrace();
			return result;
		}
		System.out.println("2222222222222");
		return result;
	}

	public ArrayList<HashMap<String, String>> selectOneTrainerDetail(int userseq) {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String, String>>result = new ArrayList<HashMap<String, String>>();
		TrainerMapper mapper = session.getMapper(TrainerMapper.class);
		try{
		result = mapper.selectOneTrainerDetail(userseq);
		}catch(Exception e){
			return result;
		}
		return result;
	}

	public ArrayList<HashMap<String, String>> selectTrainerMemberCount() {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String, String>>result = new ArrayList<HashMap<String, String>>();
		TrainerMapper mapper = session.getMapper(TrainerMapper.class);
		try{
		result = mapper.selectTrainerMemberCount();
		}catch(Exception e){
			return result;
		}
		return result;
	}

	public TrainerDTO selectTrainerAll(int trainerseq) {
		// TODO Auto-generated method stub
		TrainerDTO result = new TrainerDTO();
		TrainerMapper mapper = session.getMapper(TrainerMapper.class);
		try{
		result = mapper.selectTrainerAll(trainerseq);
		}catch(Exception e){
			return result;
		}
		return result;
	}

	public ArrayList<HashMap<String, String>> selectTrainerLessonCount() {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String, String>>result = new ArrayList<HashMap<String, String>>();
		TrainerMapper mapper = session.getMapper(TrainerMapper.class);
		try{
		result = mapper.selectTrainerLessonCount();
		}catch(Exception e){
			return result;
		}
		return result;
	}

	public ArrayList<HashMap<String, String>> selectOneTrainerDetailNoLesson(int userseq) {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String, String>>result = new ArrayList<HashMap<String, String>>();
		TrainerMapper mapper = session.getMapper(TrainerMapper.class);
		try{
		result = mapper.selectOneTrainerDetailNoLesson(userseq);
		}catch(Exception e){
			return result;
		}
		return result;
	}

	public ArrayList<HashMap<String, String>> selectRankTrainer() {
		ArrayList<HashMap<String, String>>lList = new ArrayList<HashMap<String, String>>();
		
		TrainerMapper mapper = session.getMapper(TrainerMapper.class);
		try{
			lList = mapper.selectRankTrainer();
		}catch(Exception e){
			e.printStackTrace();
			return lList;
		}
		return lList;
	}

	
}
