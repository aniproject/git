package global.sesoc.wod.repository;


import java.util.ArrayList;

import global.sesoc.wod.dto.FoodDTO;
import global.sesoc.wod.dto.MealDTO;
import global.sesoc.wod.dto.MealMenuDTO;

public interface FoodMapper {

	boolean insertFoodDB(FoodDTO foodDTO);

	boolean insertMealMenu(MealMenuDTO mealmenu);

	ArrayList<FoodDTO> selectFoodList(String keyWord);

	boolean insertMeal(MealDTO meal);

	MealMenuDTO selectMealMenu(MealMenuDTO m);

	ArrayList<MealDTO> selectMeal(MealMenuDTO mealmenu);

	FoodDTO selectOneFood(int foodseq);

	boolean updateMealMenu(MealMenuDTO mealmenu);

	ArrayList<MealMenuDTO> selectMealMenuList(int memberseq);

	MealMenuDTO selectOneMealMenu(MealMenuDTO mealmenu2);

	boolean updateMeal(int mealmenuseq);

	boolean deleteMealMenu(int mealmenuseq);

	ArrayList<MealMenuDTO> selectOneMealMenuCheck(MealMenuDTO mealmenu);

	boolean deleteMeal(MealDTO mealDTO);

	int selectFoodDBCount();

}
