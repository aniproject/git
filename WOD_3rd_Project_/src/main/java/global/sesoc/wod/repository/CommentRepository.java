package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.CommentDTO;

@Repository
public class CommentRepository {
   
   @Autowired
   SqlSession session;
   
   public int insertDetailLessonComment(CommentDTO comment) {
      // TODO Auto-generated method stub
      int result=0;
      CommentMapper mapper = session.getMapper(CommentMapper.class);
      try{
         result = mapper.insertDetailLessonComment(comment);
      }catch(Exception e){
         e.printStackTrace();
         return result;
      }
      return result;
   }
   
   
   public ArrayList<HashMap<String,Object>> selectCommentList(CommentDTO comment) {
      // TODO Auto-generated method stub
      ArrayList<HashMap<String,Object>> result= new ArrayList<HashMap<String,Object>>();
      CommentMapper mapper = session.getMapper(CommentMapper.class);
      try{
         result = mapper.selectCommentList(comment);
      }catch(Exception e){
         e.printStackTrace();
         return result;
      }
      return result;
   }
   
   
   public int deleteComment(CommentDTO comment) {
      // TODO Auto-generated method stub
      int result=0;
      CommentMapper mapper = session.getMapper(CommentMapper.class);
      try{
         result = mapper.deleteComment(comment);
      }catch(Exception e){
         e.printStackTrace();
         return result;
      }
      return result;
   }
   
   
   public int deletelessonComment(int lessonlistseq) {
      // TODO Auto-generated method stub
      int result=0;
      CommentMapper mapper = session.getMapper(CommentMapper.class);
      try{
         result = mapper.deletelessonComment(lessonlistseq);
      }catch(Exception e){
         e.printStackTrace();
         return result;
      }
      return result;
   }
   
   
}