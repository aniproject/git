package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import global.sesoc.wod.dto.CommentDTO;

public interface CommentMapper {

   int insertDetailLessonComment(CommentDTO comment);
   
   ArrayList<HashMap<String,Object>> selectCommentList(CommentDTO comment);
   
   int deleteComment(CommentDTO comment);
   
   int deletelessonComment(int lessonlistseq);
   
}