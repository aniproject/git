package global.sesoc.wod.repository;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import global.sesoc.wod.dto.QnADTO;
import global.sesoc.wod.dto.QreplyDTO;
import global.sesoc.wod.dto.SearchDTO;

public interface QnAMapper {
	public int qnaRegister(QnADTO qna);
	public ArrayList<HashMap<String, Object>> viewQna(String searchWord, RowBounds rb);//게시물 정보 가져오기
	public int updateQna(QnADTO qna);
	public int deleteQna(int qnaseq);
	public List<QnADTO> search(QnADTO qna);
	public int qreplyRegist(QreplyDTO qre);
	public ArrayList<HashMap<String, String>> viewQreply(QnADTO qna);
	public int updateQreply(QreplyDTO qre);
	public int deleteQreply(QreplyDTO qre);
	public List<QnADTO> selectContent(QnADTO qna);
	public int selectContentCnt(Map<String, Object> paramMap);
	public List<QreplyDTO> selectBoardReplyList(QnADTO qna);
	public HashMap<String, Object> selectContentView(QnADTO qna);
	public ArrayList<HashMap<String, Object>> qqList(int qnaseq);
	public ArrayList<HashMap<String, Object>> getAdminBoard(QnADTO qna);
	public int selectCount(String searchWord);//전체 레코드 수
	public int deleteQnAReply(int qnaseq);
	public String chkCusName(int userSeq);
	public HashMap<String, Object> adminView(int qnaseq);
	public int updateAdminQna(QnADTO qna);
	public ArrayList<QnADTO> selectAllQnAList(int userseq);
	public ArrayList<QnADTO> selectSearchQNAList(SearchDTO search);
	
}
