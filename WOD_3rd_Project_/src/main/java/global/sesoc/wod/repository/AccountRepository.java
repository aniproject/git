package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.AccountDTO;
import global.sesoc.wod.dto.PaymentDTO;

@Repository
public class AccountRepository {

   @Autowired
   SqlSession session;

   public AccountDTO chkAccount(int userseq) {
      AccountMapper mapper = session.getMapper(AccountMapper.class);
      AccountDTO result = new AccountDTO();
      try {
         result = mapper.chkAccount(userseq);
      } catch (Exception e) {
         e.printStackTrace();
         return result;
      }
         return result;
      
   }

   public int insertAccount(int userseq) {
      AccountMapper mapper = session.getMapper(AccountMapper.class);
      int result = 0;
      try {
         result = mapper.insertAccount(userseq);
      } catch (Exception e) {
         e.printStackTrace();
         return result;
      }
         return result;
   }

   public int updateAccount(AccountDTO account) {
      AccountMapper mapper = session.getMapper(AccountMapper.class);
      int result = 0;
      try {
         result = mapper.updateAccount(account);
      } catch (Exception e) {
         e.printStackTrace();
         return result;
      }
         return result;
   }

   public AccountDTO chkTrainer(int trainerseq1) {
      AccountMapper mapper = session.getMapper(AccountMapper.class);
      AccountDTO result = new AccountDTO();
      try {
         result = mapper.chkTrainer(trainerseq1);
      } catch (Exception e) {
         e.printStackTrace();
         return result;
      }
         return result;
   }

public boolean insertPayment(PaymentDTO pay) {
	// TODO Auto-generated method stub
	boolean insertPayment =false;
	AccountMapper mapper = session.getMapper(AccountMapper.class);
	try{
	insertPayment = mapper.insertPayment(pay);
	}catch(Exception e){
		return insertPayment;
	}
	return insertPayment;
}

public ArrayList<PaymentDTO> selectPaymentList(PaymentDTO pay) {
	// TODO Auto-generated method stub
	ArrayList<PaymentDTO>pList = new ArrayList<PaymentDTO>();
	AccountMapper mapper = session.getMapper(AccountMapper.class);
	try{
		pList = mapper.selectPaymentList(pay);
	}catch(Exception e){
		return pList;
	}
	return pList;
	

}

public ArrayList<PaymentDTO> selectAllPayment() {
	// TODO Auto-generated method stub
	ArrayList<PaymentDTO>pList = new ArrayList<PaymentDTO>();
	AccountMapper mapper = session.getMapper(AccountMapper.class);
	try{
		pList = mapper.selectAllPayment();
	}catch(Exception e){
		return pList;
	}
	return pList;
	
	
}

public int chkAdmin() {
	// TODO Auto-generated method stub
	AccountMapper mapper = session.getMapper(AccountMapper.class);
	int result =0;
	try{
		result = mapper.chkAdmin();
	}catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		return result;
	}
	return result;
}

public ArrayList<HashMap<String, String>> selectTrainerPaymentList(int userseq) {
	// TODO Auto-generated method stub
	ArrayList<HashMap<String, String>>pList = new ArrayList<HashMap<String, String>>();
	AccountMapper mapper = session.getMapper(AccountMapper.class);
	try{
		pList = mapper.selectTrainerPaymentList(userseq);
	}catch(Exception e){
		return pList;
	}
	return pList;
	
}

}