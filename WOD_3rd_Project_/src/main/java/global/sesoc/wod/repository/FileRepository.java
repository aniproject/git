package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.FileDTO;
import global.sesoc.wod.dto.TrainingDTO;

@Repository
public class FileRepository {
	@Autowired
	SqlSession session;
	
	public int insertApplyFile(FileDTO fvo) {
		// TODO Auto-generated method stub
		int result=0;
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
		mapper.insertApplyFile(fvo);
		}catch(Exception e){
			return result;
		}
		return result;
	}
	
	public boolean applyImgUpload(FileDTO image) {
		// TODO Auto-generated method stub
		boolean applyImgUploadResult = false;
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
		applyImgUploadResult = mapper.applyImgUpload(image);
		}catch(Exception e){
			e.printStackTrace();
			return applyImgUploadResult;
		}
		return applyImgUploadResult;
	}
	public boolean applyResumeUpload(FileDTO resume) {
		// TODO Auto-generated method stub
		boolean applyResumeUploadResult = false;
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
			applyResumeUploadResult = mapper.applyResumeUpload(resume);
		}catch(Exception e){
			e.printStackTrace();
			return applyResumeUploadResult;
		}
		return applyResumeUploadResult;
		
		
	}
	public FileDTO selectResume(int userseq) {
		// TODO Auto-generated method stub
		FileDTO resume = new FileDTO();
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
		resume = mapper.selectResume(userseq);
		}catch(Exception e){
			return resume;
		}
		return resume;
	}

	public boolean insertVideo(FileDTO file) {
		// TODO Auto-generated method stub
		boolean insertResult = false;
		
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
		insertResult = mapper.insertVideo(file);
		}catch(Exception e){
			e.printStackTrace();
			return insertResult;
		}
		return insertResult;
	}

	public FileDTO selectVideoFile(int trainingseq) {
		// TODO Auto-generated method stub
		FileDTO result = new FileDTO();
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
		result = mapper.selectVideoFile(trainingseq);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}

	public boolean deleteVideo(TrainingDTO trainingDTO) {
		// TODO Auto-generated method stub
		boolean result =false;
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
			result = mapper.deleteVideo(trainingDTO);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
		
	}

	public boolean insertMemberVideo(FileDTO file) {
		// TODO Auto-generated method stub
		boolean insertResult = false;
		
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
		insertResult = mapper.insertMemberVideo(file);
		}catch(Exception e){
			e.printStackTrace();
			return insertResult;
		}
		return insertResult;
		
	}
	
	public FileDTO selectMemberVideo(int membercommentseq) {
		// TODO Auto-generated method stub
		FileDTO result = new FileDTO();
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
		result = mapper.selectMemberVideo(membercommentseq);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
		
	}
	public boolean insertFiles(FileDTO file) {
		// TODO Auto-generated method stub
		boolean insertResult = false;
		
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
		insertResult = mapper.insertFiles(file);
		}catch(Exception e){
			e.printStackTrace();
			return insertResult;
		}
		return insertResult;
	}

	public boolean updateProfileFile(int userseq) {
		// TODO Auto-generated method stub
		boolean result=false;
		FileMapper mapper =session.getMapper(FileMapper.class);
		try{
			result= mapper.updateProfileFile(userseq);
		}catch(Exception e)
		{
			return result;
		}
		return result;
	}

	public ArrayList<FileDTO> selectChannelFile(int channelseq) {
		// TODO Auto-generated method stub
		ArrayList<FileDTO> result = new ArrayList<FileDTO>();
				FileMapper mapper = session.getMapper(FileMapper.class);
				try{
				result = mapper.selectChannelFile(channelseq);
				}catch(Exception e){
					e.printStackTrace();
					return result;
				}
				return result;
	}

	public boolean insertFilesToChannel(FileDTO file) {
		boolean insertResult = false;
		
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
		insertResult = mapper.insertFilesToChannel(file);
		}catch(Exception e){
			e.printStackTrace();
			return insertResult;
		}
		return insertResult;
	}

	public FileDTO selectCurval() {
		FileDTO result= new FileDTO();
		
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
			result = mapper.selectCurval();
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}

	public boolean updaterepresentive(int lessonlistseq) {
		// TODO Auto-generated method stub
				boolean result=false;
				FileMapper mapper =session.getMapper(FileMapper.class);
				try{
					result= mapper.updaterepresentive(lessonlistseq);
				}catch(Exception e)
				{
					return result;
				}
				return result;
	}

	public ArrayList<HashMap<String, String>> selectUserLessonList(int customerseq) {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String, String>>lList = new ArrayList<HashMap<String, String>>();
		
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
			lList = mapper.selectUserLessonList(customerseq);
		}catch(Exception e){
			e.printStackTrace();
			return lList;
		}
		return lList;
	}

	public boolean deleteApplyFile(int userseq) {
		// TODO Auto-generated method stub
		boolean result = false;
		
		FileMapper mapper = session.getMapper(FileMapper.class);
		try{
			result = mapper.deleteApplyFile(userseq);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}
	
}
