package global.sesoc.wod.repository;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.FoodDTO;
import global.sesoc.wod.dto.MealDTO;
import global.sesoc.wod.dto.MealMenuDTO;

@Repository
public class FoodRepository {
	
	@Autowired
	SqlSession session;

	public boolean insertFoodDB(FoodDTO foodDTO) {
		// TODO Auto-generated method stub
		boolean result=false;
		
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
		result = mapper.insertFoodDB(foodDTO);
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}

	public boolean insertMealMenu(MealMenuDTO mealmenu) {
		// TODO Auto-generated method stub
		boolean insertMealMenuResult = false;
		
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
		insertMealMenuResult = mapper.insertMealMenu(mealmenu);
		}catch(Exception e){
			e.printStackTrace();
			return insertMealMenuResult;
		}
		
		return insertMealMenuResult;
	}

	public ArrayList<FoodDTO> selectFoodList(String keyWord) {
		// TODO Auto-generated method stub
		ArrayList<FoodDTO>fList = new ArrayList<FoodDTO>();
		
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
		fList = mapper.selectFoodList(keyWord);
		}catch(Exception e){
			return fList;
		}
		
		return fList;
	}

	public boolean insertMeal(MealDTO meal) {
		// TODO Auto-generated method stub
		boolean insertMealResult = false;
		
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
			insertMealResult = mapper.insertMeal(meal);
		}catch(Exception e){
			e.printStackTrace();
			return insertMealResult;
		}
		
		return insertMealResult;
		
	}

	public MealMenuDTO selectMealMenu(MealMenuDTO m) {
		// TODO Auto-generated method stub
		MealMenuDTO mealmenu = new MealMenuDTO();
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
			mealmenu = mapper.selectMealMenu(m);
		}catch(Exception e){
			e.printStackTrace();
			return mealmenu;
		}
		
		return mealmenu;
		
	}

	public ArrayList<MealDTO> selectMeal(MealMenuDTO mealmenu) {
		// TODO Auto-generated method stub
		ArrayList<MealDTO>mList = new ArrayList<MealDTO>();
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
		mList = mapper.selectMeal(mealmenu);
		}catch(Exception e){
			return mList;
		}
		return mList;
	}

	public FoodDTO selectOneFood(int foodseq) {
		// TODO Auto-generated method stub
		FoodDTO food = new FoodDTO();
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
		food = mapper.selectOneFood(foodseq);
		}catch(Exception e){
			return food;
		}
		return food;
	}

	public boolean updateMealMenu(MealMenuDTO mealmenu) {
		// TODO Auto-generated method stub
		boolean updateMealMenuResult = false;
		
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
			updateMealMenuResult = mapper.updateMealMenu(mealmenu);
		}catch(Exception e){
			e.printStackTrace();
			return updateMealMenuResult;
		}
		
		return updateMealMenuResult;
		
	
	}

	public ArrayList<MealMenuDTO> selectMealMenuList(int memberseq) {
		// TODO Auto-generated method stub
		ArrayList<MealMenuDTO>mList = new ArrayList<MealMenuDTO>();
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
		mList = mapper.selectMealMenuList(memberseq);
		}catch(Exception e){
			return mList;
		}
		return mList;
	}

	public MealMenuDTO selectOneMealMenu(MealMenuDTO mealmenu2) {
		// TODO Auto-generated method stub
		MealMenuDTO mealmenu = new MealMenuDTO();
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
			mealmenu = mapper.selectOneMealMenu(mealmenu2);
		}catch(Exception e){
			e.printStackTrace();
			return mealmenu;
		}
		
		return mealmenu;
		
		
	}

	public boolean updateMeal(int mealmenuseq) {
		// TODO Auto-generated method stub
		boolean updateMealResult = false;
		
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
			updateMealResult = mapper.updateMeal(mealmenuseq);
		}catch(Exception e){
			e.printStackTrace();
			return updateMealResult;
		}
		
		return updateMealResult;
		
		
	}

	public boolean deleteMealMenu(int mealmenuseq) {
		// TODO Auto-generated method stub
		boolean deleteMealMenuResult = false;
		
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
			deleteMealMenuResult = mapper.deleteMealMenu(mealmenuseq);
		}catch(Exception e){
			e.printStackTrace();
			return deleteMealMenuResult;
		}
		
		return deleteMealMenuResult;
		
		
	}

	public ArrayList<MealMenuDTO> selectOneMealMenuCheck(MealMenuDTO mealmenu) {
		// TODO Auto-generated method stub
		ArrayList<MealMenuDTO> m = new ArrayList<MealMenuDTO>();
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
			m = mapper.selectOneMealMenuCheck(mealmenu);
		}catch(Exception e){
			e.printStackTrace();
			return m;
		}
		
		return m;
		


	}

	public boolean deleteMeal(MealDTO mealDTO) {
		// TODO Auto-generated method stub
		boolean deleteMealResult = false;
		
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
			deleteMealResult = mapper.deleteMeal(mealDTO);
		}catch(Exception e){
			e.printStackTrace();
			return deleteMealResult;
		}
		
		return deleteMealResult;
	
	}

	public int selectFoodDBCount() {
		// TODO Auto-generated method stub
		int Count = 0;
		FoodMapper mapper = session.getMapper(FoodMapper.class);
		try{
			Count = mapper.selectFoodDBCount();
		}catch(Exception e){
			e.printStackTrace();
			return Count;
			
		}
		
		return Count;
	
	}

	
	



}
