package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import global.sesoc.wod.dto.MemberCommentDTO;
import global.sesoc.wod.dto.MemberDTO;

public interface MemberMapper {

	
	ArrayList<HashMap<String, String>> selectTrainingCustomers(int trainerseq);
	
	int insertMember(MemberDTO member);
	
	int selectLastMemberseq();

	boolean insertMemberComment(MemberCommentDTO mComment);

	MemberCommentDTO selectMemberComment(MemberCommentDTO m);

	ArrayList<MemberCommentDTO> selectMemberCommentList(MemberCommentDTO mc);

	MemberCommentDTO selectOneMemberComment(int membercommnetseq);

	int deleteMember(int memberseq);

	ArrayList<MemberDTO> selectMember(MemberDTO member);

	int selectCountMember(int trainerseq);
	
	
}
