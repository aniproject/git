package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.QnADTO;
import global.sesoc.wod.dto.QreplyDTO;
import global.sesoc.wod.dto.SearchDTO;
import global.sesoc.wod.util.PageNavigator;

@Repository
public class QnARepository {

	@Autowired
	SqlSession session;
	
	public int qnaRegister(QnADTO qna){
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		int result = 0;
		try {
			result = mapper.qnaRegister(qna);
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}
		
		
	}
	public ArrayList<HashMap<String, Object>> viewQna(String searchWord, PageNavigator navi){
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		 ArrayList<HashMap<String, Object>> result = null;
		try{
			
			RowBounds rb = new RowBounds(navi.getStartRecord(),navi.getCountPerPage());
			
			
			result = mapper.viewQna(searchWord, rb);
			return result;
			
			
			
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}
		
	}
	public int updateQna(QnADTO qna){
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		int result = 0;
		try {
			result = mapper.updateQna(qna);
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}
	}
	public int deleteQna(int qnaseq){
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		int result = 0;
		try {
			result = mapper.deleteQna(qnaseq);
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}
	}
	public List<QnADTO> search(QnADTO qna){
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		List<QnADTO> result = null;
		try {
			result = mapper.search(qna);
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}
	}
	public int qreplyRegist(QreplyDTO qre){
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		int result = 0;
		try {
			result = mapper.qreplyRegist(qre);
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}
	}
	public ArrayList<HashMap<String, String>> viewQreply(QnADTO qna){
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		
		ArrayList<HashMap<String, String>>  result = null;

		try {
			result = mapper.viewQreply(qna);
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}	
	}
	public int updateQreply(QreplyDTO qre){
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		int result = 0;
		try {
			result = mapper.updateQreply(qre);
			return result;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}
	}
	public int deleteQreply(QreplyDTO qre){
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		int result = 0;
		try {
			result = mapper.deleteQreply(qre);
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}
	}
	public int getContentCnt(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		int result=0;
		try {
			result = mapper.selectContentCnt(paramMap);
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}

	}
	/*public List<QnADTO> getContentList(String searchWord, PageNavigator navi) {
		// TODO Auto-generated method stub
		List<QnADTO> result = null;
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		try {
			RowBounds rb = new RowBounds(navi.getStartRecord(),navi.getCountPerPage());

			result = mapper.viewQna(searchWord, rb);
			return result;
		} catch (Exception e) {
			// TODO: handle exception\
			e.printStackTrace();
			return result;
		}
	}*/
	public List<QreplyDTO> getReplyList(Map<String, Object> paramMap, QnADTO qna) {
		// TODO Auto-generated method stub
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		return mapper.selectBoardReplyList(qna);
	}
	public HashMap<String, Object> getContentView(QnADTO qna) {
		// TODO Auto-generated method stub
		QnAMapper mapper = session.getMapper(QnAMapper.class);

		return mapper.selectContentView(qna);
	}
	public ArrayList<HashMap<String, Object>> qqList(int qnaseq) {
		// TODO Auto-generated method stub
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		ArrayList<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
		try {
			result = mapper.qqList(qnaseq);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}
		return result;
	}
	public ArrayList<HashMap<String, Object>> getAdminBoard(QnADTO qna) {
		// TODO Auto-generated method stub
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		ArrayList<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
		try {
			result = mapper.getAdminBoard(qna);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}
		return result;
	}
	public int selectCount(String searchWord) {
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		int result = 0;
		try {
			result = mapper.selectCount(searchWord);
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
		return result;
	}
	public int deleteQnAReply(int qnaseq) {
		// TODO Auto-generated method stub
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		int result = 0;
		try {
			result = mapper.deleteQnAReply(qnaseq);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}
		return 0;
	}
	public String chkCusName(int userSeq) {
		// TODO Auto-generated method stub
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		String result = null;
		try {
			result = mapper.chkCusName(userSeq);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}
		return result;
	}
	public HashMap<String, Object> adminView(int qnaseq) {
		// TODO Auto-generated method stub
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			result = mapper.adminView(qnaseq);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}
		return result;
	}
	public int updateAdminQna(QnADTO qna) {
		// TODO Auto-generated method stub
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		int result = 0;
		try {
			result = mapper.updateAdminQna(qna);
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return result;
		}

	}
	public ArrayList<QnADTO> selectAllQnAList(int userseq) {
		// TODO Auto-generated method stub
		ArrayList<QnADTO>qList = new ArrayList<QnADTO>();
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		try{
		qList = mapper.selectAllQnAList(userseq);
		}catch(Exception e){
			return qList;
		}
	
		return qList;
}
	public ArrayList<QnADTO> selectSearchQNAList(SearchDTO search) {
		// TODO Auto-generated method stub
		ArrayList<QnADTO>qList = new ArrayList<QnADTO>();
		QnAMapper mapper = session.getMapper(QnAMapper.class);
		try{
		qList = mapper.selectSearchQNAList(search);
		}catch(Exception e){
			return qList;
		}
	
		return qList;
	}
	}
