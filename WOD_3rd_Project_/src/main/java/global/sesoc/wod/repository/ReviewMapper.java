package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import global.sesoc.wod.dto.ReviewDTO;

public interface ReviewMapper {

   int insertReview(ReviewDTO review);
   
   ArrayList<HashMap<String, Object>> selectReviewList(int channelseq);
   
   int deleteReview(int reviewseq);
   
}