package global.sesoc.wod.repository;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import global.sesoc.wod.dto.ReviewDTO;

@Repository
public class ReviewRepository {

   @Autowired
   SqlSession session;
   
   public int insertReview(ReviewDTO review){
      int result = 0;
      ReviewMapper mapper = session.getMapper(ReviewMapper.class);
      try{
         result = mapper.insertReview(review);
      }catch(Exception e){
         e.printStackTrace();
         return result;
      }
      return result;
   }
   
   
   public ArrayList<HashMap<String, Object>> selectReviewList(int channelseq){
      ArrayList<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
      ReviewMapper mapper = session.getMapper(ReviewMapper.class);
      try{
         result = mapper.selectReviewList(channelseq);
      }catch(Exception e){
         e.printStackTrace();
         return result;
      }
      return result;
   }
   
   
   public int deleteReview(int reviewseq){
      int result = 0;
      ReviewMapper mapper = session.getMapper(ReviewMapper.class);
      try{
         result = mapper.deleteReview(reviewseq);
      }catch(Exception e){
         e.printStackTrace();
         return result;
      }
      return result;
   }
   
}