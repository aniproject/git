package global.sesoc.wod.repository;

import java.util.ArrayList;

import global.sesoc.wod.dto.ChatDTO;
import global.sesoc.wod.dto.ChatRoomDTO;

public interface ChatMapper {

	boolean insertChat(ChatDTO chat);

	ArrayList<ChatDTO> selectChatList(ChatDTO chat);

	ChatRoomDTO selectChatRoom(ChatDTO chat);

	boolean insertChatRoom(ChatDTO chat);

	ArrayList<ChatRoomDTO> selectChatRoomList(ChatRoomDTO cRoom);

	boolean updateRecentDate(int chatroomseq);

}
