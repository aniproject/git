package global.sesoc.wod.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
//UserCustomerMyPage에 유저가 아니면 못들어오게 
public class ChkUserInterceptor extends HandlerInterceptorAdapter{

   @Override
   public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
         throws Exception {
      // TODO Auto-generated method stub
      HttpSession session = request.getSession();
      
      if(!session.getAttribute("roleSeq").equals("101")){
         
         response.sendRedirect(request.getContextPath());
         return false;
         
      }else{
      
      return super.preHandle(request, response, handler);
   }
   }
}
