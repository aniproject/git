package global.sesoc.wod.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class adminInterceptor extends HandlerInterceptorAdapter{

   @Override
   public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
         throws Exception {
      // TODO Auto-generated method stub
      
      HttpSession session = request.getSession();
      if((Integer) session.getAttribute("roleSeq")!= null){
         int ic = (Integer) session.getAttribute("roleSeq");
         
         if(ic != 10001){
            
            response.sendRedirect(request.getContextPath());
            return false;
         }else{
            return super.preHandle(request, response, handler);
         }
         
         
      }else{
         response.sendRedirect(request.getContextPath());
         return false;
      }
   
   
   }
   
   
}