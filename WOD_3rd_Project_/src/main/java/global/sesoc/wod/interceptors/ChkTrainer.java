package global.sesoc.wod.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class ChkTrainer extends HandlerInterceptorAdapter{

   @Override
   public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
         throws Exception {
      // TODO Auto-generated method stub
      
      HttpSession session = request.getSession();
      
      if ((Integer)session.getAttribute("roleSeq")!=null) {
         
         int rs = (Integer)session.getAttribute("roleSeq");
         
         if(rs != 1001 || rs !=101){
            response.sendRedirect(request.getContentType());
            return false;
            
         }else{
            return super.preHandle(request, response, handler);
         }
         
         
         
      }else{
         
         response.sendRedirect(request.getContextPath());
         return false;
      }
      
      
      
   
   }

   
   
}