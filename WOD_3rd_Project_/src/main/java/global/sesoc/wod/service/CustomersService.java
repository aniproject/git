package global.sesoc.wod.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import global.sesoc.wod.dto.CustomersDTO;
import global.sesoc.wod.repository.CustomersRepository;

@Service
public class CustomersService {
	
	@Autowired
	CustomersRepository cRepo;

	public boolean insertCustomers(CustomersDTO customer) {
		// TODO Auto-generated method stub
		return cRepo.insertCustomers(customer);
	}

	public ArrayList<HashMap<String, String>> selectAllCustomer() {
		// TODO Auto-generated method stub
		return cRepo.selectAllCustomer();
	}

	public HashMap<String, String> selectOneCustomers(int userseq) {
		// TODO Auto-generated method stub
		return cRepo.selectOneCustomers(userseq);
	}

	public boolean deleteCustomers(int userseq) {
		// TODO Auto-generated method stub
		return cRepo.deleteCustomers(userseq);
	}
	
	public boolean updateCustomers(CustomersDTO customer) {
		// TODO Auto-generated method stub
		return cRepo.updateCustomers(customer);
	}
	
	public CustomersDTO selectOneCustomer(int userseq) {
		// TODO Auto-generated method stub
		return cRepo.selectOneCustomer(userseq);
	}

	public CustomersDTO selectCustomersByMemberSeq(int memberseq) {
		// TODO Auto-generated method stub
		return cRepo.selectCustomersByMemberSeq(memberseq);
	}

	public CustomersDTO selectCustomersAll(int customerseq) {
		// TODO Auto-generated method stub
		return cRepo.selectCustomersAll(customerseq);
	}
	
	
}
