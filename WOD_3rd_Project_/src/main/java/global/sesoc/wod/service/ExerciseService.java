package global.sesoc.wod.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import global.sesoc.wod.dto.ExerciseDTO;
import global.sesoc.wod.dto.TrainingDTO;
import global.sesoc.wod.repository.ExerciseRepository;

@Service
public class ExerciseService {
	
	@Autowired
	ExerciseRepository eRepo;

	public boolean insertExercise(ExerciseDTO ex) {
		// TODO Auto-generated method stub
		return eRepo.insertExercise(ex);
	}

	public ExerciseDTO selectExercise(ExerciseDTO ex) {
		// TODO Auto-generated method stub
		return eRepo.selectExercise(ex);
	}

	public boolean insertTraining(TrainingDTO tr) {
		// TODO Auto-generated method stub
		return eRepo.insertTraining(tr);
	}

	public ArrayList<TrainingDTO> selectTrainingList(ExerciseDTO exercise) {
		// TODO Auto-generated method stub
		return eRepo.selectTrainingList(exercise);
	}

	public ArrayList<ExerciseDTO> selectExerciseCheck(ExerciseDTO exercise) {
		// TODO Auto-generated method stub
		return eRepo.selectExerciseCheck(exercise);
	}

	public boolean updateExercise(ExerciseDTO ex) {
		// TODO Auto-generated method stub
		return eRepo.updateExercise(ex);
	}

	public boolean deleteTraining(TrainingDTO trainingDTO) {
		// TODO Auto-generated method stub
		return eRepo.deleteTraining(trainingDTO);
	}

	public boolean deleteExercise(int exerciseseq) {
		// TODO Auto-generated method stub
		return eRepo.deleteExercise(exerciseseq);
	}

	public ArrayList<ExerciseDTO> selectExerciseList(int memberseq) {
		// TODO Auto-generated method stub
		return eRepo.selectExerciseList(memberseq);
	}

	public ExerciseDTO selectOneExercise(ExerciseDTO ex) {
		// TODO Auto-generated method stub
		return eRepo.selectOneExercise(ex);
	}

	public ExerciseDTO selectExerciseByMemberSeq(int memberseq) {
		// TODO Auto-generated method stub
		return eRepo.selectExerciseByMemberSeq(memberseq);
	}

	public TrainingDTO selectTraining(ExerciseDTO ex) {
		// TODO Auto-generated method stub
		return eRepo.selectTraining(ex);
	}

}
