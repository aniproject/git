package global.sesoc.wod.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import global.sesoc.wod.dto.User_BasicDTO;
import global.sesoc.wod.dto.User_RoleDTO;
import global.sesoc.wod.repository.UserRepository;
import global.sesoc.wod.util.MailHandler;
import global.sesoc.wod.util.TempKey;

@Service
public class UserService {
	
	@Autowired
	UserRepository uRepo;
	
	@Autowired
	JavaMailSender mailSender;

	public User_BasicDTO selectOneUser(User_BasicDTO user) {
		// TODO Auto-generated method stub
		return uRepo.selectOneUser(user);
	}
	
	@Transactional
	public boolean insertUser(User_BasicDTO user) {
		// TODO Auto-generated method stub

		String authkey = new TempKey().getKey(50, false);
		user.setUserkey(authkey);
		MailHandler sendMail;
		try {
			sendMail = new MailHandler(mailSender);
			sendMail.setSubject("회원가입 이메일 인증");
			sendMail.setText(new StringBuffer().append("<h1>[이메일 인증]</h1>")
					.append("<p>이메일 인증 확인 링크를 클릭하신 후, 아래 코드를 입력하시면 이메일 인증이 완료됩니다.</p>")
					.append("<p><h1>"+authkey+"</h1></p>")
					.append("<a href='http://localhost:8080/wod/emailConfirm?userid=")
					.append(user.getUserid())
					.append("&useremail=")
					.append(user.getUseremail())
					.append("' target='_blank'>이메일 인증 확인</a>")
					.toString());
			sendMail.setFrom("admin", "WOD'sGYM");
			sendMail.setTo(user.getUseremail());
			sendMail.send();
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return uRepo.insertUser(user);
	}

	public boolean insertUserRole(User_BasicDTO u) {
		// TODO Auto-generated method stub
		return uRepo.insertUserRole(u);
	}

	public User_RoleDTO selectRoleseq(int userseq) {
		// TODO Auto-generated method stub
		return uRepo.selectRoleseq(userseq);
	}

	public boolean updateUserRole(User_RoleDTO uRole) {
		// TODO Auto-generated method stub
		return uRepo.updateUserRole(uRole);
	}

	public boolean rollbackUserRole(User_RoleDTO uRole) {
		// TODO Auto-generated method stub
		return uRepo.rollbackUserRole(uRole);
	}

	public User_BasicDTO passwordCheck(User_BasicDTO user) {
		// TODO Auto-generated method stub
		return uRepo.passwordCheck(user);
	}

	public boolean updateUserBasic(User_BasicDTO user) {
		// TODO Auto-generated method stub
		return uRepo.updateUserBasic(user);
	}

	public boolean deleteUserRole(int userseq) {
		// TODO Auto-generated method stub
		return uRepo.deleteUserRole(userseq);
	}

	public boolean deleteUserBasic(int userseq) {
		// TODO Auto-generated method stub
		return uRepo.deleteUserBasic(userseq);
	}

	public User_BasicDTO userkeyCheck(User_BasicDTO codeCheck) {
		// TODO Auto-generated method stub
		return uRepo.userkeyCheck(codeCheck);
	}

	public boolean updateUserkey(int userseq) {
		// TODO Auto-generated method stub
		return uRepo.updateUserkey(userseq);
	}

	public User_BasicDTO selectUserByMemberseq(int memberseq) {
		// TODO Auto-generated method stub
		return uRepo.selectUserByMemberseq(memberseq);
	}

	public HashMap<String, Object> chkUser(int userseq) {
		// TODO Auto-generated method stub
		return uRepo.chkUser(userseq);
	}

	public int selectAdminSeq() {
		// TODO Auto-generated method stub
		return uRepo.selectAdminSeq();
	}


}
