package global.sesoc.wod.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import global.sesoc.wod.dto.ReviewDTO;
import global.sesoc.wod.repository.ReviewRepository;

@Service
public class ReviewService {

   @Autowired
   ReviewRepository rRepo;
   
   public int insertReview(ReviewDTO review){
      return rRepo.insertReview(review);
   }
   
   public ArrayList<HashMap<String, Object>> selectReviewList(int channelseq){
      return rRepo.selectReviewList(channelseq);
   }
   
   
   public int deleteReview(int reviewseq){
      return rRepo.deleteReview(reviewseq);
   }
   
   
}