package global.sesoc.wod.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import global.sesoc.wod.dto.CommentDTO;
import global.sesoc.wod.repository.CommentRepository;

@Service
public class CommentService {

   @Autowired
   CommentRepository coRepo;

   public int insertDetailLessonComment(CommentDTO comment) {
      // TODO Auto-generated method stub
      return coRepo.insertDetailLessonComment(comment);
   }
   
   
   public ArrayList<HashMap<String,Object>> selectCommentList(CommentDTO comment) {
      // TODO Auto-generated method stub
      return coRepo.selectCommentList(comment);
   }
   
   
   public int deleteComment(CommentDTO comment) {
      // TODO Auto-generated method stub
      return coRepo.deleteComment(comment);
   }
   
   
   public int deletelessonComment(int lessonlistseq) {
      // TODO Auto-generated method stub
      return coRepo.deletelessonComment(lessonlistseq);
   }
   
   
   
}