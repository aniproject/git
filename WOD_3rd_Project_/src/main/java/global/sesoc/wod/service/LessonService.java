package global.sesoc.wod.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import global.sesoc.wod.dto.LessonDTO;
import global.sesoc.wod.dto.SearchDTO;
import global.sesoc.wod.repository.LessonRepository;

@Service
public class LessonService {
	
	@Autowired
	LessonRepository lRepo;
	
	public int insertTrainerNewLessonlist(LessonDTO lesson){
		return lRepo.insertTrainerNewLessonlist(lesson);
	}
	
	public ArrayList<LessonDTO> selectOneTrainerLessonlist(LessonDTO lesson) {
		// TODO Auto-generated method stub
		return lRepo.selectOneTrainerLessonlist(lesson);
	}
	
	public int deleteLessonlist(LessonDTO lesson){
		return lRepo.deleteLessonlist(lesson);
	}
	
	
	public LessonDTO selectOneLesson(int lessonlistseq){
		return lRepo.selectOneLesson(lessonlistseq);
	}

	public ArrayList<HashMap<String, String>> selectAllLessonList(int cSeq) {
		// TODO Auto-generated method stub
		return lRepo.selectAllLessonList(cSeq);
	}

	public ArrayList<HashMap<String, String>> selectSearchLessonList(SearchDTO search) {
		// TODO Auto-generated method stub
		return lRepo.selectSearchLessonList(search);
	}
	
	public ArrayList<LessonDTO> selectLessonlistByLessontype(LessonDTO lesson) {
	      // TODO Auto-generated method stub
	      return lRepo.selectLessonlistByLessontype(lesson);
	   }
	   
	   
	public LessonDTO selectEqualLessonlist(LessonDTO lesson){
	      return lRepo.selectEqualLessonlist(lesson);
	   }
	   
	   
   public int checkDeleteLessonlist(){
	      return lRepo.checkDeleteLessonlist();
	   }
	   
	
   public ArrayList<LessonDTO> selectDeleteMemberseq(){
	      return lRepo.selectDeleteMemberseq();
	   }

public ArrayList<LessonDTO> selectLessonList(int userseq) {
	// TODO Auto-generated method stub
	return lRepo.selectLessonList(userseq);
}

public ArrayList<HashMap<String, String>> selectRecommend() {
	// TODO Auto-generated method stub
	return lRepo.selectRecommend();
}
	
	
	
	
}
