package global.sesoc.wod.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import global.sesoc.wod.dto.AccountDTO;
import global.sesoc.wod.dto.PaymentDTO;
import global.sesoc.wod.repository.AccountRepository;

@Service
public class AccountService {
   
   @Autowired
   AccountRepository acRepo;

   public AccountDTO chkAccount(int userseq) {
      
      return acRepo.chkAccount(userseq);
   }

   public int insertAccount(int userseq) {
      return acRepo.insertAccount(userseq);
   }

   public int updateAccount(AccountDTO account) {
      
      return acRepo.updateAccount(account);
   }
   
   public AccountDTO chkTrainer(int trainerseq1){
      return acRepo.chkTrainer(trainerseq1);
   }

public boolean insertPayment(PaymentDTO pay) {
	// TODO Auto-generated method stub
	return acRepo.insertPayment(pay);
}

public ArrayList<PaymentDTO> selectPaymentList(PaymentDTO pay) {
	// TODO Auto-generated method stub
	return acRepo.selectPaymentList(pay);
}

public ArrayList<PaymentDTO> selectAllPayment() {
	// TODO Auto-generated method stub
	return acRepo.selectAllPayment();
}

public int chkAdmin() {
	// TODO Auto-generated method stub
	return acRepo.chkAdmin();
}

public ArrayList<HashMap<String, String>> selectTrainerPaymentList(int userseq) {
	// TODO Auto-generated method stub
	return acRepo.selectTrainerPaymentList(userseq);
}
}