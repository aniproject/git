package global.sesoc.wod.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import global.sesoc.wod.dto.FoodDTO;
import global.sesoc.wod.dto.MealDTO;
import global.sesoc.wod.dto.MealMenuDTO;
import global.sesoc.wod.repository.FoodRepository;

@Service
public class FoodService {
	
	@Autowired
	FoodRepository foodRepo;

	
	public boolean insertFoodDB(FoodDTO foodDTO) {
		// TODO Auto-generated method stub
		return foodRepo.insertFoodDB(foodDTO);
	}


	public boolean insertMealMenu(MealMenuDTO mealmenu) {
		// TODO Auto-generated method stub
		return foodRepo.insertMealMenu(mealmenu);
	}


	public ArrayList<FoodDTO> selectFoodList(String keyWord) {
		// TODO Auto-generated method stub
		return foodRepo.selectFoodList(keyWord);
	}


	public boolean insertMeal(MealDTO meal) {
		// TODO Auto-generated method stub
		return foodRepo.insertMeal(meal);
	}


	public MealMenuDTO selectMealMenu(MealMenuDTO m) {
		// TODO Auto-generated method stub
		return foodRepo.selectMealMenu(m);
	}


	public ArrayList<MealDTO> selectMeal(MealMenuDTO mealmenu) {
		// TODO Auto-generated method stub
		return foodRepo.selectMeal(mealmenu);
	}


	public FoodDTO selectOneFood(int foodseq) {
		// TODO Auto-generated method stub
		return foodRepo.selectOneFood(foodseq);
	}


	public boolean updateMealMenu(MealMenuDTO mealmenu) {
		// TODO Auto-generated method stub
		return foodRepo.updateMealMenu(mealmenu);
	}


	public ArrayList<MealMenuDTO> selectMealMenuList(int memberseq) {
		// TODO Auto-generated method stub
		return foodRepo.selectMealMenuList(memberseq);
	}


	public MealMenuDTO selectOneMealMenu(MealMenuDTO mealmenu) {
		// TODO Auto-generated method stub
		return foodRepo.selectOneMealMenu(mealmenu);
	}


	public boolean updateMeal(int mealmenuseq) {
		// TODO Auto-generated method stub
		return foodRepo.updateMeal(mealmenuseq);
	}


	public boolean deleteMealMenu(int mealmenuseq) {
		// TODO Auto-generated method stub
		return foodRepo.deleteMealMenu(mealmenuseq);
	}


	public ArrayList<MealMenuDTO> selectOneMealMenuCheck(MealMenuDTO mealmenu) {
		// TODO Auto-generated method stub
		return foodRepo.selectOneMealMenuCheck(mealmenu);
	}


	public boolean deleteMeal(MealDTO mealDTO) {
		// TODO Auto-generated method stub
		return foodRepo.deleteMeal(mealDTO);
		
	}


	public int selectFoodDBCount() {
		// TODO Auto-generated method stub
		return foodRepo.selectFoodDBCount();
	}



}
