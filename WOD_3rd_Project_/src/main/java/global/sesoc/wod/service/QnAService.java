package global.sesoc.wod.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import global.sesoc.wod.dto.QnADTO;
import global.sesoc.wod.dto.QreplyDTO;
import global.sesoc.wod.dto.SearchDTO;
import global.sesoc.wod.repository.QnARepository;
import global.sesoc.wod.util.PageNavigator;

@Service
public class QnAService {

	@Autowired
	QnARepository qrepo;
	
	public int qnaRegister(QnADTO qna){
		
		return qrepo.qnaRegister(qna);
	}
	public ArrayList<HashMap<String, Object>> viewQna(String searchWord, PageNavigator navi){
		
		return qrepo.viewQna(searchWord, navi);
	}
	public int updateQna(QnADTO qna){
		
		return qrepo.updateQna(qna);
	}
	public int deleteQna(int qnaseq){
		
		return qrepo.deleteQna(qnaseq);
	}
	public List<QnADTO> search(QnADTO qna){
		
		return qrepo.search(qna);
	}
	public int qreplyRegist(QreplyDTO qre){
		return qrepo.qreplyRegist(qre);
	}
	public ArrayList<HashMap<String,String>> viewQreply(QnADTO qna){
		return qrepo.viewQreply(qna);
	}
	public int updateQreply(QreplyDTO qre){
		return qrepo.updateQreply(qre);
	}
	public int deleteQreply(QreplyDTO qre){
		return qrepo.deleteQreply(qre);
	}
	public int getContentCnt(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return qrepo.getContentCnt(paramMap);
	}
	public List<QreplyDTO>getReplyList(Map<String, Object> paramMap) {
	
		List<QreplyDTO> newBoardReplyList = new ArrayList<QreplyDTO>();

	
		return newBoardReplyList;

	}
	
	public HashMap<String, Object> getContentView(QnADTO qna) {
		// TODO Auto-generated method stub
		return qrepo.getContentView(qna);
	}
	public ArrayList<HashMap<String, Object>> qqList(int qnaseq) {
		// TODO Auto-generated method stub
		return qrepo.qqList(qnaseq);
	}
	public ArrayList<HashMap<String, Object>> getAdminBoard(QnADTO qna) {
		// TODO Auto-generated method stub
		return qrepo.getAdminBoard(qna);
	}
	public int selectCount(String searchWord) {
		// TODO Auto-generated method stub
		return qrepo.selectCount(searchWord);
	}
	public int deleteQnAReply(int qnaseq) {
		// TODO Auto-generated method stub
		return qrepo.deleteQnAReply(qnaseq);
	}
	public String chkCusName(int userSeq) {
		// TODO Auto-generated method stub
		return qrepo.chkCusName(userSeq);
	}
	public HashMap<String, Object> adminView(int qnaseq) {
		// TODO Auto-generated method stub
		return qrepo.adminView(qnaseq);
	}
	public int updateAdminQna(QnADTO qna) {
		// TODO Auto-generated method stub
		return qrepo.updateAdminQna(qna);
	}
	public ArrayList<QnADTO> selectAllQnAList(int userseq) {
		// TODO Auto-generated method stub
		return qrepo.selectAllQnAList(userseq);
	}
	public ArrayList<QnADTO> selectSearchQNAList(SearchDTO search) {
		// TODO Auto-generated method stub
		return qrepo.selectSearchQNAList(search);
	}

}
