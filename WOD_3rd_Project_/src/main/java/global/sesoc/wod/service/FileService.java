package global.sesoc.wod.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import global.sesoc.wod.dto.FileDTO;
import global.sesoc.wod.dto.TrainingDTO;
import global.sesoc.wod.repository.FileRepository;


@Service
public class FileService {
	@Autowired
	FileRepository fRepo;
	
	
	public int insertApplyFile(FileDTO fvo) {
		// TODO Auto-generated method stub
		return fRepo.insertApplyFile(fvo);
	}
	public boolean applyImgUpload(FileDTO image) {
		// TODO Auto-generated method stub
		return fRepo.applyImgUpload(image);
	}
	public boolean applyResumeUpload(FileDTO resume) {
		// TODO Auto-generated method stub
		return fRepo.applyResumeUpload(resume);
	}
	public FileDTO selectResume(int userseq) {
		// TODO Auto-generated method stub
		return fRepo.selectResume(userseq);
	}
	public boolean insertVideo(FileDTO file) {
		// TODO Auto-generated method stub
		return fRepo.insertVideo(file);
	}
	public FileDTO selectVideoFile(int trainingseq) {
		// TODO Auto-generated method stub
		return fRepo.selectVideoFile(trainingseq);
	}
	public boolean deleteVideo(TrainingDTO trainingDTO) {
		// TODO Auto-generated method stub
		return fRepo.deleteVideo(trainingDTO);
		
		
	}
	public boolean insertMemberVideo(FileDTO file) {
		// TODO Auto-generated method stub
		return fRepo.insertMemberVideo(file);
	}
	public FileDTO selectMemberVideo(int membercommentseq) {
		// TODO Auto-generated method stub
		return fRepo.selectMemberVideo(membercommentseq);
	}
	public boolean insertFiles(FileDTO file) {
		// TODO Auto-generated method stub
		return fRepo.insertFiles(file);
	}
	public boolean insertFilesToChannel(FileDTO file) {
		// TODO Auto-generated method stub
		return fRepo.insertFilesToChannel(file);
	}
	public boolean updateProfileFile(int userseq) {
		// TODO Auto-generated method stub
		return fRepo.updateProfileFile(userseq);
	}
	public ArrayList<FileDTO> selectChannelFile(int channelseq) {
		// TODO Auto-generated method stub
		return fRepo.selectChannelFile(channelseq);
	}
	public FileDTO selectCurval() {
		// TODO Auto-generated method stub
		return fRepo.selectCurval();
	}
	public boolean updaterepresentive(int lessonlistseq) {
		// TODO Auto-generated method stub
		return fRepo.updaterepresentive(lessonlistseq);
	}
	public ArrayList<HashMap<String, String>> selectUserLessonList(int customerseq) {
		// TODO Auto-generated method stub
		return fRepo.selectUserLessonList(customerseq);
	}
	public boolean deleteApplyFile(int userseq) {
		// TODO Auto-generated method stub
		return fRepo.deleteApplyFile(userseq);
	}
	

}
