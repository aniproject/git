package global.sesoc.wod.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import global.sesoc.wod.dto.ChatDTO;
import global.sesoc.wod.dto.ChatRoomDTO;
import global.sesoc.wod.repository.ChatRepository;

@Service
public class ChatService {
	
	@Autowired
	ChatRepository chatRepo;

	public boolean insertChat(ChatDTO chat) {
		// TODO Auto-generated method stub
		return chatRepo.insertChat(chat);
	}

	public ArrayList<ChatDTO> selectChatList(ChatDTO chat) {
		// TODO Auto-generated method stub
		return chatRepo.selectChatList(chat);
	}

	public ChatRoomDTO selectChatRoom(ChatDTO chat) {
		// TODO Auto-generated method stub
		return chatRepo.selectChatRoom(chat);
	}

	public boolean insertChatRoom(ChatDTO chat) {
		// TODO Auto-generated method stub
		return chatRepo.insertChatRoom(chat);
	}

	public ArrayList<ChatRoomDTO> selectChatRoomList(ChatRoomDTO cRoom) {
		// TODO Auto-generated method stub
		return chatRepo.selectChatRoomList(cRoom);
	}

	public boolean updateRecentDate(int chatroomseq) {
		// TODO Auto-generated method stub
		return chatRepo.updateRecentDate(chatroomseq);
	}

}
