package global.sesoc.wod.service;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import global.sesoc.wod.dto.ApplyDTO;
import global.sesoc.wod.dto.CustomersDTO;
import global.sesoc.wod.dto.TrainerDTO;
import global.sesoc.wod.repository.TrainerRepository;

@Service
public class TrainerService {
	
	@Autowired
	TrainerRepository tRepo;
	

	public ArrayList<HashMap<String,String>> selectTrainer(String selectValue) {
		// TODO Auto-generated method stub
		return tRepo.selectTrainer(selectValue);
	}


	public TrainerDTO selectOneTrainer(int userseq) {
		// TODO Auto-generated method stub
		return tRepo.selectOneTrainer(userseq);
	}


	//작성자 홍기표
	//의도 = hashmap 파라미터 객체로 변환
	public boolean insertTrainer(HashMap<String, String> customer,HttpSession session) {
		// TODO Auto-generated method stub
		CustomersDTO cus = new CustomersDTO();
		String name = customer.get("NAME");
		String gender = customer.get("GENDER");
		String point = customer.get("POINT");
		String userSeq = customer.get("USERSEQ");
		cus.setName(name);
		cus.setGender(gender);
		cus.setPoint(Integer.parseInt(point));
		cus.setUserseq(Integer.parseInt(userSeq));
		return tRepo.insertTrainer(cus);
	}


	public boolean insertApply(ApplyDTO apply) {
		// TODO Auto-generated method stub
		return tRepo.insertApply(apply);
	}


	public boolean deleteApply(int userseq) {
		// TODO Auto-generated method stub
		return tRepo.deleteApply(userseq);
	}
	
	
	public ArrayList<HashMap<String,String>> selectTrainerAndChannel(int trainerseq) {
		// TODO Auto-generated method stub
		return tRepo.selectTrainerAndChannel(trainerseq);
	}

	public int updateTrainerComment(TrainerDTO trainer) {
		// TODO Auto-generated method stub
		System.out.println("33333333333333333333333");
		return tRepo.updateTrainerComment(trainer);
	}


	public ArrayList<HashMap<String, String>> selectOneTrainerDetail(int userseq) {
		// TODO Auto-generated method stub
		return tRepo.selectOneTrainerDetail(userseq);
	}


	public ArrayList<HashMap<String, String>> selectTrainerMemberCount() {
		// TODO Auto-generated method stub
		return tRepo.selectTrainerMemberCount();
	}


	public TrainerDTO selectTrainerAll(int trainerseq) {
		// TODO Auto-generated method stub
		return tRepo.selectTrainerAll(trainerseq);
	}


	public ArrayList<HashMap<String, String>> selectTrainerLessonCount() {
		// TODO Auto-generated method stub
		return tRepo.selectTrainerLessonCount();
	}


	public ArrayList<HashMap<String, String>> selectOneTrainerDetailNoLesson(int userseq) {
		// TODO Auto-generated method stub
		return tRepo.selectOneTrainerDetailNoLesson(userseq);
	}


	public ArrayList<HashMap<String, String>> selectRankTrainer() {
		// TODO Auto-generated method stub
		return tRepo.selectRankTrainer();
	}



	
}
