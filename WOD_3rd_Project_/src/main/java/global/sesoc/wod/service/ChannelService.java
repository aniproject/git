package global.sesoc.wod.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import global.sesoc.wod.dto.ChannelDTO;
import global.sesoc.wod.dto.TrainerDTO;
import global.sesoc.wod.repository.ChannelRepository;

@Service
public class ChannelService {

	@Autowired
	ChannelRepository chRepo;
	
	
	public ChannelDTO selectChannel(int trainerseq) {
		// TODO Auto-generated method stub
		return chRepo.selectChannel(trainerseq);
	}


	public boolean insertChannel(TrainerDTO trainer) {
		// TODO Auto-generated method stub
		return chRepo.insertChannel(trainer);
	}
	
	
	public int plusLikeAndRecommend(int channelseq) {
		// TODO Auto-generated method stub
		return chRepo.plusLikeAndRecommend(channelseq);
	}
	
	public int minusLikeAndRecommend(int channelseq) {
		// TODO Auto-generated method stub
		return chRepo.minusLikeAndRecommend(channelseq);
	}
	
	
}
