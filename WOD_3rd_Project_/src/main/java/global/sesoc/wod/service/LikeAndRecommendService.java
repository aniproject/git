package global.sesoc.wod.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import global.sesoc.wod.dto.LikeAndRecommendDTO;
import global.sesoc.wod.repository.LikeAndRecommendRepository;

@Service
public class LikeAndRecommendService {

	@Autowired
	LikeAndRecommendRepository larRepo;
	
	public int insertLike(LikeAndRecommendDTO lar){
		return larRepo.insertLike(lar);
	}
	
	public int deleteLike(LikeAndRecommendDTO lar){
		return larRepo.deleteLike(lar);
	}
	
	
	public LikeAndRecommendDTO selectLike(LikeAndRecommendDTO lar){
		return larRepo.selectLike(lar);
	}
	
	public LikeAndRecommendDTO selectLikeAndRecommendCount(int channelseq){
	      return larRepo.selectLikeAndRecommendCount(channelseq);
	   }

	public ArrayList<HashMap<String, String>> selectLikeList(int userseq) {
		// TODO Auto-generated method stub
		return larRepo.selectLikeList(userseq);
	}
	
	
	
}
