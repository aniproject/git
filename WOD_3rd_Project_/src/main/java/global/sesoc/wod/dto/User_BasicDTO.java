package global.sesoc.wod.dto;

public class User_BasicDTO {
	private int userseq;
	private String userid;
	private String userpw;
	private String userdate;
	private String useremail;
	private String userkey;
	public User_BasicDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User_BasicDTO(int userseq, String userid, String userpw, String userdate, String useremail, String userkey) {
		super();
		this.userseq = userseq;
		this.userid = userid;
		this.userpw = userpw;
		this.userdate = userdate;
		this.useremail = useremail;
		this.userkey = userkey;
	}
	public int getUserseq() {
		return userseq;
	}
	public void setUserseq(int userseq) {
		this.userseq = userseq;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUserpw() {
		return userpw;
	}
	public void setUserpw(String userpw) {
		this.userpw = userpw;
	}
	public String getUserdate() {
		return userdate;
	}
	public void setUserdate(String userdate) {
		this.userdate = userdate;
	}
	public String getUseremail() {
		return useremail;
	}
	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}
	public String getUserkey() {
		return userkey;
	}
	public void setUserkey(String userkey) {
		this.userkey = userkey;
	}
	@Override
	public String toString() {
		return String.format("User_BasicDTO [userseq=%s, userid=%s, userpw=%s, userdate=%s, useremail=%s, userkey=%s]",
				userseq, userid, userpw, userdate, useremail, userkey);
	}
	
	

}
