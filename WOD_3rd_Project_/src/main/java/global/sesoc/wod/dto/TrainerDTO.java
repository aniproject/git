package global.sesoc.wod.dto;

public class TrainerDTO {
	private int trainerseq;
	private String name;
	private String age;
	private String gender;
	private int point;
	private String lessontype;
	private int recommend;
	private int fees;
	private int userseq;
	private String trainercomment;
	private int count;
	public TrainerDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TrainerDTO(int trainerseq, String name, String age, String gender, int point, String lessontype,
			int recommend, int fees, int userseq, String trainercomment, int count) {
		super();
		this.trainerseq = trainerseq;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.point = point;
		this.lessontype = lessontype;
		this.recommend = recommend;
		this.fees = fees;
		this.userseq = userseq;
		this.trainercomment = trainercomment;
		this.count = count;
	}
	public int getTrainerseq() {
		return trainerseq;
	}
	public void setTrainerseq(int trainerseq) {
		this.trainerseq = trainerseq;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}
	public String getLessontype() {
		return lessontype;
	}
	public void setLessontype(String lessontype) {
		this.lessontype = lessontype;
	}
	public int getRecommend() {
		return recommend;
	}
	public void setRecommend(int recommend) {
		this.recommend = recommend;
	}
	public int getFees() {
		return fees;
	}
	public void setFees(int fees) {
		this.fees = fees;
	}
	public int getUserseq() {
		return userseq;
	}
	public void setUserseq(int userseq) {
		this.userseq = userseq;
	}
	public String getTrainercomment() {
		return trainercomment;
	}
	public void setTrainercomment(String trainercomment) {
		this.trainercomment = trainercomment;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return String.format(
				"TrainerDTO [trainerseq=%s, name=%s, age=%s, gender=%s, point=%s, lessontype=%s, recommend=%s, fees=%s, userseq=%s, trainercomment=%s, count=%s]",
				trainerseq, name, age, gender, point, lessontype, recommend, fees, userseq, trainercomment, count);
	}
	
	
	
}
