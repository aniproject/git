package global.sesoc.wod.dto;

public class PaymentDTO {
	private int paymentseq;
	private int pay;
	private String lessonname;
	private String paydate;
	private int accountseq;
	private int trainerseq;
	private int customerseq;
	private String trainername;
	private String customername;
	public PaymentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PaymentDTO(int paymentseq, int pay, String lessonname, String paydate, int accountseq, int trainerseq,
			int customerseq, String trainername, String customername) {
		super();
		this.paymentseq = paymentseq;
		this.pay = pay;
		this.lessonname = lessonname;
		this.paydate = paydate;
		this.accountseq = accountseq;
		this.trainerseq = trainerseq;
		this.customerseq = customerseq;
		this.trainername = trainername;
		this.customername = customername;
	}
	public int getPaymentseq() {
		return paymentseq;
	}
	public void setPaymentseq(int paymentseq) {
		this.paymentseq = paymentseq;
	}
	public int getPay() {
		return pay;
	}
	public void setPay(int pay) {
		this.pay = pay;
	}
	public String getLessonname() {
		return lessonname;
	}
	public void setLessonname(String lessonname) {
		this.lessonname = lessonname;
	}
	public String getPaydate() {
		return paydate;
	}
	public void setPaydate(String paydate) {
		this.paydate = paydate;
	}
	public int getAccountseq() {
		return accountseq;
	}
	public void setAccountseq(int accountseq) {
		this.accountseq = accountseq;
	}
	public int getTrainerseq() {
		return trainerseq;
	}
	public void setTrainerseq(int trainerseq) {
		this.trainerseq = trainerseq;
	}
	public int getCustomerseq() {
		return customerseq;
	}
	public void setCustomerseq(int customerseq) {
		this.customerseq = customerseq;
	}
	public String getTrainername() {
		return trainername;
	}
	public void setTrainername(String trainername) {
		this.trainername = trainername;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	@Override
	public String toString() {
		return String.format(
				"PaymentDTO [paymentseq=%s, pay=%s, lessonname=%s, paydate=%s, accountseq=%s, trainerseq=%s, customerseq=%s, trainername=%s, customername=%s]",
				paymentseq, pay, lessonname, paydate, accountseq, trainerseq, customerseq, trainername, customername);
	}
	
	

}
