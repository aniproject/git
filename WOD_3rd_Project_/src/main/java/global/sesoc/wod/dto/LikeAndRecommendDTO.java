package global.sesoc.wod.dto;

public class LikeAndRecommendDTO {
   
   private int likeandrecommendseq;
   private int userseq;
   private int channelseq;
   private int trainerlike;
   private int trainerrecommend;
   private int trainerlikeCount;
   private int trainerrecommendCount;
   
   public LikeAndRecommendDTO() {
      super();
      // TODO Auto-generated constructor stub
   }

   public LikeAndRecommendDTO(int likeandrecommendseq, int userseq, int channelseq, int trainerlike,
         int trainerrecommend, int trainerlikeCount, int trainerrecommendCount) {
      super();
      this.likeandrecommendseq = likeandrecommendseq;
      this.userseq = userseq;
      this.channelseq = channelseq;
      this.trainerlike = trainerlike;
      this.trainerrecommend = trainerrecommend;
      this.trainerlikeCount = trainerlikeCount;
      this.trainerrecommendCount = trainerrecommendCount;
   }

   public int getLikeandrecommendseq() {
      return likeandrecommendseq;
   }

   public void setLikeandrecommendseq(int likeandrecommendseq) {
      this.likeandrecommendseq = likeandrecommendseq;
   }

   public int getUserseq() {
      return userseq;
   }

   public void setUserseq(int userseq) {
      this.userseq = userseq;
   }

   public int getChannelseq() {
      return channelseq;
   }

   public void setChannelseq(int channelseq) {
      this.channelseq = channelseq;
   }

   public int getTrainerlike() {
      return trainerlike;
   }

   public void setTrainerlike(int trainerlike) {
      this.trainerlike = trainerlike;
   }

   public int getTrainerrecommend() {
      return trainerrecommend;
   }

   public void setTrainerrecommend(int trainerrecommend) {
      this.trainerrecommend = trainerrecommend;
   }

   public int getTrainerlikeCount() {
      return trainerlikeCount;
   }

   public void setTrainerlikeCount(int trainerlikeCount) {
      this.trainerlikeCount = trainerlikeCount;
   }

   public int getTrainerrecommendCount() {
      return trainerrecommendCount;
   }

   public void setTrainerrecommendCount(int trainerrecommendCount) {
      this.trainerrecommendCount = trainerrecommendCount;
   }

   @Override
   public String toString() {
      return "LikeAndRecommendDTO [likeandrecommendseq=" + likeandrecommendseq + ", userseq=" + userseq
            + ", channelseq=" + channelseq + ", trainerlike=" + trainerlike + ", trainerrecommend="
            + trainerrecommend + ", trainerlikeCount=" + trainerlikeCount + ", trainerrecommendCount="
            + trainerrecommendCount + "]";
   }
   
   
   
   
}