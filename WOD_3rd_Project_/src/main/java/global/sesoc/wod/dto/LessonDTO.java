package global.sesoc.wod.dto;

public class LessonDTO {
	private int lessonlistseq;
	private int lessonscore; 
	private int lessonlike;
	private int lessonrecommend;
	private int lessonprice;
	private String lessonname;
	private String lessonstartdate;
	private String lessonenddate;
	private int trainerseq;
	private String lessontype;
	private int viewcount;
	private int channelseq;
	private String categoryname;
	private int memberseq;
	private String lessoncontent;
	private int lessoncoursedate; // 코스의 기간  - 최문석
	private int lessoncheck; // 강의목록의 강좌면 0,  수강생이 듣고있는 강좌면 1
	private int restdate; // 수강잔여일 - 최문석 0908
	private int imgfile;
	private int videofile;
	private String imgsave;
	private String videosave;
	public LessonDTO() {
		super();
		// TODO Auto-generated constructor stub
	}





	public LessonDTO(int lessonlistseq, int lessonscore, int lessonlike, int lessonrecommend, int lessonprice,
			String lessonname, String lessonstartdate, String lessonenddate, int trainerseq, String lessontype,
			int viewcount, int channelseq, String categoryname, int memberseq, String lessoncontent,
			int lessoncoursedate, int lessoncheck, int restdate, int imgfile, int videofile, String imgsave,
			String videosave) {
		super();
		this.lessonlistseq = lessonlistseq;
		this.lessonscore = lessonscore;
		this.lessonlike = lessonlike;
		this.lessonrecommend = lessonrecommend;
		this.lessonprice = lessonprice;
		this.lessonname = lessonname;
		this.lessonstartdate = lessonstartdate;
		this.lessonenddate = lessonenddate;
		this.trainerseq = trainerseq;
		this.lessontype = lessontype;
		this.viewcount = viewcount;
		this.channelseq = channelseq;
		this.categoryname = categoryname;
		this.memberseq = memberseq;
		this.lessoncontent = lessoncontent;
		this.lessoncoursedate = lessoncoursedate;
		this.lessoncheck = lessoncheck;
		this.restdate = restdate;
		this.imgfile = imgfile;
		this.videofile = videofile;
		this.imgsave = imgsave;
		this.videosave = videosave;
	}





	public String getImgsave() {
		return imgsave;
	}

	public void setImgsave(String imgsave) {
		this.imgsave = imgsave;
	}

	public String getVideosave() {
		return videosave;
	}

	public void setVideosave(String videosave) {
		this.videosave = videosave;
	}

	public int getImgfile() {
		return imgfile;
	}


	public void setImgfile(int imgfile) {
		this.imgfile = imgfile;
	}


	public int getVideofile() {
		return videofile;
	}


	public void setVideofile(int videofile) {
		this.videofile = videofile;
	}

	public int getLessonlistseq() {
		return lessonlistseq;
	}


	public void setLessonlistseq(int lessonlistseq) {
		this.lessonlistseq = lessonlistseq;
	}


	public int getLessonscore() {
		return lessonscore;
	}


	public void setLessonscore(int lessonscore) {
		this.lessonscore = lessonscore;
	}


	public int getLessonlike() {
		return lessonlike;
	}


	public void setLessonlike(int lessonlike) {
		this.lessonlike = lessonlike;
	}


	public int getLessonrecommend() {
		return lessonrecommend;
	}


	public void setLessonrecommend(int lessonrecommend) {
		this.lessonrecommend = lessonrecommend;
	}


	public int getLessonprice() {
		return lessonprice;
	}


	public void setLessonprice(int lessonprice) {
		this.lessonprice = lessonprice;
	}


	public String getLessonname() {
		return lessonname;
	}


	public void setLessonname(String lessonname) {
		this.lessonname = lessonname;
	}


	public String getLessonstartdate() {
		return lessonstartdate;
	}


	public void setLessonstartdate(String lessonstartdate) {
		this.lessonstartdate = lessonstartdate;
	}


	public String getLessonenddate() {
		return lessonenddate;
	}


	public void setLessonenddate(String lessonenddate) {
		this.lessonenddate = lessonenddate;
	}


	public int getTrainerseq() {
		return trainerseq;
	}


	public void setTrainerseq(int trainerseq) {
		this.trainerseq = trainerseq;
	}


	public String getLessontype() {
		return lessontype;
	}


	public void setLessontype(String lessontype) {
		this.lessontype = lessontype;
	}


	public int getViewcount() {
		return viewcount;
	}


	public void setViewcount(int viewcount) {
		this.viewcount = viewcount;
	}


	public int getChannelseq() {
		return channelseq;
	}


	public void setChannelseq(int channelseq) {
		this.channelseq = channelseq;
	}


	public String getCategoryname() {
		return categoryname;
	}


	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}


	public int getMemberseq() {
		return memberseq;
	}


	public void setMemberseq(int memberseq) {
		this.memberseq = memberseq;
	}


	public String getLessoncontent() {
		return lessoncontent;
	}


	public void setLessoncontent(String lessoncontent) {
		this.lessoncontent = lessoncontent;
	}


	public int getLessoncoursedate() {
		return lessoncoursedate;
	}


	public void setLessoncoursedate(int lessoncoursedate) {
		this.lessoncoursedate = lessoncoursedate;
	}


	public int getLessoncheck() {
		return lessoncheck;
	}


	public void setLessoncheck(int lessoncheck) {
		this.lessoncheck = lessoncheck;
	}


	public int getRestdate() {
		return restdate;
	}


	public void setRestdate(int restdate) {
		this.restdate = restdate;
	}





	@Override
	public String toString() {
		return "LessonDTO [lessonlistseq=" + lessonlistseq + ", lessonscore=" + lessonscore + ", lessonlike="
				+ lessonlike + ", lessonrecommend=" + lessonrecommend + ", lessonprice=" + lessonprice + ", lessonname="
				+ lessonname + ", lessonstartdate=" + lessonstartdate + ", lessonenddate=" + lessonenddate
				+ ", trainerseq=" + trainerseq + ", lessontype=" + lessontype + ", viewcount=" + viewcount
				+ ", channelseq=" + channelseq + ", categoryname=" + categoryname + ", memberseq=" + memberseq
				+ ", lessoncontent=" + lessoncontent + ", lessoncoursedate=" + lessoncoursedate + ", lessoncheck="
				+ lessoncheck + ", restdate=" + restdate + ", imgfile=" + imgfile + ", videofile=" + videofile
				+ ", imgsave=" + imgsave + ", videosave=" + videosave + "]";
	}




	
	
	
	
	
}
