package global.sesoc.wod.dto;

public class ChatRoomDTO {
	private int chatroomseq;
	private int userseq1;
	private int userseq2;
	private String othersname;
	private String recentdate;
	public ChatRoomDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ChatRoomDTO(int chatroomseq, int userseq1, int userseq2, String othersname, String recentdate) {
		super();
		this.chatroomseq = chatroomseq;
		this.userseq1 = userseq1;
		this.userseq2 = userseq2;
		this.othersname = othersname;
		this.recentdate = recentdate;
	}
	public int getChatroomseq() {
		return chatroomseq;
	}
	public void setChatroomseq(int chatroomseq) {
		this.chatroomseq = chatroomseq;
	}
	public int getUserseq1() {
		return userseq1;
	}
	public void setUserseq1(int userseq1) {
		this.userseq1 = userseq1;
	}
	public int getUserseq2() {
		return userseq2;
	}
	public void setUserseq2(int userseq2) {
		this.userseq2 = userseq2;
	}
	public String getOthersname() {
		return othersname;
	}
	public void setOthersname(String othersname) {
		this.othersname = othersname;
	}
	public String getRecentdate() {
		return recentdate;
	}
	public void setRecentdate(String recentdate) {
		this.recentdate = recentdate;
	}
	@Override
	public String toString() {
		return String.format("ChatRoomDTO [chatroomseq=%s, userseq1=%s, userseq2=%s, othersname=%s, recentdate=%s]",
				chatroomseq, userseq1, userseq2, othersname, recentdate);
	}
	
}
