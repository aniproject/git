package global.sesoc.wod.dto;

public class SearchDTO {
	
	private String searchvalue;
	private int seq;
	public SearchDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SearchDTO(String searchvalue, int seq) {
		super();
		this.searchvalue = searchvalue;
		this.seq = seq;
	}
	public String getSearchvalue() {
		return searchvalue;
	}
	public void setSearchvalue(String searchvalue) {
		this.searchvalue = searchvalue;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	@Override
	public String toString() {
		return String.format("SearchDTO [searchvalue=%s, seq=%s]", searchvalue, seq);
	}
	
	

}
