package global.sesoc.wod.dto;

public class MealDTO {
	private int mealseq;
	private int foodseq;
	private int mealmenuseq;
	private int quantity;
	private String mealname;
	public MealDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MealDTO(int mealseq, int foodseq, int mealmenuseq, int quantity, String mealname) {
		super();
		this.mealseq = mealseq;
		this.foodseq = foodseq;
		this.mealmenuseq = mealmenuseq;
		this.quantity = quantity;
		this.mealname = mealname;
	}
	public int getMealseq() {
		return mealseq;
	}
	public void setMealseq(int mealseq) {
		this.mealseq = mealseq;
	}
	public int getFoodseq() {
		return foodseq;
	}
	public void setFoodseq(int foodseq) {
		this.foodseq = foodseq;
	}
	public int getMealmenuseq() {
		return mealmenuseq;
	}
	public void setMealmenuseq(int mealmenuseq) {
		this.mealmenuseq = mealmenuseq;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getMealname() {
		return mealname;
	}
	public void setMealname(String mealname) {
		this.mealname = mealname;
	}
	@Override
	public String toString() {
		return String.format("MealDTO [mealseq=%s, foodseq=%s, mealmenuseq=%s, quantity=%s, mealname=%s]", mealseq,
				foodseq, mealmenuseq, quantity, mealname);
	}
	
	
	
	

}
