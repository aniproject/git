package global.sesoc.wod.dto;

public class CustomersDTO {
	private int customerseq;
	private String name;
	private String age;
	private String gender;
	private String trainerid;
	private int point;
	private int userseq;
	public CustomersDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CustomersDTO(int customerseq, String name, String age, String gender, String trainerid, int point,
			int userseq) {
		super();
		this.customerseq = customerseq;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.trainerid = trainerid;
		this.point = point;
		this.userseq = userseq;
	}
	public int getCustomerseq() {
		return customerseq;
	}
	public void setCustomerseq(int customerseq) {
		this.customerseq = customerseq;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getTrainerid() {
		return trainerid;
	}
	public void setTrainerid(String trainerid) {
		this.trainerid = trainerid;
	}
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}
	public int getUserseq() {
		return userseq;
	}
	public void setUserseq(int userseq) {
		this.userseq = userseq;
	}
	@Override
	public String toString() {
		return "CustomersDTO [customerseq=" + customerseq + ", name=" + name + ", age=" + age + ", gender=" + gender
				+ ", trainerid=" + trainerid + ", point=" + point + ", userseq=" + userseq + "]";
	}
	
	
	
}
