package global.sesoc.wod.dto;

public class MemberDTO {

	private int memberseq;
	private int customerseq;
	private int trainerseq;
	private int memberlevel;
	
	
	public MemberDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public MemberDTO(int memberseq, int customerseq, int trainerseq, int memberlevel) {
		super();
		this.memberseq = memberseq;
		this.customerseq = customerseq;
		this.trainerseq = trainerseq;
		this.memberlevel = memberlevel;
	}


	public int getMemberseq() {
		return memberseq;
	}


	public void setMemberseq(int memberseq) {
		this.memberseq = memberseq;
	}


	public int getCustomerseq() {
		return customerseq;
	}


	public void setCustomerseq(int customerseq) {
		this.customerseq = customerseq;
	}


	public int getTrainerseq() {
		return trainerseq;
	}


	public void setTrainerseq(int trainerseq) {
		this.trainerseq = trainerseq;
	}


	public int getMemberlevel() {
		return memberlevel;
	}


	public void setMemberlevel(int memberlevel) {
		this.memberlevel = memberlevel;
	}


	@Override
	public String toString() {
		return "MemberDTO [memberseq=" + memberseq + ", customerseq=" + customerseq + ", trainerseq=" + trainerseq
				+ ", memberlevel=" + memberlevel + "]";
	}

	
	
	
	
}
