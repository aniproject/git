package global.sesoc.wod.dto;

public class MemberCommentDTO {
	private int membercommentseq;
	private String comments;
	private String membercommentdate;
	private int memberseq;
	private int trainerseq;
	public MemberCommentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MemberCommentDTO(int membercommentseq, String comments, String membercommentdate, int memberseq,
			int trainerseq) {
		super();
		this.membercommentseq = membercommentseq;
		this.comments = comments;
		this.membercommentdate = membercommentdate;
		this.memberseq = memberseq;
		this.trainerseq = trainerseq;
	}
	public int getMembercommentseq() {
		return membercommentseq;
	}
	public void setMembercommentseq(int membercommentseq) {
		this.membercommentseq = membercommentseq;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getMembercommentdate() {
		return membercommentdate;
	}
	public void setMembercommentdate(String membercommentdate) {
		this.membercommentdate = membercommentdate;
	}
	public int getMemberseq() {
		return memberseq;
	}
	public void setMemberseq(int memberseq) {
		this.memberseq = memberseq;
	}
	public int getTrainerseq() {
		return trainerseq;
	}
	public void setTrainerseq(int trainerseq) {
		this.trainerseq = trainerseq;
	}
	@Override
	public String toString() {
		return String.format(
				"MemberCommentDTO [membercommentseq=%s, comments=%s, membercommentdate=%s, memberseq=%s, trainerseq=%s]",
				membercommentseq, comments, membercommentdate, memberseq, trainerseq);
	}
	
	
	
	

}
