package global.sesoc.wod.dto;

public class ReviewDTO {

   private int reviewseq;
   private int channelseq;
   private int userseq;
   private int depth;
   private String content;
   private String registerdate;
   private int star;
   
   public ReviewDTO() {
      super();
      // TODO Auto-generated constructor stub
   }

   public ReviewDTO(int reviewseq, int channelseq, int userseq, int depth, String content, String registerdate,
         int star) {
      super();
      this.reviewseq = reviewseq;
      this.channelseq = channelseq;
      this.userseq = userseq;
      this.depth = depth;
      this.content = content;
      this.registerdate = registerdate;
      this.star = star;
   }

   public int getReviewseq() {
      return reviewseq;
   }

   public void setReviewseq(int reviewseq) {
      this.reviewseq = reviewseq;
   }

   public int getChannelseq() {
      return channelseq;
   }

   public void setChannelseq(int channelseq) {
      this.channelseq = channelseq;
   }

   public int getUserseq() {
      return userseq;
   }

   public void setUserseq(int userseq) {
      this.userseq = userseq;
   }

   public int getDepth() {
      return depth;
   }

   public void setDepth(int depth) {
      this.depth = depth;
   }

   public String getContent() {
      return content;
   }

   public void setContent(String content) {
      this.content = content;
   }

   public String getRegisterdate() {
      return registerdate;
   }

   public void setRegisterdate(String registerdate) {
      this.registerdate = registerdate;
   }

   public int getStar() {
      return star;
   }

   public void setStar(int star) {
      this.star = star;
   }

   @Override
   public String toString() {
      return "ReviewDTO [reviewseq=" + reviewseq + ", channelseq=" + channelseq + ", userseq=" + userseq + ", depth="
            + depth + ", content=" + content + ", registerdate=" + registerdate + ", star=" + star + "]";
   }
   
   
   
}