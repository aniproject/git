package global.sesoc.wod.dto;

public class FileDTO {
	private int filesseq;
	private String savename;
	private String oriname;
	private String savepath;
	private int chatseq;
	private int trainingseq;
	private int lessonlistseq;
	private int channelseq;
	private int userseq;
	private String type;
	private int membercommentseq;
	
	public FileDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FileDTO(int filesseq, String savename, String oriname, String savepath, int chatseq, int trainingseq,
			int lessonlistseq, int channelseq, int userseq, String type, int membercommentseq) {
		super();
		this.filesseq = filesseq;
		this.savename = savename;
		this.oriname = oriname;
		this.savepath = savepath;
		this.chatseq = chatseq;
		this.trainingseq = trainingseq;
		this.lessonlistseq = lessonlistseq;
		this.channelseq = channelseq;
		this.userseq = userseq;
		this.type = type;
		this.membercommentseq = membercommentseq;
	}
	
	public int getFilesseq() {
		return filesseq;
	}
	public void setFilesseq(int filesseq) {
		this.filesseq = filesseq;
	}
	public String getSavename() {
		return savename;
	}
	public void setSavename(String savename) {
		this.savename = savename;
	}
	public String getOriname() {
		return oriname;
	}
	public void setOriname(String oriname) {
		this.oriname = oriname;
	}
	public String getSavepath() {
		return savepath;
	}
	public void setSavepath(String savepath) {
		this.savepath = savepath;
	}
	public int getChatseq() {
		return chatseq;
	}
	public void setChatseq(int chatseq) {
		this.chatseq = chatseq;
	}
	public int getTrainingseq() {
		return trainingseq;
	}
	public void setTrainingseq(int trainingseq) {
		this.trainingseq = trainingseq;
	}
	public int getLessonlistseq() {
		return lessonlistseq;
	}
	public void setLessonlistseq(int lessonlistseq) {
		this.lessonlistseq = lessonlistseq;
	}
	public int getChannelseq() {
		return channelseq;
	}
	public void setChannelseq(int channelseq) {
		this.channelseq = channelseq;
	}
	public int getUserseq() {
		return userseq;
	}
	public void setUserseq(int userseq) {
		this.userseq = userseq;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getMembercommentseq() {
		return membercommentseq;
	}
	public void setMembercommentseq(int membercommentseq) {
		this.membercommentseq = membercommentseq;
	}
	@Override
	public String toString() {
		return String.format(
				"FileDTO [fileseq=%s, savename=%s, oriname=%s, savepath=%s, chatseq=%s, trainingseq=%s, lessonlistseq=%s, channelseq=%s, userseq=%s, type=%s, membercommentseq=%s]",
				filesseq, savename, oriname, savepath, chatseq, trainingseq, lessonlistseq, channelseq, userseq, type,
				membercommentseq);
	}
	
	
	
}
