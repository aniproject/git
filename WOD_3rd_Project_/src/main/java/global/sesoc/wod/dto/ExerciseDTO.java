package global.sesoc.wod.dto;

public class ExerciseDTO {
	private int exerciseseq;
	private String exercisedate;
	private String exercisetype;
	private int memberseq;
	private String memo;
	private String checked;
	public ExerciseDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ExerciseDTO(int exerciseseq, String exercisedate, String exercisetype, int memberseq, String memo,
			String checked) {
		super();
		this.exerciseseq = exerciseseq;
		this.exercisedate = exercisedate;
		this.exercisetype = exercisetype;
		this.memberseq = memberseq;
		this.memo = memo;
		this.checked = checked;
	}
	public int getExerciseseq() {
		return exerciseseq;
	}
	public void setExerciseseq(int exerciseseq) {
		this.exerciseseq = exerciseseq;
	}
	public String getExercisedate() {
		return exercisedate;
	}
	public void setExercisedate(String exercisedate) {
		this.exercisedate = exercisedate;
	}
	public String getExercisetype() {
		return exercisetype;
	}
	public void setExercisetype(String exercisetype) {
		this.exercisetype = exercisetype;
	}
	public int getMemberseq() {
		return memberseq;
	}
	public void setMemberseq(int memberseq) {
		this.memberseq = memberseq;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	@Override
	public String toString() {
		return String.format(
				"ExerciseDTO [exerciseseq=%s, exercisedate=%s, exercisetype=%s, memberseq=%s, memo=%s, checked=%s]",
				exerciseseq, exercisedate, exercisetype, memberseq, memo, checked);
	}
	
	
	
	
	
}
