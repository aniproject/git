package global.sesoc.wod.dto;

public class ApplyDTO {
	private int applyseq;
	private String applytype;
	private int userseq;
	public ApplyDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ApplyDTO(int applyseq, String applytype, int userseq) {
		super();
		this.applyseq = applyseq;
		this.applytype = applytype;
		this.userseq = userseq;
	}
	public int getApplyseq() {
		return applyseq;
	}
	public void setApplyseq(int applyseq) {
		this.applyseq = applyseq;
	}
	public String getApplytype() {
		return applytype;
	}
	public void setApplytype(String applytype) {
		this.applytype = applytype;
	}
	public int getUserseq() {
		return userseq;
	}
	public void setUserseq(int userseq) {
		this.userseq = userseq;
	}
	@Override
	public String toString() {
		return String.format("ApplyDTO [applyseq=%s, applytype=%s, userseq=%s]", applyseq, applytype, userseq);
	}
	

}
