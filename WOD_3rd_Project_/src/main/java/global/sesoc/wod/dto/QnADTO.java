package global.sesoc.wod.dto;

public class QnADTO {
	private int qnaseq;
	private int userseq;
	private String qtitle;
	private String qcontent;
	private String indate;
	private String qtype;
	private String userid;
	public QnADTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public QnADTO(int qnaseq, int userseq, String qtitle, String qcontent, String indate, String qtype, String userid) {
		super();
		this.qnaseq = qnaseq;
		this.userseq = userseq;
		this.qtitle = qtitle;
		this.qcontent = qcontent;
		this.indate = indate;
		this.qtype = qtype;
		this.userid = userid;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getQtype() {
		return qtype;
	}
	public void setQtype(String qtype) {
		this.qtype = qtype;
	}
	public int getQnaseq() {
		return qnaseq;
	}
	public void setQnaseq(int qnaseq) {
		this.qnaseq = qnaseq;
	}
	public int getUserseq() {
		return userseq;
	}
	public void setUserseq(int userseq) {
		this.userseq = userseq;
	}
	public String getQtitle() {
		return qtitle;
	}
	public void setQtitle(String qtitle) {
		this.qtitle = qtitle;
	}
	public String getQcontent() {
		return qcontent;
	}
	public void setQcontent(String qcontent) {
		this.qcontent = qcontent;
	}
	public String getIndate() {
		return indate;
	}
	public void setIndate(String indate) {
		this.indate = indate;
	}
	@Override
	public String toString() {
		return String.format("QnADTO [qnaseq=%s, userseq=%s, qtitle=%s, qcontent=%s, indate=%s, qtype=%s, userid=%s]", qnaseq,
				userseq, qtitle, qcontent, indate, qtype, userid);
	}
	
}
