package global.sesoc.wod.dto;

public class TrainingDTO {
	private int trainingseq;
	private String trainingname;
	private String trainingtype;
	private String trainingtarget;
	private int trainingcount;
	private int trainingset;
	private String trainingdone;
	private int exerciseseq;
	private int lessonlistseq;
	private int weightandtime;
	private int filesseq;
	public TrainingDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TrainingDTO(int trainingseq, String trainingname, String trainingtype, String trainingtarget,
			int trainingcount, int trainingset, String trainingdone, int exerciseseq, int lessonlistseq,
			int weightandtime, int filesseq) {
		super();
		this.trainingseq = trainingseq;
		this.trainingname = trainingname;
		this.trainingtype = trainingtype;
		this.trainingtarget = trainingtarget;
		this.trainingcount = trainingcount;
		this.trainingset = trainingset;
		this.trainingdone = trainingdone;
		this.exerciseseq = exerciseseq;
		this.lessonlistseq = lessonlistseq;
		this.weightandtime = weightandtime;
		this.filesseq = filesseq;
	}
	public int getTrainingseq() {
		return trainingseq;
	}
	public void setTrainingseq(int trainingseq) {
		this.trainingseq = trainingseq;
	}
	public String getTrainingname() {
		return trainingname;
	}
	public void setTrainingname(String trainingname) {
		this.trainingname = trainingname;
	}
	public String getTrainingtype() {
		return trainingtype;
	}
	public void setTrainingtype(String trainingtype) {
		this.trainingtype = trainingtype;
	}
	public String getTrainingtarget() {
		return trainingtarget;
	}
	public void setTrainingtarget(String trainingtarget) {
		this.trainingtarget = trainingtarget;
	}
	public int getTrainingcount() {
		return trainingcount;
	}
	public void setTrainingcount(int trainingcount) {
		this.trainingcount = trainingcount;
	}
	public int getTrainingset() {
		return trainingset;
	}
	public void setTrainingset(int trainingset) {
		this.trainingset = trainingset;
	}
	public String getTrainingdone() {
		return trainingdone;
	}
	public void setTrainingdone(String trainingdone) {
		this.trainingdone = trainingdone;
	}
	public int getExerciseseq() {
		return exerciseseq;
	}
	public void setExerciseseq(int exerciseseq) {
		this.exerciseseq = exerciseseq;
	}
	public int getLessonlistseq() {
		return lessonlistseq;
	}
	public void setLessonlistseq(int lessonlistseq) {
		this.lessonlistseq = lessonlistseq;
	}
	public int getWeightandtime() {
		return weightandtime;
	}
	public void setWeightandtime(int weightandtime) {
		this.weightandtime = weightandtime;
	}
	public int getFilesseq() {
		return filesseq;
	}
	public void setFilesseq(int filesseq) {
		this.filesseq = filesseq;
	}
	@Override
	public String toString() {
		return String.format(
				"TrainingDTO [trainingseq=%s, trainingname=%s, trainingtype=%s, trainingtarget=%s, trainingcount=%s, trainingset=%s, trainingdone=%s, exerciseseq=%s, lessonlistseq=%s, weightandtime=%s, filesseq=%s]",
				trainingseq, trainingname, trainingtype, trainingtarget, trainingcount, trainingset, trainingdone,
				exerciseseq, lessonlistseq, weightandtime, filesseq);
	}
	

}
