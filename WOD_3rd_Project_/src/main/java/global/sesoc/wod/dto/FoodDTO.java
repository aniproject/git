package global.sesoc.wod.dto;

public class FoodDTO {
	private int foodseq;
	private String foodname;
	private String category;
	private String calory;
	private String gram;
	private String carbohydrate;
	private String protein;
	private String fat;
	private String salt;
	private int mealmenuseq;
	public FoodDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FoodDTO(int foodseq, String foodname, String category, String calory, String gram, String carbohydrate,
			String protein, String fat, String salt, int mealmenuseq) {
		super();
		this.foodseq = foodseq;
		this.foodname = foodname;
		this.category = category;
		this.calory = calory;
		this.gram = gram;
		this.carbohydrate = carbohydrate;
		this.protein = protein;
		this.fat = fat;
		this.salt = salt;
		this.mealmenuseq = mealmenuseq;
	}
	public int getFoodseq() {
		return foodseq;
	}
	public void setFoodseq(int foodseq) {
		this.foodseq = foodseq;
	}
	public String getFoodname() {
		return foodname;
	}
	public void setFoodname(String foodname) {
		this.foodname = foodname;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCalory() {
		return calory;
	}
	public void setCalory(String calory) {
		this.calory = calory;
	}
	public String getGram() {
		return gram;
	}
	public void setGram(String gram) {
		this.gram = gram;
	}
	public String getCarbohydrate() {
		return carbohydrate;
	}
	public void setCarbohydrate(String carbohydrate) {
		this.carbohydrate = carbohydrate;
	}
	public String getProtein() {
		return protein;
	}
	public void setProtein(String protein) {
		this.protein = protein;
	}
	public String getFat() {
		return fat;
	}
	public void setFat(String fat) {
		this.fat = fat;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public int getMealmenuseq() {
		return mealmenuseq;
	}
	public void setMealmenuseq(int mealmenuseq) {
		this.mealmenuseq = mealmenuseq;
	}
	@Override
	public String toString() {
		return String.format(
				"FoodDTO [foodseq=%s, foodname=%s, category=%s, calory=%s, gram=%s, carbohydrate=%s, protein=%s, fat=%s, salt=%s, mealmenuseq=%s]",
				foodseq, foodname, category, calory, gram, carbohydrate, protein, fat, salt, mealmenuseq);
	}
	
	

	

}
