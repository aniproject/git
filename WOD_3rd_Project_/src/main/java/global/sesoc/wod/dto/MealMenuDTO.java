package global.sesoc.wod.dto;

public class MealMenuDTO {
	private int mealmenuseq;
	private String mealdate;
	private String menuwhen;
	private int memberseq;
	private String memo;
	private int quantity;
	private int foodseq;
	private String checked;
	public MealMenuDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MealMenuDTO(int mealmenuseq, String mealdate, String menuwhen, int memberseq, String memo, int quantity,
			int foodseq, String checked) {
		super();
		this.mealmenuseq = mealmenuseq;
		this.mealdate = mealdate;
		this.menuwhen = menuwhen;
		this.memberseq = memberseq;
		this.memo = memo;
		this.quantity = quantity;
		this.foodseq = foodseq;
		this.checked = checked;
	}
	public int getMealmenuseq() {
		return mealmenuseq;
	}
	public void setMealmenuseq(int mealmenuseq) {
		this.mealmenuseq = mealmenuseq;
	}
	public String getMealdate() {
		return mealdate;
	}
	public void setMealdate(String mealdate) {
		this.mealdate = mealdate;
	}
	public String getMenuwhen() {
		return menuwhen;
	}
	public void setMenuwhen(String menuwhen) {
		this.menuwhen = menuwhen;
	}
	public int getMemberseq() {
		return memberseq;
	}
	public void setMemberseq(int memberseq) {
		this.memberseq = memberseq;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getFoodseq() {
		return foodseq;
	}
	public void setFoodseq(int foodseq) {
		this.foodseq = foodseq;
	}
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	@Override
	public String toString() {
		return String.format(
				"MealMenuDTO [mealmenuseq=%s, mealdate=%s, menuwhen=%s, memberseq=%s, memo=%s, quantity=%s, foodseq=%s, checked=%s]",
				mealmenuseq, mealdate, menuwhen, memberseq, memo, quantity, foodseq, checked);
	}
	
	
	
	

}
