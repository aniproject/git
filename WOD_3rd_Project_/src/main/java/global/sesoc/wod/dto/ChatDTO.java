package global.sesoc.wod.dto;

public class ChatDTO {
	private int chatseq;
	private int sendseq;
	private int receiveseq;
	private String content;
	private String chatdate;
	private int chatroomseq;
	private String name;
	public ChatDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ChatDTO(int chatseq, int sendseq, int receiveseq, String content, String chatdate, int chatroomseq,
			String name) {
		super();
		this.chatseq = chatseq;
		this.sendseq = sendseq;
		this.receiveseq = receiveseq;
		this.content = content;
		this.chatdate = chatdate;
		this.chatroomseq = chatroomseq;
		this.name = name;
	}
	public int getChatseq() {
		return chatseq;
	}
	public void setChatseq(int chatseq) {
		this.chatseq = chatseq;
	}
	public int getSendseq() {
		return sendseq;
	}
	public void setSendseq(int sendseq) {
		this.sendseq = sendseq;
	}
	public int getReceiveseq() {
		return receiveseq;
	}
	public void setReceiveseq(int receiveseq) {
		this.receiveseq = receiveseq;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getChatdate() {
		return chatdate;
	}
	public void setChatdate(String chatdate) {
		this.chatdate = chatdate;
	}
	public int getChatroomseq() {
		return chatroomseq;
	}
	public void setChatroomseq(int chatroomseq) {
		this.chatroomseq = chatroomseq;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return String.format(
				"ChatDTO [chatseq=%s, sendseq=%s, receiveseq=%s, content=%s, chatdate=%s, chatroomseq=%s, name=%s]",
				chatseq, sendseq, receiveseq, content, chatdate, chatroomseq, name);
	}
	
	
	

}
