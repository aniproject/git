package global.sesoc.wod.dto;

public class AccountDTO {
   private int accountseq;
   private int money;
   private int sales;
   private int userseq;
   public AccountDTO() {
      super();
      // TODO Auto-generated constructor stub
   }
   public AccountDTO(int accountseq, int money, int userseq, int sales) {
      super();
      this.accountseq = accountseq;
      this.money = money;
      this.userseq = userseq;
      this.sales = sales;
   }
   
   public int getSales() {
      return sales;
   }
   public void setSales(int sales) {
      this.sales = sales;
   }
   public int getAccountseq() {
      return accountseq;
   }
   public void setAccountseq(int accountseq) {
      this.accountseq = accountseq;
   }
   public int getMoney() {
      return money;
   }
   public void setMoney(int money) {
      this.money = money;
   }
   public int getUserseq() {
      return userseq;
   }
   public void setUserseq(int userseq) {
      this.userseq = userseq;
   }
   @Override
   public String toString() {
      return "AccountDTO [accountseq=" + accountseq + ", money=" + money + ", userseq=" + userseq + ", sales="+sales+"]";
   }
   
}