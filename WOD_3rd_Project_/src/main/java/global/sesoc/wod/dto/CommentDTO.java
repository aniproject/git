package global.sesoc.wod.dto;

public class CommentDTO {
   
   private int replyseq;
   private int depth;
   private int origin;
   private String content;
   private int userseq;
   private int lessonlistseq;
   private String registerdate; 
   private String name;
   public CommentDTO() {
      super();
      // TODO Auto-generated constructor stub
   }
   public CommentDTO(int replyseq, int depth, int origin, String content, int userseq, int lessonlistseq,
         String registerdate, String name) {
      super();
      this.replyseq = replyseq;
      this.depth = depth;
      this.origin = origin;
      this.content = content;
      this.userseq = userseq;
      this.lessonlistseq = lessonlistseq;
      this.registerdate = registerdate;
      this.name = name;
   }
   public int getReplyseq() {
      return replyseq;
   }
   public void setReplyseq(int replyseq) {
      this.replyseq = replyseq;
   }
   public int getDepth() {
      return depth;
   }
   public void setDepth(int depth) {
      this.depth = depth;
   }
   public int getOrigin() {
      return origin;
   }
   public void setOrigin(int origin) {
      this.origin = origin;
   }
   public String getContent() {
      return content;
   }
   public void setContent(String content) {
      this.content = content;
   }
   public int getUserseq() {
      return userseq;
   }
   public void setUserseq(int userseq) {
      this.userseq = userseq;
   }
   public int getLessonlistseq() {
      return lessonlistseq;
   }
   public void setLessonlistseq(int lessonlistseq) {
      this.lessonlistseq = lessonlistseq;
   }
   public String getRegisterdate() {
      return registerdate;
   }
   public void setRegisterdate(String registerdate) {
      this.registerdate = registerdate;
   }
   public String getName() {
      return name;
   }
   public void setName(String name) {
      this.name = name;
   }
   @Override
   public String toString() {
      return "CommentDTO [replyseq=" + replyseq + ", depth=" + depth + ", origin=" + origin + ", content=" + content
            + ", userseq=" + userseq + ", lessonlistseq=" + lessonlistseq + ", registerdate=" + registerdate
            + ", name=" + name + "]";
   }
   
   
   
   
}