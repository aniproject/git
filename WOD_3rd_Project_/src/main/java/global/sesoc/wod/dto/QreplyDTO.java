package global.sesoc.wod.dto;

public class QreplyDTO {
	private int qreplyseq;
	  private int qnaseq;
	  private int userseq;
	  private int depth;
	  private String qreindate;
	  private String qrecontent;
	  private String qreName;
	  
	public String getQreName() {
		return qreName;
	}

	public void setQreName(String qreName) {
		this.qreName = qreName;
	}

	public QreplyDTO() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {
		return "QreplyDTO [qreplyseq=" + qreplyseq + ", qnaseq=" + qnaseq + ", userseq=" + userseq + ", depth=" + depth
				+ ", qreindate=" + qreindate + ", qrecontent=" + qrecontent + ", qreName=" + qreName + "]";
	}

	public QreplyDTO(int qreplyseq, int qnaseq, int userseq, int depth, String qreindate, String qrecontent, String qreName) {
		super();
		this.qreplyseq = qreplyseq;
		this.qnaseq = qnaseq;
		this.userseq = userseq;
		this.depth = depth;
		this.qreindate = qreindate;
		this.qrecontent = qrecontent;
		this.qreName = qreName;
	}

	public int getQreplyseq() {
		return qreplyseq;
	}
	public void setQreplyseq(int qreplyseq) {
		this.qreplyseq = qreplyseq;
	}
	public int getQnaseq() {
		return qnaseq;
	}
	public void setQnaseq(int qnaseq) {
		this.qnaseq = qnaseq;
	}
	public int getUserseq() {
		return userseq;
	}
	public void setUserseq(int userseq) {
		this.userseq = userseq;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	public String getQreindate() {
		return qreindate;
	}
	public void setQreindate(String qreindate) {
		this.qreindate = qreindate;
	}
	public String getQrecontent() {
		return qrecontent;
	}
	public void setQrecontent(String qrecontent) {
		this.qrecontent = qrecontent;
	}

	  
}
