package global.sesoc.wod.dto;

public class User_RoleDTO {
	private int userseq;
	private int roleseq;
	public User_RoleDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User_RoleDTO(int userseq, int roleseq) {
		super();
		this.userseq = userseq;
		this.roleseq = roleseq;
	}
	public int getUserseq() {
		return userseq;
	}
	public void setUserseq(int userseq) {
		this.userseq = userseq;
	}
	public int getRoleseq() {
		return roleseq;
	}
	public void setRoleseq(int roleseq) {
		this.roleseq = roleseq;
	}
	@Override
	public String toString() {
		return String.format("User_RoleDTO [userseq=%s, roleseq=%s]", userseq, roleseq);
	}
	
	
	

}
