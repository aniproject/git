package global.sesoc.wod.dto;

public class ChannelDTO {
	private int channelseq;
	private int trainerseq;
	private String writedate;
	private String trainerlike;
	private String trainerrecommend;
	private int cartlistseq;
	public ChannelDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ChannelDTO(int channelseq, int trainerseq, String writedate, String trainerlike, String trainerrecommend,
			int cartlistseq) {
		super();
		this.channelseq = channelseq;
		this.trainerseq = trainerseq;
		this.writedate = writedate;
		this.trainerlike = trainerlike;
		this.trainerrecommend = trainerrecommend;
		this.cartlistseq = cartlistseq;
	}
	public int getChannelseq() {
		return channelseq;
	}
	public void setChannelseq(int channelseq) {
		this.channelseq = channelseq;
	}
	public int getTrainerseq() {
		return trainerseq;
	}
	public void setTrainerseq(int trainerseq) {
		this.trainerseq = trainerseq;
	}
	public String getWritedate() {
		return writedate;
	}
	public void setWritedate(String writedate) {
		this.writedate = writedate;
	}
	public String getTrainerlike() {
		return trainerlike;
	}
	public void setTrainerlike(String trainerlike) {
		this.trainerlike = trainerlike;
	}
	public String getTrainerrecommend() {
		return trainerrecommend;
	}
	public void setTrainerrecommend(String trainerrecommend) {
		this.trainerrecommend = trainerrecommend;
	}
	public int getCartlistseq() {
		return cartlistseq;
	}
	public void setCartlistseq(int cartlistseq) {
		this.cartlistseq = cartlistseq;
	}
	@Override
	public String toString() {
		return String.format(
				"ChannelDTO [channelseq=%s, trainerseq=%s, writedate=%s, trainerlike=%s, trainerrecommend=%s, cartlistseq=%s, getChannelseq()=%s, getTrainerseq()=%s, getWritedate()=%s, getTrainerlike()=%s, getTrainerrecommend()=%s, getCartlistseq()=%s, getClass()=%s, hashCode()=%s, toString()=%s]",
				channelseq, trainerseq, writedate, trainerlike, trainerrecommend, cartlistseq, getChannelseq(),
				getTrainerseq(), getWritedate(), getTrainerlike(), getTrainerrecommend(), getCartlistseq(), getClass(),
				hashCode(), super.toString());
	}
	
	
	
}
