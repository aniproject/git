package global.sesoc.wod.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import global.sesoc.wod.dto.CustomersDTO;
import global.sesoc.wod.dto.User_BasicDTO;
import global.sesoc.wod.service.CustomersService;
import global.sesoc.wod.service.TrainerService;
import global.sesoc.wod.service.UserService;

@Controller
public class CustomersController {
	
	@Autowired
	CustomersService cService;
	
	@Autowired
	TrainerService tServic;
	
	@Autowired
	UserService uService;
	

	@RequestMapping(value="selectAllCustomer", method = RequestMethod.GET)
	public @ResponseBody ArrayList<HashMap<String,String>>selectAllCustomer(Long Paging){
		ArrayList<HashMap<String,String>>cList = new ArrayList<HashMap<String,String>>();
		cList = cService.selectAllCustomer();
		ArrayList<HashMap<String,String>>pagingCList = new ArrayList<HashMap<String,String>>();
		if(Paging!=null){
			for(int i = 0 ; i < cList.size() ; i++){
				if (i > Paging-9) {
					if(i <= Paging){
						pagingCList.add(cList.get(i));
					}
				}
			}
			return pagingCList;
		}
		return cList;
	}
	
	@RequestMapping(value="selectOneCustomers", method = RequestMethod.GET)
	public @ResponseBody HashMap<String,String> selectOneCustomers(HttpSession session){
		int userseq = (Integer) session.getAttribute("loginSeq");
		HashMap<String,String> customer = new HashMap<String,String>();
		customer = cService.selectOneCustomers(userseq);
		return customer;
		
	}
	
	
	
	@RequestMapping(value="updateCustomers", method = RequestMethod.POST)
	public @ResponseBody String updateCustomers(CustomersDTO customer, User_BasicDTO user, HttpSession session){

		int userSeq = (Integer) session.getAttribute("loginSeq");
		customer.setUserseq(userSeq);
		
		boolean updateCustomersResult;
		updateCustomersResult = cService.updateCustomers(customer);
		
		user.setUserseq(userSeq);
		boolean updateUserBasicResult;
		updateUserBasicResult = uService.updateUserBasic(user);
		
		if(updateCustomersResult&&updateUserBasicResult){
			return "suc";
		}else{
			return "fail";
		}
	}
	

}
