package global.sesoc.wod.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import global.sesoc.wod.dto.FoodDTO;
import global.sesoc.wod.dto.MealDTO;
import global.sesoc.wod.dto.MealMenuDTO;
import global.sesoc.wod.service.FoodService;

@Controller
public class FoodController {
	
	@Autowired
	FoodService foodService;
	
	
	
	@RequestMapping(value = "insertFoodDB", method = RequestMethod.POST)
	public @ResponseBody String insertFoodDB(HttpServletRequest request){
		ArrayList<FoodDTO>fList = new ArrayList<FoodDTO>();
		   try {
			   String CRAWLINGPATH = request.getSession().getServletContext().getRealPath("/resources/식품.xlsx");
	            File file = new File(CRAWLINGPATH);
	 
	            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(file));
	            Cell cell = null;
	         
	            for (Row row : wb.getSheetAt(0)) { 
	                if (row.getRowNum() < 2) {
	                    continue;
	                }
	                if(row.getCell(1) == null){
	                    break;
	                }
	                FoodDTO food = new FoodDTO();
	                food.setFoodname(String.valueOf(row.getCell(2)));
	                food.setCategory(String.valueOf(row.getCell(1)));
	                food.setGram(String.valueOf(row.getCell(3)));
	                food.setCalory(String.valueOf(row.getCell(4)));
	                food.setCarbohydrate(String.valueOf(row.getCell(5)));
	                food.setProtein(String.valueOf(row.getCell(6)));
	                food.setFat(String.valueOf(row.getCell(7)));
	                food.setSalt(String.valueOf(row.getCell(9)));
	             
	                fList.add(food);
	           
	            }
	            
	            FileOutputStream fileOut = new FileOutputStream(file);
	            wb.write(fileOut);
	        } catch (FileNotFoundException fe) {
	        } catch (IOException ie) {
	        } catch (NumberFormatException nE){
	        }
		   
		   for(int i = 0 ; i < fList.size() ; i++){
			  	try{
			  		Double.parseDouble(fList.get(i).getSalt());
			  	}catch(Exception e){
			  		fList.get(i).setSalt("0");
			  	}

			  	try{
			  		Double.parseDouble(fList.get(i).getCalory());
			  	}catch(Exception e){
			  		fList.get(i).setCalory("0");
			  	}
			  	
			  	try{
			  		Double.parseDouble(fList.get(i).getGram());
			  	}catch(Exception e){
			  		fList.get(i).setGram("0");
			  	}
			  	
			  	try{
			  		Double.parseDouble(fList.get(i).getCarbohydrate());
			  	}catch(Exception e){
			  		fList.get(i).setCarbohydrate("0");
			  	}
			  	
			  	try{
			  		Double.parseDouble(fList.get(i).getProtein());
			  	}catch(Exception e){
			  		fList.get(i).setProtein("0");
			  	}
			  	
			 	try{
			  		Double.parseDouble(fList.get(i).getFat());
			  	}catch(Exception e){
			  		fList.get(i).setFat("0");
			  	}	 	
			 	foodService.insertFoodDB(fList.get(i));
		  }
		   return String.valueOf(fList.size());
	}
	
	@RequestMapping(value = "insertRetortFoodDB", method = RequestMethod.POST)
	public String insertRetortFoodDB(HttpServletRequest request){
		ArrayList<FoodDTO>fList = new ArrayList<FoodDTO>();
		   try {
			   String CRAWLINGPATH = request.getSession().getServletContext().getRealPath("/resources/가공.xlsx");
	            File file = new File(CRAWLINGPATH);
	 
	            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(file));
	            Cell cell = null;
	         
	            for (Row row : wb.getSheetAt(0)) { 
	                if (row.getRowNum() < 2) {
	                    continue;
	                }
	                if(row.getCell(1) == null){
	                    break;
	                }
	                FoodDTO food = new FoodDTO();
	                food.setFoodname(String.valueOf(row.getCell(2)));
	                food.setCategory(String.valueOf(row.getCell(1)));
	                food.setGram(String.valueOf(row.getCell(3)));
	                food.setCalory(String.valueOf(row.getCell(4)));
	                food.setCarbohydrate(String.valueOf(row.getCell(5)));
	                food.setProtein(String.valueOf(row.getCell(6)));
	                food.setFat(String.valueOf(row.getCell(7)));
	                food.setSalt(String.valueOf(row.getCell(9)));
	             
	                fList.add(food);
	           
	            }
	            
	            FileOutputStream fileOut = new FileOutputStream(file);
	            wb.write(fileOut);
	        } catch (FileNotFoundException fe) {
	        } catch (IOException ie) {
	        } catch (NumberFormatException nE){
	        	
	        }
		   
		   for(int i = 0 ; i < fList.size() ; i++){
			  	try{
			  		Double.parseDouble(fList.get(i).getSalt());
			  	}catch(Exception e){
			  		fList.get(i).setSalt("0");
			  	}

			  	try{
			  		Double.parseDouble(fList.get(i).getCalory());
			  	}catch(Exception e){
			  		fList.get(i).setCalory("0");
			  	}
			  	
			  	try{
			  		Double.parseDouble(fList.get(i).getGram());
			  	}catch(Exception e){
			  		fList.get(i).setGram("0");
			  	}
			  	
			  	try{
			  		Double.parseDouble(fList.get(i).getCarbohydrate());
			  	}catch(Exception e){
			  		fList.get(i).setCarbohydrate("0");
			  	}
			  	
			  	try{
			  		Double.parseDouble(fList.get(i).getProtein());
			  	}catch(Exception e){
			  		fList.get(i).setProtein("0");
			  	}
			  	
			 	try{
			  		Double.parseDouble(fList.get(i).getFat());
			  	}catch(Exception e){
			  		fList.get(i).setFat("0");
			  	}
			 	
				foodService.insertFoodDB(fList.get(i));
				
				if(i==13522){
				  return String.valueOf(fList.size()-1);
				}
		  }
		   return String.valueOf(fList.size());
	}
	
	@RequestMapping(value = "insertMealDB", method = RequestMethod.POST)
	public String insertMealDB(HttpServletRequest request){
		ArrayList<FoodDTO>fList = new ArrayList<FoodDTO>();
		   try {
			   String CRAWLINGPATH = request.getSession().getServletContext().getRealPath("/resources/음식.xlsx");
	            File file = new File(CRAWLINGPATH);
	 
	            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(file));
	            Cell cell = null;
	         
	            for (Row row : wb.getSheetAt(0)) { 
	                if (row.getRowNum() < 2) {
	                    continue;
	                }
	                if(row.getCell(1) == null){
	                    break;
	                }
	                FoodDTO food = new FoodDTO();
	                food.setFoodname(String.valueOf(row.getCell(3)));
	                food.setCategory(String.valueOf(row.getCell(1)));
	                food.setGram(String.valueOf(row.getCell(4)));
	                food.setCalory(String.valueOf(row.getCell(5)));
	                food.setCarbohydrate(String.valueOf(row.getCell(6)));
	                food.setProtein(String.valueOf(row.getCell(7)));
	                food.setFat(String.valueOf(row.getCell(8)));
	                food.setSalt(String.valueOf(row.getCell(10)));
	             
	                fList.add(food);
	           
	            }
	            
	            FileOutputStream fileOut = new FileOutputStream(file);
	            wb.write(fileOut);
	        } catch (FileNotFoundException fe) {
	        } catch (IOException ie) {
	        } catch (NumberFormatException nE){
	        	
	        }
		   
		   for(int i = 0 ; i < fList.size() ; i++){
			  	try{
			  		Double.parseDouble(fList.get(i).getSalt());
			  	}catch(Exception e){
			  		fList.get(i).setSalt("0");
			  	}

			  	try{
			  		Double.parseDouble(fList.get(i).getCalory());
			  	}catch(Exception e){
			  		fList.get(i).setCalory("0");
			  	}
			  	
			  	try{
			  		Double.parseDouble(fList.get(i).getGram());
			  	}catch(Exception e){
			  		fList.get(i).setGram("0");
			  	}
			  	
			  	try{
			  		Double.parseDouble(fList.get(i).getCarbohydrate());
			  	}catch(Exception e){
			  		fList.get(i).setCarbohydrate("0");
			  	}
			  	
			  	try{
			  		Double.parseDouble(fList.get(i).getProtein());
			  	}catch(Exception e){
			  		fList.get(i).setProtein("0");
			  	}
			  	
			 	try{
			  		Double.parseDouble(fList.get(i).getFat());
			  	}catch(Exception e){
			  		fList.get(i).setFat("0");
			  	}
				foodService.insertFoodDB(fList.get(i));
		  }
		   return String.valueOf(fList.size());
	}
	
	
	@RequestMapping(value = "insertMealMenu", method = RequestMethod.POST)
	public @ResponseBody String insertMealMenu(MealMenuDTO mealmenu){
		boolean insertMealMenuResult;
		insertMealMenuResult = foodService.insertMealMenu(mealmenu);
		if(!insertMealMenuResult){
			return "fail";
		}else{
			return "suc";
		}
	};
	
	@RequestMapping(value = "selectFoodList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<FoodDTO> selectFoodList(String keyWord){
		ArrayList<FoodDTO>fList = new ArrayList<FoodDTO>();
		fList = foodService.selectFoodList(keyWord);
		return fList;

	};
	
	@RequestMapping(value = "insertMeal", method = RequestMethod.POST)
	public @ResponseBody String insertMeal(MealMenuDTO m, int quantity, int foodseq){
		boolean insertMealResult;
		MealMenuDTO mealmenu = new MealMenuDTO();
		mealmenu = foodService.selectMealMenu(m);
		MealDTO meal = new MealDTO(0,foodseq,mealmenu.getMealmenuseq(),quantity,null);
		insertMealResult = foodService.insertMeal(meal);
		if(insertMealResult){
			return "suc";
		}else{
			return "fail";
		}
	}
	
	@RequestMapping(value = "selectMeal", method = RequestMethod.GET)
	public @ResponseBody ArrayList<MealDTO>selectMeal(MealMenuDTO mealmenu){
		MealMenuDTO m = foodService.selectMealMenu(mealmenu);
		ArrayList<MealDTO>mList = new ArrayList<MealDTO>();
		mList = foodService.selectMeal(m);
		return mList;
	}
	
	@RequestMapping(value = "selectOneFood", method = RequestMethod.GET)
	public @ResponseBody FoodDTO selectOneFood(int foodseq){
		FoodDTO food = new FoodDTO();
		food = foodService.selectOneFood(foodseq);
		return food;
		
	}
	
	@RequestMapping(value = "updateMealMenu", method = RequestMethod.POST)
	public String updateMealMenu(MealMenuDTO mealmenu, Model model){
		ArrayList<MealMenuDTO> check = new ArrayList<MealMenuDTO>();
		check=foodService.selectOneMealMenuCheck(mealmenu);
		if(check.size()==1){
			MealMenuDTO m = new MealMenuDTO();
			m = foodService.selectMealMenu(mealmenu);
			m.setMemo(mealmenu.getMemo());
			
			boolean updateMealMenuResult;
			updateMealMenuResult = foodService.updateMealMenu(m);
			if(updateMealMenuResult){
				model.addAttribute("message","1000");
				return "Message";
			}
			return "home";
			
		}else{
			ArrayList<MealDTO>mList = new ArrayList<MealDTO>();
			ArrayList<MealDTO>resultList = new ArrayList<MealDTO>();
			for(int i = 0 ; i < check.size() ; i++){
				if(check.get(i).getChecked()==null){
					mList = foodService.selectMeal(check.get(i));
					for(int j = 0 ; j < mList.size() ; j++){
						resultList.add(mList.get(j));
					}
				}
			}
			
			for(int i = 0 ; i < check.size() ; i++){
				if(check.get(i).getChecked()!=null){
					for(int j = 0 ; j < resultList.size() ; j++){
						foodService.deleteMeal(resultList.get(j));
						resultList.get(j).setMealmenuseq(check.get(i).getMealmenuseq());
						foodService.insertMeal(resultList.get(j));			
					}
				}
			}
			
			for(int i = 0; i <check.size() ; i++){
				if(check.get(i).getChecked()==null){
					foodService.deleteMealMenu(check.get(i).getMealmenuseq());
				}	
			}
			model.addAttribute("message","10000");
			return "Message";
		}
	}
	
	@RequestMapping(value = "selectMealMenuList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<MealMenuDTO>selectMealMenuList(int memberseq){
		ArrayList<MealMenuDTO>mList = new ArrayList<MealMenuDTO>();
		mList = foodService.selectMealMenuList(memberseq);
		return mList;
	}
	
	@RequestMapping(value = "selectOneMealMenu", method = RequestMethod.GET)
	public @ResponseBody MealMenuDTO selectOneMealMenu(MealMenuDTO mealmenu){
		MealMenuDTO mealMenu = new MealMenuDTO();
		mealMenu = foodService.selectOneMealMenu(mealmenu);
		return mealMenu;
	}
	
	@RequestMapping(value = "selectMealList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<MealDTO>selectMealList(MealMenuDTO mealmenu){
		ArrayList<MealDTO>mList = new ArrayList<MealDTO>();
		mList = foodService.selectMeal(mealmenu);
		return mList;	
	}
	
	@RequestMapping(value = "selectFoodDBCount", method = RequestMethod.GET)
	public @ResponseBody int selectFoodDBCount(){
		int Count;
		Count = foodService.selectFoodDBCount();
		return Count;	
	}
}
