package global.sesoc.wod.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import global.sesoc.wod.dto.QnADTO;
import global.sesoc.wod.dto.QreplyDTO;
import global.sesoc.wod.dto.SearchDTO;
import global.sesoc.wod.service.QnAService;
import global.sesoc.wod.util.PageNavigator;

@Controller
public class QnAContorller {

	@Autowired
	QnAService qService;
	static int COUNTPERPAGE = 7;
	static int PAGEPERGROUP = 10;
	
	@RequestMapping(value = "/goQnA", method = RequestMethod.GET)
	public String goCService(String searchWord, Model model,@RequestParam(value="page", defaultValue="1")int currentPage) {
		
		PageNavigator navi = new PageNavigator(COUNTPERPAGE, PAGEPERGROUP,currentPage, qService.selectCount(searchWord));
        model.addAttribute("boardList", qService.viewQna(searchWord, navi));
        model.addAttribute("navi", navi);
        model.addAttribute("searchWord", searchWord);
		return "QnA/QnA2";
	}

	@RequestMapping(value= "/adminBoard", method = RequestMethod.GET)
	@ResponseBody
	public ArrayList<HashMap<String, Object>> adminBoard(QnADTO qna){
		ArrayList<HashMap<String, Object>> result = qService.getAdminBoard(qna);
		return result;
	}
	
	@RequestMapping(value = "/adminView", method=RequestMethod.GET)
	public String boardView(int qnaseq, Model model, HttpSession session){
		HashMap<String, Object> boardView = qService.adminView(qnaseq);
		boardView.put("COMMENTCOUNT", 0);
		boardView.put("NAME", "관리자");
		model.addAttribute("boardView", boardView);
		model.addAttribute("roleSeq", 1001);
		int userSeq = (Integer)session.getAttribute("loginSeq");
		model.addAttribute("userSeq", userSeq);
		return "QnA/QnAView";
	}
	
	@RequestMapping(value = "/qnaView" , method=RequestMethod.GET )
	public String boardView(QnADTO qna,  Model model, HttpSession session) {
		int userseq = (Integer)session.getAttribute("loginSeq");
		ArrayList<HashMap<String,String>> qreList = qService.viewQreply(qna);
		model.addAttribute("qreList", qreList);
		int commentCount = 0;
		if(qreList != null){
			for (int i = 0; i < qreList.size(); i++) {
				commentCount +=1;
			}
		}else{
			commentCount = 0;
		}
		HashMap<String, Object> boardView = qService.getContentView(qna);
		if (boardView.get("USERSEQ") != "1") {
			boardView.put("COMMENTCOUNT", commentCount);
		}
		
		model.addAttribute("boardView", boardView);
		model.addAttribute("userSeq", userseq);
		int roleSeq = (Integer)session.getAttribute("roleSeq");
		model.addAttribute("roleSeq", roleSeq);
		return "QnA/QnAView";

	}

	@RequestMapping(value = "/goQnARegister", method = RequestMethod.GET)
	public String goQnARegister(QnADTO qna){
		return "QnA/QnARegister2";
	}
	

	@RequestMapping(value="/QnAregister", method = RequestMethod.POST)
	@ResponseBody
	public String QnARegister(QnADTO qna, Model model,HttpSession session){
		int result = 0;	
		qna.setUserseq((Integer)session.getAttribute("loginSeq"));
		result = qService.qnaRegister(qna);
		if(result == 0){
			return  "no";
		}
		return "ok";
	}
	
	@RequestMapping(value = "/goUpdateQna", method = RequestMethod.GET)
	public String goUpdateQna(QnADTO qna,HttpSession session,Model model){
		int userSeq = (Integer)session.getAttribute("loginSeq");
		int qnaseq = qna.getQnaseq();
		if(userSeq == 1){
			HashMap<String,Object> boardView = qService.adminView(qnaseq);
			model.addAttribute("boardView", boardView);
		}else{
		HashMap<String, Object> boardView = qService.getContentView(qna);
		model.addAttribute("boardView",  boardView);
		}
		return "QnA/QnARegister2";
	}

	@RequestMapping(value = "/updateQna", method = RequestMethod.POST)
	@ResponseBody
	public String updateQna(QnADTO qna, HttpSession session){
		qna.setUserseq((Integer)session.getAttribute("loginSeq"));
		int result = 0;
		result = qService.updateQna(qna);
		if(result == 0){
			return "no";
		}else{
		return "ok";
		}
	}

	@RequestMapping(value = "/updateAdminQna", method = RequestMethod.POST)
	@ResponseBody
	public String updateAdminQna(QnADTO qna, HttpSession session){
		qna.setUserseq((Integer)session.getAttribute("loginSeq"));
		int result = 0;
		result = qService.updateAdminQna(qna);
		if(result == 0){
			return "no";
		}else{
		return "ok";
		}
	}

	
	@RequestMapping(value = "/deleteQna", method = RequestMethod.POST)
	@ResponseBody
	public String deleteQna(int qnaseq){
		int result1 = 0;
		QnADTO qna = new QnADTO();
		qna.setQnaseq(qnaseq);
		qService.deleteQnAReply(qnaseq);
		result1 =qService.deleteQna(qnaseq);
		if(result1 == 0){
			return "no";
		}
		return "ok";
	}
	
	@RequestMapping(value = "/searchQna", method = RequestMethod.GET)
	public List<QnADTO> search(QnADTO qna){
	return null;
	}

	@RequestMapping(value = "qreInsert", method = RequestMethod.POST)
	@ResponseBody
	public  String qreplyRegist(HttpSession session,int qnaseq, String qrecontent){
		QreplyDTO qre = new QreplyDTO();
		int userSeq = (Integer)session.getAttribute("loginSeq");
		String qreName = qService.chkCusName(userSeq);
		int result = 0;
		if(qreName != null){
			qre.setQreName(qreName);
		}else{
			qre.setQreName("관리자");
		}
		
		qre.setUserseq(userSeq);
		qre.setQnaseq(qnaseq);
		qre.setQrecontent(qrecontent);
		qre.setDepth(0);
		result = qService.qreplyRegist(qre);
		if(result == 0){
			return "no";
		}
		return "ok";
		
	}

	@RequestMapping(value = "updateQreply", method = RequestMethod.POST)
	@ResponseBody
	public String updateQreply(QreplyDTO qre, QnADTO qna, Model model, HttpSession session){
		qre.setUserseq((Integer)session.getAttribute("loginSeq"));
		int result = 0;
		result = qService.updateQreply(qre);
		if(result == 0){
			return "no";
		}else{
			return "ok";
		}

	}

	@RequestMapping(value = "deleteQreply", method=RequestMethod.POST)
	@ResponseBody
	public String deleteQreply(QreplyDTO qre, HttpSession session){
		qre.setUserseq((Integer )session.getAttribute("loginSeq"));
		int result = 0;
		result = qService.deleteQreply(qre);
		if (result == 0) {
			return "no";
		}
		return "success";
	}
	
	@RequestMapping(value = "reQreplyRegist" , method=RequestMethod.POST)
	@ResponseBody
	public String reQreplyRegist(QreplyDTO qre, HttpSession session){
		int userSeq = (Integer)session.getAttribute("loginSeq");
		qre.setDepth(qre.getQreplyseq());
		qre.setUserseq(userSeq);
		String qreName = qService.chkCusName(userSeq);
		int result = 0;
		if(qreName != null){
			qre.setQreName(qreName);
		}else{
			qre.setQreName("관리자");
		}
		result = qService.qreplyRegist(qre);
		if(result == 0){
			return "no";
		}else{		
		return "ok";
		}
	}
	
	@RequestMapping(value = "/qqList" , method=RequestMethod.GET )
	@ResponseBody
	public ArrayList<HashMap<String, Object>> qqList(int qnaseq) {
		ArrayList<HashMap<String, Object>> result = qService.qqList(qnaseq);
		return result;
	}
	
	@RequestMapping(value = "selectAllQnAList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<QnADTO>selectAllQnAList(int userseq){
		ArrayList<QnADTO>qList = new ArrayList<QnADTO>();
		qList = qService.selectAllQnAList(userseq);
		return qList;
	}
	
	@RequestMapping(value = "selectSearchQNAList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<QnADTO>selectSearchQNAList(int userseq, String searchVal){
		ArrayList<QnADTO>qList = new ArrayList<QnADTO>();
		SearchDTO search = new SearchDTO(searchVal,userseq);
		qList = qService.selectSearchQNAList(search);
		return qList;
	}
}
