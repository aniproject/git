package global.sesoc.wod.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import global.sesoc.wod.dto.CustomersDTO;
import global.sesoc.wod.dto.ExerciseDTO;
import global.sesoc.wod.dto.FileDTO;
import global.sesoc.wod.dto.MemberCommentDTO;
import global.sesoc.wod.dto.TrainingDTO;
import global.sesoc.wod.service.CustomersService;
import global.sesoc.wod.service.ExerciseService;
import global.sesoc.wod.service.FileService;
import global.sesoc.wod.service.MemberService;

@Controller
public class FileController {
	
	@Autowired
	FileService fService;
	
	@Autowired
	ExerciseService eService;
	
	@Autowired
	MemberService mService;
	@Autowired
	CustomersService customerService;

	@RequestMapping(value = "selectUserLessonList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<HashMap<String,String>>selectAllLessonList(int userseq){
			try
			{
			ArrayList<HashMap<String,String>>lList = new ArrayList<HashMap<String,String>>();
			CustomersDTO cDTO = customerService.selectOneCustomer(userseq);
			lList = fService.selectUserLessonList(cDTO.getCustomerseq());
			return lList;
			}catch(Exception e)
			{
				return null;
			}
		}
	
	@RequestMapping(value = "selectCurval", method = RequestMethod.POST)
	public @ResponseBody int selectCurval(){
		
		FileDTO result=fService.selectCurval();
		int num=result.getFilesseq();
		return num;
	}
	
	@RequestMapping(value = "updaterepresentive", method = RequestMethod.POST)
	public @ResponseBody String updaterepresentive(int lessonlistseq,HttpSession session){
		boolean result=fService.updaterepresentive(lessonlistseq);
		if(result==true)
			return "success";
		else
			return "fail";
	}
	
	@RequestMapping(value = "updateProfileFile", method = RequestMethod.POST)
	public @ResponseBody String updateProfileFile(int userseq,HttpSession session){
		boolean result=fService.updateProfileFile(userseq);
		if(result==true)
			return "success";
		else
			return "fail";
	}
	
	@RequestMapping(value = "insertFilesToChannel", method = RequestMethod.POST)
	public @ResponseBody String insertFilesToChannel(String sav, int channelseq,String oriname,String type){

		FileDTO file = new FileDTO();
		file.setSavename(sav);
		file.setOriname(oriname);
		file.setChannelseq(channelseq);
		file.setType(type);
		boolean flag;
		flag =  fService.insertFilesToChannel(file);
		if(flag){
			return sav;
		}else{
			return "fail";
		}
	}
	
	@RequestMapping(value = "moveinsertLesson", method = RequestMethod.GET)
	public String  moveinsertLesson(int channelseq ,Model model,HttpSession session){
		model.addAttribute("channelseq",channelseq);
		return "Trainer/insertLesson";			
	}
	
	@RequestMapping(value = "movebannerinsert", method = RequestMethod.GET)
	public String  movebannerinsert(int channelseq ,Model model,HttpSession session){
		model.addAttribute("channelseq",channelseq);
		return "Trainer/bannerinsert";			
	}
	
	@RequestMapping(value = "selectChannelFile", method = RequestMethod.GET)
	public @ResponseBody ArrayList<FileDTO> selectChannelFile(int channelseq){
		ArrayList<FileDTO> files=new ArrayList<FileDTO>();
		files = fService.selectChannelFile(channelseq);
		return files;	
	}
	
	@RequestMapping(value = "insertFiles", method = RequestMethod.POST)
	public @ResponseBody String insertFiles(String sav, int userseq,String oriname,String type){

		FileDTO file = new FileDTO();
		file.setSavename(sav);
		file.setOriname(oriname);
		file.setUserseq(userseq);
		file.setType(type);
		boolean flag;
		flag =  fService.insertFiles(file);
		if(flag){
			return sav;
		}else{
			return "fail";
		}
	}
	
	@RequestMapping(value = "selectResume", method = RequestMethod.GET)
	public @ResponseBody FileDTO selectResumeSavepath(int userseq){
		FileDTO resume = new FileDTO();
		resume = fService.selectResume(userseq);
		return resume;
	}
	
	@RequestMapping(value = "insertVideoFiles", method = RequestMethod.POST)
	public @ResponseBody String insertVideoFiles(String date, int memberseq,String oriname){
		ExerciseDTO ex = new ExerciseDTO();
		ExerciseDTO para = new ExerciseDTO();
		para.setMemberseq(memberseq);
		para.setExercisedate(date);
		ex = eService.selectExercise(para);
		TrainingDTO tr = new TrainingDTO();
		tr = eService.selectTraining(ex);
		String SAVENAME = date+memberseq+ex.getExerciseseq()+tr.getTrainingseq()+oriname;
		FileDTO file = new FileDTO();
		file.setSavename(SAVENAME);
		file.setOriname(oriname);
		file.setTrainingseq(tr.getTrainingseq());
		boolean insertVideo;
		insertVideo =  fService.insertVideo(file);
		if(insertVideo){
			return SAVENAME;
		}else{
			return "fail";
		}
	}
	
	@RequestMapping(value = "selectVideoFile", method = RequestMethod.GET)
	public @ResponseBody FileDTO selectVideoFile(int trainingseq){
		FileDTO video = new FileDTO();
		video = fService.selectVideoFile(trainingseq);
		return video;
	}
	
	@RequestMapping(value = "insertMemberVideo", method = RequestMethod.POST)
	public @ResponseBody String insertMemberVideo(MemberCommentDTO m,String oriname){
		MemberCommentDTO mComment = new MemberCommentDTO();
		mComment = mService.selectMemberComment(m);
		String SAVENAME = mComment.getMembercommentdate()+mComment.getTrainerseq()+mComment.getMembercommentseq()+m.getMemberseq()+oriname;
		FileDTO file = new FileDTO();
		file.setSavename(SAVENAME);
		file.setOriname(oriname);
		file.setMembercommentseq(mComment.getMembercommentseq());
		boolean insertVideo;
		insertVideo =  fService.insertMemberVideo(file);
		if(insertVideo){
			return SAVENAME;
		}else{
			return "fail";
		}
	}
	
	@RequestMapping(value = "selectMemberVideo", method = RequestMethod.GET)
	public @ResponseBody FileDTO selectMemberVideo(int membercommentseq){
		FileDTO video = new FileDTO();
		video = fService.selectMemberVideo(membercommentseq);
		return video;
	}
	
	@RequestMapping(value = "downloadResume", method = RequestMethod.GET)
	public String downloadResume(HttpServletResponse response, HttpServletRequest request){
		String FILEPATH = request.getSession().getServletContext().getRealPath("/resources/resume.pdf");
		response.setHeader("Content-Disposition", "attachment;filename=WodsGymResume.pdf");
		try {
			FileInputStream fis = new FileInputStream(new File(FILEPATH));
			ServletOutputStream sos = response.getOutputStream();
			FileCopyUtils.copy(fis, sos);
		} catch (IOException e) {	
		}
		return "Trainer/TrainerApply";
	}
}
