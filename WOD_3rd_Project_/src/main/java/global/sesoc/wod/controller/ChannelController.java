package global.sesoc.wod.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import global.sesoc.wod.dto.ChannelDTO;
import global.sesoc.wod.service.ChannelService;


@Controller
public class ChannelController {

	@Autowired
	ChannelService chService;
	
	
		@RequestMapping(value="selectChannel",method = RequestMethod.GET)
		public @ResponseBody ChannelDTO selectChannel(int trainerseq){
			ChannelDTO result = new ChannelDTO();
			result = chService.selectChannel(trainerseq);
			return result;
		}
		
}
