package global.sesoc.wod.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import global.sesoc.wod.dto.ReviewDTO;
import global.sesoc.wod.service.ChannelService;
import global.sesoc.wod.service.ReviewService;

@Controller
public class ReviewController {

   @Autowired
   ReviewService rService;
   
   @Autowired
   ChannelService chService;
   
   @RequestMapping(value="insertReview", method=RequestMethod.POST)
   public @ResponseBody String insertReview(ReviewDTO review, HttpSession session){
      
      int result = 0 ;
      review.setUserseq((Integer) session.getAttribute("loginSeq"));
      result = rService.insertReview(review);
      if(result == 1){
         return "success";
      }else{
         return "fail";
      }
   }
   
   @RequestMapping(value="selectReviewList", method=RequestMethod.GET)
   public @ResponseBody ArrayList<HashMap<String, Object>> selectReviewList(int trainerseq){
      ArrayList<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
      int channelseq = chService.selectChannel(trainerseq).getChannelseq();
      result = rService.selectReviewList(channelseq);
      return result;
   }
   
   @RequestMapping(value="deleteReview", method=RequestMethod.POST)
   public @ResponseBody String deleteReview(int reviewseq){
      int result = 0;
      result = rService.deleteReview(reviewseq);
      if(result == 1){
         return "success";
      }else{
         return "fail";
      }
   }
 
}