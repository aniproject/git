package global.sesoc.wod.controller;


import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import global.sesoc.wod.dto.CommentDTO;
import global.sesoc.wod.service.CommentService;

@Controller
public class CommentController {
	
	@Autowired
	CommentService coService;
	
	
	@RequestMapping(value="insertDetailLessonComment", method = RequestMethod.POST)
	public @ResponseBody String insertDetailLessonComment(CommentDTO comment){
		int result = 0;
		result = coService.insertDetailLessonComment(comment);
		if(result == 1){
			return "success";
		}else{
			return "fail";
		}
		
	}
	
	   @RequestMapping(value="selectCommentList", method = RequestMethod.GET)
	   public @ResponseBody ArrayList<HashMap<String,Object>> selectCommentList(CommentDTO comment){
	      ArrayList<HashMap<String, Object>> result = new ArrayList<HashMap<String,Object>>();
	      result = coService.selectCommentList(comment);
	      return result;
	   }
	   
	   
	   @RequestMapping(value="deleteComment", method = RequestMethod.POST)
	   public @ResponseBody String deleteComment(CommentDTO comment){
	      int result = 0;
	      result = coService.deleteComment(comment);
	      if(result == 1){
	         return "success";
	      }else{
	         return "fail";
	      }
	   }
}
