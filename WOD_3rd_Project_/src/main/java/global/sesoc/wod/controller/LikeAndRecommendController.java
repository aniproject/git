package global.sesoc.wod.controller;


import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import global.sesoc.wod.dto.LikeAndRecommendDTO;
import global.sesoc.wod.service.ChannelService;
import global.sesoc.wod.service.LikeAndRecommendService;

@Controller
public class LikeAndRecommendController {

   @Autowired
   LikeAndRecommendService larServer;
   
   @Autowired
   ChannelService chService;
   
   @RequestMapping(value="/choiceLike", method=RequestMethod.POST )
   public @ResponseBody String insertLike(int channelseq, HttpSession session){
      int result = 0;
      int chResult = 0;
      LikeAndRecommendDTO lar = new LikeAndRecommendDTO();
      LikeAndRecommendDTO checkLar = new LikeAndRecommendDTO(); 
      lar.setChannelseq(channelseq);
      lar.setUserseq((Integer) session.getAttribute("loginSeq"));
      checkLar = larServer.selectLike(lar);
      
      if(checkLar == null){
         result = larServer.insertLike(lar);
         chResult =  chService.plusLikeAndRecommend(channelseq);
         if(result == 1 && chResult == 1 ){
            return "insertSuccess";
         }else{
            return "insertFail";
         }
      }else{
         result = larServer.deleteLike(lar);
         chResult = chService.minusLikeAndRecommend(channelseq);
         if(result == 1 && chResult == 1){
            return "deleteSuccess";
         }else{
            return "deleteFail";
         }
      }
   }
   
   
   @RequestMapping(value="/selectLikeAndRecommendCount", method=RequestMethod.GET )
   public @ResponseBody LikeAndRecommendDTO selectLikeAndRecommendCount(int channelseq){
      LikeAndRecommendDTO result = new LikeAndRecommendDTO();
      result = larServer.selectLikeAndRecommendCount(channelseq);
      return result;
   }
   
   
   @RequestMapping(value = "selectLikeList", method = RequestMethod.GET)
   public @ResponseBody ArrayList<HashMap<String,String>>selectLikeList(int userseq){
	   ArrayList<HashMap<String,String>>lrList = new ArrayList<HashMap<String,String>>();
	   lrList = larServer.selectLikeList(userseq);
	   return lrList;
	 
   }
}