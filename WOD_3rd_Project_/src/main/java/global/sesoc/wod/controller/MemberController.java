package global.sesoc.wod.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import global.sesoc.wod.dto.CustomersDTO;
import global.sesoc.wod.dto.MemberCommentDTO;
import global.sesoc.wod.dto.TrainerDTO;
import global.sesoc.wod.service.CustomersService;
import global.sesoc.wod.service.MemberService;
import global.sesoc.wod.service.TrainerService;


@Controller
public class MemberController {

	@Autowired
	MemberService mService;
	
	@Autowired
	TrainerService tService;
	
	@Autowired
	CustomersService cService;
	
	
	
		@RequestMapping(value = "moveInsertMemberVideo", method = RequestMethod.GET)
		public String moveInsertMemberVideo(String val, int trainerseq, Model m, int memberseq){
			m.addAttribute("date",val);
			m.addAttribute("trainerseq",trainerseq);
			m.addAttribute("memberseq",memberseq);
			return "Trainer/insertMemberVideo";
			
		}
	
		@RequestMapping(value="selectTrainingCustomers", method = RequestMethod.GET)
		public @ResponseBody ArrayList<HashMap<String, String>> selectTrainingCustomers(HttpSession session){
			ArrayList<HashMap<String, String>>cList = new ArrayList<HashMap<String, String>>();
			int userseq = (Integer) session.getAttribute("loginSeq");
			TrainerDTO trainer = null;
			trainer = tService.selectOneTrainer(userseq); 
			int trainerseq = trainer.getTrainerseq();
			cList = mService.selectTrainingCustomers(trainerseq);
			return cList;
		}
		
		@RequestMapping(value = "selectCustomersByMemberSeq", method = RequestMethod.GET)
		public @ResponseBody CustomersDTO selectCustomersByMemberSeq(int memberseq){
			CustomersDTO cDTO = new CustomersDTO();
			cDTO = cService.selectCustomersByMemberSeq(memberseq);
			return cDTO;
		}
		
		@RequestMapping(value = "insertMemberComment", method = RequestMethod.POST)
		public @ResponseBody String insertMemberComment(MemberCommentDTO mComment){
			boolean insertResult;
			insertResult = mService.insertMemberComment(mComment);
			if(insertResult){
				return "suc";
			}else{
				return "fail";
			}
		}
		
		@RequestMapping(value = "selectMemberCommentList", method = RequestMethod.GET)
		public @ResponseBody ArrayList<MemberCommentDTO>selectMemberCommentList(MemberCommentDTO mc){
			ArrayList<MemberCommentDTO>mcList = new ArrayList<MemberCommentDTO>();
			mcList = mService.selectMemberCommentList(mc);
			return mcList;
		}
		
		@RequestMapping(value = "selectOneMemberComment", method = RequestMethod.GET)
		public @ResponseBody MemberCommentDTO selectOneMemberComment(int membercommentseq){
			MemberCommentDTO mc = new MemberCommentDTO();
			mc = mService.selectOneMemberComment(membercommentseq);
			return mc;	
		}
}
