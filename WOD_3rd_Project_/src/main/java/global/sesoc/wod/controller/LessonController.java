package global.sesoc.wod.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import global.sesoc.wod.dto.CustomersDTO;
import global.sesoc.wod.dto.LessonDTO;
import global.sesoc.wod.dto.MemberDTO;
import global.sesoc.wod.dto.SearchDTO;
import global.sesoc.wod.dto.TrainerDTO;
import global.sesoc.wod.service.ChannelService;
import global.sesoc.wod.service.CommentService;
import global.sesoc.wod.service.CustomersService;
import global.sesoc.wod.service.LessonService;
import global.sesoc.wod.service.MemberService;
import global.sesoc.wod.service.TrainerService;

@Controller
public class LessonController {

	@Autowired
	LessonService lService;
	
	@Autowired
	TrainerService tService;
	
	@Autowired
	ChannelService chService;
	
	@Autowired
	CustomersService customerService;
	
	@Autowired
	MemberService mService;
	
	@Autowired
	CommentService commentService;
	
	
	
	@RequestMapping(value="/insertLessonlistAndFile", method=RequestMethod.POST )
	public String insertLessonlistAndFile(LessonDTO lesson, HttpSession session,Model model){
		int result = 0;
		TrainerDTO trainer = tService.selectOneTrainer((Integer) session.getAttribute("loginSeq"));
		lesson.setLessoncheck(0);
		lesson.setTrainerseq(trainer.getTrainerseq());
		lesson.setChannelseq(chService.selectChannel(trainer.getTrainerseq()).getChannelseq());
		result = lService.insertTrainerNewLessonlist(lesson);
		if(result==0){
			model.addAttribute("message", "222");
			return "Message";
		}else{
			model.addAttribute("message", "333");
			return "Message";
		}	
	}
	
	@RequestMapping(value="selectOneTrainerLessonlist",method = RequestMethod.GET)
	   public @ResponseBody ArrayList<LessonDTO> selectOneTrainerLessonlist(LessonDTO lesson){
	      ArrayList<LessonDTO> result = new ArrayList<LessonDTO>();
	      ArrayList<LessonDTO> saveMemberseq = new ArrayList<LessonDTO>();
	   
	      saveMemberseq = lService.selectDeleteMemberseq();
	      lService.checkDeleteLessonlist();
	      for(int i = 0; i < saveMemberseq.size(); i++){
	         mService.deleteMember(saveMemberseq.get(i).getMemberseq());
	      }
	      result = lService.selectOneTrainerLessonlist(lesson);
	      return result;
	   }
	
	@RequestMapping(value="deleteLessonlist",method = RequestMethod.POST)
	   public @ResponseBody String deleteLessonlist(LessonDTO lesson){
	      int result = 0;
	      commentService.deletelessonComment(lesson.getLessonlistseq());
	      result = lService.deleteLessonlist(lesson);
	      if(result == 0){
	         return "fail";
	      }else{
	         return "success"; 
	      }
	   }
	
	@RequestMapping(value="selelctDetailLesson",method = RequestMethod.GET)
	public @ResponseBody ArrayList<LessonDTO> selelctDetailLesson(LessonDTO lesson){
		ArrayList<LessonDTO> result = new ArrayList<LessonDTO>();
		result = lService.selectOneTrainerLessonlist(lesson);
		return result;
	}
	
	@RequestMapping(value = "selectRecommend", method = RequestMethod.GET)
	public @ResponseBody ArrayList<HashMap<String,String>>selectRecommend(){
			ArrayList<HashMap<String,String>>lList = new ArrayList<HashMap<String,String>>();
			lList = lService.selectRecommend();
			return lList;
		}
	
	@RequestMapping(value = "selectAllLessonList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<HashMap<String,String>>selectAllLessonList(int userseq){
			ArrayList<HashMap<String,String>>lList = new ArrayList<HashMap<String,String>>();
			CustomersDTO cDTO = customerService.selectOneCustomer(userseq);
			lList = lService.selectAllLessonList(cDTO.getCustomerseq());
			return lList;
		}
	
	   @RequestMapping(value = "selectLessonlist", method = RequestMethod.GET)
	   public @ResponseBody ArrayList<LessonDTO> selectLessonlist(LessonDTO lesson){
	      ArrayList<LessonDTO> result = new ArrayList<LessonDTO>();
	      result = lService.selectLessonlistByLessontype(lesson);
	      return result; 
	   }
	   
	   @RequestMapping(value = "selectEqualLessonlist", method = RequestMethod.GET)
	   public @ResponseBody ArrayList<LessonDTO> selectEqualLessonlist(int lessonlistseq, int trainerseq, HttpSession session,String lessonname,String lessoncontent,int lessonprice){

	      MemberDTO member = new MemberDTO();
	      CustomersDTO customer = customerService.selectOneCustomer((Integer) session.getAttribute("loginSeq"));
	      member.setTrainerseq(trainerseq);
	      member.setCustomerseq(customer.getCustomerseq());
	      
	      ArrayList<MemberDTO>mList = mService.selectMember(member);
	      ArrayList<LessonDTO>lList = new ArrayList<LessonDTO>();
	   
	      for(int i = 0 ; i < mList.size(); i++){
	         LessonDTO lessonParameter = new LessonDTO();
	         lessonParameter.setMemberseq(mList.get(i).getMemberseq());
	         lessonParameter.setTrainerseq(mList.get(i).getTrainerseq());
	         lessonParameter.setLessonname(lessonname);
	         lessonParameter.setLessoncontent(lessoncontent);
	         lessonParameter.setLessoncheck(1);
	         lessonParameter.setLessonprice(lessonprice);
	         LessonDTO lesson = lService.selectEqualLessonlist(lessonParameter);
	         if(lesson!=null){
	            lList.add(lesson);
	         }
	      }
	      return lList;
	   }
	
	@RequestMapping(value = "selectSearchLessonList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<HashMap<String,String>>selectSearchLessonList(int userseq, String searchVal){
	
			ArrayList<HashMap<String,String>>lList = new ArrayList<HashMap<String,String>>();
			CustomersDTO cDTO = customerService.selectOneCustomer(userseq);
			SearchDTO saerch = new SearchDTO(searchVal,cDTO.getCustomerseq());
			lList = lService.selectSearchLessonList(saerch);

			return lList;
	}
}
