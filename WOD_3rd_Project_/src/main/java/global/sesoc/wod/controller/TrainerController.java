package global.sesoc.wod.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import global.sesoc.wod.dto.ApplyDTO;
import global.sesoc.wod.dto.CustomersDTO;
import global.sesoc.wod.dto.LessonDTO;
import global.sesoc.wod.dto.MemberDTO;
import global.sesoc.wod.dto.TrainerDTO;
import global.sesoc.wod.dto.User_BasicDTO;
import global.sesoc.wod.dto.User_RoleDTO;
import global.sesoc.wod.service.ChannelService;
import global.sesoc.wod.service.CustomersService;
import global.sesoc.wod.service.FileService;
import global.sesoc.wod.service.LessonService;
import global.sesoc.wod.service.MemberService;
import global.sesoc.wod.service.TrainerService;
import global.sesoc.wod.service.UserService;

@Controller
public class TrainerController {
	
	@Autowired
	TrainerService tService;
	
	@Autowired
	CustomersService cService;
	
	@Autowired
	UserService uService;
	
	@Autowired
	FileService fService;
	
	@Autowired
	ChannelService channelService;
	
	@Autowired
	MemberService mService;
	
	@Autowired
	LessonService lService;
	

	
	@RequestMapping(value = "selectRankTrainer", method = RequestMethod.GET)
	public @ResponseBody ArrayList<HashMap<String,String>>selectRankTrainer(){
		try
		{
		ArrayList<HashMap<String,String>>lList = new ArrayList<HashMap<String,String>>();
	
		lList = tService.selectRankTrainer();
		
		return lList;
		}catch(Exception e)
		{
			return null;
		}
		}
	
	@RequestMapping(value = "moveTrainerChannel", method = RequestMethod.GET)
	   public String moveTrainerChannel(int trainerSeq, Model m, HttpSession session) {
	      m.addAttribute("trainerSeq", trainerSeq);
	      int channelseq = channelService.selectChannel(trainerSeq).getChannelseq();
	      TrainerDTO tr = tService.selectTrainerAll(trainerSeq);
	      m.addAttribute("channelseq", channelseq);
	      m.addAttribute("tUserseq",tr.getUserseq());

	      if ((Integer) session.getAttribute("roleSeq") == 1001) {
	         TrainerDTO trainer = selectOneTrainer((Integer) session.getAttribute("loginSeq"));

	         if (trainer.getTrainerseq() == trainerSeq) {
	            m.addAttribute("isTrainer", 1);
	            return "Trainer/TrainerChannel";
	         } else {
	            m.addAttribute("isTrainer", 0);
	            return "Trainer/TrainerChannel";
	         }
	      } else {
	         m.addAttribute("isTrainer", 0);
	         return "Trainer/TrainerChannel";
	      }
	   }
	
	   @RequestMapping(value = "moveTrainerChannel2", method = RequestMethod.GET)
	   public String moveTrainerMyChannel(HttpSession session, Model m) {
	      TrainerDTO trainer = tService.selectOneTrainer((Integer) session.getAttribute("loginSeq"));
	      int trainerSeq =trainer.getTrainerseq();
	      m.addAttribute("trainerSeq", trainerSeq);
	      int channelseq = channelService.selectChannel(trainerSeq).getChannelseq();
	      TrainerDTO tr = tService.selectTrainerAll(trainerSeq);
	      m.addAttribute("channelseq", channelseq);
	      m.addAttribute("tUserseq",tr.getUserseq());

	      if ((Integer) session.getAttribute("roleSeq") == 1001) {
	         if (trainer.getTrainerseq() == trainerSeq) {
	            m.addAttribute("isTrainer", 1);
	            return "Trainer/TrainerChannel";
	         } else {
	            m.addAttribute("isTrainer", 0);
	            return "Trainer/TrainerChannel";
	         }
	      } else {
	         m.addAttribute("isTrainer", 0);
	         return "Trainer/TrainerChannel";
	      }
	   }
	
	
	@RequestMapping(value="moveApply", method = RequestMethod.GET)
	public String moveApply(Model model,HttpSession session) {
		int userseq = (Integer) session.getAttribute("loginSeq");
		
			User_RoleDTO uRole = new User_RoleDTO();
			uRole = uService.selectRoleseq(userseq);
			if(uRole.getRoleseq()==101||uRole.getRoleseq()==10001){
				return "Trainer/TrainerApply";
			}else{
				return "home";
			}
	}
	
	@RequestMapping(value = "moveInsertTrainingFoodForm", method = RequestMethod.GET)
	public String moveInsertTrainingFoodForm(String val, Model m, int memberseq){
		m.addAttribute("date",val);
		m.addAttribute("memberseq",memberseq);
		return "Trainer/insertTrainingFoodForm";
	}
	
	@RequestMapping(value = "moveViewFood", method = RequestMethod.GET)
	public String moveViewTrainingFood(String val, Model m){
		m.addAttribute("mealmenuseq",val);
		return "Trainer/viewFood";
	}
	
	@RequestMapping(value = "moveViewTraining", method = RequestMethod.GET)
	public String moveViewTraining(String val, Model m){
		m.addAttribute("exerciseseq",val);
		return "Trainer/viewTraining";
	}
	
	@RequestMapping(value = "moveViewMemberComment", method = RequestMethod.GET)
	public String moveViewMemberComment(String val, Model m){
		m.addAttribute("membercommentseq",val);
		return "Trainer/viewMemberComment";
	}
	
	@RequestMapping(value="selectTrainer", method=RequestMethod.GET)
	public @ResponseBody ArrayList<HashMap<String,String>>selectTrainer(String selectValue, Long Paging,Model m){

		ArrayList<HashMap<String,String>> tList = null;	
		ArrayList<HashMap<String,String>> result = new ArrayList<HashMap<String,String>>();
		tList = tService.selectTrainer(selectValue);
		if(Paging!=null){
			for(int i = 0 ; i < tList.size() ; i++){
				if (i > Paging-4) {
					if(i <= Paging){
						result.add(tList.get(i));
					}
				}
			}
			return result;
		}
		return tList;

	}
	
	
	@RequestMapping(value="TrainerApply", method=RequestMethod.POST)
	 public String TrainerApply(HttpSession session, String lessonType)
	 {
	         boolean insertApplyResult=false;
	         boolean updateUserRoleResult=false;
	         int userseq=(Integer) session.getAttribute("loginSeq");
	         ApplyDTO apply = new ApplyDTO();
	         apply.setApplytype(lessonType);
	         apply.setUserseq(userseq);
	         insertApplyResult = tService.insertApply(apply);
	         User_RoleDTO uRole = new User_RoleDTO();
	         uRole = uService.selectRoleseq(userseq);
	         updateUserRoleResult = uService.updateUserRole(uRole);
	         if(insertApplyResult==false || updateUserRoleResult==false)         
	            return "redirect:/moveApply";
	         else
	            return "home";
	            
	   } 
	
	
	@RequestMapping(value="grantTrainer", method = RequestMethod.POST)
	public @ResponseBody String grantTrainer(int userseq){
		HashMap<String,String> customer = new HashMap<String,String>();

		customer = cService.selectOneCustomers(userseq);
		boolean insertTrainerResult;
		insertTrainerResult = tService.insertTrainer(customer, null);
		if(insertTrainerResult){
			User_RoleDTO uRole = new User_RoleDTO();
			uRole = uService.selectRoleseq(userseq);
			boolean updateUserRoleResult;
			updateUserRoleResult = uService.updateUserRole(uRole);
			
			if(updateUserRoleResult){
				boolean deleteApplyResult;
				deleteApplyResult = tService.deleteApply(userseq);
				if(deleteApplyResult){
					boolean deleteCustomersResult;
					deleteCustomersResult = cService.deleteCustomers(userseq);
					
					if(deleteCustomersResult){
						boolean insertChannelResult;
						TrainerDTO trainer = tService.selectOneTrainer(userseq);
						insertChannelResult = channelService.insertChannel(trainer);
						fService.updateProfileFile(userseq);
						if(insertChannelResult){
							return "suc";
						}else{
							return "fail";
						}
					}else{
						return "fail";
					}
				}else{
					return "fail";
				}
			}else{
				return "fail";
			}
		}else{
			return "fail";
		}
	}
	
	
	@RequestMapping(value="rejectTrainer",method = RequestMethod.POST)
	public @ResponseBody String rejectTrainer(int userseq){
		User_RoleDTO uRole = new User_RoleDTO();
		uRole = uService.selectRoleseq(userseq);
		boolean rejectTrainerResult;
		rejectTrainerResult = uService.rollbackUserRole(uRole);
		boolean deleteApplyResult;
		deleteApplyResult = tService.deleteApply(userseq);
		if(rejectTrainerResult&&deleteApplyResult){
			return "suc";
		}else{
			return "fail";
		}
	}
	
	
	@RequestMapping(value="selectTrainerAndChannel",method = RequestMethod.GET)
	public @ResponseBody ArrayList<HashMap<String,String>> selectTrainerAndChannel(int trainerseq){
		ArrayList<HashMap<String,String>> result = new ArrayList<HashMap<String,String>>();
		result = tService.selectTrainerAndChannel(trainerseq);
		return result;
		
	}
	
	
	@RequestMapping(value="selectOneTrainer",method = RequestMethod.GET)
	public @ResponseBody TrainerDTO selectOneTrainer(int userseq){
		TrainerDTO result = new TrainerDTO();
		result = tService.selectOneTrainer(userseq);
		return result;
	}
	
	@RequestMapping(value="updateTrainerComment",method = RequestMethod.POST)
	public @ResponseBody String updateTrainerComment(TrainerDTO trainer){
		int result = 0;
		result = tService.updateTrainerComment(trainer);
		if(result==0){
			return "fail";
		}else{
			return "success";
		}		
	}
	
	@RequestMapping(value="paymentLesson",method = RequestMethod.POST)
	public @ResponseBody String paymentLesson(int lessonlistseq, int trainerseq, HttpSession session){
		MemberDTO member = new MemberDTO();
		CustomersDTO customer = cService.selectOneCustomer((Integer) session.getAttribute("loginSeq"));
		member.setCustomerseq(customer.getCustomerseq());
		member.setTrainerseq(trainerseq);
		mService.insertMember(member);
		int lastMemberseq = mService.selectLastMemberseq();
		
		LessonDTO lessonResult = lService.selectOneLesson(lessonlistseq);
		lessonResult.setLessoncheck(1);
		lessonResult.setMemberseq(lastMemberseq);
		int result = lService.insertTrainerNewLessonlist(lessonResult);
		if(result == 0){
			return "fail";
		}else{
			return "success";
		}
	}
	
	@RequestMapping(value="moveCalendar",method = RequestMethod.GET)
	public String moveCalendar(int memberseq, Model model,HttpSession session, int trainerSeq){
	
		int roleSeq = (Integer) session.getAttribute("roleSeq");
		int userSeq = (Integer) session.getAttribute("loginSeq");
		User_BasicDTO user = new User_BasicDTO();
		user = uService.selectUserByMemberseq(memberseq);
		TrainerDTO tr = tService.selectOneTrainer(trainerSeq);
		
		if(roleSeq==1001&&trainerSeq==0){
			TrainerDTO t = new TrainerDTO();
			int seq = (Integer) session.getAttribute("loginSeq");
			t = tService.selectOneTrainer(seq);
			model.addAttribute("MyTraUserseq",userSeq);
			model.addAttribute("reverseUserseq",user.getUserseq());
			model.addAttribute("reverseTrainerUserseq",t.getTrainerseq());
			model.addAttribute("memberseq", memberseq);
			model.addAttribute("trainerCheck",roleSeq);
			return "calendar";
			
		}else{
			model.addAttribute("MycusUserseq",userSeq);
			model.addAttribute("reverseTrainerUserseq",tr.getTrainerseq());
			model.addAttribute("memberseq", memberseq);
			return "calendar";	
		}
	}
	
	@RequestMapping(value = "selectTrainerMemberCountList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<TrainerDTO> trainerMemberCountList(){
		ArrayList<HashMap<String,String>>Count = new ArrayList<HashMap<String,String>>();
		ArrayList<TrainerDTO>tList = new ArrayList<TrainerDTO>();
		Count = tService.selectTrainerMemberCount();
		
		for(int i = 0 ; i < Count.size() ; i++){
			String trseq = String.valueOf(Count.get(i).get("TRAINERSEQ"));
			int trainerseq = Integer.parseInt(trseq);
			TrainerDTO trainer = tService.selectTrainerAll(trainerseq);
			String cntVAL = String.valueOf(Count.get(i).get("COUNT"));
			int cnt = Integer.parseInt(cntVAL);
			trainer.setCount(cnt);
			tList.add(trainer);
		};
		return tList;
	
	}
	
	@RequestMapping(value = "selectTrainerLessonCountList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<TrainerDTO> selectTrainerLessonCountList(){
		ArrayList<HashMap<String,String>>Count = new ArrayList<HashMap<String,String>>();
		ArrayList<TrainerDTO>tList = new ArrayList<TrainerDTO>();
		Count = tService.selectTrainerLessonCount();
		
		for(int i = 0 ; i < Count.size() ; i++){
			String trseq = String.valueOf(Count.get(i).get("TRAINERSEQ"));
			int trainerseq = Integer.parseInt(trseq);
			TrainerDTO trainer = tService.selectTrainerAll(trainerseq);
			String cntVAL = String.valueOf(Count.get(i).get("COUNT"));
			int cnt = Integer.parseInt(cntVAL);
			trainer.setCount(cnt);
			tList.add(trainer);
		};
		return tList;
	}
}
