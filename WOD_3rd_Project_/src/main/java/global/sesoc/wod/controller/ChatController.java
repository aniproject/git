package global.sesoc.wod.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import global.sesoc.wod.dto.ChatDTO;
import global.sesoc.wod.dto.ChatRoomDTO;
import global.sesoc.wod.dto.CustomersDTO;
import global.sesoc.wod.dto.TrainerDTO;
import global.sesoc.wod.dto.User_RoleDTO;
import global.sesoc.wod.service.ChatService;
import global.sesoc.wod.service.CustomersService;
import global.sesoc.wod.service.TrainerService;
import global.sesoc.wod.service.UserService;

@Controller
public class ChatController {
	
	@Autowired
	ChatService chatService;
	
	@Autowired
	CustomersService customerService;
	
	@Autowired
	TrainerService trainerService;
	
	@Autowired
	UserService uService;
	
	
	
	@RequestMapping(value = "insertChat", method = RequestMethod.POST)
	public @ResponseBody String insertChat(ChatDTO chat){
		ChatDTO reverse = new ChatDTO();
		ChatRoomDTO chatroomCheck = new ChatRoomDTO();
		chatroomCheck = chatService.selectChatRoom(chat); 
		reverse.setSendseq(chat.getReceiveseq());
		reverse.setReceiveseq(chat.getSendseq());
		ChatRoomDTO chatroomCheck2 = new ChatRoomDTO();
		chatroomCheck2 = chatService.selectChatRoom(reverse); 
		if(chatroomCheck==null&&chatroomCheck2==null){
			boolean insertChatRoomResult;
			CustomersDTO customer = customerService.selectOneCustomer(chat.getSendseq());
			TrainerDTO trainer = trainerService.selectOneTrainer(chat.getSendseq());
			if(customer!=null&&trainer==null){
				chat.setName(customer.getName());
			}else if(customer==null&&trainer!=null){
				chat.setName(trainer.getName());
			}else{
				chat.setName("관리자");
			}
			insertChatRoomResult = chatService.insertChatRoom(chat);
			if(insertChatRoomResult){
				ChatRoomDTO cRoom = new ChatRoomDTO();
				cRoom = chatService.selectChatRoom(chat);
				boolean insertChatResult;
				chat.setChatroomseq(cRoom.getChatroomseq());
				insertChatResult = chatService.insertChat(chat);
				if(insertChatResult){
					return "suc";
				}else{
					return "fail";
				}
			}else{
				return "fail";
			}
		}else{
			CustomersDTO customer = customerService.selectOneCustomer(chat.getSendseq());
			TrainerDTO trainer = trainerService.selectOneTrainer(chat.getSendseq());
			if(customer!=null&&trainer==null){
				chat.setName(customer.getName());
			}else if(customer==null&&trainer!=null){
				chat.setName(trainer.getName());
			}else{
				chat.setName("관리자");
			}
			ChatRoomDTO cRoom = new ChatRoomDTO();
			ChatRoomDTO cRoom2 = new ChatRoomDTO();
			ChatDTO check = new ChatDTO();
			check.setSendseq(chat.getReceiveseq());
			check.setReceiveseq(chat.getSendseq());
			cRoom = chatService.selectChatRoom(chat);
			cRoom2 = chatService.selectChatRoom(check);
			boolean insertChatResult;
			if(cRoom!=null&&cRoom2==null){
				chat.setChatroomseq(cRoom.getChatroomseq());
				chatService.updateRecentDate(cRoom.getChatroomseq());
			}else if(cRoom==null&&cRoom2!=null){
				chat.setChatroomseq(cRoom2.getChatroomseq());
				chatService.updateRecentDate(cRoom2.getChatroomseq());
			}
			insertChatResult = chatService.insertChat(chat);
			if(insertChatResult){
				return "suc";
			}else{
				return "fail";
			}
			}
	}
	
	@RequestMapping(value = "selectChatList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<ChatDTO>selectChatList(ChatDTO chat){
		ChatRoomDTO chatroomCheck = new ChatRoomDTO();
		chatroomCheck = chatService.selectChatRoom(chat); 
		if(chatroomCheck==null){
			ChatDTO reverse = new ChatDTO();
			reverse.setSendseq(chat.getReceiveseq());
			reverse.setReceiveseq(chat.getSendseq());
			ChatRoomDTO chatroomCheck2 = new ChatRoomDTO();
			chatroomCheck2 = chatService.selectChatRoom(reverse); 
			if(chatroomCheck2==null){
				return null;
			}else{
				reverse.setChatroomseq(chatroomCheck2.getChatroomseq());
				ArrayList<ChatDTO>cList = new ArrayList<ChatDTO>();
				cList = chatService.selectChatList(reverse);
				return cList;
			}
		}else{
			chat.setChatroomseq(chatroomCheck.getChatroomseq());
			ArrayList<ChatDTO>cList = new ArrayList<ChatDTO>();
			cList = chatService.selectChatList(chat);
			return cList;
		}
	}
	
	
	
	@RequestMapping(value = "selectAdminChatRoomList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<ChatRoomDTO>selectAdminChatRoomList(ChatRoomDTO cRoom, Long Paging){
		ArrayList<ChatRoomDTO>cList = new ArrayList<ChatRoomDTO>();
		cList = chatService.selectChatRoomList(cRoom);
		for(int i = 0 ; i < cList.size(); i++){
			User_RoleDTO uRole = uService.selectRoleseq(cList.get(i).getUserseq1());
			switch(uRole.getRoleseq()){
			case 101: 
				CustomersDTO customer = customerService.selectOneCustomer(cList.get(i).getUserseq1());
				cList.get(i).setOthersname(customer.getName());
				break;
			case 1000:
				CustomersDTO cus = customerService.selectOneCustomer(cList.get(i).getUserseq1());
				cList.get(i).setOthersname(cus.getName());
				break;
			case 1001:
				TrainerDTO trainer = trainerService.selectOneTrainer(cList.get(i).getUserseq1());
				cList.get(i).setOthersname(trainer.getName());
				break;
			default:
				User_RoleDTO uRole2 = uService.selectRoleseq(cList.get(i).getUserseq2());
				switch(uRole2.getRoleseq()){
				case 101:
					CustomersDTO customer2 = customerService.selectOneCustomer(cList.get(i).getUserseq2());
					cList.get(i).setOthersname(customer2.getName());
					break;
				case 1000:
					CustomersDTO cus2 = customerService.selectOneCustomer(cList.get(i).getUserseq2());
					cList.get(i).setOthersname(cus2.getName());
					break;
				case 1001:
					TrainerDTO trainer2 = trainerService.selectOneTrainer(cList.get(i).getUserseq2());
					cList.get(i).setOthersname(trainer2.getName());
					break;
				default:
					cList.get(i).setOthersname("탈퇴한 회원");
					break;
				}
			}
		}
		ArrayList<ChatRoomDTO>PagingcList = new ArrayList<ChatRoomDTO>();
		if(Paging!=null){
			for(int i = 0 ; i < cList.size() ; i++){
				if (i > Paging-9) {
					if(i <= Paging){
						PagingcList.add(cList.get(i));
					}
				}
			}
			return PagingcList;
		}
		return cList;
		
	}
	
	@RequestMapping(value = "selectTrainerChatRoomList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<ChatRoomDTO>selectTrainerChatRoomList(ChatRoomDTO cRoom){
		ArrayList<ChatRoomDTO>cList = new ArrayList<ChatRoomDTO>();
		cList = chatService.selectChatRoomList(cRoom);
		for(int i = 0 ; i < cList.size(); i++){
			User_RoleDTO uRole = uService.selectRoleseq(cList.get(i).getUserseq1());
			switch(uRole.getRoleseq()){
			case 101: 
				CustomersDTO customer = customerService.selectOneCustomer(cList.get(i).getUserseq1());
				cList.get(i).setOthersname(customer.getName());
				break;
			case 1000:
				CustomersDTO cus = customerService.selectOneCustomer(cList.get(i).getUserseq1());
				cList.get(i).setOthersname(cus.getName());
				break;
			case 10001:
				cList.get(i).setOthersname("관리자");
				break;
			default:
				User_RoleDTO uRole2 = uService.selectRoleseq(cList.get(i).getUserseq2());
				switch(uRole2.getRoleseq()){
				case 101:
					CustomersDTO customer2 = customerService.selectOneCustomer(cList.get(i).getUserseq2());
					cList.get(i).setOthersname(customer2.getName());
					break;
				case 1000:
					CustomersDTO cus2 = customerService.selectOneCustomer(cList.get(i).getUserseq2());
					cList.get(i).setOthersname(cus2.getName());
					break;
				case 10001:
					cList.get(i).setOthersname("관리자");
					break;
				default:
					cList.get(i).setOthersname("탈퇴한 회원");
					break;
				}
			}
		}
		return cList;
		
	}
	
	
	@RequestMapping(value = "selectCustomersChatRoomList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<ChatRoomDTO>selectCustomersChatRoomList(ChatRoomDTO cRoom){
		ArrayList<ChatRoomDTO>cList = new ArrayList<ChatRoomDTO>();
		cList = chatService.selectChatRoomList(cRoom);
		for(int i = 0 ; i < cList.size(); i++){
			User_RoleDTO uRole = uService.selectRoleseq(cList.get(i).getUserseq1());
			switch(uRole.getRoleseq()){
			case 1001:
				TrainerDTO trainer = trainerService.selectOneTrainer(cList.get(i).getUserseq1());
				cList.get(i).setOthersname(trainer.getName());
				break;
			case 10001:
				cList.get(i).setOthersname("관리자");
				break;
			default:
				User_RoleDTO uRole2 = uService.selectRoleseq(cList.get(i).getUserseq2());
				switch(uRole2.getRoleseq()){
				case 1001:
					TrainerDTO trainer2 = trainerService.selectOneTrainer(cList.get(i).getUserseq2());
					cList.get(i).setOthersname(trainer2.getName());
					break;
				case 10001:
					cList.get(i).setOthersname("관리자");
					break;
				default:
					cList.get(i).setOthersname("탈퇴한 회원");
					break;
				}
			}
		}
		return cList;
	}
}
