package global.sesoc.wod.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import global.sesoc.wod.dto.ExerciseDTO;
import global.sesoc.wod.dto.FileDTO;
import global.sesoc.wod.dto.TrainingDTO;
import global.sesoc.wod.service.ExerciseService;
import global.sesoc.wod.service.FileService;

@Controller
public class ExerciseController {
	
	@Autowired
	ExerciseService eService;
	
	@Autowired
	FileService fService;
	
	
	@RequestMapping(value = "insertExercise", method = RequestMethod.POST)
	public @ResponseBody String insertExercise(ExerciseDTO ex){
		
		boolean insertResult;
		insertResult = eService.insertExercise(ex);
		if(insertResult){
			return "suc";
		}else{
			return "fail";
		}
		
		
		
	}
	
	@RequestMapping(value = "insertTraining", method = RequestMethod.POST)
	public String insertTraining(ExerciseDTO ex, TrainingDTO tr){
		boolean insertTrainingResult;
		ExerciseDTO exercise = new ExerciseDTO();
		exercise = eService.selectExercise(ex);
		tr.setExerciseseq(exercise.getExerciseseq());
		insertTrainingResult = eService.insertTraining(tr);
		if(insertTrainingResult){
			return "suc";
			}else{
				return "fail";
				}
		}
	
	@RequestMapping(value = "selectTrainingList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<TrainingDTO>selectTrainingList(ExerciseDTO ex){
		if(ex.getExercisedate()==null){
			ArrayList<TrainingDTO>tList = new ArrayList<TrainingDTO>();
			tList = eService.selectTrainingList(ex);
			return tList;
			
		}else{
			ExerciseDTO exercise = eService.selectExercise(ex);
			ArrayList<TrainingDTO>tList = new ArrayList<TrainingDTO>();
			tList = eService.selectTrainingList(exercise);
			return tList;
				
			
		}
	
		
		
	}
	
	@RequestMapping(value = "updateExercise", method = RequestMethod.POST)
	public String updateExercise(ExerciseDTO exercise, Model model){
		
		ArrayList<ExerciseDTO> check = new ArrayList<ExerciseDTO>();
		check=eService.selectExerciseCheck(exercise);
	
		if(check.size()==1){
			ExerciseDTO ex = new ExerciseDTO();
			ex = eService.selectExercise(exercise);
			ex.setMemo(exercise.getMemo());
	
			boolean updateExerciseResult;
			updateExerciseResult = eService.updateExercise(ex);
			if(updateExerciseResult){
				model.addAttribute("message","2000");
				return "Message";
			}
			return "home";
			
		}else{
			ArrayList<TrainingDTO>tList = new ArrayList<TrainingDTO>();
			ArrayList<TrainingDTO>resultList = new ArrayList<TrainingDTO>();
			for(int i = 0 ; i < check.size() ; i++){
				if(check.get(i).getChecked()==null){
					tList = eService.selectTrainingList(check.get(i));
					for(int j = 0 ; j < tList.size() ; j++){
						resultList.add(tList.get(j));
					}
				}
			}
			
			for(int i = 0 ; i < check.size() ; i++){
				if(check.get(i).getChecked()!=null){
					for(int j = 0 ; j < resultList.size() ; j++){
						FileDTO video = new FileDTO();
						video = fService.selectVideoFile(resultList.get(j).getTrainingseq());
						fService.deleteVideo(resultList.get(j));
						eService.deleteTraining(resultList.get(j));
						resultList.get(j).setExerciseseq(check.get(i).getExerciseseq());
						eService.insertTraining(resultList.get(j));
						TrainingDTO tr = new TrainingDTO();
						ExerciseDTO ex = new ExerciseDTO();
						tr=eService.selectTraining(ex);
						video.setTrainingseq(tr.getTrainingseq());
						fService.insertVideo(video);
					}
				}
			}
			for(int i = 0; i <check.size() ; i++){
				if(check.get(i).getChecked()==null){
					eService.deleteExercise(check.get(i).getExerciseseq());
				}
			}
			model.addAttribute("message","20000");
			return "Message";
		}
	}
	
	
	
	@RequestMapping(value = "selectExerciseList", method = RequestMethod.GET)
	public @ResponseBody ArrayList<ExerciseDTO>selectExerciseList(int memberseq){
		ArrayList<ExerciseDTO>eList = new ArrayList<ExerciseDTO>();
		eList = eService.selectExerciseList(memberseq);
		return eList;
	}
	
	@RequestMapping(value = "selectOneExercise", method = RequestMethod.GET)
	public @ResponseBody ExerciseDTO selectOneExercise(ExerciseDTO ex){
		ExerciseDTO exer = new ExerciseDTO();
		exer = eService.selectOneExercise(ex);
		return exer;
	}
}
