<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>WOD'sGYM</title>
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- REVOLUTION SLIDER -->
		<link href="resources/assets/plugins/slider.revolution.v5/css/pack.css" rel="stylesheet" type="text/css" />

		<!-- THEME CSS -->
		<link href="resources/assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/layout.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/thematics-restaurant.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="resources/assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
	</head>

	<body class="smoothscroll enable-animation">

		<!-- SLIDE TOP -->
		<div id="slidetop">

			<div class="container">
				
				<div class="row">

					<div class="col-md-4">
						<h6><i class="icon-heart"></i> WHY SMARTY?</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque. Ut enim massa, sodales tempor convallis et, iaculis ac massa. </p>
					</div>

					<div class="col-md-4">
						<h6><i class="fa-facheck"></i> RECENTLY VISITED</h6>
						<ul class="list-unstyled">
							<li><a href="#"><i class="fa fa-angle-right"></i> Consectetur adipiscing elit amet</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> This is a very long text, very very very very very very very very very very very very </a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Lorem ipsum dolor sit amet</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Dolor sit amet,consectetur adipiscing elit amet</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Consectetur adipiscing elit amet,consectetur adipiscing elit</a></li>
						</ul>
					</div>

					<div class="col-md-4">
						<h6><i class="icon-envelope"></i> CONTACT INFO</h6>
						<ul class="list-unstyled">
							<li><b>Address:</b> PO Box 21132, Here Weare St, <br /> Melbourne, Vivas 2355 Australia</li>
							<li><b>Phone:</b> 1-800-565-2390</li>
							<li><b>Email:</b> <a href="mailto:support@yourname.com">support@yourname.com</a></li>
						</ul>
					</div>

				</div>

			</div>

			<a class="slidetop-toggle" href="#"><!-- toggle button --></a>

		</div>
		<!-- /SLIDE TOP -->


		<!-- wrapper -->
		<div id="wrapper">

			

			<section>
				<div class="container">
					
						<!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
						<div id="rev_slider_60_1"  style="display:none;" data-version="5.0.7">
							<ul>
								<!-- SLIDE  -->
								<li data-index="rs-137221490" data-slotamount="7"  data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500"  data-thumb="https://i.vimeocdn.com/video/532468261_640.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Ishtar X Tussilago" data-param1="25/08/2015" data-description="">
									<!-- MAIN IMAGE -->
									<img src="https://i.vimeocdn.com/video/532468261_640.jpg"  alt=""  data-bgposition="center center" data-bgfit="100% 0%" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
									<!-- LAYERS -->

									<!-- LAYER NR. 1 -->
									<div class="tp-caption   tp-resizeme tp-videolayer" 
										id="slide-137221490-layer-1" 
										data-x="" 
										data-y="" 
										data-width="['auto']"
										data-height="['auto']"
										data-transform_idle="o:1;"
							 
										data-transform_in="opacity:0;s:700;e:Power1.easeInOut;" 
										data-transform_out="opacity:0;s:700;e:Power1.easeInOut;s:700;e:Power1.easeInOut;" 
										data-start="1000" 
										data-responsive_offset="on" 

										data-vimeoid="137221490" data-videoattributes="title=0&amp;byline=0&amp;portrait=0&amp;api=1" data-videowidth="1230" data-videoheight="692" data-videoloop="none"			data-autoplay="on" 
										data-nextslideatend="true" 
										data-volume="100" data-forcerewind="on"
										style="z-index: 5;"> 
									</div>
								</li>
				
							</ul>
						
						</div>
					</div><!-- END REVOLUTION SLIDER -->
		
			</section>



		

		</div>
		<!-- /wrapper -->


		<!-- SCROLL TO TOP -->
		<a href="#" id="toTop"></a>


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->


		<!-- JAVASCRIPT FILES -->
		<script>var plugin_path = 'resources/assets/plugins/';</script>
		<script src="resources/assets/plugins/jquery/jquery-3.3.1.min.js"></script>

		<script src="resources/assets/js/scripts.js"></script>
		
		<!-- STYLESWITCHER - REMOVE -->
		<script async src="resouces/demo_files/styleswitcher/styleswitcher.js"></script>

		<!-- REVOLUTION SLIDER -->
		<script src="resources/assets/plugins/slider.revolution.v5/js/jquery.themepunch.tools.min.js"></script>
		<script src="resources/assets/plugins/slider.revolution.v5/js/jquery.themepunch.revolution.min.js"></script>
		<script>
			jQuery(document).ready(function() {
				if(jQuery("#rev_slider_60_1").revolution == undefined){
					revslider_showDoubleJqueryError("#rev_slider_60_1");
				}else{
					revapi60 = jQuery("#rev_slider_60_1").show().revolution({
						sliderType:"standard",
						jsFileLocation:plugin_path + "slider.revolution.v5/js/",
						sliderLayout:"auto",
						dottedOverlay:"none",
						delay:9000,
						navigation: {
							keyboardNavigation:"off",
							keyboard_direction: "horizontal",
							mouseScrollNavigation:"off",
							onHoverStop:"off",
							arrows: {
								style:"uranus",
								enable:true,
								hide_onmobile:true,
								hide_under:778,
								hide_onleave:true,
								hide_delay:200,
								hide_delay_mobile:1200,
								tmp:'',
								left: {
									h_align:"left",
									v_align:"center",
									h_offset:20,
									v_offset:0
								},
								right: {
									h_align:"right",
									v_align:"center",
									h_offset:20,
									v_offset:0
								}
							}
							,
							thumbnails: {
								style:"erinyen",
								enable:true,
								width:200,
								height:113,
								min_width:170,
								wrapper_padding:30,
								wrapper_color:"#333333",
								wrapper_opacity:"1",
								tmp:'<span class="tp-thumb-over"></span><span class="tp-thumb-image"></span><span class="tp-thumb-title">{{title}}</span><span class="tp-thumb-more"></span>',
								visibleAmount:10,
								hide_onmobile:false,
								hide_onleave:false,
								direction:"horizontal",
								span:true,
								position:"outer-top",
								space:20,
								h_align:"center",
								v_align:"top",
								h_offset:0,
								v_offset:0
							}
						},
						gridwidth:1230,
						gridheight:692,
						lazyType:"none",
						shadow:0,
						spinner:"spinner2",
						stopLoop:"on",
						stopAfterLoops:0,
						stopAtSlide:1,
						shuffle:"off",
						autoHeight:"off",
						disableProgressBar:"on",
						hideThumbsOnMobile:"off",
						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						debugMode:false,
						fallbacks: {
							simplifyAll:"off",
							nextSlideOnWindowFocus:"off",
							disableFocusListener:false,
						}
					});
				}
			});
		</script>

	</body>
</html>