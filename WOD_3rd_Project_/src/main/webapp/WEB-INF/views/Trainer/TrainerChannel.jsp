<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
 
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8" />
      <title>WOD'sGYM</title>
      <meta name="description" content="" />
      <meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

      <!-- mobile settings -->
      <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
      <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

      <!-- WEB FONTS : use %7C instead of | (pipe) -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

      <!-- CORE CSS -->
      <link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

      <!-- REVOLUTION SLIDER -->
      <link href="resources/assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css" />
      <link href="resources/assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css" />

      <!-- THEME CSS -->
      <link href="resources/assets/css/essentials.css" rel="stylesheet" type="text/css" />
      <link href="resources/assets/css/layout.css" rel="stylesheet" type="text/css" />

      <!-- PAGE LEVEL SCRIPTS -->
      <link href="resources/assets/css/header-1.css" rel="stylesheet" type="text/css" />
      <link href="resources/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
      <link href="resources/assets/css/color_scheme/blue.css" rel="stylesheet" type="text/css" />>
      <link href="resources/assets/css/pack-realestate.css" rel="stylesheet" type="text/css" />
      <link href="resources/assets/css/custom-forms-v2.css" rel="stylesheet" type="text/css" />   
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://sdk.amazonaws.com/js/aws-sdk-2.283.1.min.js"></script>
		<script src="resources/aws.js"></script>
		<script src="resources/Calltrainerlist.js"></script>
      <script>
      
      var trainerseq = ${trainerSeq};
      var channelseq = ${channelseq};
      var isTrainer = ${isTrainer};
      var lessonlistseq;
      var sumbuf=[];
      var sumcnt=0;
      var searchvideo=[];
   	 var vcnt=0;
      var flag = true;
      function bannerinsert()
  		{
  		window.name = "parentForm";
  		var popupX = (window.screen.width/2)-(400/2);
  		var popupY= (window.screen.height/2)-(400/2);
  		var newWindow;
  		
  		
  		window.open("movebannerinsert?channelseq="+chaseq,"newWindow", "width=1650, height=400, scrollbar=no");

  		}
  	function insertLesson()
  	{
  		window.name = "parentForm";
  		var popupX = (window.screen.width/2)-(400/2);
  		var popupY= (window.screen.height/2)-(400/2);
  		var newWindow;
  		window.open("moveinsertLesson?channelseq="+chaseq,"newWindow", "width=850, height=600, scrollbar=no");

  	}
    function selectLessontypeLessonlist(lessontype){
       $.ajax({
          url : "selectLessonlist",
          data : {
             trainerseq : trainerseq,
             lessontype : lessontype
          },
          success : function(serverData){
        	  
             printLessonlistAndLessontype(serverData);
          }
          
       })
    
    }
    var search=[];
  	var searchcnt=0;
  	function getsaveUrl(albumName,tempbuf,type,serverData)
	  {
		  var img=[];
		  var albumPhotosKey = encodeURIComponent(albumName) + '//';
		  var filebuf=[];
		  searchcnt=0;
		  search=[];
		  var cnt=0;
		  var photoKey;
		  var str="";
		  var comparator=albumName+'//';
		  searchcnt=0;
		  s3.listObjects({Prefix: albumPhotosKey}, function(err, data) 
		  {
		    if (err) 
		      return alert('There was an error viewing your album: ' + err.message);
		
		    var href = this.request.httpRequest.endpoint.href;
		    var bucketUrl = href + albumBucketName + '/';
		    	   
		    var photos = data.Contents.map(function(photo)
		    {
		      var photoKey = photo.Key;
		      var photoUrl = bucketUrl + encodeURIComponent(photoKey);
		     
		      img[cnt]=photoUrl;
		      filebuf[cnt]=photoKey;
		      cnt++;
		     	
		     }
		    );
		    	
			
		    for(var i=0;i<filebuf.length;i++)
		    {	
		    	for(var j=0;j<tempbuf.length;j++)
		    	{   	

		    		if(filebuf[i]==(comparator+tempbuf[j]))  	
		    		{	
		    			search[searchcnt]=img[i];
		    			
		    			searchcnt++;
		    			if(type=='증명사진')
		    				$("#profileimg").attr("src", img[i]);
		    			if(type=='배너')
		    				$("#bannerimg").attr("src", img[i]);
		    			if(type=='최신강의')
		    				$("#todayhot").attr("src", img[i]);
		    			if(type=='대표강의')
		    				$("#repre").attr("src", img[i]);
		    		} 
		    		 		
		    	}
		    }   
		  	if(type=='섬네일')
		  	{
		  
		  		printLessonlistAndLessontype(serverData);
		  	}
		  })
	  }
  	 
  	function getloadUrl(albumName,tempbuf,type,serverData)
	  {
		  var img=[];
		  var albumPhotosKey = encodeURIComponent(albumName) + '//';
		  var filebuf=[];
		  vcnt=0;
		  searchvideo=[];
		  var cnt=0;
		  var photoKey;
		  var str="";
		  var comparator=albumName+'//';
		
		  s3.listObjects({Prefix: albumPhotosKey}, function(err, data) 
		  {
		    if (err) 
		      return alert('There was an error viewing your album: ' + err.message);
		
		    var href = this.request.httpRequest.endpoint.href;
		    var bucketUrl = href + albumBucketName + '/';
		    	   
		    var photos = data.Contents.map(function(photo)
		    {
		      var photoKey = photo.Key;
		      var photoUrl = bucketUrl + encodeURIComponent(photoKey);
		     
		      img[cnt]=photoUrl;
		      filebuf[cnt]=photoKey;
		      
		      cnt++;
		     	
		     }
		    );
		    	
			
		    for(var i=0;i<filebuf.length;i++)
		    {	
		    	for(var j=0;j<tempbuf.length;j++)
		    	{   	
		    		
		    		if(filebuf[i]==(comparator+tempbuf[j]))  	
		    		{	
	
		    			searchvideo[vcnt]=img[i];
		    			
		    			printDetail(serverData);
		    			vcnt++;
		    			
		    		} 
		    		 		
		    	}
		    }   
		  	
		  })
		  
	  }
  
  	var chaseq;
  	function selectChannel(){
  	  $("#printPaymentTable").html("");
  		$.ajax({
  			url : "selectChannel",
  			data : {
  				trainerseq : trainerseq
  			},
  			success : function(serverData){
  				
  				channelseq = serverData.channelseq;
  				chaseq=channelseq;
  				
  				selectChannelFile(channelseq);
  			
  			}
  		})
  		selectTrainerProfile();
  	}
  	var channelFile=[];
  	var cnt=4;
  	function selectChannelFile(channelseq){
		$.ajax({
			url : "selectChannelFile",
			data : {
				channelseq : channelseq
			},
			success : function(serverData){
				
				channelFile[0]="";
				channelFile[1]="";
				channelFile[2]="";
				channelFile[3]="";
				channelFile[4]="";
				cnt=4;
				var tempbuf=[];
				for(var i=0; i<serverData.length;++i)
				{

					var name=serverData[i].savename;		
					var type=serverData[i].type;
			
					if(type=='증명사진')
					{
						channelFile[0]=name;
						tempbuf[0]=name;	
						
						getsaveUrl('profile',tempbuf,type);
						
						
					}
					else if(type=='배너')
					{				
						channelFile[1]=name;
						tempbuf[1]=name;
						getsaveUrl('banner',tempbuf,type);
					}
					
					else if(type=='최신강의')
					{
						channelFile[2]=name;
						tempbuf[2]=name;
						getsaveUrl('wod',tempbuf,type);
						
					}
					else if(type=='대표강의')
					{
						channelFile[3]=name;
						tempbuf[3]=name;					
						getsaveUrl('wod',tempbuf,type);
					}

				}
			}

		})
		
	}
      $(function(){
    	  
    	  trainerList();
          selectChannel();
          
          
          selectLikeAndRecommend();
          
          var videos = document.getElementsByTagName("video"), fraction = 0.8;
          $(document).on("click","#banneredit",bannerinsert);
          $("#selectCustomersList").on("click",selectMyCustomers);
          $("#trainingListButton").on("click",function(){
        	  $("#printPaymentTable").html("");
             TrainerLessonlistPrint();
          }); 
          
        
          $(document).on("click","#update",updatevideo);
          
          $(document).on("click","#insertNewLessonlist",insertLesson);
          
          $("#profileButton").on("click",selectChannel);
          
          $(document).on("click", "#deleteLessonlist", deleteLessonlist); 
          
          $(document).on("click", "#updateTrainerCommnetButton", updateTrainerComment);
          
          $(document).on("click", "#cancleTrainerCommnetButton", function(){
             $("#trainerCommentText").val("");
             updateTrainerComment();
          })
          
           
          $(document).on("click","#detail",saveLessonlistseq);
          $(document).on("click",".buyLesson",function(){
             flag = true;
             var lessonlistseq = $(this).attr("this-lessonlistseq");
             var lessonname = $(this).attr("this-lessonname");
             var lessoncontent = $(this).attr("this-lessoncontent");
             var lessonprice = $(this).attr("this-lessonprice");
             var trainerseq = ${trainerSeq};
             
             selectEqualLessonlist(lessonlistseq,lessonname,lessoncontent,lessonprice,trainerseq);
             
          });
          
          $(document).on("click","#chatBtn",function(){
             var receiveseq = $(this).attr("userseq");
             var userseq = ${sessionScope.loginSeq};
             openChat(userseq,receiveseq);
             
          });
          
          $("#MessageListBtn").click(function(){
        	  var userseq = ${sessionScope.loginSeq};
             MessageList(userseq);
             
          });
          
          $(document).on("click", "#likeButton", like);
          
          $(document).on("click", "#recsButton", like);
          
          $(document).on("click","#sendReviewButton", insertReview);
          
          $(document).on("click",".deleteReviews", deleteReviews);
          
          $(document).on("change", "#selectLessontype", function(){
             var lessontype = $("#selectLessontype").val();
             selectLessontypeLessonlist(lessontype);
          });
          
          $("#paymentListBtn").click(function(){
        	  var userseq = ${sessionScope.loginSeq};
        	  paymentList(userseq);
        	  
          });
          
          $("#MessageBtn").click(function(){
        	  
        		  var receiveseq = ${tUserseq};
        		  var userseq = ${sessionScope.loginSeq};
				openChat(userseq,receiveseq);
        	  
          });
	});
		
      function updatevideo()
      {
    	
    	  lessonlistseq = $(this).attr("data-lessonlistseq");
    	  $.ajax({
              url : "updaterepresentive",
              type : "post",
              data : {
            	  lessonlistseq : lessonlistseq
              },
              success : function(serverData){
                alert("비디오 업로드에 성공하셨습니다.");
              }
           })
      }
      
      function deleteReviews(){
         var reviewseq = $(this).attr("delete-reviewseq");
         
         if(confirm("정말로 리뷰를 삭제하시겠습니까? " )){
            $.ajax({
               url : "deleteReview",
               type : "post",
               data : {
                  reviewseq : reviewseq
               },
               success : function(serverData){
                  if(serverData == "success"){
                     selectReview();
                     alert("삭제가 성공하였습니다.");
                  }
               }
            })
            
         }else{  
        	 alert("취소되었습니다.");
         }
         
         
         
      }
      
      function insertReview(){
         var content = $("#reviewText").val();  
         var star = $(":input:radio[name=product-review-vote]:checked").val();

         if(content == ""){
        	alert("내용을 입력해주세요.");
            return false;
         }
         $.ajax({
            url : "insertReview",
            type : "post",
            data : {
               channelseq : channelseq,
               content : content,
               star : star
            },
            success : function(serverData){
               if(serverData =="success"){
                  $("#reviewText").val("");  
                  selectReview();
               }
            }
         })   
         
      }
      
      function like(){
         $.ajax({
            url : "choiceLike",
            type : "post",
            data : {
               channelseq : channelseq
            },
            success : function(serverData){
               selectLikeAndRecommend();
            }
            
         })
          
      }
      
      function selectLikeAndRecommend(){
         $.ajax({
            url : "selectLikeAndRecommendCount",
            data : {
               channelseq : channelseq
            },
            success : function(serverData){
               $("#likes").text(serverData.trainerlikeCount);
               $("#recs").text(serverData.trainerrecommendCount);
                  
            }
            
         })
         
      }
      
      function buyLesson(lessonlistseq, lessonprice, trainerseq){
         var price = lessonprice;
         var result = confirm("정말로 구입하시겠습니까?");
         if(result){
            
            checkPrice(price, lessonlistseq,trainerseq);
         }else{
             alert("결제를 취소하셨습니다.");
         }
      }
      
      
        function checkPrice(price, lessonlistseq,trainerseq){
             $.ajax({
                  url : 'compareUserMoney',
                  data :{
                     price:price
                  },
                   success : function(data){
                      if(data == "ok"){
                        dealClass(price, lessonlistseq,trainerseq);
                     }else{
                        alert("잔액이 부족합니다.");
                        location.href = "moveAccount";
                     } 
                     
                  }
             });
          };
          
     
	
           function dealClass(price, lessonlistseq,trainerseq){
              $.ajax({
                url : 'dealClass'
                , type : 'POST'
                , data : {
                   price : price
                   , trainerseq : trainerseq
                   ,lessonlistseq:lessonlistseq
                   
                }
                , success : function(data){
                   if(data=="ok"){
                      alert('구입 완료되었습니다!');
                      paymentLesson(lessonlistseq);
                   }else{
                      alert('계산에 문제가 발생했습니다.');
                   }
                }
              })
           }
          
           function paymentLesson(lessonlistseq){
                   $.ajax({
                      url : "paymentLesson",
                      type : "post",
                      data : {
                         lessonlistseq : lessonlistseq,
                         trainerseq : trainerseq
                      },
                      success : function(serverData){
                         location.href = "moveCustomer";
                      }
                   });
           };
             
          
      function updateTrainerComment(){
         var trainercomment = $("#trainerCommentText").val();
         $.ajax({
            url : "updateTrainerComment",
            type : "post",
            data : {
               trainerseq : trainerseq,
               trainercomment : trainercomment
            },
            success : function(serverData){
               if(serverData == "success"){
                  alert("수정에 성공하셨습니다!");
               }
               
            }
         })
      }
      
      
      function insertDetailLessonComment(){
         var userseq = ${sessionScope.loginSeq};
         var content = $("#comment").val();
         
         if(content == ""){
        	 alert("내용을 입력해주세요");
            return false;
         }
         
         $.ajax({
            url : "insertDetailLessonComment",
            type : "post",
            data : {
               lessonlistseq : lessonlistseq,
               userseq : userseq,
               content : content
            },
            success : function(serverData){
               if(serverData == "success"){
                  $("#comment").val("");
                  commentPrint();
               }
            }
         })
         
      }
      
      function saveLessonlistseq(){
         lessonlistseq = $(this).attr("data-lessonlistseq");
         detailLesson();
      }
      
      function detailLesson(){
    	  
    	 
         var userseq = ${sessionScope.loginSeq};
         titlePrint();
         commentPrint();
      }
         
       function titlePrint(){
         $.ajax({
            url : "selelctDetailLesson",
            data : {
               lessonlistseq : lessonlistseq
            },
            success : function(serverData){
               var str = "";
              
               var tempbuf=[];
               tempbuf[0]=serverData[0].videosave;
               getloadUrl('wod',tempbuf,'강의',serverData);
               
            }
         })
      }
   	function printDetail(serverData)
   	{
   	
   		for(var i = 0; i<serverData.length; i++){
    		var str="";
            str +="<div class='row'>";
            str +="<div class='col-md-11 col-sm-9'>";
            str+='<h4><b>'+serverData[i].lessontype+'</b>'+serverData[i].lessonname+'</h4>';
            str +='<div class="container">';
            str +='<div class="clearfix mb-60">';
            str+='<div class="embed-responsive embed-responsive-16by9">';
            str+='<video class="" preload="none" width="800" height="455" controls autoplay>';
            str+='<source src="'+ searchvideo[i] +'" type="video/mp4" />';
            str+='</video>';
            str+='</div>';
            str+='</div>';
            str +="<p>#" + serverData[i].categoryname + "</p>";               
            str +="<p> "+ serverData[i].lessoncontent + "</p>";
            $("#printTable2").html(str);   
            
            str +="<p> lessonlike = " + serverData[i].lessonlike + "</p>";
            str +="<p> lessonrecommend = " + serverData[i].lessonrecommend + "</p>";
            str +="<div class='divider divider-dotted'><!-- divider --></div>";
            str +="<div class='clearfix mt-30'>";
            str +="<span class='float-left mt-6 bold hidden-xs-down'>";
            str +="Share Post: ";
            str +="</span>";
            str +="</a>";
            str +="</div>";
            str +="</div>";
         }
   	}
      function commentPrint(){
    	  
         $.ajax({
            url : "selectCommentList",
            data : {
               lessonlistseq : lessonlistseq
            },
            success : printout
         });
         
         function printout(serverData){
            
            var str = "";
            str +="<div id='comments' class='comments'>";
            str +="<h4 class='page-header mb-60 fs-20'>";
            str +="<span>"+serverData.length+"</span> COMMENTS";
            str +="</h4>";
           
            for(var i =0; i < serverData.length; i++){
            	     
               str +="<div class='comment-item'>";
               str +="<span class='user-avatar'>";
               str+="<img class='float-left media-object' src='resources/assets/images/_smarty/avatar2.jpg' width='64' height='64' alt=''>";
               str +="</span>";
               str +="<div class='media-body'>";
               str +="<h4 class='media-heading bold'>"+serverData[i].NAME+"</h4>";
              
               if(${sessionScope.loginSeq} == serverData[i].USERSEQ ){
                  str +="<a id='deleteComment' delete-replyseq='"+serverData[i].REPLYSEQ+"' class='scrollTo comment-delete'>delete</a><br>";
               }
               str +="<small class='block'>"+serverData[i].REGISTERDATE+"</small>";
               str += serverData[i].CONTENT;
               
          
               str +="</div>";
               str +="</div>";
               
            }
   
            str +="</div>";
               
            if(${sessionScope.roleSeq==101}){
               str +="<h4 class='page-header fs-20 mb-60 mt-100'>";
               str +="LEAVE A <span>COMMENT</span>";
               str +="</h4>";
               str +="<div class='row'>";
               str +="<div class='col-md-12'>";
               str +="<label>COMMENT</label>";
               str +="<textarea required='required' maxlength='5000' rows='5' class='form-control' name='comment' id='comment' placeholder='댓글을 남겨주세요' ></textarea>";
               str +="</div>";
               str +="</div>";
               str +="<div class='row'>";
               str +="<div class='col-md-12'>";
               str +="<button class='btn btn-3d btn-lg btn-reveal btn-black' id='insertCommentButton'>";
               str +="<i class='fa fa-check'></i>";
               str +="<span>SUBMIT MESSAGE </span>";
               str +="</button>";
               str +="</div>";
               str +="</div>";
            }
            
            str +="</div>";
            str +="</div>";
            str +="<div class='col-md-3 col-sm-3'>";
            str +="</div>";
            $("#CommentArea").html(str);
            $("#insertCommentButton").on("click",function(){
               insertDetailLessonComment();
            })
           
            $("#deleteComment").on("click",function(){
               var replyseq = $(this).attr("delete-replyseq");
               if(confirm("정말로 댓글을(를) 삭제하시겠습니까?  "  )){
                  deleteComments(replyseq);
               }
            })
         }
      }
      
      function deleteComments(replyseq){
         $.ajax({
            url : "deleteComment",
            type : "post",
            data : {
               replyseq : replyseq   
            },
            success :function(serverData){
               if(serverData == "success"){
                  commentPrint();
               }
            }
         })
         
      }
        
      function selectMyCustomers(){

         $("#printTable2").html("");
         if (isTrainer==1) {
            $.ajax({
               url:"selectTrainingCustomers",
               success:function(serverData){
                  
                  var str = "";
                  $("#CommentArea").html(str);
                  $("#PrintTable2").html(str);
                  $("#insertTable").html(str);
                  str+='<div class="col-lg-12">';
                  str+="<div id='panel-2' class='panel panel-default'>";
                  str+="<div class='panel-heading'>";
                  str+="<span class='title elipsis'>";
                  str+="<strong>수강생목록</strong>";
                  str+="</span>";
                  str+="<ul class='nav nav-tabs pull-right'>";
                  
                  str+="</ul>";
                  str+="</div>";
                  str+="<div class='panel-body'>";
                  str+="<div class='tab-content transparent'>";
                  str+="<div id='ttab1_nobg' class='tab-pane active'>";
                  str+="<div class='table-responsive'>";
                  str+="<table class='table table-striped table-hover table-bordered'>";
                  str+="<thead>";
                  str+="<tr>";
                  str+="<th><center>성함</center></th>";
                  str+="<th><center>나이</center></th>";
                  str+="<th><center>성별</center></th>";
                  str+="<th><center>강의명</center></th>";
                  str+="<th><center>수강기간</center></th>";
                  str+="<th><center>남은수강일</center></th>";
                  str+="<th><center>Calendar</center></th>";
                  str+="<th><center>상담</center></th>";
                  str+="</tr>";
                  str+="</thead>";
                  str+="<tbody>";
                  
                  for(var i = 0 ; i <serverData.length ; i++){
                     
                        var birth_day = serverData[i].AGE;
                        var birthday = new Date(birth_day);
                        var today = new Date();
                        var age = today.getFullYear() - birthday.getFullYear();
                        if(isNaN(age)){
                           age=0;
                        }
                        
                     str+="<tr>";
                     str+="<td><center>"+serverData[i].NAME+"</center></td>";
                     str+="<td><center>"+age+"</center></td>";
                     str+="<td><center>"+serverData[i].GENDER+"</center></td>";
                     str+="<td><center>"+serverData[i].LESSONNAME+"</center></td>";
                     str+="<td><center>"+serverData[i].LESSONSTARTDATE+" ~ "+serverData[i].LESSONENDDATE+ "</center></td>";
                     str+="<td><center>"+serverData[i].RESTDATE+"</center></td>";
                     str+="<td><center><a href='moveCalendar?memberseq="+serverData[i].MEMBERSEQ+"&trainerSeq=0',  id = 'viewDetailBtn''><i class='ico-rounded ico-hover icon-calendar'></i></a></center></td>"; 
                     str+="<td><center><i class='ico-rounded ico-hover icon-chat' id = 'chatBtn' userseq='"+serverData[i].USERSEQ+"'></i></center></td>";
                     str+="</tr>";
                  };
         
                  str+="</tbody>";
                  str+="</table>";
                  str+="</div>";
                  str+="</div>";
                  str+='</div>';
                  $("#printPaymentTable").html("");
                  $("#printTable").html(str);
               }
            });
         }
      };
      
      
     
      function bufload(serverData)
      {
    	  sumbuf=[];
    	  sumcnt=0;
    	
    	  for(var i = 0; i<serverData.length; i++)
   	   		{
   				sumbuf[i]=serverData[i].imgsave;
   			 
   	   		}
    	  getsaveUrl('wodimg',sumbuf,'섬네일',serverData);
    	
      }
      function TrainerLessonlistPrint(){
            $.ajax({
               url : "selectOneTrainerLessonlist",
               data : {
                  trainerseq : trainerseq               
               },
               success : function(serverData){
            	   bufload(serverData);
               }
            })
      }
      
      
      function printLessonlistAndLessontype(serverData){
         var str = "";
         
         for(var i = 0; i<serverData.length; i++){
               str += '<div class="col-12 col-md-4">';   
               str += '<div class="property-item property-item-box">';   
               str +='<div class="property-ribbon">';
               str+='<span class="bg-blue text-white">-10% OFF</span>';
               str+='</div>';
               str += '<a class="property-image"   data-lessonlistseq="'+serverData[i].lessonlistseq+'"href="#" id="detail">';
               if(serverData[i].lessonrecommend==0)
                  str+='<span class="rating rating-1 fs-17 text-white"><!-- rating-1 ... rating-5 --></span>';
               if(serverData[i].lessonrecommend==1)
                  str+='<span class="rating rating-2 fs-17 text-white"><!-- rating-1 ... rating-5 --></span>';
               if(serverData[i].lessonrecommend==2)
                  str+='<span class="rating rating-3 fs-17 text-white"><!-- rating-1 ... rating-5 --></span>';
               if(serverData[i].lessonrecommend==3)
                  str+='<span class="rating rating-4 fs-17 text-white"><!-- rating-1 ... rating-5 --></span>';
               if(serverData[i].lessonrecommend==4)
                  str+='<span class="rating rating-5 fs-17 text-white"><!-- rating-1 ... rating-5 --></span>';
               str += '<img class="img-fluid lazy rad-4" src="'+ search[i]+'"alt="">';
               str+='</a>';
               
               str+='<h4 class="property-item-price">';
               str+='<strong class="text-primary">$';
               str+=serverData[i].lessonprice;
               str+='</strong>';
               str+='<small class="float-right text-danger">강의명:';
               str+=serverData[i].lessonname;
               str+='</small>';
               str+='</h4>';
               str+= '<div class="buyButton" ></div>';
               
               if(isTrainer == 1){
                  str+='<input type="checkbox" class="deleteCheck" data-lessonname="' + serverData[i].lessonname +'" name="deleteValues" value="'+serverData[i].lessonlistseq+'">';
               }
               str+='레슨유형 : ' + serverData[i].lessontype ;
               
               if( ${sessionScope.roleSeq == 101}){
                  
                  str +="&nbsp &nbsp&nbsp";
                  str +='<a class="buyLesson"  this-lessonlistseq="' + serverData[i].lessonlistseq + '"' ;
                  str +='this-lessonname="'+serverData[i].lessonname+'" this-lessoncontent="'+serverData[i].lessoncontent+'"' ;
                  str +='this-lessonprice="'+serverData[i].lessonprice+'"'; 
                  str += '><span class="badge badge-success">결제</span></a>';
                  
               }
              
               str+='<p class="property-item-desc fs-15">';
               str+='30일 완성 요가코스';
               str+='</p>';
               str+='<hr /><!-- mobile and boxed separator -->';
               str+='<strong class="poperty-item-location">';
               str+=serverData[i].lessoncoursedate+'일 완성 ' +serverData[i].lessontype+ '코스';
               str+='</strong>';
               str+='<hr /><!-- mobile and boxed separator -->';
               str+='<div class="property-item-btn">';
               if(isTrainer==1){
               str+='<a href="#"  data-lessonlistseq="'+serverData[i].lessonlistseq+'" id="update">대표 강좌 설정</a>';
               }
               str+='</div>';
               str+='</div>';
               str+='</div>';
            }      
            if(true){
               var temp="";
               
               temp +='<div class="property-list-opt">';
               temp +='<div class="property-list-btn hidden-xs-down">';
               temp +='<select   class="form-control" name="list-order-option" id = "selectLessontype">';
               temp+='<option class="selectLessontype" value="">종류를 선택하세요</option>';
               temp+='<option class="selectLessontype" value="">전체</option>';
               temp+='<option class="selectLessontype" value="보디빌딩">보디빌딩</option>';
               temp +='<option class="selectLessontype" value="다이어트">다이어트</option>';
               temp+='<option  class="selectLessontype" value="요가">요가</option>';
               temp +='<option class="selectLessontype" value="필라테스">필라테스</option>';
               temp+='</select>';
               temp+='</div>';
               temp+='</div>';
               if(isTrainer==1){
                  temp += '<button  id="insertNewLessonlist" type="button" class="btn btn-default">강좌개설</button> ';
                  temp += '<button  id="deleteLessonlist" type="button" class="btn btn-default">강좌삭제</button> ';
               }
               
               $("#insertTable").html(temp);
            }
            
            $("#printTable").html(str);
            
      }
      
      
      
      function selectEqualLessonlist(lessonlistseq,lessonname,lessoncontent,lessonprice,trainerseq){
         
         $.ajax({
            url : "selectEqualLessonlist",
            data : {
               lessonlistseq : lessonlistseq,
               trainerseq : trainerseq,
               lessonname:lessonname,
               lessoncontent:lessoncontent,
               lessonprice:lessonprice
            },
            success : function(serverData){
               if(serverData.length==0){
                  buyLesson(lessonlistseq, lessonprice, trainerseq);
               }else{
            	   alert("이미 구매한 강좌입니다.");
               }
            }
         })
         
         
      };
       
      function deleteLessonlist(){
         $("input[name=deleteValues]:checked").each(function() {
              var lessonlistseq = $(this).val();
              var lessonname = $(this).attr("data-lessonname");
              
              if(confirm("정말로 '" +lessonname + "'을(를) 삭제하시겠습니까? "   )){
                 $.ajax({
                    url : "deleteLessonlist",
                    type : "post",
                    data : {
                       lessonlistseq : lessonlistseq
                    },
                    successs : function(serverData){
                       
                    }
                    
                 });
              	alert("삭제를 완료하였습니다.");
               TrainerLessonlistPrint();
            }else{       
                alert("삭제를 취소하였습니다.");
            }
              
         });
 
      }
      

      function selectTrainerProfile(){
         $.ajax({
            url : "selectTrainerAndChannel",
            data : {
               trainerseq : trainerseq
            },
            success : function(serverData){
               var str = "";
               var str2="";
               $("#CommentArea").html(str2);
               str2+='<div class="col-sm-6">';
               str2+='<h4><b>Intro</b> Video</h4>';
               str2+='<video class="" preload="none" width="500" height="315" id="repre" controls autoplay>';
               str2+='<source src="https://s3.ap-northeast-2.amazonaws.com/wodgym.tk/wod//sim3.mp4" type="video/mp4" />';
               str2+='</video>';
               str2+='</div>';
      
               str2+='<div class="col-sm-4">';
               str2+='<h4><b>Today</b> Hot</h4>';
               str2+='<video class="" preload="none" width="500" height="315" id="todayhot" controls autoplay>';
               str2+='<source src="https://s3.ap-northeast-2.amazonaws.com/wodgym.tk/wod//sim2.mp4" type="video/mp4" />';
               str2+='</video>';
               str2+='</div>';
               
               $("#printTable2").html(str2);
               
               for(var i = 0; i<serverData.length; i++){
                  str+= "<div class='shop-item-price'>";            
                  $("#name").html(serverData[i].NAME);
                  $("#subject").html(serverData[i].LESSONTYPE);
                  
                  str+= "</div>";
                  str+= "<hr />";
                  str+= "<p>";

                  str+='<div class="col-lg-12">';
                  str+="<ul id='myTab' class='nav nav-tabs nav-top-border mt-80' role='tablist'>";
                  str+="<li class='nav-item'><a class='nav-link active' href='#trainerComment' data-toggle='tab'>인사말</a></li>";
                  str+="<li class='nav-item'><a class='nav-link' href='#review' data-toggle='tab'>리뷰</a></li>";
                  str+="</ul>";
                  str+="<div class='tab-content pt-20'>";
                  str+="<div role='tabpanel' class='tab-pane active' id='trainerComment'>";
           
                  if(isTrainer==1){
                            str+="<div class='mb-30'>";
                            if(serverData[i].TRAINERCOMMENT == null){
                               str+="<textarea name='text' id='trainerCommentText' class='form-control' rows='6' placeholder='인사말을 남겨주세요' maxlength='1000'></textarea>";
                            }else{
                               str+="<textarea name='text' id='trainerCommentText' class='form-control' rows='6' placeholder='인사말을 남겨주세요' maxlength='1000'> " + serverData[i].TRAINERCOMMENT+ "  </textarea>";
                            }
                            
                           str+="</div>";
                           str+="<button id='updateTrainerCommnetButton' type='button' class='btn btn-primary'><i class='fa fa-check'></i> 수정</button> &nbsp; ";
                           str+="<button id='cancleTrainerCommnetButton' type='button' class='btn btn-primary'><i class='fa fa-check'></i> 취소</button>";
                        }else{
                           str+="<div class='mb-30'>";
                           if(serverData[i].TRAINERCOMMENT==null){
                              str += "<p>인사말이 없습니다</p>";
                           }else{
                              str += "<p>" + serverData[i].TRAINERCOMMENT+ " </p>";
                           }
                           str+="</div>";
                        }
              
                        str+="</div>";
                        str+="<div role='tabpanel' class='tab-pane fade' id='QnA'>";
                        str+="<p>리뷰</p>";
                        str+="</div>";
                        str+="<div role='tabpanel' class='tab-pane fade' id='review'>";
                        str+="<div  class='block mb-60'>";
                        str += "<div id='reviewForPrint'> </div>";
                        selectReview();
                        str+="</div>";
                     if(${sessionScope.roleSeq == 101}){
                        str+="<h4 class='page-header mb-40'>Review</h4>";
                        str+="<form method='post' action='#' id='form'>";
                        str+="<div class='mb-30'>";
                        str+="<textarea id='reviewText' name='text' id='text' class='form-control' rows='6' placeholder='리뷰을 남겨주세요' maxlength='1000'></textarea>";
                        str+="</div>";      
                        str+="<div class='product-star-vote clearfix'>";
                        str+="<label class='radio float-left'>";
                        str+="</label>";
                        str+="<label class='radio float-left'>";
                        str+="</label>";
                        str+="<label class='radio float-left'>";
                        str+="</label>";
                        str+="<label class='radio float-left'>";
                        str+="</label>";
                        str+="<label class='radio float-left'>";
                        str+="</label>";
                        str+="<label class='radio float-left'>";
                        str+="</label>";

                        str+="<label class='radio float-left'>";
                        str+="<input class='reviewStar' type='radio' name='product-review-vote' value='1' />";
                        str+="<i></i> 1 Star";
                        str+="</label>";
                        str+="<label class='radio float-left'>";
                        str+="<input class='reviewStar' type='radio' name='product-review-vote' value='2' />";
                        str+="<i></i> 2 Stars";
                        str+="</label>";
                        str+="<label class='radio float-left'>";
                        str+="<input class='reviewStar' type='radio' name='product-review-vote' checked value='3' />";
                        str+="<i></i> 3 Stars";
                        str+="</label>";
                        str+="<label class='radio float-left'>";
                        str+="<input class='reviewStar' type='radio' name='product-review-vote' value='4' />";
                        str+="<i></i> 4 Stars";
                        str+="</label>";
                        str+="<label class='radio float-left'>";
                        str+="<input class='reviewStar' type='radio' name='product-review-vote' value='5' />";
                        str+="<i></i> 5 Stars";
                        str+="</label>";
                        str+="</div>";
                        str+="<br>";
                        str+="<center><button type='button' id='sendReviewButton' class='btn btn-primary'><i class='fa fa-check'></i> 리뷰등록</button></center>";
                        str+="</form>";
                        
                     }
                        str+='</div>';
                     } 
                     $("#printTable").html(str);
                     $("#insertTable").html("");
                     
                  } 
            
               }); 
      }
      
       function selectReview(){
         $.ajax({
            url : "selectReviewList",
            data : {
               trainerseq : trainerseq
            },
            success : function(serverData){
               
               var str ="";
               for(var i =0; i < serverData.length; i++){
                        str+="<span class='user-avatar'>";
                        str+="<img class='float-left media-object' src='resources/assets/images/_smarty/avatar2.jpg' width='64' height='64' alt=''>";
                        str+="</span>";
                        str+="<div class='media-body'>";
                        str+="<h4 class='media-heading fs-14'>";
                        str+= serverData[i].NAME + "&ndash; ";
                        str+="<span class='text-muted'>" + serverData[i].REGISTERDATE + "</span> &ndash;";
                        str+="<span class='fs-14 text-muted'>";
                        if(serverData[i].STAR == 1){
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star-o'></i>";
                           str+="<i class='fa fa-star-o'></i>";
                           str+="<i class='fa fa-star-o'></i>";
                           str+="<i class='fa fa-star-o'></i>";
                        }else if(serverData[i].STAR == 2){
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star-o'></i>";
                           str+="<i class='fa fa-star-o'></i>";
                           str+="<i class='fa fa-star-o'></i>";
                        }else if(serverData[i].STAR == 3){
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star-o'></i>";
                           str+="<i class='fa fa-star-o'></i>";
                        }else if(serverData[i].STAR == 4){
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star-o'></i>";
                        }else if(serverData[i].STAR == 5){
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star'></i>";
                           str+="<i class='fa fa-star'></i>";
                        }
                        if(serverData[i].USERSEQ == ${sessionScope.loginSeq }){
                            str +="&nbsp; <a class='deleteReviews'  delete-reviewseq='"+serverData[i].REVIEWSEQ +"' >delete   </a>";
                        }
                        str+="</span>";
                        str+="</h4>";
                        str+="<p>" + serverData[i].CONTENT + "</p>";
                        str+="</div>";
                        
               }
               $("#reviewForPrint").html(str);
               
               
            }
            
         })         
         
      }
       
       
      
      function openChat(userseq,receiveseq){
         
         window.name = "parentForm";
         var popupX = (window.screen.width/2)-(400/2);
         var popupY= (window.screen.height/2)-(400/2);
           var newWindow;
           window.open("moveChat?sendseq="+userseq+"&receiveseq="+receiveseq,"newWindow", "width=700, height=900, scrollbar=no");
          
      };
      
      function MessageList(userseq){
         $.ajax({
            url:"selectTrainerChatRoomList",
            data:{
               userseq1:userseq
            },
            success:function(serverData){
               var str="";
               str+="<div class='table-responsive'>";
               str+="<table class='table table-hover'>";
               str+="<thead>";
               str+="<tr>";
               str+="<th></th>";
               str+="<th>대화상대</th>";
               str+="<th>메세지</th>";
               str+="<th>최근 수신/발신</th>";
               str+="</tr>";
               str+="</thead>";
               str+="<tbody>";
               for(var i = 0 ; i < serverData.length ; i++){

                  str+="<tr>";
                  if(serverData[i].userseq1==userseq){
                     str+="<td></td>";
                     str+="<td>"+serverData[i].othersname+"</td>";
                     str+="<td><i class='ico-rounded ico-hover icon-chat' id = 'chatBtn' UserSeq='"+serverData[i].userseq2+"'></i></td>";
                     str+="<td>"+serverData[i].recentdate+"</td>";
                  }else if(serverData[i].userseq2==userseq){
                     str+="<td></td>";
                     str+="<td>"+serverData[i].othersname+"</td>";
                     str+="<td><i class='ico-rounded ico-hover icon-chat' id = 'chatBtn' UserSeq='"+serverData[i].userseq1+"'></i></td>";
                     str+="<td>"+serverData[i].recentdate+"</td>";
                  }
                  str+="</tr>";
               }
               str+="</tbody>";
               str+="</table>";
               str+="</div>";
               $("#printTable").html("");
               $("#printPaymentTable").html(str);
               
            }
         });
         
      };
      
      function paymentList(userseq){
    	  
      
    	  $.ajax({
              url:"selectTrainerPaymentList",
              data:{
            	  userseq:userseq
              },
              success:function(serverData){
                 var str="";
           	    str+="<div class='table-responsive'>";
				str+="<table class='table table-bordered table-striped'>";
				str+="<thead>";
				str+="<tr>";
				str+="<th><center> 트레이너 이름 </center></th>";
				str+="<th><center> 현재 보유 포인트 </center></th>";
				str+="<th><center> 총매출 </center></th>";
				str+="</tr>";
				str+="</thead>";
				str+="<tbody>";
				str+="<tr>";
				str+="<td><center>"+serverData[0].NAME+"</center></td>";
				str+="<td><center>"+serverData[0].MONEY+"</center></td>";
				str+="<td><center>"+serverData[0].SALES+"</center></td>";
				str+="</tr>";
				str+="</tbody>";
				str+="</table>";
				str+="</div>";
                 
                 str+="<div class='table-responsive'>";
                 str+="<table class='table table-hover'>";
                 str+="<thead>";
                 str+="<tr>";
                 str+="<th></th>";
                 str+="<th><center>회원 이름</center></th>";
                 str+="<th><center>강좌명</center></th>";
                 str+="<th><center>판매가</center></th>";
                 str+="<th><center>판매날짜</center></th>";
                 str+="</tr>";
                 str+="</thead>";
                 str+="<tbody>";
                 for(var i = 0 ; i < serverData.length ; i++){

                    str+="<tr>";
                    
                    str+="<td></td>";
                    str+="<td><center>"+serverData[i].CUSTOMERSNAME+"</center></td>";
                    str+="<td><center>"+serverData[i].LESSONNAME+"</center></td>";
                    str+="<td><center>"+serverData[i].PAY+"</center></td>";
                    str+="<td><center>"+serverData[i].PAYDATE+"</center></td>";
                    
                    str+="</tr>";
                 }
                 str+="</tbody>";
                 str+="</table>";
                 str+="</div>";
                 $("#printTable").html("");
                 $("#printPaymentTable").html(str);
                 
              }
           });
      };
    	  
      </script>   
         
         
         
         
   </head>

   <body class="smoothscroll enable-animation">

	<div id="wrapper">
		<!-- Top Bar -->
		<div id="topBar">
			<div class="container">
				<!-- right -->
				<ul class="top-links list-inline float-right">
					<!-- 로그인에 따른 TEXT 출력 관리 -->
					<li class="text-welcome"><c:if
							test="${empty sessionScope.loginId }">
							<strong>손님</strong>, 지금 바로 로그인하세요 !
						</c:if> <c:if test="${sessionScope.roleSeq==101 }">
							<strong>${sessionScope.loginId }님</strong>, 회원 로그인했습니다. 
						</c:if> <c:if test="${sessionScope.roleSeq==1001 }">
							<strong>${sessionScope.loginId }님</strong>, 트레이너 로그인했습니다.
						</c:if> <c:if test="${sessionScope.roleSeq==1000 }">
							<strong>${sessionScope.loginId }님</strong>, 회원 로그인했습니다. 
						</c:if> <c:if test="${sessionScope.roleSeq==10001 }">
							<strong>${sessionScope.loginId }님</strong>, 관리자 로그인 했습니다.
						</c:if></li>
					<li><a class="dropdown-toggle no-text-underline"
						data-toggle="dropdown" href="#"><i
							class="fa fa-user hidden-xs-down"></i> 내 계정</a>
						<ul class="dropdown-menu">	
							<li>
							<c:if test="${empty sessionScope.loginId }">
							<a tabindex="-1" href="moveLogin"><i
									class="glyphicon glyphicon-off"></i> LOGIN</a>
							</c:if>
							<c:if test="${!empty sessionScope.loginId }">
							<a tabindex="-1" href="Logout"><i
									class="glyphicon glyphicon-off"></i> LOGOUT</a>
							</c:if>
							</li>
						</ul></li>
					<!-- 로그인 유무에 따른 메뉴 변경 -->
					<li><c:if test="${empty sessionScope.loginId }">
							<a href="moveLogin">LOGIN</a>
						</c:if> <c:if test="${!empty sessionScope.loginId }">
							<a href="Logout">LOGOUT</a>
						</c:if></li>
					<li><a href="moveSignUp">REGISTER</a></li>
				</ul>

				<!-- left -->
				<ul class="top-links list-inline">
					
				</ul>

			</div>
		</div>
		<div id="header" class="navbar-toggleable-md sticky clearfix">

			<!-- TOP NAV -->
			<header id="topNav">
				<div class="container">

					<!-- Mobile Menu Button -->
					<button class="btn btn-mobile" data-toggle="collapse"
						data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>

					
					<!-- /BUTTONS -->


					<!-- Logo -->
					<a class="logo float-left" href="home"> <img
						src="resources/assets/images/_smarty/logo.png" alt="" />
					</a>


					<div class="navbar-collapse collapse float-right nav-main-collapse">
						<nav class="nav-main">


							<ul id="topMain" class="nav nav-pills nav-main">
								<li class="dropdown">
									<!-- HOME --> <a class="dropdown-toggle noicon" href="#"> HOME </a>

								</li>
								<li
									class="dropdown mega-menu nav-animate-fadeIn nav-hover-animate hover-animate-bounceIn">
									<!-- THEMATIC --> <a class="dropdown-toggle" href="#">
										<span class="badge badge-danger float-right fs-11">New
											Trainer</span> <b>TRAINER</b>
								</a>
									<ul class="dropdown-menu dropdown-menu-clean">
										<li>
											<div class="row">

												<div class="col-md-5th">
													<ul class="list-unstyled">
														<li><span>Bodybuilding</span></li>

														<div id="bodybuildingListPrintArea"></div>
													</ul>
												</div>

												<div class="col-md-5th">
													<ul class="list-unstyled">
														<li><span>Diet</span></li>
														<div id="dietListPrintArea"></div>

													</ul>
												</div>

												<div class="col-md-5th">
													<ul class="list-unstyled">
														<li><span>Yoga & Pilates</span></li>
														<div id="yogaListPrintArea"></div>

														<li class="divider"></li>
														<li><span class="fs-11 mt-0 pb-15 pt-15 text-info">
														여러분도 트레이너에 도전하세요!</span></li>

														<li><a href="moveApply"><i class="et-browser"></i>
																트레이너 지원하기</a></li>
													
														<li class="divider"></li>
													</ul>
												</div>

												<div class="col-md-6 hidden-sm text-center">
													<div class="p-15 block">
													
													</div>
													<p
														class="menu-caption hidden-xs-down text-muted text-center"
														id="trainerName"></p>
												</div>

											</div>
										</li>
									</ul>
								</li>
						

								<c:if test="${sessionScope.roleSeq==101 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle noicon"
										href="moveCustomer"> MY PAGE </a></li>
								</c:if>
								<c:if test="${sessionScope.roleSeq==1000 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle noicon"
										href="moveCustomer"> MY PAGE </a></li>
								</c:if>
								<c:if test="${sessionScope.roleSeq==1001 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle noicon"
										href="moveTrainerChannel2"> TRAINER PAGE </a></li>
								</c:if>


								<li class="dropdown mega-menu">
									<!-- SHORTCODES --> <a class="dropdown-toggle noicon" href="goQnA">
										고객센터 </a>
								</li>
								<c:if test="${sessionScope.roleSeq==10001 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle noicon"
										href="moveAdmin"> ADMINPAGE </a></li>
								</c:if>

							</ul>
						</nav>
					</div>

				</div>
			</header>
			<!-- /Top Nav -->
		</div>




         <section class="page-header" >
            <img id="bannerimg" src="resources/img/default.png" alt="img" width="100%" height="auto">
            <div class="container">
               <!-- breadcrumbs -->
                     <ol class="breadcrumb">
                     	<c:if test="${isTrainer==1}">
                        <li><a href="#" id="banneredit">배너Edit</a></li>
                        </c:if>
                       <!--  <li><a href="#">プロフィールEdit</a></li> -->
                     </ol>

            </div>
         </section>
         <!-- /PAGE HEADER -->
         <!-- -->
         

      </div>
      <!-- /wrapper -->      
         <section>
            <div class="container">         
               <div class="row">
                  <!-- 상단 메뉴바 시작 -->
                  <div class="col-sm-3">
                     <div class="premium-thumbnail-circle">
                        <a href="#">
                            <div class="spinner"></div>
                            <figure>
                               <img id= "profileimg" src="resources/img/88.png" alt="img">
                            </figure>

                            <div class="info">
                              <div class="info-back">
                                <h3 id="name"></h3>

                                <p id="subject">
                                   
                                   
                                </p>
                              </div>
                            </div>
                           
                         </a>
                     </div>
                     
                  </div>
                  
                  <div class="col-lg-3">   
                        <i id="likeButton" class="ico-rounded ico-hover et-heart">  </i>
                        <div class="heading-title heading-line-single">
                           <h4>좋아요 <span id="likes"></span></h1>
                        </div> 
                                 
                        <i id="recsButton" class="ico-rounded ico-hover et-basket"> </i>  
                        <div class="heading-title heading-line-single">
                           <h4>추천 <span id ="recs"></span>
                        </div> 
                        <c:if test="${isTrainer==0}">
                        <i id="MessageBtn" class="ico-rounded ico-hover et-chat"> </i>  
                        <div class="heading-title heading-line-single">
                           <h4>1:1 상담 <span id ="recs"></span>
                        </div> 
                        </c:if>
                  </div>
                  
                  <div class="container">

                  
                        <ul class="nav nav-tabs nav-justified">
                           <li class="nav-item"><a id="profileButton" class="nav-link active show" href="#" data-toggle="tab">프로필</a></li>
                           <li class="nav-item"><a id="trainingListButton" class="nav-link" href="#" data-toggle="tab">강의목록</a></li>
                           <c:if test="${isTrainer==1}">
                              <li class="nav-item"><a id="selectCustomersList" class="nav-link" href="#" data-toggle="tab">수강생목록</a></li>
                           </c:if>
                           <c:if test="${isTrainer==1}">
                              <li class="nav-item"><a id="paymentListBtn" class="nav-link" href="#" data-toggle="tab">매상</a></li>
                           </c:if>
                           <c:if test="${isTrainer==1}">
                              <li class="nav-item"><a id="MessageListBtn" class="nav-link" href="#" data-toggle="tab">메시지</a></li>
                           </c:if>
                          
                        </ul>
                        <br>
               </div>         
                  <!-- 상단메뉴바 끝  -->
                  
                  <!--  상단 출력화면 시작 -->
      
                  <!-- 하단출력화면 끝 -->               
               </div>
               <!-- 하단출력화면 끝 -->
               <section id="packages" class="alternate-2 y-2">
               <div class="container">
                  <div class="row"  id="printTable2">            
                  </div>
               </div>   
               </section>   
               <div id="CommentArea"></div>
               <div class="container">
               <div id="insertTable">
               </div>
               
             
               <div class="property-list-opt">
               
                  <div class="property-list-btn hidden-xs-down">
                  
                     
                     <a href="#" class="active" data-class="property-item-box">
                        <i class="fa fa-th-large"></i><!-- boxed -->
                     </a>

                     <a href="#" data-class="">
                        <i class="fa fa-bars"></i><!-- normal list -->
                     </a>
                     
                  </div>         
                     
               </div>
              
            
               <div class="row property-item-list" id="printTable">
                           
               </div>
               
                <div id="printPaymentTable">
                           
               </div>

            </div>
      
               
            </div>
         
      </section>
         
         
         <!-- / -->




   <footer id="footer" class="footer-light">
		<div class="container">			
			<div class="row">
			</div>
		</div>
		<div class="copyright">
			<div class="container">
				<ul class="float-right m-0 list-inline mobile-block">
					<li><a href="#">wod & Conditions</a></li>
					<li>&bull;</li>
					<li><a href="#">Privacy</a></li>
				</ul>
				&copy; All Rights Reserved, Company wod
			</div>
		</div>

	</footer>
   
      <!-- SCROLL TO TOP -->
      <a href="#" id="toTop"></a>


      <!-- PRELOADER -->
      <div id="preloader">
         <div class="inner">
            <span class="loader"></span>
         </div>
      </div><!-- /PRELOADER -->
      <script type ="text/javascript" src="resources/videojs-contrib-hls.min.js"/></script>
   <script type ="text/javascript" src="resources/videojs-contrib-hls.js"/></script>
   <script type ="text/javascript" src="resources/video.min.js"/></script>
      
      <!-- JAVASCRIPT FILES -->
      <script>var plugin_path = 'resources/assets/plugins/'; </script>
      <script src="resources/assets/plugins/jquery/jquery-3.3.1.min.js"></script>

      <!-- REVOLUTION SLIDER -->
      <script src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
      <script src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
      <script src="resources/assets/js/view/demo.revolution_slider.js"></script>
      <script async src="resources/assets/js/view/pack_realestate.js"></script>
      <!-- SCRIPTS -->
      <script src="resources/assets/js/scripts.js"></script>
      
   </body>
</html>