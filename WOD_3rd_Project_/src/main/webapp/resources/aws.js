/**
 * 
 */

  var albumBucketName = 'wodgym.tk';
  var bucketRegion = 'ap-northeast-2';
  var IdentityPoolId = 'wodgym';
  var bufiles=[];
  var imfiles=[];
  AWS.config.update({
    region: bucketRegion,
    credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-northeast-2:c8796957-9385-495b-bb5d-4ab9111e9381'
    })
  });

  var s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: {Bucket: albumBucketName}
  });
  
  function listAlbums() {
	  s3.listObjects({Delimiter: '/'}, function(err, data) {
	    if (err) {
	      return alert('There was an error listing your albums: ' + err.message);
	    } else {
	      var albums = data.CommonPrefixes.map(function(commonPrefix) {
	        var prefix = commonPrefix.Prefix;
	        var albumName = decodeURIComponent(prefix.replace('/', ''));
	        return getHtml([
	          '<li>',
	            '<span onclick="deleteAlbum(\'' + albumName + '\')">X</span>',
	            '<span onclick="viewAlbum(\'' + albumName + '\')">',
	              albumName,
	            '</span>',
	          '</li>'
	        ]);
	      });
	   
	      var htmlTemplate = [
	        '<h2>Albums</h2>',
	        '<ul>',
	          getHtml(albums),
	        '</ul>',
	        '<button onclick="createAlbum(prompt(\'Enter Album Name:\'))">',
	          'Create New Album',
	        '</button>'
	      ]
	      document.getElementById('app').innerHTML = getHtml(htmlTemplate);
	    }
	  });
	}
  function createAlbum(albumName) {
	  albumName = albumName.trim();
	  if (!albumName) {
	    return alert('Album names must contain at least one non-space character.');
	  }
	  if (albumName.indexOf('/') !== -1) {
	    return alert('Album names cannot contain slashes.');
	  }
	  var albumKey = encodeURIComponent(albumName) + '/';
	  s3.headObject({Key: albumKey}, function(err, data) {
	    if (!err) {
	      return alert('Album already exists.');
	    }
	    if (err.code !== 'NotFound') {
	      return alert('There was an error creating your album: ' + err.message);
	    }
	    s3.putObject({Key: albumKey}, function(err, data) {
	      if (err) {
	        return alert('There was an error creating your album: ' + err.message);
	      }
	      alert('Successfully created album.');
	      viewAlbum(albumName);
	    });
	  });
	}
  function viewAlbum(albumName) {
	  var albumPhotosKey = encodeURIComponent(albumName) + '//';
	  s3.listObjects({Prefix: albumPhotosKey}, function(err, data) {
	    if (err) {
	      return alert('There was an error viewing your album: ' + err.message);
	    }
	    // 'this' references the AWS.Response instance that represents the response
	    var href = this.request.httpRequest.endpoint.href;
	    var bucketUrl = href + albumBucketName + '/';

	    var photos = data.Contents.map(function(photo) {
	      var photoKey = photo.Key;
	      var photoUrl = bucketUrl + encodeURIComponent(photoKey);
	      
	  
	      var getvideo='<video width="40% height="20%" controls autoplay="autoplay"> <source src='+photoUrl+' type="video/mp4" /> </video>'
		  var getimg='<img style="width:128px;height:128px;" src="' + photoUrl + '"/>';		
	      return getHtml([
	        '<span>',
	          '<div>',
	          getvideo,
	          '</div>',
	          '<div>',
	            '<span onclick="deletePhoto(\'' + albumName + "','" + photoKey + '\')">',
	              'X',
	            '</span>',
	            '<span>',
	              photoKey.replace(albumPhotosKey, ''),
	            '</span>',
	          '</div>',
	        '</span>',
	      ]);
	    });
	  
	   
	    var htmlTemplate = [
	      '<h2>',
	        'Album: ' + albumName,
	      '</h2>',
	      '<div>',
	        getHtml(photos),
	      '</div>',
	      '<input id="photoupload" type="file" accept="image/*">',
	    //  '<button id="addphoto" onclick="addPhoto(\'' + albumName +'\')">',
	        'Add Photo',
	      '</button>',
	      '<button onclick="listAlbums()">',
	        'Back To Albums',
	      '</button>',
	    ]
	    document.getElementById('app').innerHTML = getHtml(htmlTemplate);
	  });
	}
  

  function getUrl(albumName,buf)
  {
	
	  var albumPhotosKey = encodeURIComponent(albumName) + '//';

	  var cnt=0;
	  var photoKey;
	  var str="";
	  s3.listObjects({Prefix: albumPhotosKey}, function(err, data) 
	  {
	    if (err) 
	      return alert('There was an error viewing your album: ' + err.message);
	
	    var href = this.request.httpRequest.endpoint.href;
	    var bucketUrl = href + albumBucketName + '/';
	    var filename="";	 
	    var comparator=albumName+'//';
	    var filebuf=[];
	    var photos = data.Contents.map(function(photo)
	    {
	      var photoKey = photo.Key;
	      var photoUrl = bucketUrl + encodeURIComponent(photoKey);
	      bufiles[cnt]=photoUrl;
	      filebuf[cnt]=photoKey;
	       cnt++;
	        
	     }
	    );
	    var pagenum=0;
	    for(var i=0;i<bufiles.length;++i)
	    {
	    	for(var j=0;j<buf.length;++j)
	    	{
	    	if(filebuf[i]==(comparator+buf[j]))  	
	    	{
	    	str="";	    	
	    	str+='<div class="col-md-4 col-sm-6 mb-60">';
	    	str+='<div class="mb-50">'; 	
	    	str+='<a class="thumbnail" target="_blank" href="#">';
	    	str+='<video class="" preload="none" width="100%" height="93%" controls>';
	    	str+='<source src="'+bufiles[i]+'" type="video/mp4" />';
	    	str+='</video>';
	    	str+='</a>';
	    	str+='</div>';
	    	str+='<h4 class="fs-15">';
	    	str+='<a target="_blank" href="#">대표강의</a>';
	    	str+='<i class="fa fa-clock-o"></i> ';
			str+='<span class="font-lato">WODGYM</span>';
			str+=' </h4>';
			str+='</div>';
			pagenum++;
			document.getElementById('home').innerHTML+=str;
	    	}
	    	
	    	}
			
	    }
	    document.getElementById('pagenum').innerHTML=pagenum;
	  })

  }
  function getUrlrecommend(albumName,buf)
  {
	
	  var albumPhotosKey = encodeURIComponent(albumName) + '//';

	  var cnt=0;
	  var photoKey;
	  var str="";
	  s3.listObjects({Prefix: albumPhotosKey}, function(err, data) 
	  {
	    if (err) 
	      return alert('There was an error viewing your album: ' + err.message);
	
	    var href = this.request.httpRequest.endpoint.href;
	    var bucketUrl = href + albumBucketName + '/';
	    var filename="";	 
	    var comparator=albumName+'//';
	    var filebuf=[];
	    var photos = data.Contents.map(function(photo)
	    {
	      var photoKey = photo.Key;
	      var photoUrl = bucketUrl + encodeURIComponent(photoKey);
	      bufiles[cnt]=photoUrl;
	      filebuf[cnt]=photoKey;
	       cnt++;
	        
	     }
	    );
	    var pagenum2=0;
	    for(var i=0;i<bufiles.length;++i)
	    {
	    	for(var j=0;j<buf.length;++j)
	    	{
	    	if(filebuf[i]==(comparator+buf[j]))  	
	    	{
	    	str="";	    	
	    	str+='<div class="col-md-4 col-sm-6 mb-60">';
	    	str+='<div class="mb-50">'; 	
	    	str+='<a class="thumbnail" target="_blank" href="#">';
	    	str+='<video class="" preload="none" width="100%" height="93%" controls>';
	    	str+='<source src="'+bufiles[i]+'" type="video/mp4" />';
	    	str+='</video>';
	    	str+='</a>';
	    	str+='</div>';
	    	str+='<h4 class="fs-15">';
	    	str+='<a target="_blank" href="#">대표강의</a>';
	    	str+='<i class="fa fa-clock-o"></i> ';
			str+='<span class="font-lato">Wodgym</span>';
			str+=' </h4>';
			str+='</div>';
			pagenum2++;
			document.getElementById('recommend').innerHTML+=str;
	    	}
	    	}
			
	    }
	    document.getElementById('pagenum2').innerHTML=pagenum2;
	  })

  }
  function getUrlToStr(albumName,buf,str,id)
  {
	
	  var albumPhotosKey = encodeURIComponent(albumName) + '//';

	  var cnt=0;
	  var photoKey;
	  s3.listObjects({Prefix: albumPhotosKey}, function(err, data) 
	  {
	    if (err) 
	      return alert('There was an error viewing your album: ' + err.message);
	
	    var href = this.request.httpRequest.endpoint.href;
	    var bucketUrl = href + albumBucketName + '/';
	    var filename="";	 
	    var comparator=albumName+'//';
	    var filebuf=[];
	    var photos = data.Contents.map(function(photo)
	    {
	      var photoKey = photo.Key;
	      var photoUrl = bucketUrl + encodeURIComponent(photoKey);
	      bufiles[cnt]=photoUrl;
	      filebuf[cnt]=photoKey;
	       cnt++;
	        
	     }
	    );
	    var pagenum=0;
	    for(var i=0;i<bufiles.length;++i)
	    {
	    	for(var j=0;j<buf.length;++j)
	    	{
	    		if(filebuf[i]==(comparator+buf[j]))  	
	    		{
	    			document.getElementById(id).innerHTML+=str;
	    		}
	    	}		
	    }
	  }) 
  }
  

  function getimgUrl(albumName,buf)
  {
	
	  var albumPhotosKey = encodeURIComponent(albumName) + '//';
	  var filebuf=[];
	  var cnt=0;
	  var photoKey;
	  var str="";
	  var comparator=albumName+'//';
	  s3.listObjects({Prefix: albumPhotosKey}, function(err, data) 
	  {
	    if (err) 
	      return alert('There was an error viewing your album: ' + err.message);
	
	    var href = this.request.httpRequest.endpoint.href;
	    var bucketUrl = href + albumBucketName + '/';
	    	   
	    var photos = data.Contents.map(function(photo)
	    {
	      var photoKey = photo.Key;
	      var photoUrl = bucketUrl + encodeURIComponent(photoKey);
	     
	      imfiles[cnt]=photoUrl;
	      filebuf[cnt]=photoKey;
	      cnt++;
	     
	     }
	    );
	    	
		
	    for(var i=0;i<imfiles.length;++i)
	    {
	    	
	    	for(var j=0;j<buf.length;++j)
	    	{
	    		
	    	if(filebuf[i]==(comparator+buf[j]))  	
	    	{	
	    	str="";
	    	str+='<div class="shop-item m-0">';
	    	str+='<div class="thumbnail">';
	    	str+='<a class="shop-item-image" href="#">';
	    	str+='<img class="img-fluid" src="'+imfiles[i]+'"alt="shop first image" />';
	    	str+='</a>';
	    	str+='<div class="shop-item-info">';
	    	str+='<div class="rating rating-4 fs-13"><!-- rating-0 ... rating-5 --></div>';
	    	str+='<h5>심으뜸 </h5>';
	    	str+='<a class="btn btn-light" href="#"><i class="fa fa-cart-plus"></i> 채널로 가기</a>';
	    	str+='</div>';
	    	str+='</div>';
	    	str+='</div>';
	    	document.getElementById('trainerrec').innerHTML+=str;
	    	}
	    	}

	    }   
	  })
  }
  function addFile(albumName,savname) {
	  var files = document.getElementById('image').files;
	  if (!files.length) {
	    return alert('Please choose a file to upload first.');
	  }
	  var file = files[0];
	  var fileName = savname;
	  var albumPhotosKey = encodeURIComponent(albumName) + '//';
	  var photoKey = albumPhotosKey + fileName;
	  alert(photoKey);
	  s3.upload({
	    Key: photoKey,
	    Body: file,
	    ACL: 'public-read'
	  }, function(err, data) {
	    if (err) {
	      return alert('업로드실패: ', err.message);
	    }
	    alert('업로드 성공');
	    //document.getElementById('mainarea').innerHTML="";
	   // getUrl(albumName);
	  });
	}
  function deleteFile(albumName, photoKey) {
	  s3.deleteObject({Key: photoKey}, function(err, data) {
	    if (err) {
	      return alert('There was an error deleting your photo: ', err.message);
	    }
	    alert('Successfully deleted photo.');
	    
	  });
	}
  function deleteAlbum(albumName) {
	  var albumKey = encodeURIComponent(albumName) + '/';
	  s3.listObjects({Prefix: albumKey}, function(err, data) {
	    if (err) {
	      return alert('There was an error deleting your album: ', err.message);
	    }
	    var objects = data.Contents.map(function(object) {
	      return {Key: object.Key};
	    });
	    s3.deleteObjects({
	      Delete: {Objects: objects, Quiet: true}
	    }, function(err, data) {
	      if (err) {
	        return alert('There was an error deleting your album: ', err.message);
	      }
	      alert('Successfully deleted album.');
	     
	      
	    });
	  });
	}