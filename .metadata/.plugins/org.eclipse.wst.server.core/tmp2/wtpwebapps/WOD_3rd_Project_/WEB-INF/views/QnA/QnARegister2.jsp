<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>WOD'sGYM</title>
<meta name="description" content="" />
<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

<!-- mobile settings -->
<meta name="viewport"
	content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

<!-- WEB FONTS : use %7C instead of | (pipe) -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700"
	rel="stylesheet" type="text/css" />

<!-- CORE CSS -->
<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<!-- THEME CSS -->
<link href="resources/assets/css/essentials.css" rel="stylesheet"
	type="text/css" />
<link href="resources/assets/css/layout.css" rel="stylesheet"
	type="text/css" />

<!-- PAGE LEVEL SCRIPTS -->
<link href="resources/assets/css/header-1.css" rel="stylesheet"
	type="text/css" />
<link href="resources/assets/css/color_scheme/green.css"
	rel="stylesheet" type="text/css" id="color_scheme" />
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	        <script src="//cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
<script>
function QnARegister(){
	var qtype = document.getElementById("qtype").value;
	var qtitle = document.getElementById("qtitle").value;
	var qcontent = document.getElementById("qcontent").value;
	
	$.ajax({
		url : 'QnAregister'
		, type : 'post'
		, data : {
			"qtype" : qtype
			, "qtitle" : qtitle
			, "qcontent" : qcontent
		}
		, success : function (data) {
			if(data == "ok"){
				location.href = "goQnA";
			}else{
				alert("등록에 실패하셨습니다.");
			}
		}
		, error : function () {
		
		}
	});
}


function upgrade(){
	var qtype = document.getElementById("updateQtype").value;
	var qtitle = document.getElementById("updateQtitle").value;
	var qcontent = document.getElementById("updateQcontent").value;
	var qnaseq = document.getElementById("updateQnaseq").value;
	
	
	$.ajax({
		url : 'updateQna'
		, type : 'POST'
		, data : {
			qnaseq : qnaseq
			, qtype : qtype
			, qtitle : qtitle
			, qcontent : qcontent
		}
		, success : function(data){
			if(data=="ok"){

				location.href = "qnaView?qnaseq="+qnaseq;		// 여기 고치자~!!!

			}else{
				alert("업데이트가 실패하였습니다.");
			}
		}
		
	})
	
	
}
function upgradeAdmin(){
	var qtype = document.getElementById("updateQtype").value;
	var qtitle = document.getElementById("updateQtitle").value;
	var qcontent = document.getElementById("updateQcontent").value;
	var qnaseq = document.getElementById("updateQnaseq").value;
	
	$.ajax({
		url : 'updateAdminQna'
		, type : 'POST'
		, data : {
			qnaseq : qnaseq
			, qtype : qtype
			, qtitle : qtitle
			, qcontent : qcontent
		}
		, success : function(data){
			if(data=="ok"){

				location.href = "adminView?qnaseq="+qnaseq;		// 여기 고치자~!!!

			}else{
				alert("업데이트가 실패하였습니다.");
			}
		}
		
	})
	
}

	</script>
</head>


<body class="smoothscroll enable-animation">
	<%
		String userID = null;
	if(session.getAttribute("loginId")!=null){
		userID = (String)session.getAttribute("loginId");
	}
	%>

	<div id="wrapper">
		<!-- Top Bar -->
		<div id="topBar">
			<div class="container">
				<!-- right -->
				<ul class="top-links list-inline float-right">
					<!-- 로그인에 따른 TEXT 출력 관리 -->
					<li class="text-welcome"><c:if
							test="${empty sessionScope.loginId }">
							<strong>손님</strong>, 지금 바로 로그인하세요 !
						</c:if> <c:if test="${sessionScope.roleSeq==101 }">
							<strong>${sessionScope.loginId }님</strong>, 회원 로그인했습니다. 
						</c:if> <c:if test="${sessionScope.roleSeq==1001 }">
							<strong>${sessionScope.loginId }님</strong>, 트레이너 로그인했습니다.
						</c:if> <c:if test="${sessionScope.roleSeq==1000 }">
							<strong>${sessionScope.loginId }님</strong>, 회원 로그인했습니다. 
						</c:if> <c:if test="${sessionScope.roleSeq==10001 }">
							<strong>${sessionScope.loginId }님</strong>, 관리자 로그인 했습니다.
						</c:if></li>
					<li><a class="dropdown-toggle no-text-underline"
						data-toggle="dropdown" href="#"><i
							class="fa fa-user hidden-xs-down"></i> 내 계정</a>
						<ul class="dropdown-menu">	
							<li>
							<c:if test="${empty sessionScope.loginId }">
							<a tabindex="-1" href="moveLogin"><i
									class="glyphicon glyphicon-off"></i> LOGIN</a>
							</c:if>
							<c:if test="${!empty sessionScope.loginId }">
							<a tabindex="-1" href="Logout"><i
									class="glyphicon glyphicon-off"></i> LOGOUT</a>
							</c:if>
							</li>
						</ul></li>
					<!-- 로그인 유무에 따른 메뉴 변경 -->
					<li><c:if test="${empty sessionScope.loginId }">
							<a href="moveLogin">LOGIN</a>
						</c:if> <c:if test="${!empty sessionScope.loginId }">
							<a href="Logout">LOGOUT</a>
						</c:if></li>
					<li><a href="moveSignUp">REGISTER</a></li>
				</ul>

				<!-- left -->
				<ul class="top-links list-inline">
					
				</ul>

			</div>
		</div>
		<div id="header" class="navbar-toggleable-md sticky clearfix">

			<!-- TOP NAV -->
			<header id="topNav">
				<div class="container">

					<!-- Mobile Menu Button -->
					<button class="btn btn-mobile" data-toggle="collapse"
						data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>

					
					<!-- /BUTTONS -->


					<!-- Logo -->
					<a class="logo float-left" href="home"> <img
						src="resources/assets/images/_smarty/logo.png" alt="" />
					</a>


					<div class="navbar-collapse collapse float-right nav-main-collapse">
						<nav class="nav-main">


							<ul id="topMain" class="nav nav-pills nav-main">
								<li class="dropdown">
									<!-- HOME --> <a class="dropdown-toggle noicon" href="#"> HOME </a>

								</li>
								<li
									class="dropdown mega-menu nav-animate-fadeIn nav-hover-animate hover-animate-bounceIn">
									<!-- THEMATIC --> <a class="dropdown-toggle" href="#">
										<span class="badge badge-danger float-right fs-11">New
											Trainer</span> <b>TRAINER</b>
								</a>
									<ul class="dropdown-menu dropdown-menu-clean">
										<li>
											<div class="row">

												<div class="col-md-5th">
													<ul class="list-unstyled">
														<li><span>Bodybuilding</span></li>

														<div id="bodybuildingListPrintArea"></div>
													</ul>
												</div>

												<div class="col-md-5th">
													<ul class="list-unstyled">
														<li><span>Diet</span></li>
														<div id="dietListPrintArea"></div>

													</ul>
												</div>

												<div class="col-md-5th">
													<ul class="list-unstyled">
														<li><span>Yoga & Pilates</span></li>
														<div id="yogaListPrintArea"></div>

														<li class="divider"></li>
														<li><span class="fs-11 mt-0 pb-15 pt-15 text-info">
														여러분도 트레이너에 도전하세요!</span></li>

														<li><a href="moveApply"><i class="et-browser"></i>
																트레이너 지원하기</a></li>
													
														<li class="divider"></li>
													</ul>
												</div>

												<div class="col-md-6 hidden-sm text-center">
													<div class="p-15 block">
													
													</div>
													<p
														class="menu-caption hidden-xs-down text-muted text-center"
														id="trainerName"></p>
												</div>

											</div>
										</li>
									</ul>
								</li>
						

								<c:if test="${sessionScope.roleSeq==101 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle noicon"
										href="moveCustomer"> MY PAGE </a></li>
								</c:if>
								<c:if test="${sessionScope.roleSeq==1000 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle noicon"
										href="moveCustomer"> MY PAGE </a></li>
								</c:if>
								<c:if test="${sessionScope.roleSeq==1001 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle noicon"
										href="moveTrainerChannel2"> TRAINER PAGE </a></li>
								</c:if>


								<li class="dropdown mega-menu">
									<!-- SHORTCODES --> <a class="dropdown-toggle noicon" href="goQnA">
										고객센터 </a>
								</li>
								<c:if test="${sessionScope.roleSeq==10001 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle noicon"
										href="moveAdmin"> ADMINPAGE </a></li>
								</c:if>

							</ul>
						</nav>
					</div>

				</div>
			</header>
			<!-- /Top Nav -->
		</div>

		<section class="page-header parallax parallax-3"
			style="background-image: url('resources/assets/images/_smarty/imgpattern2.jpg')">
			<div class="overlay dark-0">
				<!-- dark overlay [0 to 9 opacity] -->
			</div>

			<div class="container">

				<h1>Q&A Register</h1>

				<!-- breadcrumbs -->
				<ol class="breadcrumb">
					<li><a href="home">Home</a></li>
					<li><a href="goQnA">고객센터</a></li>
				
				</ol>
				<!-- /breadcrumbs -->

			</div>
		</section>
		<!-- /PAGE HEADER -->




		<!-- -->
		<section>
			<div class="container">

				<div class="row">
					<div class="col-md-6 offset-md-3">

						<div	class="toggle toggle-transparent toggle-accordion toggle-noicon">
						
							<div class="toggle active">
								<label class="fs-20"><i class="fa fa-leaf"></i>Q&A 등록</label>
								<div class="toggle-content">
									<c:if test="${empty boardView}">
								
										<div class="clearfix">
								

											<!-- 질문 -->
											<label for="qtype">질문 유형을 선택하세요</label>
											<select id="qtype" name="qtype" >
												<option value="SERVICE">서비스/기타</option>
												<option value="MONEY">거래 관련</option>
												<option value="VIDEO">동영상 관련</option>
											</select>
		
											<label>제목</label> <label class="input mb-10" > <input
												required="" type="text" id="qtitle" name="qtitle" > <b
												class="tooltip tooltip-bottom-right">제목을 입력하세요.</b>
											</label>


											
											<!-- 내용 -->
											
											<label>문의 사항</label> <label class="textarea mb-10">
												<textarea required="" cols="60" rows="15" id="qcontent" name="qcontent"></textarea> <b
												class="tooltip tooltip-bottom-right">문의사항을 입력하세요.</b>
											</label>


										</div>

										<div class="row">


											<div class="col-md-6 col-sm-6 col-6 text-right">
												<button class="btn btn-primary" id="sQbutton" name="sQbutton" onclick="QnARegister();">
													<i class="fa fa-check"></i> 등록
												</button>
										
											</div>

										</div>

									
									</c:if>

									<c:if test="${!empty boardView}">
									<input type="hidden" id="updateQnaseq" name="updateQnaseq" value="${boardView.QNASEQ}" />
										<div class="clearfix">
								

											<!-- 질문 -->
											<label for="updateQtype">질문 유형을 선택하세요</label>
											<select id="updateQtype" name="updateQtype" value="${boardView.QTYPE }">
												<option value="SERVICE">서비스/기타</option>
												<option value="MONEY">거래 관련</option>
												<option value="VIDEO">동영상 관련</option>
											</select>
		
											<label>제목</label> <label class="input mb-10" > <input
												required="" type="text" id="updateQtitle" name="updateQtitle" value="${boardView.QTITLE }"> <b
												class="tooltip tooltip-bottom-right">제목을 입력하세요.</b>
											</label>


											
											<!-- 내용 -->
											
											<label>문의 사항</label> <label class="textarea mb-10">
												<textarea required="" cols="40" rows="15" id="updateQcontent" name="updateQcontent">${boardView.QCONTENT}</textarea> <b
												class="tooltip tooltip-bottom-right">문의사항을 입력하세요.</b>
											</label>


										</div>

										<div class="row">


											<div class="col-md-6 col-sm-6 col-6 text-right">
											<c:if test="${boardView.USERSEQ != 1}">
												<button class="btn btn-primary" id="qbutton" name="qbutton" onclick="upgrade();">
													<i class="fa fa-check"></i> 수정
												</button>
											</c:if>	
											<c:if test="${boardView.USERSEQ == 1}">
												<button class="btn btn-primary" id="qbutton" name="qbutton" onclick="upgradeAdmin();">
													<i class="fa fa-check"></i> 수정
												</button>
											</c:if>										
											</div>

										</div>

									
									</c:if>



								</div>
							</div>



						</div>

					</div>
				</div>

			</div>
		</section>
		<!-- / -->





	<footer id="footer" class="footer-light">
		<div class="container">			
			<div class="row">
			</div>
		</div>
		<div class="copyright">
			<div class="container">
				<ul class="float-right m-0 list-inline mobile-block">
					<li><a href="#">wod & Conditions</a></li>
					<li>&bull;</li>
					<li><a href="#">Privacy</a></li>
				</ul>
				&copy; All Rights Reserved, Company wod
			</div>
		</div>
		
	</footer>
	<!-- SCROLL TO TOP -->
	<a href="#" id="toTop"></a>


	<!-- PRELOADER -->
	<div id="preloader">
		<div class="inner">
			<span class="loader"></span>
		</div>
	</div>
	<!-- /PRELOADER -->


	<!-- JAVASCRIPT FILES -->
	<script>
		var plugin_path = 'resources/assets/plugins/';
	</script>
	<script src="resources/assets/plugins/jquery/jquery-3.3.1.min.js"></script>

	<script src="resources/assets/js/scripts.js"></script>

	<!-- STYLESWITCHER - REMOVE -->
	<script async src="demo_files/styleswitcher/styleswitcher.js"></script>
</body>
</html>