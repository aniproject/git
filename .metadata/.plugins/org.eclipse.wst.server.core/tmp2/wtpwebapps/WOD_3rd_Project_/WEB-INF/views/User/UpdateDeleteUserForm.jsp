<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-US">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>WOD'sGYM</title>
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

		<!-- WEB FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="resources/assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/layout.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/color_scheme/blue.css" rel="stylesheet" type="text/css" id="color_scheme" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script>
		var updateFlag = false;
	    
		$(function(){
			var checkNumber = $("#pwd").val().search(/[0-9]/g);
		    var checkEnglish = $("#pwd").val().search(/[a-z]/ig);
		    
			$("#updateCustomersBtn").click(function(){
				if($("#pwd").val().search(/[0-9]/g) <0 || $("#pwd").val().search(/[a-z]/ig) <0){
					alert("새로운 비밀번호는 숫자와 영문자를 혼용하여야 합니다.");
					$("#pwd").val().focus();
				}else if($("#pwdCheck").val().search(/[0-9]/g) <0 || $("#pwdCheck").val().search(/[a-z]/ig) <0){
					alert("비밀번호 확인은 숫자와 영문자를 혼용하여야 합니다.");
					$("#pwdCheck").val().focus();
				}else if($("#pwd").val()!=$("#pwdCheck").val()){
					alert("새로운 비밀번호와 비밀번호 확인이 동일하지 않습니다.");
					$("#pwdCheck").val().focus();
				}else if($("#name").val().length==0){
					alert("성함을 입력해주세요.");
					$("#name").val().focus();
				}else if($("#gender").val()==null){
					alert("성별을 선택해주세요.");
					$("#gender").val().focus();
				}else if($("#email").val().length==0){
					alert("이메일을 입력해주세요.");
					$("#email").val().focus();
				}else if(! email_check($("#email").val())){
					alert("이메일 양식에 맞지 않습니다.");
					$("#email").val().focus();
				}else{
					updateFlag=true;
				}
				
				if(updateFlag==true){
					$.ajax({
						url:"updateCustomers",
						data:{
							userpw:$("#pwd").val(),
							name:$("#name").val(),
							gender:$("#gender").val(),
							useremail:$("#email").val()
						},
						type:"POST",
						success:function(serverData){
							if(serverData=="suc"){
								alert("회원 정보 수정에 성공하였습니다!");
								window.close();
							}else{
								alert("회원 정보 수정에 실패하였습니다");
								
							}
							
						}
						
					})
				}
				
			});
			
			$("#exitBtn").click(function(){
				window.close();
			});
			
		});
		
		function email_check(email) {    
		    var regex=/([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		    return (email != '' && email != 'undefined' && regex.test(email)); 
		}
		
		</script>
			
		
	</head>
	<!--
		.boxed = boxed version
	-->
	<body>


		<div class="padding-15">

			<div class="login-box">

				<!-- login form -->
				<form action="index.html" method="post" class="sky-form boxed">
					<header><i class="fa fa-users"></i> WOD'sGYM User</header>
					
					
					<fieldset style=width:1200px;>
				<!-- 	<div class='callout alert alert-border mt-20'> -->
					<div class='row countTo-sm text-center'>
					<div class='col-md-8 col-sm-7'>
			 		<div class='box-static box-transparent box-bordered p-30'> 
					<div class='box-title mb-30'>
					<h2 class='fs-20'>${sessionScope.loginId}님 회원 정보 수정</h2>
					</div>
					
				
					<form class='m-0 sky-form' action='#' method='post'>
					
					<div class='row'>
					<div class='col-md-6 col-sm-6'>
					<label>새로운 비밀번호 *</label>
					<label class='input mb-10'>
					<i class='ico-append fa fa-lock'></i>
					<input type='password' id = 'pwd'>
					<b class='tooltip tooltip-bottom-right'>New Password</b>
					</label>
					</div>
			
					<div class='col-md-6 col-sm-6'>
					<label>비밀번호 확인 *</label>
					<label class='input mb-10'>
					<i class='ico-append fa fa-lock'></i>
					<input type='password' id = 'pwdCheck'>
					<b class='tooltip tooltip-bottom-right'>New Password</b>
					</label>
					</div>
					</div>
			
					<div class='row'>
					<div class='col-md-6 col-sm-6'>
					<label>이름 *</label>
					<label class='input mb-10'>
					<i class='ico-append fa fa-user'></i>
					<input type='text' id = 'name'>
					<b class='tooltip tooltip-bottom-right'>New Name</b>
					</label>
					</div>
			
			
					<div class='col-md-6 col-sm-6'>
						<label>성별 *</label>
						<label class="select mb-10">
										<select id = "gender" name = "gender">
											<option value="0" selected disabled>성별</option>
											<option value="남성">남자</option>
											<option value="여성">여자</option>
											<option value="그 외 성별">그외</option>
										</select>
										<i></i>
									</label>
									</div>
								</div>
			
					
 					<div class='row'>
					<div class='col-md-6 col-sm-6'>
					<label>E-MAIL *</label>
					<label class='input mb-10'>
					<input required='' type='email' id = 'email' style='width:605px;'>
					<b class='tooltip tooltip-bottom-right'>New E-Mail</b> 
					</label>
					</div>
		 			</div>
		 			<div class='row'>
					<div class='col-md-6 col-sm-6'>
		 			<a href='#' class='btn btn-3d btn-lg btn-reveal btn-black' id = 'updateCustomersBtn'>
		 			<i class='et-user'></i>
		 			<span>회원 정보 수정</span>
		 			</a>
		 			</div>
		 			<div class='col-md-6 col-sm-6'>
		 			<a href='#' class='btn btn-3d btn-lg btn-reveal btn-black' id = 'exitBtn'>
		 			<i class='et-user'></i>
		 			<span>닫기</span>
		 			</a>
		 			</div>
		 		
		 			</fieldset>
					

			</div>

		</div>

		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'resources/assets/plugins/';</script>
		<script type="text/javascript" src="resources/assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="resources/assets/js/app.js"></script>
	</body>
</html>