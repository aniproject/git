<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>WOD'sGYM</title>
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- REVOLUTION SLIDER -->
		<link href="resources/assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css" />

		<!-- THEME CSS -->
		<link href="resources/assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="resources/assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
			
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		
		<script type="text/javascript">
		
		$(function(){
			$("#selectCustomersList").on("click",selectMyCustomers);
			$(document).on('click','#viewDetailBtn',function(){
				var lessonlistseq = $(this).attr("lessonlistseq");
				var name = $(this).attr("name");
				insertCustomerExcersise(lessonlistseq, name);
			})
			
			$("#trainingListButton").on("click",trainingList);
			$(document).on("click","#insertLessonlistButton",insertLessonlist);
			selectOneTrainerLessonlist();
			
		})
		
		function selectMyCustomers(){
			if (${sessionScope.roleSeq==1001} ) {
				$.ajax({
					url:"selectTrainingCustomers",
					success:function(serverData){
						
						var str = "";
						str+="<div id='panel-2' class='panel panel-default'>";
						str+="<div class='panel-heading'>";
						str+="<span class='title elipsis'>";
						str+="<strong>수강생LIST</strong>";
						str+="</span>";
						str+="<ul class='nav nav-tabs pull-right'>";
			
						str+="<div class='col-md-30'>";
						str+="<select class='form-control pointer' name='contact[department]''>";
						str+="<option value=''> Select </option>";
						str+="<option value='Marketing'>Marketing</option>";
						str+="</select>";
						str+="</div>";
			
						str+="</ul>";
						str+="</div>";
						str+="<div class='panel-body'>";
						str+="<div class='tab-content transparent'>";
						str+="<div id='ttab1_nobg' class='tab-pane active'>";
						str+="<div class='table-responsive'>";
						str+="<table class='table table-striped table-hover table-bordered'>";
						str+="<thead>";
						str+="<tr>";
						str+="<th>성함</th>";
						str+="<th>나이</th>";
						str+="<th>성별</th>";
						str+="<th>강의명</th>";
						str+="<th>수강기간</th>";
						str+="<th>남은수강일</th>";
						str+="<th>운동</th>";
						str+="</tr>";
						str+="</thead>";
						str+="<tbody>";
						
						for(var i = 0 ; i <serverData.length ; i++){
							
								var birth_day = serverData[i].age;
								var birthday = new Date(birth_day);
								var today = new Date();
								var age = today.getFullYear() - birthday.getFullYear();
								if(isNaN(age)){
									age=0;
								}
							str+="<tr>";
							str+="<td>"+serverData[i].name+"</td>";
							str+="<td>"+age+"</td>";
							str+="<td>"+serverData[i].gender+"</td>";
							str+="<td>"+serverData[i].lessonName+"</td>";
							str+="<td>"+serverData[i].lessonstartdate+" ~ "+serverData[i].lessonenddate+ "</td>";
							str+="<td>"+serverData[i].restdate+"</td>";
							str+="<td><a href='#' class='btn btn-default btn-xs btn-block' id = 'viewDetailBtn' lessonlistseq = '"+serverData[i].lessonlistseq+"' name = '"+serverData[i].name+"'>운동insert</a></td>"; 
							str+="</tr>";
						};
			
						str+="</tbody>";
						str+="</table>";
						str+="</div>";
						str+="</div>";
						
						$("#rightTable").html(str);
						
					}
				});
			}
		};
		
		function insertCustomerExcersise(lessonlistseq, name){
			var str = "";
			str += "<div class='col-lg-7 col-sm-7'> ";
			str += "<div class='heading-title'>";
			str += "<div class='heading-title'> <h2>"+name+"님의 운동Insert</h2> </div> ";
			str += "<fieldset class='mt-60'> <div class='row'> <div class='col-md-6 col-sm-6'> ";
			str += "<label for='billing_firstname'>LESSONNAME </label> <input id='billing_firstname' name='billing[firstname]' type='text' class='form-control required' /> </div> ";
			str += "<div class='col-md-6 col-sm-6'> <label for='billing_lastname'>LESSONTYPE </label>";
			str += "<select id='billing_state' name='billing[state]' class='form-control pointer required'> ";
			str += "<option value=''>Select...</option> <option value='1'>유산소</option> <option value='2'>근력</option>";
			str += "</select> </div> </div> ";
			str += "<div class='row'> <div class='col-md-6 col-sm-6'> <label for='billing_email'>EXERCISETARGET</label> ";
			str += "<select id='billing_state' name='billing[state]' class='form-control pointer required'> ";
			str += "<option value=''>Select...</option> <option value='arm'>팔</option> <option value='leg'>다리</option> </select> </div>";
			str += "<div class='col-md-6 col-sm-6'> <label for='billing_company'>LESSONCONTENT </label>";
			str += "<input id='billing_company' name='billing[company]' type='text' class='form-control' /> </div> </div>";
			str += "<div class='row'> <div class='col-lg-12'> <label for='billing_address1'>123123</label>";
			str += "<input id='billing_address1' name='billing[address][]' type='text' class='form-control required' placeholder='빈칸1' />";
			str += "<input id='billing_address2' name='billing[address][]' type='text' class='form-control mt-10' placeholder='빈칸2' /> </div> </div>";
			str += "<div class='row'> <div class='col-md-6 col-sm-6'> <label for='billing_city'>TRANINGCOUNT</label>";
			str += "<select id='billing_state' name='billing[state]' class='form-control pointer required'>";
			str += "<option value=''>Select...</option> <option value='5'>5</option> <option value='10'>10</option>";
			str += "<option value='15'>15</option> <option value='20'>20</option> <option value='25'>25</option>";
			str += "<option value='30'>30</option> </select> </div>";
			str += "<div class='col-md-6 col-sm-6'> <label for='billing_state'>TRANINGSET</label>";
			str += "<select id='billing_state' name='billing[state]' class='form-control pointer required'>";
			str += "<option value=''>Select...</option> <option value='1'>1</option> <option value='2'>2</option> ";
			str += "<option value='3'>3</option> <option value='4'>4</option> <option value='5'>5</option>";
			str += "</select> </div> </div> ";
			str += "<div class='row'> <div class='col-md-6 col-sm-6'> <label for='billing_zipcode'>TRANINGDONE</label>";	
			str += "<input id='billing_zipcode' name='billing[zipcode]' type='date' class='form-control required' /> </div>";
			str += "<div class='col-md-6 col-sm-6'> <label for='billing_country'>TRANINGDATE</label>";
			str += "<input id='billing_country' name='billing[country]' type='date' class='form-control pointer required'>";
			str += "</div> </div>";
			str += "<div class='row'> <div class='col-md-6 col-sm-6'> <label for='billing_phone'>123</label>";
			str += "<input id='billing_phone' name='billing[phone]' type='text' class='form-control required' /> </div>";
			str += "<div class='col-md-6 col-sm-6'> <label for='billing_fax'>123</label>";
			str += "<input id='billing_fax' name='billing[fax]' type='text' class='form-control' /> </div> </div> <hr />";
			str += "<div class='row'> </div>";
			str += "</fieldset> </div> ";
			var wnd = window.open("", "new window", "width=400,height=400");  
			wnd.document.write("<html><head></head><body>"+str+"</body></html>");  
		
		}
	
		function trainingList(){
			var str = "";
			str += "<c:if test='${sessionScope.roleSeq==1001}'>";
			str += "	<input id='insertLessonlistButton' type='button' value='listInsert' >"; 
			str += "</c:if>";
			$("#rightTable").html(str);
		
		}
		
		function insertLessonlist(){
			/* insert새창 */
/* 			var url = "asd.html";
			var wnd = window.open(url, "자식창", "width=400,height=400");  
			//wnd.document.write("<html><head></head><body>"+str+"</body></html>");  
 */
			
		}
		
		
		function selectOneTrainerLessonlist(){
			var trainerseq = ${trainerSeq};
			alert(trainerseq);
			$.ajax({
				url : "selectOneTrainerLessonlist",
				data : {
					trainerseq : trainerseq					
				},
				success : function(serverData){
					alert(serverData);
					
				}
				 
				
			})
			
			
		}
		
		
		
		</script>	
			
			
			
			
	</head>

	<body class="smoothscroll enable-animation">

	
	
		<div id="wrapper">

			<!-- Top Bar -->
			<div id="topBar">
				<div class="container">

					<!-- right -->
					<ul class="top-links list-inline float-right">
					
					<!-- 로그인에 따른 TEXT 출력 관리 -->
						<li class="text-welcome">
						<c:if test="${empty sessionScope.loginId }">
						<strong>손님</strong>, 지금 바로 로그인하세요!
						</c:if>
						<c:if test="${sessionScope.roleSeq==101 }">
						<strong>${sessionScope.loginId }님</strong>, 회원 로그인하셨습니다. 
						</c:if>
						<c:if test="${sessionScope.roleSeq==1001 }">
						<strong>${sessionScope.loginId }님</strong>, 트레이너 로그인하셨습니다. 
						</c:if>
						<c:if test="${sessionScope.roleSeq==1000 }">
						<strong>${sessionScope.loginId }님</strong>, 회원 로그인하셨습니다. 
						</c:if>
						<c:if test="${sessionScope.roleSeq==10001 }">
						<strong>${sessionScope.loginId }님</strong>, 관리자 로그인하셨습니다. 
						</c:if>
						</li>
						<li>
							<a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#"><i class="fa fa-user hidden-xs-down"></i> MY ACCOUNT</a>
							<ul class="dropdown-menu">
								<li><a tabindex="-1" href="moveUserPage"><i class="fa fa-history"></i> ORDER HISTORY</a></li>
								<li class="divider"></li>
								<li><a tabindex="-1" href="moveUserPage"><i class="fa fa-bookmark"></i> MY WISHLIST</a></li>
								<li><a tabindex="-1" href="moveUserPage"><i class="fa fa-edit"></i> MY REVIEWS</a></li>
								<li><a tabindex="-1" href="moveUserPage"><i class="fa fa-cog"></i> MY SETTINGS</a></li>
								<li class="divider"></li>
								<li><a tabindex="-1" href="moveUserPage"><i class="glyphicon glyphicon-off"></i> LOGOUT</a></li>
							</ul>
						</li>
						
						<!-- 로그인 유무에 따른 메뉴 변경 -->
						<li>
						<c:if test="${empty sessionScope.loginId }">
						<a href="moveLogin">LOGIN</a>
						</c:if>
						<c:if test="${!empty sessionScope.loginId }">
						<a href="Logout">LOGOUT</a>
						</c:if>
						</li>
						<li><a href="moveSignUp">REGISTER</a></li>
					</ul>

					<!-- left -->
					<ul class="top-links list-inline">
						<div class="social-icons float-right hidden-xs-down">
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook">
							<i class="icon-facebook"></i>
							<i class="icon-facebook"></i>
						</a>
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter">
							<i class="icon-twitter"></i>
							<i class="icon-twitter"></i>
						</a>
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-gplus" data-toggle="tooltip" data-placement="bottom" title="Google Plus">
							<i class="icon-gplus"></i>
							<i class="icon-gplus"></i>
						</a>
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-linkedin" data-toggle="tooltip" data-placement="bottom" title="Linkedin">
							<i class="icon-linkedin"></i>
							<i class="icon-linkedin"></i>
						</a>
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-flickr" data-toggle="tooltip" data-placement="bottom" title="Flickr">
							<i class="icon-flickr"></i>
							<i class="icon-flickr"></i>
						</a>
					</div>
					</ul>

				</div>
			</div>
			<div id="header" class="navbar-toggleable-md sticky clearfix">

				<!-- TOP NAV -->
				<header id="topNav">
					<div class="container">

						<!-- Mobile Menu Button -->
						<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
							<i class="fa fa-bars"></i>
						</button>

						<!-- BUTTONS -->
						<ul class="float-right nav nav-pills nav-second-main">

							<!-- SEARCH -->
							<li class="search">
								<a href="javascript:;">
									<i class="fa fa-search"></i>
								</a>
								<div class="search-box">
									<form action="page-search-result-1.html" method="get">
										<div class="input-group">
											<input type="text" name="src" placeholder="Search" class="form-control" />
											<span class="input-group-btn">
												<button class="btn btn-primary" type="submit">Search</button>
											</span>
										</div>
									</form>
								</div> 
							</li>
							<!-- /SEARCH -->

						</ul>
						<!-- /BUTTONS -->


						<!-- Logo -->
						<a class="logo float-left" href="home">
							<img src="resources/assets/images/_smarty/logo_dark.png" alt="" />
						</a>


						<div class="navbar-collapse collapse float-right nav-main-collapse">
							<nav class="nav-main">


								<ul id="topMain" class="nav nav-pills nav-main">
									<li class="dropdown"><!-- HOME -->
										<a class="dropdown-toggle" href="#">
											HOME
										</a>
										
									</li>
									<li class="dropdown"><!-- PAGES -->
										<a class="dropdown-toggle" href="#">
											트레이너
										</a>
										<ul class="dropdown-menu">
											<li><a href="moveApply"><i class="et-browser"></i> 트레이너 신청</a></li>
							
										</ul>	
									</li>
									<li class="dropdown active"><!-- FEATURES -->
										<a class="dropdown-toggle" href="Goboard">
											FEATURES
										</a>
																		
									</li>
								
									<c:if test="${sessionScope.roleSeq==101 }">
										<li class="dropdown mega-menu">
											<a class="dropdown-toggle" href="moveCustomer">
												회원 마이페이지
											</a>
										</li>
									</c:if>
									<c:if test="${sessionScope.roleSeq==1000 }">
										<li class="dropdown mega-menu">
											<a class="dropdown-toggle" href="moveCustomer">
												회원 마이페이지
											</a>
										</li>
									</c:if>
									<c:if test="${sessionScope.roleSeq==1001 }">
											<li class="dropdown mega-menu">
												<a class="dropdown-toggle" href="#">
													트레이너 마이페이지
												</a>
										</li>
									</c:if>
									
									
									<li class="dropdown mega-menu"><!-- SHORTCODES -->
										<a class="dropdown-toggle" href="#">
											고객센터
										</a>
									</li>
									<c:if test="${sessionScope.roleSeq==10001 }">
									<li class="dropdown mega-menu">
										<a class="dropdown-toggle" href="moveAdmin">
											관리자 페이지
										</a>
									</li>
									</c:if>

								</ul>
							</nav>
						</div>

					</div>
				</header>
				<!-- /Top Nav -->
			</div>
				

<section class="page-header page-header-xs">
				<div class="container">

					<!-- breadcrumbs -->
					<ol class="breadcrumb breadcrumb-inverse">
						<li><a href="#">Home</a></li>
						<li><a href="#">Pages</a></li>
						<li class="active">Felicia Doe</li>
					</ol><!-- /breadcrumbs -->

				</div>
			</section>
			<!-- /PAGE HEADER -->




			<!-- -->
			<section>
				<div class="container">
					<div class="row">

						<!-- LEFT -->
						<div class="col-lg-3 col-md-3 col-sm-4">
						
							<div class="thumbnail text-center">
								<img class="img-fluid" src="resources/img/bok.png" alt="" />
								<h2 class="fs-18 mt-10 mb-0">복복</h2>
								<h3 class="fs-11 mt-0 mb-10 text-muted">DEVELOPER</h3>
							</div>

		
							<!-- SIDE NAV -->
							<ul class="side-nav list-group mb-60" id="sidebar-nav">
								<li class="list-group-item"><a href="page-profile.html"><i class="fa fa-eye"></i> 프로필</a></li>
								
								<li id="selectCustomersList" class="list-group-item active"><a data-toggle="collapse" data-parent="#sidebar-nav" href="#collapse-1"><i class="fa fa-tasks"></i> 수강생목록확인</a></li>
									<!-- 누르면 아래로 리스트 생성되는 창 -->
									<!-- 
									<ul id="collapse-1" class="collapse">NOTE: "collapse in" to be open on page load
										<li id="selectCustomersList"><a href="#"><i class="fa fa-angle-right"></i>수강생목록확인</a></li>
										<li>
											<span class="badge badge-red">New</span>
											<a href="#"><i class="fa fa-angle-right"></i> SUMBENU 2</a>
										</li>
										<li class="active"><a href="#"><i class="fa fa-angle-right"></i> SUMBENU 3</a></li>
										<li><a href="#"><i class="fa fa-angle-right"></i> SUMBENU 4</a></li>
									</ul>
								 -->
								 
								<li id="trainingListButton" class="list-group-item"><a href="#"><i class="fa fa-comments-o"></i> 강좌리스트</a></li>
								
								<li class="list-group-item"><a href="page-profile-history.html"><i class="fa fa-history"></i> 연혁</a></li>
								<li class="list-group-item"><a href="page-profile-settings.html"><i class="fa fa-gears"></i> 설정</a></li>
								
							</ul>
							
						</div>


						<!-- RIGHT -->
						<div id="rightTable" class="col-lg-9 col-md-9 col-sm-8">
						
							
							<!-- 테이블시작 -->
			
								
								<!-- USER LIST -->
								<table class="table table-striped table-bordered table-hover m-top30 table-vertical-middle">
									<thead>
										<tr>
											<th>강좌명</th>
											<th>L.Name</th>
											<th>Stars</th>
											<th>Email / Username</th>
											<th>City</th>
											<th>Progress</th>
											<th>Share</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><img src="assets/images/_smarty/female.png" alt="" width="20"></td>
											<td>John</td>
											<td>Doe</td>
											<td><div class="rating rating-1 fs-13 fw-100"><!-- rating-0 ... rating-5 --></div></td>
											<td>john.doe@yourdomain.com</td>
											<td>New York</td>
											<td>
												<div class="progress progress-xxs mb-0">
													<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100" style="width: 66%; min-width: 2em;">
														<span class="sr-only">66% Complete</span>
													</div>
												</div>
											</td>
											<td>
												<ul class="list-inline m-0 fs-12">
													<li><a href="#" class="icon-facebook" data-toggle="tooltip" data-placement="top" title="Facebook"></a></li>
													<li><a href="#" class="icon-twitter" data-toggle="tooltip" data-placement="top" title="Twitter"></a></li>
													<li><a href="#" class="icon-gplus" data-toggle="tooltip" data-placement="top" title="Google Plus"></a></li>
													<li><a href="#" class="icon-linkedin" data-toggle="tooltip" data-placement="top" title="Linkedin"></a></li>
													<li><a href="#" class="icon-pinterest" data-toggle="tooltip" data-placement="top" title="Pinterest"></a></li>
												</ul>
											</td>
											<td><span class="badge badge-success">Active</span></td>
										</tr>

									</tbody>
								</table>
								<!-- /USER LIST -->

							</div>
			
			<!-- 테이블끝 -->
							
							
						</div>
					</div>
				</div>
			</section>
			<!-- / -->




	<footer id="footer" class="footer-light">
	<div class="container">

		<div class="row">
			
			<div class="col-md-8">

				<!-- Footer Logo -->
				<img class="footer-logo footer-2" src="resources/assets/images/_smarty/logo-footer.png" alt="" />

				<!-- Small Description -->
				<p>푸터라인 </p>

				<hr />

				<div class="row">
					<div class="col-md-6 col-sm-6">

						<!-- Newsletter Form -->
						<p class="mb-10">Subscribe to Our <strong>Newsletter</strong></p>

						<form action="php/newsletter.php".box-shadow- method="post">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								<input type="email" id="email" name="email" class="form-control" placeholder="Enter your Email" required="required">
								<span class="input-group-btn">
									<button class="btn btn-success" type="submit">Subscribe</button>
								</span>
							</div>
						</form>
						<!-- /Newsletter Form -->
					</div>

					<div class="col-md-6 col-sm-6 hidden-xs-down">

						<!-- Social Icons -->
						<div class="ml-50 clearfix">

							<p class="mb-10">Follow Us</p>
							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-facebook float-left" data-toggle="tooltip" data-placement="top" title="Facebook">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-twitter float-left" data-toggle="tooltip" data-placement="top" title="Twitter">
								<i class="icon-twitter"></i>
								<i class="icon-twitter"></i>
							</a>

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-gplus float-left" data-toggle="tooltip" data-placement="top" title="Google plus">
								<i class="icon-gplus"></i>
								<i class="icon-gplus"></i>
							</a>

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-linkedin float-left" data-toggle="tooltip" data-placement="top" title="Linkedin">
								<i class="icon-linkedin"></i>
								<i class="icon-linkedin"></i>
							</a>

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-rss float-left" data-toggle="tooltip" data-placement="top" title="Rss">
								<i class="icon-rss"></i>
								<i class="icon-rss"></i>
							</a>

						</div>
						<!-- /Social Icons -->

					</div>
					
				</div>

			</div>

			<div class="col-md-4">
				<h4 class="letter-spacing-1">PHOTO GALLERY</h4>
				
				<div class="footer-gallery lightbox" data-plugin-options='{"delegate": "a", "gallery": {"enabled": true}}'>
					<a href="resources/demo_files/images/1200x800/10-min.jpg">
						<img src="resources/demo_files/images/150x99/10-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="resources/demo_files/images/1200x800/19-min.jpg">
						<img src="resources/demo_files/images/150x99/19-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="resources/demo_files/images/1200x800/12-min.jpg">
						<img src="resources/demo_files/images/150x99/12-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="resources/demo_files/images/1200x800/13-min.jpg">
						<img src="resources/demo_files/images/150x99/13-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="resources/demo_files/images/1200x800/18-min.jpg">
						<img src="resources/demo_files/images/150x99/18-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="resources/demo_files/images/1200x800/15-min.jpg">
						<img src="resources/demo_files/images/150x99/15-min.jpg" width="106" height="70" alt="" />
					</a>
				</div>

			</div>

		</div>

	</div>

	<div class="copyright">
		<div class="container">
			<ul class="float-right m-0 list-inline mobile-block">
				<li><a href="#">Terms & Conditions</a></li>
				<li>&bull;</li>
				<li><a href="#">Privacy</a></li>
			</ul>
			&copy; All Rights Reserved, Company LTD
		</div>
	</div>
	</footer>
		<!-- SCROLL TO TOP -->
		<a href="#" id="toTop"></a>


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->

		<script type ="text/javascript" src="resources/videojs-contrib-hls.min.js"/></script>
	<script type ="text/javascript" src="resources/videojs-contrib-hls.js"/></script>
	<script type ="text/javascript" src="resources/video.min.js"/></script>
		
		<!-- JAVASCRIPT FILES -->
		<script>var plugin_path = 'resources/assets/plugins/';</script>
		<script src="resources/assets/plugins/jquery/jquery-3.3.1.min.js"></script>

		<!-- REVOLUTION SLIDER -->
		<script src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
		<script src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
		<script src="resources/assets/js/view/demo.revolution_slider.js"></script>

		<!-- SCRIPTS -->
		<script src="resources/assets/js/scripts.js"></script>
		
	</body>
</html>