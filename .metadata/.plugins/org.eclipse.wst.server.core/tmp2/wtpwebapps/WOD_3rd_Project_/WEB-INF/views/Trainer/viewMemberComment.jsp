<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-US">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title> WOD'sGYM </title>

    </style> 
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

		<!-- WEB FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="resources/assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/layout.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/color_scheme/blue.css" rel="stylesheet" type="text/css" id="color_scheme" />
		<script src="https://sdk.amazonaws.com/js/aws-sdk-2.283.1.min.js"></script>
		<script src="resources/aws.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script>		

	
		$(function(){
			var membercommentseq = $("#dataValue").val();
			
			printMemberComment(membercommentseq);
			
			$(document).on("click","#viewVideoBtn",function(){
				viewVideo(membercommentseq);
				
			});
		});
		
		function printMemberComment(membercommentseq){

			$.ajax({
				url:"selectOneMemberComment",
				data:{
					membercommentseq:membercommentseq
				},
				success:function(serverData){
					var str="";
					str+="<div class='fancy-form'>";
					str+="<textarea rows='5' class='form-control word-count' data-maxlength='200' data-info='textarea-words-info' id = 'commentArea' readonly='readonly'></textarea>";
					str+="<span class='fancy-hint fs-11 text-muted'>";
					str+="<strong>Advice!</strong> 더욱 상세한 상담을 원하시면 스케쥴 상단의 1:1 상담 기능을 이용하세요!";
					str+="<span class='float-right'>";
					str+="<span id='textarea-words-info'>0/200</span> Words";
					str+="</span>";
					str+="</span>";
					str+="</div>";
					str+="<a href='#' class='social-icon social-googleplay' id = 'viewVideoBtn' data-toggle='tooltip' data-placement='top' title='Video'>";
					str+="<i class='icon-googleplay'></i><i class='icon-googleplay'></i></a>";
					
					$("#printArea").html(str);
					$("#commentArea").val(serverData.comments);
					
				}
			
			});
		
		};
		
		 function getimgUrl2(albumName,buf)
		  {
		     var albumPhotosKey = encodeURIComponent(albumName) + '//';
		     var filebuf=[];
		     var cnt=0;
		     var photoKey;
		     var str="";
		     var comparator=albumName+'//';
		     s3.listObjects({Prefix: albumPhotosKey}, function(err, data) 
		     {
		       if (err) 
		         return alert('There was an error viewing your album: ' + err.message);
		   
		       var href = this.request.httpRequest.endpoint.href;
		       var bucketUrl = href + albumBucketName + '/';
		             
		       var photos = data.Contents.map(function(photo)
		       {
		         var photoKey = photo.Key;
		         var photoUrl = bucketUrl + encodeURIComponent(photoKey);
		        
		         imfiles[cnt]=photoUrl;
		         filebuf[cnt]=photoKey;
		         cnt++;
		        
		        }
		       );
		       var idx=0;
		       for(var i=0;i<imfiles.length;++i)
		       {
		          
		          for(var j=0;j<buf.length;++j)
		          {
		        	  
		              if(filebuf[i]==(comparator+buf[j])){
		            	  
		             var str="";
		             str+='<video class="" preload="none" width="700" height="355" controls autoplay>';
                     str+='<source src="'+imfiles[i]+'" type="video/mp4" />';
                     str+='</video>';
		             $("#videoArea").html(str);
		       
		              }
		          }

		       }   
		     })
		     
		     return result;
		  }
		 
		 
		 function viewVideo(membercommentseq){
		
			 $.ajax({
				 url:"selectMemberVideo",
				 data:{
					 membercommentseq:membercommentseq
				 },
				 success:function(serverData){
					 
					  var buf=[];
					  var albumName = "wod";
					  buf.push(serverData.savename);
			
					 getimgUrl2(albumName,buf);
					
				 }
				 
			 });
	
		 };
		 
		</script>
			
		
	</head>

	<body>
	
		<div class="padding-15">

			<div class="login-box">

					<header><i class="fa fa-users"></i> WOD'sGYM Customer Menu</header>
					
					<fieldset style=width:1200px;>
				
					<div class='row countTo-sm text-center'>
					<div class='col-md-8 col-sm-7'>
			 		<div class='box-static box-transparent box-bordered p-30'> 
			 		
					<div class='box-title mb-30'>
			

					<div class='box-title mb-30'>
					<h2 class='fs-20'>${sessionScope.loginId}님의 코멘트</h2>
					</div>
					
					<div id = "printArea"></div>
					
					</div>
					
					</div>
					
					<div id = "addMealArea"></div>
					
					<div id = "submitMealArea"></div>
					<fieldset>
					<div id = "videoArea">
		
		</div>
		</fieldset>
			
		</div>
		</div>
		</div>
		</div>
	
		 			</fieldset>
					
		<input type = "hidden" value = "${membercommentseq }" id = "dataValue">
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'resources/assets/plugins/';</script>
		<script type="text/javascript" src="resources/assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="resources/assets/js/app.js"></script>
	</body>
</html>