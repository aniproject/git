function trainerList() {
		var selectValue = "trainerList";

		$.ajax({
			url : "selectTrainer",
			data : {
				selectValue : selectValue
			},
			success : function(serverData) {
				var str = "";

				for (var i = 0; i < serverData.length; i++) {
					if (serverData[i].LESSONTYPE == "Bodybuilding") {
						str += "<li><a href='moveTrainerChannel?trainerSeq="
								+ serverData[i].TRAINERSEQ
								+ "' class = 'hoverTrainer' name = '"
								+ serverData[i].NAME + "' savename = '"
								+ serverData[i].SAVENAME
								+ "'><i class='et-browser'>";
						str += "</i>&emsp;" + serverData[i].NAME + "</a></li>";
						$("#bodybuildingListPrintArea").append(str);

					} else if (serverData[i].LESSONTYPE == "Diet") {
						str += "<li><a href='moveTrainerChannel?trainerSeq="
								+ serverData[i].TRAINERSEQ
								+ "' class = 'hoverTrainer' name = '"
								+ serverData[i].NAME + "' savename = '"
								+ serverData[i].SAVENAME
								+ "'><i class='et-browser'>";
						str += "</i>&emsp;" + serverData[i].NAME + "</a></li>";
						$("#dietListPrintArea").append(str);

					} else if (serverData[i].LESSONTYPE == "Yoga & Pilates") {
						str += "<li><a href='moveTrainerChannel?trainerSeq="
								+ serverData[i].TRAINERSEQ
								+ "' class = 'hoverTrainer' name = '"
								+ serverData[i].NAME + "' savename = '"
								+ serverData[i].SAVENAME
								+ "'><i class='et-browser'>";
						str += "</i>&emsp;" + serverData[i].NAME + "</a></li>";
						$("#yogaListPrintArea").append(str);

					}
					str = "";
				}

			}
		});
	}