<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>WOD</title>


<meta name="description" content="" />
<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

<!-- mobile settings -->
<meta name="viewport"
	content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

<!-- WEB FONTS : use %7C instead of | (pipe) -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700"
	rel="stylesheet" type="text/css" />

<!-- CORE CSS -->
<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<!-- REVOLUTION SLIDER -->
<link
	href="resources/assets/plugins/slider.revolution/css/extralayers.css"
	rel="stylesheet" type="text/css" />
<link href="resources/assets/plugins/slider.revolution/css/settings.css"
	rel="stylesheet" type="text/css" />

<!-- THEME CSS -->
<link href="resources/assets/css/essentials.css" rel="stylesheet"
	type="text/css" />
<link href="resources/assets/css/layout.css" rel="stylesheet"
	type="text/css" />

<!-- PAGE LEVEL SCRIPTS -->
<link href="resources/assets/css/header-1.css" rel="stylesheet"
	type="text/css" />
<link href="resources/assets/css/color_scheme/green.css"
	rel="stylesheet" type="text/css" id="color_scheme" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://sdk.amazonaws.com/js/aws-sdk-2.283.1.min.js"></script>
<script src="resources/aws.js"></script>
<script>
	var userseq = "";
	$(function() {

		//	$("#trainerListBtn").hover(function(){
		trainerList();

		//		});

		$("#fileUploadForm").submit(function() {
			var bucket = new AWS.S3({
				params : {
					Bucket : 'wodgym.tk'
				}
			});
			var fileChooser = document.getElementById('file');
			var file = fileChooser.files[0];
			var fileName = file.name;
			var albumKey = encodeURIComponent('wod') + '//';
			var fileKey = albumKey + fileName;
			alert(fileKey);
			if (file) {
				var params = {
					Key : fileKey,
					ContentType : file.type,
					Body : file,
					ACL : 'public-read' // 접근 권한
				};

				bucket.putObject(params, function(err, data) {
					alert("성공");
				});
			}
			return false;
		});
		$(document).on("mouseover", ".hoverTrainer", function() {
			var name = $(this).attr("name");
			var saveName = $(this).attr("savename");
			viewDetailTrainer(name, saveName);
		});

		$(document).on(
				"mouseout",
				".hoverTrainer",
				function() {
					$("#trainerName").html("");
					$("#trainerImg").attr("src",
							"resources/demo_files/images/new_menu.png");
				});

	});

	function trainerList() {
		var selectValue = "trainerList";

		$.ajax({
			url : "selectTrainer",
			data : {
				selectValue : selectValue
			},
			success : function(serverData) {
				var str = "";

				for (var i = 0; i < serverData.length; i++) {
					if (serverData[i].LESSONTYPE == "Bodybuilding") {
						str += "<li><a href='moveTrainerChannel?trainerSeq="
								+ serverData[i].TRAINERSEQ
								+ "' class = 'hoverTrainer' name = '"
								+ serverData[i].NAME + "' savename = '"
								+ serverData[i].SAVENAME
								+ "'><i class='et-browser'>";
						str += "</i>&emsp;" + serverData[i].NAME + "</a></li>";
						$("#bodybuildingListPrintArea").append(str);

					} else if (serverData[i].LESSONTYPE == "Diet") {
						str += "<li><a href='moveTrainerChannel?trainerSeq="
								+ serverData[i].TRAINERSEQ
								+ "' class = 'hoverTrainer' name = '"
								+ serverData[i].NAME + "' savename = '"
								+ serverData[i].SAVENAME
								+ "'><i class='et-browser'>";
						str += "</i>&emsp;" + serverData[i].NAME + "</a></li>";
						$("#dietListPrintArea").append(str);

					} else if (serverData[i].LESSONTYPE == "Yoga & Pilates") {
						str += "<li><a href='moveTrainerChannel?trainerSeq="
								+ serverData[i].TRAINERSEQ
								+ "' class = 'hoverTrainer' name = '"
								+ serverData[i].NAME + "' savename = '"
								+ serverData[i].SAVENAME
								+ "'><i class='et-browser'>";
						str += "</i>&emsp;" + serverData[i].NAME + "</a></li>";
						$("#yogaListPrintArea").append(str);

					}
					str = "";
				}

			}
		});
	}

	function viewDetailTrainer(name, saveName) {

		$("#trainerName").html(name);
		$("#trainerImg").attr("src", "resources/img/" + saveName);

	}
	var buf=[];
	var buf2=[];
	var buf3=[];
	buf[0]='pushzxcv.mp4';
	buf[2]='sim2.mp4';
	buf[3]='sim3.mp4';
	
	buf2[0]='으뜸.png';
	buf2[1]='아영.png';
	buf2[2]='혜랑.png';
	buf2[3]='누구.png';
	buf2[4]='gg.png';
	buf2[5]='몰라.png';
	buf3[0]='The Back Squat.mp4';
	buf3[1]='zzc.mp4';
	getUrlrecommend('wod',buf3);
	getUrl('wod',buf);
	getimgUrl('wodimg',buf2);

	function getHtml(template) {
		return template.join('\n');
	}

	// listAlbums();

	///스코롤링
	var videos = document.getElementsByTagName("video"), fraction = 0.8;
	function checkScroll() {

		for (var i = 0; i < videos.length; i++) {

			var video = videos[i];

			var x = video.offsetLeft, y = video.offsetTop, w = video.offsetWidth, h = video.offsetHeight, r = x
					+ w, //right
			b = y + h, //bottom
			visibleX, visibleY, visible;

			visibleX = Math.max(0, Math.min(w, window.pageXOffset
					+ window.innerWidth - x, r - window.pageXOffset));
			visibleY = Math.max(0, Math.min(h, window.pageYOffset
					+ window.innerHeight - y, b - window.pageYOffset));

			visible = visibleX * visibleY / (w * h);

			if (visible > fraction) {
				video.play();
			} else {
				video.pause();
			}

		}

	}

	//window.addEventListener('scroll', checkScroll, false);
	window.addEventListener('resize', checkScroll, false);
</script>

</head>
<body class="smoothscroll enable-animation">

	<div id="wrapper">

		<!-- Top Bar -->
		<div id="topBar">
			<div class="container">

				<!-- right -->
				<ul class="top-links list-inline float-right">

					<!-- 로그인에 따른 TEXT 출력 관리 -->
					<li class="text-welcome"><c:if
							test="${empty sessionScope.loginId }">
							<strong>손님</strong>, 지금 바로 로그인하세요!
						</c:if> <c:if test="${sessionScope.roleSeq==101 }">
							<strong>${sessionScope.loginId }님</strong>, 회원 로그인하셨습니다. 
						</c:if> <c:if test="${sessionScope.roleSeq==1001 }">
							<strong>${sessionScope.loginId }님</strong>, 트레이너 로그인하셨습니다. 
						</c:if> <c:if test="${sessionScope.roleSeq==1000 }">
							<strong>${sessionScope.loginId }님</strong>, 회원 로그인하셨습니다. 
						</c:if> <c:if test="${sessionScope.roleSeq==10001 }">
							<strong>${sessionScope.loginId }님</strong>, 관리자 로그인하셨습니다. 
						</c:if></li>
					<li><a class="dropdown-toggle no-text-underline"
						data-toggle="dropdown" href="#"><i
							class="fa fa-user hidden-xs-down"></i> MY ACCOUNT</a>
						<ul class="dropdown-menu">
							<li><a tabindex="-1" href="moveUserPage"><i
									class="fa fa-history"></i> ORDER HISTORY</a></li>
							<li class="divider"></li>
							<li><a tabindex="-1" href="moveUserPage"><i
									class="fa fa-bookmark"></i> MY WISHLIST</a></li>
							<li><a tabindex="-1" href="moveUserPage"><i
									class="fa fa-edit"></i> MY REVIEWS</a></li>
							<li><a tabindex="-1" href="moveUserPage"><i
									class="fa fa-cog"></i> MY SETTINGS</a></li>
							<li class="divider"></li>
							<li><a tabindex="-1" href="moveUserPage"><i
									class="glyphicon glyphicon-off"></i> LOGOUT</a></li>
						</ul></li>

					<!-- 로그인 유무에 따른 메뉴 변경 -->
					<li><c:if test="${empty sessionScope.loginId }">
							<a href="moveLogin">LOGIN</a>
						</c:if> <c:if test="${!empty sessionScope.loginId }">
							<a href="Logout">LOGOUT</a>
						</c:if></li>
					<li><a href="moveSignUp">REGISTER</a></li>
				</ul>

				<!-- left -->
				<ul class="top-links list-inline">
					<div class="social-icons float-right hidden-xs-down">
						<a href="#"
							class="social-icon social-icon-sm social-icon-transparent social-facebook"
							data-toggle="tooltip" data-placement="bottom" title="Facebook">
							<i class="icon-facebook"></i> <i class="icon-facebook"></i>
						</a> <a href="#"
							class="social-icon social-icon-sm social-icon-transparent social-twitter"
							data-toggle="tooltip" data-placement="bottom" title="Twitter">
							<i class="icon-twitter"></i> <i class="icon-twitter"></i>
						</a> <a href="#"
							class="social-icon social-icon-sm social-icon-transparent social-gplus"
							data-toggle="tooltip" data-placement="bottom" title="Google Plus">
							<i class="icon-gplus"></i> <i class="icon-gplus"></i>
						</a> <a href="#"
							class="social-icon social-icon-sm social-icon-transparent social-linkedin"
							data-toggle="tooltip" data-placement="bottom" title="Linkedin">
							<i class="icon-linkedin"></i> <i class="icon-linkedin"></i>
						</a> <a href="#"
							class="social-icon social-icon-sm social-icon-transparent social-flickr"
							data-toggle="tooltip" data-placement="bottom" title="Flickr">
							<i class="icon-flickr"></i> <i class="icon-flickr"></i>
						</a>
					</div>
				</ul>

			</div>
		</div>
		<div id="header" class="navbar-toggleable-md sticky clearfix">

			<!-- TOP NAV -->
			<header id="topNav">
				<div class="container">

					<!-- Mobile Menu Button -->
					<button class="btn btn-mobile" data-toggle="collapse"
						data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>

					<!-- BUTTONS -->
					<ul class="float-right nav nav-pills nav-second-main">

						<!-- SEARCH -->
						<li class="search"><a href="javascript:;"> <i
								class="fa fa-search"></i>
						</a>
							<div class="search-box">
								<form action="page-search-result-1.html" method="get">
									<div class="input-group">
										<input type="text" name="src" placeholder="Search"
											class="form-control" /> <span class="input-group-btn">
											<button class="btn btn-primary" type="submit">Search</button>
										</span>
									</div>
								</form>
							</div></li>
						<!-- /SEARCH -->

					</ul>
					<!-- /BUTTONS -->


					<!-- Logo -->
					<a class="logo float-left" href="home"> <img
						src="resources/assets/images/_smarty/logo.png" alt="" />
					</a>


					<div class="navbar-collapse collapse float-right nav-main-collapse">
						<nav class="nav-main">


							<ul id="topMain" class="nav nav-pills nav-main">
								<li class="dropdown">
									<!-- HOME --> <a class="dropdown-toggle" href="#"> HOME </a>

								</li>
								<li
									class="dropdown mega-menu nav-animate-fadeIn nav-hover-animate hover-animate-bounceIn">
									<!-- THEMATIC --> <a class="dropdown-toggle noicon" href="#">
										<span class="badge badge-danger float-right fs-11">New
											Trainer</span> <b>트레이너</b>
								</a>
									<ul class="dropdown-menu dropdown-menu-clean">
										<li>
											<div class="row">

												<div class="col-md-5th">
													<ul class="list-unstyled">
														<li><span>Bodybuilding</span></li>

														<div id="bodybuildingListPrintArea"></div>
													</ul>
												</div>

												<div class="col-md-5th">
													<ul class="list-unstyled">
														<li><span>Diet</span></li>
														<div id="dietListPrintArea"></div>

													</ul>
												</div>

												<div class="col-md-5th">
													<ul class="list-unstyled">
														<li><span>Yoga & Pilates</span></li>
														<div id="yogaListPrintArea"></div>

														<li class="divider"></li>
														<li><span class="fs-11 mt-0 pb-15 pt-15 text-info">여러분들도
																트레이너에 도전해보세요!</span></li>

														<li><a href="moveApply"><i class="et-browser"></i>
																트레이너 지원</a></li>
														<li><a href="moveTempChanel"><i
																class="et-browser"></i> 트레이너 뷰</a></li>
														<li class="divider"></li>
													</ul>
												</div>

												<div class="col-md-6 hidden-sm text-center">
													<div class="p-15 block">
														<img class="img-fluid"
															src="resources/demo_files/images/new_menu.png"
															id="trainerImg" alt="" />
													</div>
													<p
														class="menu-caption hidden-xs-down text-muted text-center"
														id="trainerName"></p>
												</div>

											</div>
										</li>
									</ul>
								</li>
								<!-- <li class="dropdown">PAGES
										<a class="dropdown-toggle" href="#" id = "trainerListBtn">
											트레이너
										</a>
									
										<ul class="dropdown-menu">
										<div id = "trainerListPrintArea">
						
										</div>
											<li><a href="moveApply"><i class="et-browser"></i> 트레이너 지원</a></li>
							
										</ul>	
										
										
										
										
									</li> -->
								<li class="dropdown active">
									<!-- FEATURES --> <a class="dropdown-toggle" href="jsoup">
										FEATURES </a>

								</li>

								<c:if test="${sessionScope.roleSeq==101 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle"
										href="moveCustomer"> 회원 마이페이지 </a></li>
								</c:if>
								<c:if test="${sessionScope.roleSeq==1000 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle"
										href="moveCustomer"> 회원 마이페이지 </a></li>
								</c:if>
								<c:if test="${sessionScope.roleSeq==1001 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle"
										href="moveTrainerChannel2"> 트레이너 마이페이지 </a></li>
								</c:if>


								<li class="dropdown mega-menu">
									<!-- SHORTCODES --> <a class="dropdown-toggle" href="goQnA">
										고객센터 </a>
								</li>
								<c:if test="${sessionScope.roleSeq==10001 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle"
										href="moveAdmin"> 관리자 페이지 </a></li>
								</c:if>

							</ul>
						</nav>
					</div>

				</div>
			</header>
			<!-- /Top Nav -->
		</div>




		<!-- REVOLUTION SLIDER -->
		<section id="slider"
			class="slider fullwidthbanner-container roundedcorners">

			<div class="fullwidthbanner" data-height="550"
				data-navigationStyle="">
				<ul class="hide">
					<!-- SLIDE -->

					<li data-transition="fade" data-slotamount="1"
						data-masterspeed="1000" data-saveperformance="off"
						data-title="Slide">

						<div class="tp-caption tp-fade fadeout fullscreenvideo" data-x="0"
							data-y="0" data-speed="1000" data-start="1100"
							data-easing="Power4.easeOut" data-elementdelay="0.01"
							data-endelementdelay="0.1" data-endspeed="1500"
							data-endeasing="Power4.easeIn" data-autoplay="true"
							data-autoplayonlyfirsttime="false" data-nextslideatend="true"
							data-volume="mute" data-forceCover="1" data-aspectratio="16:9"
							data-forcerewind="on" style="z-index: 2;">

							<div class="tp-dottedoverlay twoxtwo">
								<!-- dotted overlay -->
							</div>
							<video class="" preload="none" width="100%" height="93%" controls>
								<source src="resources/push.mp4" type="video/mp4" />
							</video>

						</div>
					</li>
					<li data-transition="fade" data-slotamount="1"
						data-masterspeed="1000" data-saveperformance="off"
						data-title="Slide">

						<div class="tp-caption tp-fade fadeout fullscreenvideo" data-x="0"
							data-y="0" data-speed="1000" data-start="1100"
							data-easing="Power4.easeOut" data-elementdelay="0.01"
							data-endelementdelay="0.1" data-endspeed="1500"
							data-endeasing="Power4.easeIn" data-autoplay="true"
							data-autoplayonlyfirsttime="false" data-nextslideatend="true"
							data-volume="mute" data-forceCover="1" data-aspectratio="16:9"
							data-forcerewind="on" style="z-index: 2;">

							<div class="tp-dottedoverlay twoxtwo">
								<!-- dotted overlay -->
							</div>
							<video class="" preload="none" width="100%" height="93%" controls>
								<source src="resources/workout.mp4" type="video/mp4" />
							</video>

						</div>
					</li>
					<!-- SLIDE -->
					<li data-transition="fade" data-slotamount="1"
						data-masterspeed="1000" data-saveperformance="off"
						data-title="Slide">

						<div class="tp-caption tp-fade fadeout fullscreenvideo" data-x="0"
							data-y="0" data-speed="1000" data-start="1100"
							data-easing="Power4.easeOut" data-elementdelay="0.01"
							data-endelementdelay="0.1" data-endspeed="1500"
							data-endeasing="Power4.easeIn" data-autoplay="true"
							data-autoplayonlyfirsttime="false" data-nextslideatend="true"
							data-volume="mute" data-forceCover="1" data-aspectratio="16:9"
							data-forcerewind="on" style="z-index: 2;">

							<div class="tp-dottedoverlay twoxtwo">
								<!-- dotted overlay -->
							</div>
							<video class="" preload="none" width="100%" height="93%" controls>
								<source src="resources/push.mp4" type="video/mp4" />
							</video>

						</div>
					</li>

					<!-- SLIDE -->
					<li data-transition="fade" data-slotamount="1"
						data-masterspeed="1000" data-saveperformance="off"
						data-title="Slide">

						<div class="tp-caption tp-fade fadeout fullscreenvideo" data-x="0"
							data-y="0" data-speed="1000" data-start="1100"
							data-easing="Power4.easeOut" data-elementdelay="0.01"
							data-endelementdelay="0.1" data-endspeed="1500"
							data-endeasing="Power4.easeIn" data-autoplay="true"
							data-autoplayonlyfirsttime="false" data-nextslideatend="true"
							data-volume="mute" data-forceCover="1" data-aspectratio="16:9"
							data-forcerewind="on" style="z-index: 2;">

							<div class="tp-dottedoverlay twoxtwo">
								<!-- dotted overlay -->
							</div>
							<video class="" preload="none" width="100%" height="93%" controls>
								<source src="resources/chest.mp4" type="video/mp4" />
							</video>

						</div>
					</li>

					<li data-transition="fade" data-slotamount="1"
						data-masterspeed="1000" data-saveperformance="off"
						data-title="Slide">

						<div class="tp-caption tp-fade fadeout fullscreenvideo" data-x="0"
							data-y="0" data-speed="1000" data-start="1100"
							data-easing="Power4.easeOut" data-elementdelay="0.01"
							data-endelementdelay="0.1" data-endspeed="1500"
							data-endeasing="Power4.easeIn" data-autoplay="true"
							data-autoplayonlyfirsttime="false" data-nextslideatend="true"
							data-volume="mute" data-forceCover="1" data-aspectratio="16:9"
							data-forcerewind="on" style="z-index: 2;">

							<div class="tp-dottedoverlay twoxtwo">
								<!-- dotted overlay -->
							</div>
							<video class="" preload="none" width="100%" height="93%" controls>
								<source src="resources/authon.mp4" type="video/mp4" />
							</video>

						</div>
					</li>
					<li><a href="#" id="video-volume"><i
							class="fa fa-volume-down"></i></a> <iframe width="100%" height="100%"
							src="https://www.youtube.com/embed/WwwAdze4HDA?autoplay=1"
							frameborder="0"
							allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
							allowfullscreen></iframe>

						<div class="display-table">
							<div class="display-table-cell vertical-align-middle">
								<div class="container">
									<div class="slider-featured-text text-center"></div>
								</div>
							</div>
						</div></li>


					<!-- SLIDE  -->
					<li data-transition="random" data-slotamount="1"
						data-masterspeed="1000" data-saveperformance="off"
						data-title="Slide"><img
						src="resources/assets/images/_smarty/1x1.png"
						data-lazyload="resources/demo_files/images/1200x800/mainimage1.png"
						alt="" data-bgposition="center center" data-kenburns="on"
						data-duration="10000" data-ease="Linear.easeNone" data-bgfit="100"
						data-bgfitend="110" />

						<div class="tp-caption very_large_text lfb ltt tp-resizeme"
							data-x="right" data-hoffset="-100" data-y="center"
							data-voffset="-100" data-speed="600" data-start="800"
							data-easing="Power4.easeOut" data-splitin="none"
							data-splitout="none" data-elementdelay="0.01"
							data-endelementdelay="0.1" data-endspeed="500"
							data-endeasing="Power4.easeIn"></div>

						<div class="tp-caption medium_light_white lfb ltt tp-resizeme"
							data-x="right" data-hoffset="-110" data-y="center"
							data-voffset="10" data-speed="600" data-start="900"
							data-easing="Power4.easeOut" data-splitin="none"
							data-splitout="none" data-elementdelay="0.01"
							data-endelementdelay="0.1" data-endspeed="500"
							data-endeasing="Power4.easeIn">

							<span style="font-size: 24px; font-weight: 400;"></span>
						</div></li>

				</ul>
				<div class="tp-bannertimer"></div>
			</div>
		</section>
		<!-- /REVOLUTION SLIDER -->


	</div>


	<section id="multipage" class="b-0">
		<div class="container">

			<div class="heading-title heading-border mb-60">
				<h2>
					<span><span class="countTo" data-speed="2000" id="pagenum"></span>+</span> 오늘의
					강의
				</h2>
				<p class="font-lato fs-17">최신강의 모아보기</p>
			</div>
			<div class="row text-center mb-80 mt-30 wow fadeIn" id="home">

						
			</div>
			<div class="heading-title heading-border mb-60">
				<h2>
					<span><span class="countTo" data-speed="2000" id="pagenum2"></span>+</span> 추천
					강의
				</h2>
				<p class="font-lato fs-17">추천강의 모아보기</p>
			</div>
			<div class="row text-center mb-80 mt-30 wow fadeIn" id="recommend">

						
			</div>
			
		</div>
		<div class="container">
			<div class="heading-title heading-border">
				<h3>
					오늘의 추천 트레이너 <span>Details</span>
				</h3>
			</div>
			<div class="owl-carousel featured m-0 owl-padding-10" id="trainerrec"
				data-plugin-options='{"singleItem": false, "stopOnHover":false, "autoPlay":4000, "autoHeight": false, "navigation": true, "pagination": false}'>
				<!-- item -->
				

			</div>
		</div>
	</section>



	<footer id="footer" class="footer-light">
		<div class="container">

			<input id="photoupload" type="file">
			<button id="addphoto" onclick="addPhoto('wod')">업로드</button>
			<div id="app"></div>
			<div class="row">

				<div class="col-md-8">

					<!-- Footer Logo -->
					<img class="footer-logo footer-2"
						src="assets/images/_smarty/logo-footer.png" alt="" />

					<!-- Small Description -->
					<p>푸터라인</p>

					<hr />

					<div class="row">
						<div class="col-md-6 col-sm-6">

							<!-- Newsletter Form -->
							<p class="mb-10">
								Subscribe to Our <strong>Newsletter</strong>
							</p>

							<form action="php/newsletter.php" .box-shadow- method="post">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="fa fa-envelope"></i></span> <input type="email" id="email"
										name="email" class="form-control"
										placeholder="Enter your Email" required="required"> <span
										class="input-group-btn">
										<button class="btn btn-success" type="submit">Subscribe</button>
									</span>
								</div>
							</form>
							<!-- /Newsletter Form -->
						</div>

						<div class="col-md-6 col-sm-6 hidden-xs-down">

							<!-- Social Icons -->
							<div class="ml-50 clearfix">

								<p class="mb-10">Follow Us</p>
								<a href="#"
									class="social-icon social-icon-sm social-icon-transparent social-facebook float-left"
									data-toggle="tooltip" data-placement="top" title="Facebook">
									<i class="icon-facebook"></i> <i class="icon-facebook"></i>
								</a> <a href="#"
									class="social-icon social-icon-sm social-icon-transparent social-twitter float-left"
									data-toggle="tooltip" data-placement="top" title="Twitter">
									<i class="icon-twitter"></i> <i class="icon-twitter"></i>
								</a> <a href="#"
									class="social-icon social-icon-sm social-icon-transparent social-gplus float-left"
									data-toggle="tooltip" data-placement="top" title="Google plus">
									<i class="icon-gplus"></i> <i class="icon-gplus"></i>
								</a> <a href="#"
									class="social-icon social-icon-sm social-icon-transparent social-linkedin float-left"
									data-toggle="tooltip" data-placement="top" title="Linkedin">
									<i class="icon-linkedin"></i> <i class="icon-linkedin"></i>
								</a> <a href="#"
									class="social-icon social-icon-sm social-icon-transparent social-rss float-left"
									data-toggle="tooltip" data-placement="top" title="Rss"> <i
									class="icon-rss"></i> <i class="icon-rss"></i>
								</a>

							</div>
							<!-- /Social Icons -->

						</div>

					</div>

				</div>

				<div class="col-md-4">
					<h4 class="letter-spacing-1">PHOTO GALLERY</h4>

					<div class="footer-gallery lightbox"
						data-plugin-options='{"delegate": "a", "gallery": {"enabled": true}}'>
						<a href="resources/demo_files/images/1200x800/10-min.jpg"> <img
							src="resources/demo_files/images/150x99/10-min.jpg" width="106"
							height="70" alt="" />
						</a> <a href="resource/demo_files/images/1200x800/19-min.jpg"> <img
							src="resources/demo_files/images/150x99/19-min.jpg" width="106"
							height="70" alt="" />
						</a> <a href="resources/demo_files/images/1200x800/12-min.jpg"> <img
							src="resources/demo_files/images/150x99/12-min.jpg" width="106"
							height="70" alt="" />
						</a> <a href="resources/demo_files/images/1200x800/13-min.jpg"> <img
							src="resources/demo_files/images/150x99/13-min.jpg" width="106"
							height="70" alt="" />
						</a> <a href="resources/demo_files/images/1200x800/18-min.jpg"> <img
							src="resources/demo_files/images/150x99/18-min.jpg" width="106"
							height="70" alt="" />
						</a> <a href="resources/demo_files/images/1200x800/15-min.jpg"> <img
							src="resources/demo_files/images/150x99/15-min.jpg" width="106"
							height="70" alt="" />
						</a>
					</div>

				</div>

			</div>

		</div>

		<div class="copyright">
			<div class="container">
				<ul class="float-right m-0 list-inline mobile-block">
					<li><a href="#">Terms & Conditions</a></li>
					<li>&bull;</li>
					<li><a href="#">Privacy</a></li>
				</ul>
				&copy; All Rights Reserved, Company LTD
			</div>
		</div>
		<form id="fileUploadForm" method="post" enctype="multipart/form-data">
			<input type="file" name="file" id="file" value="dataFile" required="">
			<input type="submit" value="Go" />
		</form>
	</footer>
	<!-- SCROLL TO TOP -->
	<a href="#" id="toTop"></a>


	<!-- PRELOADER -->
	<div id="preloader">
		<div class="inner">
			<span class="loader"></span>
		</div>
	</div>
	<!-- /PRELOADER -->


	<!-- JAVASCRIPT FILES -->
	<script>
		var plugin_path = 'resources/assets/plugins/';
	</script>
	<script src="resources/assets/plugins/jquery/jquery-3.3.1.min.js"></script>

	<!-- REVOLUTION SLIDER -->
	<script
		src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
	<script
		src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
	<script src="resources/assets/js/view/demo.revolution_slider.js"></script>
	<!-- SCRIPTS -->
	<script src="resources/assets/js/scripts.js"></script>
	<!-- 	<div class='js-layer  layer  hidden' id ='viewDetail'>zzzzzz</div> -->
</body>
</html>