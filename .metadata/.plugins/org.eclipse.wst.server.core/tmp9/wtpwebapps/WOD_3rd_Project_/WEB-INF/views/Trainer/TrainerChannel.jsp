<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
 
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Smarty - Multipurpose + Admin</title>
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- REVOLUTION SLIDER -->
		<link href="resources/assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css" />

		<!-- THEME CSS -->
		<link href="resources/assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="resources/assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
		<link href="resources/assets/css/color_scheme/blue.css" rel="stylesheet" type="text/css" />>
		<link href="resources/assets/css/pack-realestate.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/custom-forms-v2.css" rel="stylesheet" type="text/css" />	
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		
		<script>
		
		var trainerseq = ${trainerSeq};
		var channelseq;
		// 트레이너 본인이 채널에 입장했는지 확인  
		var isTrainer = ${isTrainer};  //1은 트레이너본인 && 0은 본인아님   (의도 == 버튼생성을 위해서)
		var lessonlistseq;
		
		$(function(){
			selectChannel();
			selectTrainerProfile(); // 프로필을 불러옴
			alert("isTrainer = " + isTrainer);
			
			var videos = document.getElementsByTagName("video"), fraction = 0.8;
			
			$("#selectCustomersList").on("click",selectMyCustomers);
			
			$("#trainingListButton").on("click",TrainerLessonlistPrint); //트레이너의 강좌목록 Select
			
			$(document).on("click","#insertNewLessonlist",insertNewLessonlist); //NEW강좌개설(새창)
			
			$("#profileButton").on("click",selectTrainerProfile); //프로필 클릭시 프로필 불러옴
			
			$(document).on("click","#selectTrainerLessonlist",function(){ //프로필에 영상목록 내용
				alert("REVIEW");
			});
			
			$(document).on("click", "#deleteLessonlist", deleteLessonlist); 
			
			//TrainerComment 수정 및 입력   /// 공백 또는 입력하지 않은경우 처리해야함 --- 최문석 0919 1021
			$(document).on("click", "#updateTrainerCommnetButton", updateTrainerComment);
			
			//TrainerComment 취소
			$(document).on("click", "#cancleTrainerCommnetButton", function(){
				$("#trainerCommentText").val("");
			})
			
			$(document).on("click","#detailLesson",saveLessonlistseq); //강좌목록의 상세페이지 
			
			$(document).on("click","#buyLesson",buyLesson);//강좌를 결제(구매)  아직 결제API미구현 클릭시 바로구매
			
			$(document).on("click","#chatBtn",function(){
				var receiveseq = $(this).attr("userseq");
				var userseq = ${sessionScope.loginSeq};
				openChat(userseq,receiveseq);
				
			});
			
			$("#MessageListBtn").click(function(){
				alert("Zz");
				var userseq = ${sessionScope.loginSeq};
				
				MessageList(userseq);
				
			});
			
			$(document).on("click", "#clickLike", like);	//좋아요
			
			
		})
		
		//좋아요
		function like(){
			alert("like");
			
			$.ajax({
				url : "choiceLike",
				type : "post",
				data : {
					channelseq : channelseq
				},
				success : function(serverData){
					alert("히얼 : : :  " + serverData);
					
				}
				
			})
			 
			 
		}
		
		
		
		
		//결제
		function buyLesson(){
			alert("buyLesson");
			var lessonlistseq = $(this).attr("data-lessonlistseq");
			
			///유효성 검사를 넣어야함
			
			$.ajax({
				url : "paymentLesson",
				type : "post",
				data : {
					lessonlistseq : lessonlistseq,
					trainerseq : trainerseq
				},
				success : function(serverData){
					alert("결제 결과 = " + serverData);
					
				}
			})
			
			
		}
		
		
		
		//TrainerComment를 수정 및 입력
		function updateTrainerComment(){
			var trainercomment = $("#trainerCommentText").val();
			$.ajax({
				url : "updateTrainerComment",
				type : "post",
				data : {
					trainerseq : trainerseq,
					trainercomment : trainercomment
				},
				success : function(serverData){
					alert("결과 = " + serverData);
				}
			})
		}
		
		
		//코멘트 insert
		function insertDetailLessonComment(){
			var userseq = ${sessionScope.loginSeq};
			var content = $("#comment").val();
			
			$.ajax({
				url : "insertDetailLessonComment",
				type : "post",
				data : {
					lessonlistseq : lessonlistseq,
					userseq : userseq,
					content : content
				},
				success : function(serverData){
					if(serverData == "success"){
						alert("insertSuccess");
						detailLesson();
					}else{
						alert("insertFail");
					}
				}
			})
			
		}
		
		//lessonlistseq를 변수값에 저장
		function saveLessonlistseq(){
			lessonlistseq = $(this).attr("data-lessonlistseq");
			detailLesson();
		}
		
		
		//강좌목록의 강좌의 상세보기
		function detailLesson(){
			alert("detailLesson");
			alert(lessonlistseq);
			var userseq = ${sessionScope.loginSeq};
			titlePrint(); //강좌목록에서 상세들어가서 타이틀부분을 출력
			commentPrint();//강좌목록에서 상세들어가서 코멘트부분을 출력
			
			
			 //강좌목록에서 상세들어가서 타이틀부분을 출력
			 function titlePrint(){
				alert("titlePrint");
				$.ajax({
					url : "selelctDetailLesson",
					data : {
						lessonlistseq : lessonlistseq
					},
					success : function(serverData){
						var str = "";
						
						for(var i = 0; i<serverData.length; i++){
							str +="<div class='row'>";
							str +="<div class='col-md-11 col-sm-9'>";
							str+='<h4><b>'+serverData[i].lessontype+'</b>'+serverData[i].lessonname+'</h4>';
							str +='<div class="container">';
							str +='<div class="clearfix mb-60">';
							str+='<div class="embed-responsive embed-responsive-16by9">';
							str+='<video class="" preload="none" width="800" height="455" controls autoplay>';
							str+='<source src="resources/push.mp4" type="video/mp4" />';
							str+='</video>';
							str+='</div>';
							str+='</div>';
									//<!-- /VIDEO -->								
							str +="<p>#" + serverData[i].categoryname + "</p>";					
							str +="<p> "+ serverData[i].lessoncontent + "</p>";
							$("#printTable2").html(str);	
							
							
							
							str +="<p> lessonlike = " + serverData[i].lessonlike + "</p>";
							str +="<p> lessonrecommend = " + serverData[i].lessonrecommend + "</p>";
							str +="<div class='divider divider-dotted'><!-- divider --></div>";
							str +="<div class='clearfix mt-30'>";
							str +="<span class='float-left mt-6 bold hidden-xs-down'>";
							str +="Share Post: ";
							str +="</span>";
						
							str +="</a>";
							str +="</div>";
							str +="</div>";
						}
								
						
						
					}
				})
				
			}
				
			
			//강좌목록에서 상세들어가서 코멘트부분을 출력 //수정해야함
			function commentPrint(){
				alert("commentPrint 넘어옴");
				$.ajax({
					url : "selectCommentList",
					data : {
						lessonlistseq : lessonlistseq
					},
					success : function(serverData){
						alert("결과값");
						alert(serverData.length);
						
						var str = "";
					
						//코멘트부분
						
				
						str +="<div id='comments' class='comments'>";
						str +="<h4 class='page-header mb-60 fs-20'>";
						str +="<span>3</span> COMMENTS";
						str +="</h4>";
						str +="<div class='comment-item'>";
						str +="<span class='user-avatar'>";
						str +="<img class='media-object' src='assets/images/_smarty/avatar.png' width='64' height='64' alt=''>";
						str +="</span>";
						
						str +="<div class='media-body'>";
						str +="<a href='#commentForm' class='scrollTo comment-reply'>reply</a>";
						str +="<h4 class='media-heading bold'>Melissa Doe</h4>";
						str +="<small class='block'>June 29, 2014 - 11:23</small>";
						str +="Proin eget tortor risus. Cras ultricies ligula sed magna dictum porta. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. ";
						
						
						str +="<div class='media'>";
						str +="<span class='user-avatar'>";
						str +="<img class='media-object' src='assets/images/_smarty/avatar.png' width='64' height='64' alt=''>";
						str +="</span>";
						str +="<div class='media-body'>";
						str +="<h4 class='media-heading bold'>Peter Doe</h4>";
						str +="<small class='block'>June 29, 2014 - 11:23</small>";
						str +="Proin eget tortor risus. Cras ultricies ligula sed magna dictum porta. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. ";
						str +="</div>";
						str +="</div>";
						str +="</div>";
						str +="</div>";
						str +="<h4 class='page-header fs-20 mb-60 mt-100'>";
						str +="LEAVE A <span>COMMENT</span>";
						str +="</h4>";
						//str +="<form action='#' method='post'>";
						str +="<div class='row'>";
						str +="<div class='col-md-12'>";
						str +="<label>COMMENT</label>";
						str +="<textarea required='required' maxlength='5000' rows='5' class='form-control' name='comment' id='comment'> asd</textarea>";
						str +="</div>";
						str +="</div>";
						str +="<div class='row'>";
						str +="<div class='col-md-12'>";
						str +="<button class='btn btn-3d btn-lg btn-reveal btn-black' id='insertCommentButton'>";
						str +="<i class='fa fa-check'></i>";
						str +="<span>SUBMIT MESSAGE </span>";
						str +="</button>";
						str +="</div>";
						str +="</div>";
						//str +="</form>";
						str +="</div>";
						str +="</div>";
						str +="<div class='col-md-3 col-sm-3'>";
						str +="</div>";
					
						
						$("#CommentArea").append(str);
						
						// 입력버튼
						$("#insertCommentButton").on("click",function(){
							alert("insertDetailLessonComment");
							insertDetailLessonComment();
							
						})
						
					}
				})
				
				
				
			}
			
			
			
			 
				
				
				
				
			
		}
		
		
		
		// trainerseq로 channel정보를 가져옴
		function selectChannel(){
			$.ajax({
				url : "selectChannel",
				data : {
					trainerseq : trainerseq
				},
				success : function(serverData){
					channelseq = serverData.channelseq;
				}
				
				
			})
			
		}
		
		
		//수강생 목록보기   
		function selectMyCustomers(){

			$("#printTable2").html("");
			if (isTrainer==1) {
				$.ajax({
					url:"selectTrainingCustomers",
					success:function(serverData){
						
						var str = "";
						$("#CommentArea").html(str);
						$("#PrintTable2").html(str);
						$("#insertTable").html(str);
						str+='<div class="col-lg-12">';
						str+="<div id='panel-2' class='panel panel-default'>";
						str+="<div class='panel-heading'>";
						str+="<span class='title elipsis'>";
						str+="<strong>수강생LIST</strong>";
						str+="</span>";
						str+="<ul class='nav nav-tabs pull-right'>";
						/* str+="<li class='active'>";
						str+="<a href='#ttab1_nobg' data-toggle='tab'>ALL</a>";
						str+="</li>";
						str+="<li class=''>";
						str+="<a href='#ttab2_nobg' data-toggle='tab'>만료임박회원</a>";
						str+="</li>"; */
			
						str+="<div class='col-md-30'>";
						str+="<select class='form-control pointer' name='contact[department]''>";
						str+="<option value=''> Select </option>";
						str+="<option value='Marketing'>Marketing</option>";
						str+="</select>";
						str+="</div>";
			
						str+="</ul>";
						str+="</div>";
						str+="<div class='panel-body'>";
						str+="<div class='tab-content transparent'>";
						str+="<div id='ttab1_nobg' class='tab-pane active'>";
						str+="<div class='table-responsive'>";
						str+="<table class='table table-striped table-hover table-bordered'>";
						str+="<thead>";
						str+="<tr>";
						str+="<th>성함</th>";
						str+="<th>나이</th>";
						str+="<th>성별</th>";
						str+="<th>강의명</th>";
						str+="<th>수강기간</th>";
						str+="<th>남은수강일</th>";
						str+="<th>Calendar</th>";
						str+="<th>상담</th>";
						str+="</tr>";
						str+="</thead>";
						str+="<tbody>";
						
						for(var i = 0 ; i <serverData.length ; i++){
							
								var birth_day = serverData[i].AGE;
								var birthday = new Date(birth_day);
								var today = new Date();
								var age = today.getFullYear() - birthday.getFullYear();
								if(isNaN(age)){
									age=0;
								}
								
								
							str+="<tr>";
							str+="<td>"+serverData[i].NAME+"</td>";
							str+="<td>"+age+"</td>";
							str+="<td>"+serverData[i].GENDER+"</td>";
							str+="<td>"+serverData[i].LESSONNAME+"</td>";
							str+="<td>"+serverData[i].LESSONSTARTDATE+" ~ "+serverData[i].LESSONENDDATE+ "</td>";
							str+="<td>"+serverData[i].RESTDATE+"</td>";
							str+="<td><a href='moveCalendar?memberseq="+serverData[i].MEMBERSEQ+"&trainerSeq=0' id = 'viewDetailBtn'><i class='ico-rounded ico-hover icon-calendar'></i></a></td>"; 
							str+="<td><i class='ico-rounded ico-hover icon-chat' id = 'chatBtn' userseq='"+serverData[i].USERSEQ+"'></i></td>";
							str+="</tr>";
						};
			
						str+="</tbody>";
						str+="</table>";
						str+="</div>";
						str+="</div>";
						str+='</div>';
						$("#printTable").html(str);
					}
				});
			}
		};
		
		
		
		//  강좌리스트 클릭시  트레이너의 강좌목록이 출력됨
		function TrainerLessonlistPrint(){
				$.ajax({
					url : "selectOneTrainerLessonlist",
					data : {
						trainerseq : trainerseq					
					},
					success : function(serverData){
						var str = "";
						for(var i = 0; i<serverData.length; i++){
								str += '<div class="col-12 col-md-4">';	
								str += '<div class="property-item property-item-box">';	
								str +='<div class="property-ribbon">';
								str+='<span class="bg-blue text-white">-10% OFF</span>';
								str+='</div>';
								str += '<a class="property-image"   id="detailLesson" data-lessonlistseq="'+serverData[i].lessonlistseq+'"href="#">';
								if(serverData[i].lessonrecommend==0)
									str+='<span class="rating rating-1 fs-17 text-white"><!-- rating-1 ... rating-5 --></span>';
								if(serverData[i].lessonrecommend==1)
									str+='<span class="rating rating-2 fs-17 text-white"><!-- rating-1 ... rating-5 --></span>';
								if(serverData[i].lessonrecommend==2)
									str+='<span class="rating rating-3 fs-17 text-white"><!-- rating-1 ... rating-5 --></span>';
								if(serverData[i].lessonrecommend==3)
									str+='<span class="rating rating-4 fs-17 text-white"><!-- rating-1 ... rating-5 --></span>';
								if(serverData[i].lessonrecommend==4)
									str+='<span class="rating rating-5 fs-17 text-white"><!-- rating-1 ... rating-5 --></span>';
								str += '<img class="img-fluid lazy rad-4" src="resources/img/sumexam.png"  alt="">';
								str+='</a>';
								
								str+='<h4 class="property-item-price">';
								str+='<strong class="text-primary">$';
								str+=serverData[i].lessonprice;
								str+='</strong>';
								str+='<small class="float-right text-danger">강의명:';
								str+=serverData[i].lessonname;
								str+='</small>';
								str+='</h4>';

								
								if(isTrainer == 1){
									str+='<input type="checkbox" class="deleteCheck" data-lessonname=' + serverData[i].lessonname +'name="deleteValues" value='+serverData[i].lessonlistseq+'>';
								}
								str+='레슨유형:요가';
								if(isTrainer == 0){
									str+="&nbsp &nbsp&nbsp";
									str+='<a id="buyLesson"  data-lessonlistseq="' + serverData[i].lessonlistseq + '"  href="#"><span class="badge badge-success">결제</span></a>';
								}
								str+='<p class="property-item-desc fs-15">';
								str+='30일 완성 요가코스';
								str+='</p>';
								str+='<hr /><!-- mobile and boxed separator -->';
								str+='<strong class="poperty-item-location">';
								str+='30일 완성 요가코스';
								str+='</strong>';
								str+='<hr /><!-- mobile and boxed separator -->';
								str+='<div class="property-item-btn">';
								str+='<a href="pack-realestate-property-item.html">VIEW OFFER</a>';
						
								str+='</div>';
								str+='</div>';
								str+='</div>';
								/*
								str+='<h4 class="fs-15">';
															
								str+='<span class="fs-17 mb-15 block">강좌:'+serverData[i].lessonname+'</span> </br>';
								str+='<a target="_blank" href="#">레슨타입:요가 </a> &nbsp; &nbsp; ';
								str+='<a target="_blank" href="#">강좌기간:'+serverData[i].lessoncoursedate+'일 </a>';
								str+='<a target="_blank" href="#">가격:'+serverData[i].lessonprice+'원</a>';
								str+='</h4>';
								str+='</div>';
								str+='</div>';
		
								/*
								str += "<td><div class='rating rating-1 fs-13 fw-100'><!-- rating-0 ... rating-5 --></div>"+" 추천도 : "+serverData[i].lessonrecommend+"</td>";	
								str += "<td>"+ serverData[i].lessoncoursedate+"</td>";
								str += "<td>"+ serverData[i].lessonprice+"</td>";
								str += "<td><a id='detailLesson' data-lessonlistseq=' " + serverData[i].lessonlistseq + " ' href='#'><span class='badge badge-success'>상세</span></a></td>";	
								if(isTrainer == 0){
								str += "<td><a id='buyLesson'  data-lessonlistseq=" + serverData[i].lessonlistseq + "  href='#'><span class='badge badge-success'>결제</span></a></td>";
								}
								if(isTrainer == 1){
								str += "<td> <input type='checkbox' class='deleteCheck' data-lessonname=" + serverData[i].lessonname +"  name='deleteValues' value='"+serverData[i].lessonlistseq+" ' >  </td>";
								}
								str += "</tr>";
								*/
							}		
							if(isTrainer==1){
								var temp="";
								
								temp +='<div class="property-list-opt">';
								temp +='<div class="property-list-btn hidden-xs-down">';
							
								temp +='<select class="form-control" name="list-order-option">';
								temp+='<option value="0">요가</option>';
								temp+='<option value="1">보디빌딩</option>';
								temp +='<option value="2">대표강좌</option>';
								temp+='</select>';
								temp+='</div>';
								temp+='</div>';
								temp += '<button  id="insertNewLessonlist" type="button" class="btn btn-default">강좌개설</button> ';
								temp += '<button  id="deleteLessonlist" type="button" class="btn btn-default">강좌삭제</button> ';
								
								$("#insertTable").html(temp);
							}
							
							$("#printTable").html(str);
							
					}
				})
			
		
		}
		
		/* 강좌입력 */
		function insertNewLessonlist(){
			/* channelseq를 넘겨야함 */
			var str = '';
			str += '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
			str += '<html> <head> <meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">';
			str += '<script src=" ' + 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"' + '><' + '/script>';
			str += '<script type="text/javascript">';
			str += '$(function)(){';		
			//str += '$("#submitButton").on("click",function(){';
			//str += 'opener.document.location.href="http://localhost:8888/wod/moveTrainerChannel2#"';	
			//str += 'self.close();';				
			
			str += 'location.reload(); ';
			str += '})';				
			str += '	}';
			str += '<' + '/script>';
			str += '</head>';
			str += '<body>';
			str += '<form action="insertLessonlistAndFile" method="post"> <h1> LessonList Insert </h1>';
			str += 'lessonName : <input type="test" name="lessonname" id="lessonname" ><br>';
			str += 'lessonContent : <input type="test" name="lessoncontent" id="lessoncontent" ><br> ';
			str += 'lessonType : <select name="lessontype"> <option value="">...</option> <option value="다이어트">다이어트</option> <option value="보디빌딩">보디빌딩</option> </select><br>';
			str += 'categoryName : <select name="categoryname"> <option value="">...</option> <option value="다이어트">대표운동</option> <option value="보디빌딩">선호운동</option> </select><br> ';
			str += '수강기간(lessoncoursedate) : <input type="test" name="lessoncoursedate" id="lessoncoursedate" ><br>';
			str += 'lessonPrice : <input type="test" name="lessonprice" id="lessonprice" ><br>';
			str += '<input id="submitButton" type="submit" value="등록" >';						
			str += '</form>';		
			str += '</body> </html>';
			
			
			/* insert새창 */
			var url = "";
			var wnd = window.open(url, "자식창", "width=400,height=400");  
			wnd.document.write("<html><head></head><body>"+str+"</body></html>");  
			
			window.addEventListener('message', function(event) {
			    if(event.srcElement.location.href==window.location.href){
			        /* 전달받은 event.data 를 가지고 원하는 추가 액션 수행 */
			    }
			});
			
			
		}
		
		//트레이너가 자신의 강의를 delete 함 (lessonname으로 삭제)
		function deleteLessonlist(){
			$("input[name=deleteValues]:checked").each(function() {
			  	var lessonlistseq = $(this).val();
			  	var lessonname = $(this).attr("data-lessonname");
			  	//Yes
			  	if(confirm("정말로 '" +lessonname + "'을(를) 삭제하시겠습니까? "  )){
			  		$.ajax({
				  		url : "deleteLessonlist",
				  		type : "post",
				  		data : {
				  			lessonlistseq : lessonlistseq
				  		},
				  		successs : function(serverData){
				  			
				  		}
				  		
				  	});
					alert("'" +lessonname + "' 삭제완료");
					TrainerLessonlistPrint();
				}else{
					//No					
				 	alert("삭제취소");
				}
			  	
			});
 
		}
		
		
		
		
		//프로필 불러오기    // 처음 가입하면 트레이너 채널이 있어야 불러올수있음
		function selectTrainerProfile(){
			$.ajax({
				url : "selectTrainerAndChannel",
				data : {
					trainerseq : trainerseq
				},
				success : function(serverData){
					var str = "";
					var str2="";
					$("#CommentArea").html(str2);
					str2+='<div class="col-sm-6">';
					str2+='<h4><b>Intro</b> Video</h4>';
					str2+='<video class="" preload="none" width="500" height="315" controls autoplay>';
					str2+='<source src="resources/push.mp4" type="video/mp4" />';
					str2+='</video>';
					str2+='</div>';
		
					str2+='<div class="col-sm-4">';
					str2+='<h4><b>Today</b> Hot</h4>';
					str2+='<video class="" preload="none" width="500" height="315" controls autoplay>';
					str2+='<source src="resources/sim2.mp4" type="video/mp4" />';
					str2+='</video>';
					str2+='</div>';
					
					$("#printTable2").html(str2);
					
					for(var i = 0; i<serverData.length; i++){
						str+= "<div class='shop-item-price'>";				
						$("#name").html(serverData[i].NAME);
						$("#subject").html(serverData[i].LESSONTYPE);
						$("#recs").html(serverData[i].RECOMMEND);
						$("#likes").html(serverData[i].TRAINERLIKE);
						
						str+="<c:if test='${isTrainer==0}'>";
						str+="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='ico-rounded ico-hover icon-heart' id='clickLike' title='Like' like-userseq='" +serverData[i].USERSEQ +"'></i>";
						str+="<i class='ico-rounded ico-hover icon-chat' id = 'chatBtn' UserSeq='"+serverData[i].USERSEQ+"' title='Message'></i>";
						str+="</c:if>";
						str+= "</div>";
						str+= "<hr />";
						str+= "<p>";

						str+='<div class="col-lg-12">';
						str+="<ul id='myTab' class='nav nav-tabs nav-top-border mt-80' role='tablist'>";
						str+="<li class='nav-item'><a class='nav-link active' href='#trainerComment' data-toggle='tab'>인사말</a></li>";
						str+="<li class='nav-item'><a class='nav-link' href='#review' data-toggle='tab'>REVIEW</a></li>";
						str+="<li class='nav-item'><a class='nav-link' href='#QnA' data-toggle='tab'>QnA</a></li>";
						str+="</ul>";
						str+="<div class='tab-content pt-20'>";
						str+="<div role='tabpanel' class='tab-pane active' id='trainerComment'>";
						//str+="<p>TrainerComment</p>";
						//인사말 시작
						if(isTrainer==1){  //트레이너인경우 (TrainerComment의 버튼 생성 및 수정가능)
 							str+="<div class='mb-30'>";
 							str+="<textarea name='text' id='trainerCommentText' class='form-control' rows='6' placeholder='인사말을 남겨주세요' maxlength='1000'> " + serverData[i].TRAINERCOMMENT+ "  </textarea>";
							str+="</div>";
							str+="<button id='updateTrainerCommnetButton' type='button' class='btn btn-primary'><i class='fa fa-check'></i> 수정완료</button> &nbsp; ";
							str+="<button id='cancleTrainerCommnetButton' type='button' class='btn btn-primary'><i class='fa fa-check'></i> 취소</button>";
						}else{ //트레이너가 아닌경우
							str+="<div class='mb-30'>";
							str += "<p>" + serverData[i].TRAINERCOMMENT+ " </p>";
							//str+="<textarea name='text' id='text' class='form-control' rows='6' placeholder='질문을 남겨주세요' maxlength='1000'></textarea>";
							str+="</div>";
						}
						//인사말 끝
						str+="</div>";
						str+="<div role='tabpanel' class='tab-pane fade' id='QnA'>";
						str+="<p>리뷰</p>";
						str+="</div>";
						str+="<div role='tabpanel' class='tab-pane fade' id='review'>";
						str+="<div class='block mb-60'>";
						str+="<span class='user-avatar'>";
						str+="<img class='float-left media-object' src='resources/assets/images/_smarty/avatar2.jpg' width='64' height='64' alt=''>";
						str+="</span>";
						str+="<div class='media-body'>";
						str+="<h4 class='media-heading fs-14'>";
						str+="John Doe &ndash; ";
						str+="<span class='text-muted'>June 29, 2014 - 11:23</span> &ndash;";
						str+="<span class='fs-14 text-muted'>";
						str+="<i class='fa fa-star'></i>";
						str+="<i class='fa fa-star'></i>";
						str+="<i class='fa fa-star'></i>";
						str+="<i class='fa fa-star'></i>";
						str+="<i class='fa fa-star-o'></i>";
						str+="</span>";
						str+="</h4>";
						str+="<p>Question내용</p>";
						str+="</div>";
						str+="</div>";
			
						str+="<h4 class='page-header mb-40'>Question</h4>";
						str+="<form method='post' action='#' id='form'>";
										
						str+="<div class='mb-30'>";
						str+="<textarea name='text' id='text' class='form-control' rows='6' placeholder='질문을 남겨주세요' maxlength='1000'></textarea>";
						str+="</div>";
											
						str+="<div class='product-star-vote clearfix'>";
						str+="<label class='radio float-left'>";
						str+="<input type='radio' name='product-review-vote' value='1' />";
						str+="<i></i> 1 Star";
						str+="</label>";
						str+="<label class='radio float-left'>";
						str+="<input type='radio' name='product-review-vote' value='2' />";
						str+="<i></i> 2 Stars";
						str+="</label>";
						str+="<label class='radio float-left'>";
						str+="<input type='radio' name='product-review-vote' value='3' />";
						str+="<i></i> 3 Stars";
						str+="</label>";
						str+="<label class='radio float-left'>";
						str+="<input type='radio' name='product-review-vote' value='4' />";
						str+="<i></i> 4 Stars";
						str+="</label>";
						str+="<label class='radio float-left'>";
						str+="<input type='radio' name='product-review-vote' value='5' />";
						str+="<i></i> 5 Stars";
						str+="</label>";
						str+="</div>";
						str+="<button type='submit' class='btn btn-primary'><i class='fa fa-check'></i> Send Review</button>";
						str+="</form>";
						str+='</div>';
				
					} 
					$("#printTable").html(str);
					
				}		
				
			}); 

			
		}
		
		function openChat(userseq,receiveseq){
			
			window.name = "parentForm";
			var popupX = (window.screen.width/2)-(400/2);
			var popupY= (window.screen.height/2)-(400/2);
			  var newWindow;
	
			  window.open("moveChat?sendseq="+userseq+"&receiveseq="+receiveseq,"newWindow", "width=700, height=900, scrollbar=no");
	 		
			
		};
		
		function MessageList(userseq){
			$.ajax({
				url:"selectTrainerChatRoomList",
				data:{
					userseq1:userseq
				},
				success:function(serverData){
					var str="";
					str+="<div class='table-responsive'>";
					str+="<table class='table table-hover'>";
					str+="<thead>";
					str+="<tr>";
					str+="<th></th>";
					str+="<th>대화상대</th>";
					str+="<th>메시지</th>";
					str+="<th>최근 수신/발신</th>";
					str+="</tr>";
					str+="</thead>";
					str+="<tbody>";
					for(var i = 0 ; i < serverData.length ; i++){

						str+="<tr>";
						if(serverData[i].userseq1==userseq){
							str+="<td></td>";
							str+="<td>"+serverData[i].othersname+"</td>";
							str+="<td><i class='ico-rounded ico-hover icon-chat' id = 'chatBtn' UserSeq='"+serverData[i].userseq2+"'></i></td>";
							str+="<td>"+serverData[i].recentdate+"</td>";
						}else if(serverData[i].userseq2==userseq){
							str+="<td></td>";
							str+="<td>"+serverData[i].othersname+"</td>";
							str+="<td><i class='ico-rounded ico-hover icon-chat' id = 'chatBtn' UserSeq='"+serverData[i].userseq1+"'></i></td>";
							str+="<td>"+serverData[i].recentdate+"</td>";
						}
						str+="</tr>";
					}
					str+="</tbody>";
					str+="</table>";
					str+="</div>";
					
					$("#printTable").html(str);
			
					
					
					
					
				}
				
				
				
			});
			
			
			
			
		};
		
	  

		
		
		</script>	
			
			
			
			
	</head>

	<body class="smoothscroll enable-animation">

	
	
		<div id="wrapper">

			<!-- Top Bar -->
			<div id="topBar">
				<div class="container">

					<!-- right -->
					<ul class="top-links list-inline float-right">
					
					<!-- 로그인에 따른 TEXT 출력 관리 -->
						<li class="text-welcome">
						<c:if test="${empty sessionScope.loginId }">
						<strong>손님</strong>, 지금 바로 로그인하세요!
						</c:if>
						<c:if test="${sessionScope.roleSeq==101 }">
						<strong>${sessionScope.loginId }님</strong>, 회원 로그인하셨습니다. 
						</c:if>
						<c:if test="${sessionScope.roleSeq==1001 }">
						<strong>${sessionScope.loginId }님</strong>, 트레이너 로그인하셨습니다. 
						</c:if>
						<c:if test="${sessionScope.roleSeq==1000 }">
						<strong>${sessionScope.loginId }님</strong>, 회원 로그인하셨습니다. 
						</c:if>
						<c:if test="${sessionScope.roleSeq==10001 }">
						<strong>${sessionScope.loginId }님</strong>, 관리자 로그인하셨습니다. 
						</c:if>
						</li>
						<li>
							<a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#"><i class="fa fa-user hidden-xs-down"></i> MY ACCOUNT</a>
							<ul class="dropdown-menu">
								<li><a tabindex="-1" href="moveUserPage"><i class="fa fa-history"></i> ORDER HISTORY</a></li>
								<li class="divider"></li>
								<li><a tabindex="-1" href="moveUserPage"><i class="fa fa-bookmark"></i> MY WISHLIST</a></li>
								<li><a tabindex="-1" href="moveUserPage"><i class="fa fa-edit"></i> MY REVIEWS</a></li>
								<li><a tabindex="-1" href="moveUserPage"><i class="fa fa-cog"></i> MY SETTINGS</a></li>
								<li class="divider"></li>
								<li><a tabindex="-1" href="moveUserPage"><i class="glyphicon glyphicon-off"></i> LOGOUT</a></li>
							</ul>
						</li>
						
						<!-- 로그인 유무에 따른 메뉴 변경 -->
						<li>
						<c:if test="${empty sessionScope.loginId }">
						<a href="moveLogin">LOGIN</a>
						</c:if>
						<c:if test="${!empty sessionScope.loginId }">
						<a href="Logout">LOGOUT</a>
						</c:if>
						</li>
						<li><a href="moveSignUp">REGISTER</a></li>
					</ul>

					<!-- left -->
					<ul class="top-links list-inline">
						<div class="social-icons float-right hidden-xs-down">
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook">
							<i class="icon-facebook"></i>
							<i class="icon-facebook"></i>
						</a>
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter">
							<i class="icon-twitter"></i>
							<i class="icon-twitter"></i>
						</a>
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-gplus" data-toggle="tooltip" data-placement="bottom" title="Google Plus">
							<i class="icon-gplus"></i>
							<i class="icon-gplus"></i>
						</a>
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-linkedin" data-toggle="tooltip" data-placement="bottom" title="Linkedin">
							<i class="icon-linkedin"></i>
							<i class="icon-linkedin"></i>
						</a>
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-flickr" data-toggle="tooltip" data-placement="bottom" title="Flickr">
							<i class="icon-flickr"></i>
							<i class="icon-flickr"></i>
						</a>
					</div>
					</ul>

				</div>
			</div>
			<div id="header" class="navbar-toggleable-md sticky clearfix">

				<!-- TOP NAV -->
				<header id="topNav">
					<div class="container">

						<!-- Mobile Menu Button -->
						<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
							<i class="fa fa-bars"></i>
						</button>

						<!-- BUTTONS -->
						<ul class="float-right nav nav-pills nav-second-main">

							<!-- SEARCH -->
							<li class="search">
								<a href="javascript:;">
									<i class="fa fa-search"></i>
								</a>
								<div class="search-box">
									<form action="page-search-result-1.html" method="get">
										<div class="input-group">
											<input type="text" name="src" placeholder="Search" class="form-control" />
											<span class="input-group-btn">
												<button class="btn btn-primary" type="submit">Search</button>
											</span>
										</div>
									</form>
								</div> 
							</li>
							<!-- /SEARCH -->

						</ul>
						<!-- /BUTTONS -->


						<!-- Logo -->
						<a class="logo float-left" href="home">
							<img src="resources/assets/images/_smarty/logo_dark.png" alt="" />
						</a>


						<div class="navbar-collapse collapse float-right nav-main-collapse">
							<nav class="nav-main">


								<ul id="topMain" class="nav nav-pills nav-main">
									<li class="dropdown"><!-- HOME -->
										<a class="dropdown-toggle" href="#">
											HOME
										</a>
										
									</li>
									<li class="dropdown"><!-- PAGES -->
										<a class="dropdown-toggle" href="#">
											트레이너
										</a>
										<ul class="dropdown-menu">
											<li><a href="moveApply"><i class="et-browser"></i> 트레이너 신청</a></li>
							
										</ul>	
									</li>
									<li class="dropdown active"><!-- FEATURES -->
										<a class="dropdown-toggle" href="Goboard">
											FEATURES
										</a>
																
									</li>
								
									<c:if test="${sessionScope.roleSeq==101 }">
										<li class="dropdown mega-menu">
											<a class="dropdown-toggle" href="moveCustomer">
												회원 마이페이지
											</a>
										</li>
									</c:if>
									<c:if test="${sessionScope.roleSeq==1000 }">
										<li class="dropdown mega-menu">
											<a class="dropdown-toggle" href="moveCustomer">
												회원 마이페이지
											</a>
										</li>
									</c:if>
									<c:if test="${sessionScope.roleSeq==1001 }">
											<li class="dropdown mega-menu">
												<a class="dropdown-toggle" href="#">
													트레이너 마이페이지
												</a>
										</li>
									</c:if>
									
									
									<li class="dropdown mega-menu"><!-- SHORTCODES -->
										<a class="dropdown-toggle" href="#">
											고객센터
										</a>
									</li>
									<c:if test="${sessionScope.roleSeq==10001 }">
									<li class="dropdown mega-menu">
										<a class="dropdown-toggle" href="moveAdmin">
											관리자 페이지
										</a>
									</li>
									</c:if>

								</ul>
							</nav>
						</div>

					</div>
				</header>
				<!-- /Top Nav -->
			</div>
				


			<section class="page-header" >
				<img src="resources/img/banner.png" alt="img" width="100%" height="auto">
				<div class="container">
					<!-- breadcrumbs -->
							<ol class="breadcrumb">
								<li><a href="#">배너 Edit</a></li>
								<li><a href="#">프로필 Edit</a></li>
							</ol>

				</div>
			</section>
			<!-- /PAGE HEADER -->
			<!-- -->
			

		</div>
		<!-- /wrapper -->		
			<section>
				<div class="container">			
					<div class="row">
						<!-- 상단 메뉴바 시작 -->
						<div class="col-sm-3">
							<div class="premium-thumbnail-circle">
								<a href="#">
								    <div class="spinner"></div>
								    <figure>
								    	<img src="resources/img/88.png" alt="img">
								    </figure>

								    <div class="info">
								      <div class="info-back">
								        <h3 id="name">박평순</h3>

								        <p id="subject">
								        	
								        	
								        </p>
								      </div>
								    </div>
									
							    </a>
							</div>
							
						</div>
						
						<div class="col-lg-3">	
								<i class="ico-rounded ico-hover et-heart">  </i>
								<div class="heading-title heading-line-single">
									<h4>좋아요 <span id="likes"></span></h1>
								</div> 
											
								<i class="ico-rounded ico-hover et-basket"> </i>  
								<div class="heading-title heading-line-single">
									<h4>추천 <span id ="recs"></span>
								</div> 
						</div>
						
						<div class="container">

						
								<ul class="nav nav-tabs nav-justified">
									<li class="nav-item"><a id="profileButton" class="nav-link active show" href="#" data-toggle="tab">Profile</a></li>
									<li class="nav-item"><a id="trainingListButton" class="nav-link" href="#" data-toggle="tab">강좌목록</a></li>
									 <c:if test="${isTrainer==1 }">
									<li class="nav-item"><a id="selectCustomersList" class="nav-link" href="#" data-toggle="tab">수강생목록</a></li>
									<li class="nav-item"><a id="MessageListBtn" class="nav-link" href="#" data-toggle="tab">메시지함</a></li>
									</c:if>
									
									<li class="nav-item dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Dropdown </a>
										<ul class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 44px, 0px); top: 0px; left: 0px; will-change: transform;">
											<li class="dropdown-item"><a href="#dropdown1" tabindex="-1" data-toggle="tab">@fat</a></li>
											<li class="dropdown-item"><a href="#dropdown2" tabindex="-1" data-toggle="tab">@mdo</a></li>
										</ul>
									</li>
								</ul>
								<br>
					</div>			
						<!-- 상단메뉴바 끝  -->
						
						<!--  상단 출력화면 시작 -->
		
						<!-- 하단출력화면 끝 -->					
					</div>
					<!-- 하단출력화면 끝 -->
					<section id="packages" class="alternate-2 y-2">
					<div class="container">
						<div class="row"  id="printTable2">				
						</div>
					</div>	
					</section>	
					<div id="CommentArea"></div>
					<div class="container">
					<div id="insertTable">
					</div>
					
					<div class="property-list-opt">
					
						<div class="property-list-btn hidden-xs-down">
							
							<a href="#" class="active" data-class="property-item-box">
								<i class="fa fa-th-large"></i><!-- boxed -->
							</a>

							<a href="#" data-class="">
								<i class="fa fa-bars"></i><!-- normal list -->
							</a>
						</div>			
							
					</div>
				
					<div class="row property-item-list" id="printTable">
									
					</div>

				</div>
   	
					
				</div>
			
		</section>
			
			
			<!-- / -->




	<footer id="footer" class="footer-light">
	<div class="container">

		<div class="row">
			
			<div class="col-md-8">

				<!-- Footer Logo -->
				<img class="footer-logo footer-2" src="resources/assets/images/_smarty/logo-footer.png" alt="" />

				<!-- Small Description -->
				<p>푸터라인 </p>

				<hr />

				<div class="row">
					<div class="col-md-6 col-sm-6">

						<!-- Newsletter Form -->
						<p class="mb-10">Subscribe to Our <strong>Newsletter</strong></p>

						<form action="php/newsletter.php".box-shadow- method="post">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								<input type="email" id="email" name="email" class="form-control" placeholder="Enter your Email" required="required">
								<span class="input-group-btn">
									<button class="btn btn-success" type="submit">Subscribe</button>
								</span>
							</div>
						</form>
						<!-- /Newsletter Form -->
					</div>

					<div class="col-md-6 col-sm-6 hidden-xs-down">

						<!-- Social Icons -->
						<div class="ml-50 clearfix">

							<p class="mb-10">Follow Us</p>
							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-facebook float-left" data-toggle="tooltip" data-placement="top" title="Facebook">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-twitter float-left" data-toggle="tooltip" data-placement="top" title="Twitter">
								<i class="icon-twitter"></i>
								<i class="icon-twitter"></i>
							</a>

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-gplus float-left" data-toggle="tooltip" data-placement="top" title="Google plus">
								<i class="icon-gplus"></i>
								<i class="icon-gplus"></i>
							</a>

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-linkedin float-left" data-toggle="tooltip" data-placement="top" title="Linkedin">
								<i class="icon-linkedin"></i>
								<i class="icon-linkedin"></i>
							</a>

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-rss float-left" data-toggle="tooltip" data-placement="top" title="Rss">
								<i class="icon-rss"></i>
								<i class="icon-rss"></i>
							</a>

						</div>
						<!-- /Social Icons -->

					</div>
					
				</div>

			</div>

			<div class="col-md-4">
				<h4 class="letter-spacing-1">PHOTO GALLERY</h4>
				
				<div class="footer-gallery lightbox" data-plugin-options='{"delegate": "a", "gallery": {"enabled": true}}'>
					<a href="resources/demo_files/images/1200x800/10-min.jpg">
						<img src="resources/demo_files/images/150x99/10-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="resources/demo_files/images/1200x800/19-min.jpg">
						<img src="resources/demo_files/images/150x99/19-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="resources/demo_files/images/1200x800/12-min.jpg">
						<img src="resources/demo_files/images/150x99/12-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="resources/demo_files/images/1200x800/13-min.jpg">
						<img src="resources/demo_files/images/150x99/13-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="resources/demo_files/images/1200x800/18-min.jpg">
						<img src="resources/demo_files/images/150x99/18-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="resources/demo_files/images/1200x800/15-min.jpg">
						<img src="resources/demo_files/images/150x99/15-min.jpg" width="106" height="70" alt="" />
					</a>
				</div>

			</div>

		</div>

	</div>

	<div class="copyright">
		<div class="container">
			<ul class="float-right m-0 list-inline mobile-block">
				<li><a href="#">Terms & Conditions</a></li>
				<li>&bull;</li>
				<li><a href="#">Privacy</a></li>
			</ul>
			&copy; All Rights Reserved, Company LTD
		</div>
	</div>
	</footer>
		<!-- SCROLL TO TOP -->
		<a href="#" id="toTop"></a>


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->
		<script type ="text/javascript" src="resources/videojs-contrib-hls.min.js"/></script>
	<script type ="text/javascript" src="resources/videojs-contrib-hls.js"/></script>
	<script type ="text/javascript" src="resources/video.min.js"/></script>
		
		<!-- JAVASCRIPT FILES -->
		<script>var plugin_path = 'resources/assets/plugins/'; </script>
		<script src="resources/assets/plugins/jquery/jquery-3.3.1.min.js"></script>

		<!-- REVOLUTION SLIDER -->
		<script src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
		<script src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
		<script src="resources/assets/js/view/demo.revolution_slider.js"></script>
		<script async src="resources/assets/js/view/pack_realestate.js"></script>
		<!-- SCRIPTS -->
		<script src="resources/assets/js/scripts.js"></script>
		
	</body>
</html>