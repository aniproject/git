<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Smarty - Multipurpose + Admin</title>
		
		<style>
		
		#calendar {
   height:1000px;
}
		.fc-scroller {
   overflow-y: hidden !important;
}
		
		
		
		</style>
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- REVOLUTION SLIDER -->
		<link href="resources/assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css" />

		<!-- THEME CSS -->
		<link href="resources/assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="resources/assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>

<link href='resources/calendars/fullcalendar.min.css' rel="stylesheet"/>
<link href="resources/calendars/fullcalendar.print.min.css" rel="stylesheet" media="print"/>
<script src='resources/calendars/moment.min.js'></script>
<script src='resources/calendars/jquery.min.js' type="text/javascript"></script>
<script src='resources/calendars/fullcalendar.min.js'></script>

<script>

/* 
	$(document).ready(function(){
		$("#calendar").fullCalendar({
			editable : true,
			navLinks : true,
			eventLimit : true
		});
	});
	 */
	 var today = new Date();
	 var monthDay = new Date();
	 var dd = today.getDate();
	 var mm = today.getMonth()+1; //January is 0!
	 var yyyy = today.getFullYear();
	var result =[];
	 
	 if(dd<10) {
	     dd='0'+dd
	 } 

	 if(mm<10) {
	     mm='0'+mm
	 } 

	 today = yyyy+'/'+mm+'/'+dd;
	 monthDay = yyyy+'/'+mm;
	 
	 $(document).ready(function() {
		 var memberseq = $("#memberseqVal").val();
		// var trainerCheck = $("#trainerCheck").val();
		
		 selectMealMenuList(memberseq);
		 
		 $("#chatBtn").click(function(){
			 var receiveseq = $(this).attr("userseq");
			 var userseq = $(this).attr("myseq");
			openChat(userseq,receiveseq);
			 
			 
		 });
		 
		});
	 

	 function printFoodSchedule(serverData,memberseq){
	 	
		for(var i = 0; i < serverData.length ; i++){
			
			var date = new Date(serverData[i].mealdate),
				 mnth = ("0" + (date.getMonth() + 1)).slice(-2),
			    day = ("0" + date.getDate()).slice(-2);
			  var mealdate = [date.getFullYear(), mnth, day].join("-");
		
		

			if(serverData[i].menuwhen=='breakfast'){
				result.push(
						{
							title: '아침 식단',
							check:'food',
							seq: serverData[i].mealmenuseq,
							start: mealdate,
							color:'red',
							textColor:"white",
						},	
				);
			}else if(serverData[i].menuwhen=='lunch'){
				result.push(
						{		
							title: '점심 식단',
							check:'food',
							seq: serverData[i].mealmenuseq,
							start: mealdate,
							color:'green',
							textColor:"white",
						},
				);
			}else if(serverData[i].menuwhen=='dinner'){
				result.push(
						{	
							
							title: '저녁 식단',
							check:'food',
							seq: serverData[i].mealmenuseq,
							start: mealdate,
							color:'blue',
							textColor:"white",
							
						},
						
				);
			}else if(serverData[i].menuwhen=='etc'){
				result.push(
						{	
							title: '그외 식단',
							check:'food',
							seq: serverData[i].mealmenuseq,
							start: mealdate,
							color:'gray',
							textColor:"white",
						},
				);
			}

		}
		
		selectExerciseList(memberseq,result);
		

	 };
	
	 
	function insertTraining(date){
		var memberseq = $("#memberseqVal").val();
			window.name = "parentForm";
			var popupX = (window.screen.width/2)-(400/2);
			var popupY= (window.screen.height/2)-(400/2);
			 var newWindow;
	
			window.open("moveInsertTrainingFoodForm?val="+date+"&memberseq="+memberseq,"newWindow", "width=800, height=900, scrollbar=no");


			
		};
		
		function insertMemberVideo(date){
			var memberseq = $("#memberseqVal").val();
			var trainerseq =$("#reverseTrainerUserseq").val();
			window.name = "parentForm";
			var popupX = (window.screen.width/2)-(400/2);
			var popupY= (window.screen.height/2)-(400/2);
			 var newWindow;
	
			window.open("moveInsertMemberVideo?val="+date+"&trainerseq="+trainerseq+"&memberseq="+memberseq,"newWindow", "width=800, height=900, scrollbar=no");
			
		};
		
		
		function convert(str,trainerCheck) {

			  var date = new Date(str),
			    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
			    day = ("0" + date.getDate()).slice(-2);
			  if(trainerCheck==1001){
				 insertTraining([date.getFullYear(), mnth, day].join("-"));  
			  }else{
				  insertMemberVideo([date.getFullYear(), mnth, day].join("-"));
				  
			  };
			}
		
	 	function selectMealMenuList(memberseq){
 			$.ajax({
				url:"selectMealMenuList",
				data:{
					memberseq:memberseq
				},
				success:function(serverData){
				printFoodSchedule(serverData,memberseq);
				
					
					
				}
				
				
				
			});
		};
	 	
	 	function selectExerciseList(memberseq,result){
	 	
	 		$.ajax({
				url:"selectExerciseList",
				data:{
					memberseq:memberseq
				},
				success:function(serverData){
				printExerciseSchedule(serverData,result);
				
				}
	
			});
	 		
	 		
	 	};
	 	
	 	function viewMealMenu(seq){
	 	
	 		window.name = "parentForm";
			var popupX = (window.screen.width/2)-(400/2);
			var popupY= (window.screen.height/2)-(400/2);
			  var newWindow;
	
			window.open("moveViewFood?val="+seq , "newWindow", "width=800, height=900, scrollbar=no");
	 		
	 		
	 		
	 		
	 	};
	 	
	 	function viewExercise(seq){
		 	
	 		window.name = "parentForm";
			var popupX = (window.screen.width/2)-(400/2);
			var popupY= (window.screen.height/2)-(400/2);
			  var newWindow;
	
			window.open("moveViewTraining?val="+seq , "newWindow", "width=800, height=900, scrollbar=no");
	 		
	 		
	 		
	 		
	 	};
	 	
	 	function viewMemberComment(seq){
	 		
	 		window.name = "parentForm";
			var popupX = (window.screen.width/2)-(400/2);
			var popupY= (window.screen.height/2)-(400/2);
			  var newWindow;
	
			window.open("moveViewMemberComment?val="+seq , "newWindow", "width=800, height=900, scrollbar=no");
	 		
	 		
	 	};
	 	
		 function printExerciseSchedule(serverData,result){
				for(var i = 0; i < serverData.length ; i++){
					
				
					  
					  var date = new Date(serverData[i].exercisedate),
						 mnth = ("0" + (date.getMonth() + 1)).slice(-2),
					    day = ("0" + date.getDate()).slice(-2);
					  var exercisedate = [date.getFullYear(), mnth, day].join("-");
					  
				

						result.push(
								{
									title: '운동',
									check:'ex',
									seq: serverData[i].exerciseseq,
									start: exercisedate,
									color:'black',
									textColor:"white",
								},	
						);
		

				}
				var trainerseq = $("#reverseTrainerUserseq").val();
				
				selectMemberCommentList(trainerseq,result);
				
		 };
			 			
					
			 
			 
			 function openChat(userseq,receiveseq){
					
					window.name = "parentForm";
					var popupX = (window.screen.width/2)-(400/2);
					var popupY= (window.screen.height/2)-(400/2);
					  var newWindow;
			
					  window.open("moveChat?sendseq="+userseq+"&receiveseq="+receiveseq,"newWindow", "width=700, height=900, scrollbar=no");
			 		
					
				};
				
				function selectMemberCommentList(trainerseq,result){
					 var memberseq = $("#memberseqVal").val();
					$.ajax({
						url:"selectMemberCommentList",
						data:{
							trainerseq:trainerseq,
							memberseq:memberseq
							
						},
						success:function(serverData){
						
							
							printMemberComment(serverData,result,trainerseq);
							
						}
						
						
					});
					
					
				};
				
				function printMemberComment(serverData,result,trainerseq){
				
					for(var i = 0; i < serverData.length ; i++){
						
						  var date = new Date(serverData[i].membercommentdate),
							 mnth = ("0" + (date.getMonth() + 1)).slice(-2),
						    day = ("0" + date.getDate()).slice(-2);
						  var membercommentdate = [date.getFullYear(), mnth, day].join("-");
						  
					

							result.push(
									{
										title: '회원 코멘트',
										check:'mc',
										seq: serverData[i].membercommentseq,
										start: membercommentdate,
										color:'teal',
										textColor:"white",
									},	
							);
			

					}
					
					$('#calendar').fullCalendar({
						/* 			width:750,
									height:750, */
						/* 			contentWidth:,
									contentHeight:1500, */
									header: {
										left: 'prev,next today',
										center: 'title',
										right: 'month,agendaWeek,agendaDay'
									},
									defaultDate: today,
									defaultView: 'month',
									editable: true,
									events: result
										

										
						
									,
									dayClick:  function (date,trainerCheck) {
										var trainerCheck = $("#trainerCheck").val();
											convert(date,trainerCheck);	
										
									},
									eventClick:function(info){
										if(info.check=='ex'){
											viewExercise(info.seq);
										}else if(info.check=='food'){
											viewMealMenu(info.seq);
										}else if(info.check=='mc'){
											viewMemberComment(info.seq);
										}
										
									}
									

								});
					
					
					
					
					
					
				};
	 	


	
	 
	 
</script>


</head>

	<body class="smoothscroll enable-animation">

	<div id="wrapper">
		<!-- Top Bar -->
		<div id="topBar">
			<div class="container">
				<!-- right -->
				<ul class="top-links list-inline float-right">
					<!-- 로그인에 따른 TEXT 출력 관리 -->
					<li class="text-welcome"><c:if
							test="${empty sessionScope.loginId }">
							<strong>손님</strong>, 지금 바로 로그인하세요 !
						</c:if> <c:if test="${sessionScope.roleSeq==101 }">
							<strong>${sessionScope.loginId }님</strong>, 회원 로그인했습니다. 
						</c:if> <c:if test="${sessionScope.roleSeq==1001 }">
							<strong>${sessionScope.loginId }님</strong>, 트레이너 로그인했습니다.
						</c:if> <c:if test="${sessionScope.roleSeq==1000 }">
							<strong>${sessionScope.loginId }님</strong>, 회원 로그인했습니다. 
						</c:if> <c:if test="${sessionScope.roleSeq==10001 }">
							<strong>${sessionScope.loginId }님</strong>, 관리자 로그인 했습니다.
						</c:if></li>
					<li><a class="dropdown-toggle no-text-underline"
						data-toggle="dropdown" href="#"><i
							class="fa fa-user hidden-xs-down"></i> 내 계정</a>
						<ul class="dropdown-menu">	
							<li>
							<c:if test="${empty sessionScope.loginId }">
							<a tabindex="-1" href="moveLogin"><i
									class="glyphicon glyphicon-off"></i> LOGIN</a>
							</c:if>
							<c:if test="${!empty sessionScope.loginId }">
							<a tabindex="-1" href="Logout"><i
									class="glyphicon glyphicon-off"></i> LOGOUT</a>
							</c:if>
							</li>
						</ul></li>
					<!-- 로그인 유무에 따른 메뉴 변경 -->
					<li><c:if test="${empty sessionScope.loginId }">
							<a href="moveLogin">LOGIN</a>
						</c:if> <c:if test="${!empty sessionScope.loginId }">
							<a href="Logout">LOGOUT</a>
						</c:if></li>
					<li><a href="moveSignUp">REGISTER</a></li>
				</ul>

				<!-- left -->
				<ul class="top-links list-inline">
					
				</ul>

			</div>
		</div>
		<div id="header" class="navbar-toggleable-md sticky clearfix">

			<!-- TOP NAV -->
			<header id="topNav">
				<div class="container">

					<!-- Mobile Menu Button -->
					<button class="btn btn-mobile" data-toggle="collapse"
						data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>

					
					<!-- /BUTTONS -->


					<!-- Logo -->
					<a class="logo float-left" href="home"> <img
						src="resources/assets/images/_smarty/logo.png" alt="" />
					</a>


					<div class="navbar-collapse collapse float-right nav-main-collapse">
						<nav class="nav-main">


							<ul id="topMain" class="nav nav-pills nav-main">
								<li class="dropdown">
									<!-- HOME --> <a class="dropdown-toggle noicon" href="#"> HOME </a>

								</li>
								<li
									class="dropdown mega-menu nav-animate-fadeIn nav-hover-animate hover-animate-bounceIn">
									<!-- THEMATIC --> <a class="dropdown-toggle" href="#">
										<span class="badge badge-danger float-right fs-11">New
											Trainer</span> <b>TRAINER</b>
								</a>
									<ul class="dropdown-menu dropdown-menu-clean">
										<li>
											<div class="row">

												<div class="col-md-5th">
													<ul class="list-unstyled">
														<li><span>Bodybuilding</span></li>

														<div id="bodybuildingListPrintArea"></div>
													</ul>
												</div>

												<div class="col-md-5th">
													<ul class="list-unstyled">
														<li><span>Diet</span></li>
														<div id="dietListPrintArea"></div>

													</ul>
												</div>

												<div class="col-md-5th">
													<ul class="list-unstyled">
														<li><span>Yoga & Pilates</span></li>
														<div id="yogaListPrintArea"></div>

														<li class="divider"></li>
														<li><span class="fs-11 mt-0 pb-15 pt-15 text-info">
														여러분도 트레이너에 도전하세요!</span></li>

														<li><a href="moveApply"><i class="et-browser"></i>
																트레이너 지원하기</a></li>
													
														<li class="divider"></li>
													</ul>
												</div>

												<div class="col-md-6 hidden-sm text-center">
													<div class="p-15 block">
													
													</div>
													<p
														class="menu-caption hidden-xs-down text-muted text-center"
														id="trainerName"></p>
												</div>

											</div>
										</li>
									</ul>
								</li>
						

								<c:if test="${sessionScope.roleSeq==101 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle noicon"
										href="moveCustomer"> MY PAGE </a></li>
								</c:if>
								<c:if test="${sessionScope.roleSeq==1000 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle noicon"
										href="moveCustomer"> MY PAGE </a></li>
								</c:if>
								<c:if test="${sessionScope.roleSeq==1001 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle noicon"
										href="moveTrainerChannel2"> TRAINER PAGE </a></li>
								</c:if>


								<li class="dropdown mega-menu">
									<!-- SHORTCODES --> <a class="dropdown-toggle noicon" href="goQnA">
										고객센터 </a>
								</li>
								<c:if test="${sessionScope.roleSeq==10001 }">
									<li class="dropdown mega-menu"><a class="dropdown-toggle noicon"
										href="moveAdmin"> ADMINPAGE </a></li>
								</c:if>

							</ul>
						</nav>
					</div>

				</div>
			</header>
			<!-- /Top Nav -->
		</div>



<section class="page-header">
				<div class="container">
					<c:if test="${trainerCheck!=null }">
					<h1>Personal Training Schedule     <i class='ico-rounded ico-hover icon-chat' id = 'chatBtn' myseq = "${MyTraUserseq}" userseq="${reverseUserseq}"></i></h1>
					</c:if>
					<c:if test="${trainerCheck==null }">
					<h1>Personal Training Schedule     <i class='ico-rounded ico-hover icon-chat' id = 'chatBtn' myseq = "${MycusUserseq}" userseq="${reverseTrainerUserseq}"></i></h1>
					</c:if>
					
				<!-- 	
					breadcrumbs
					<ol class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li><a href="#">Pages</a></li>
						<li class="active">Register</li>
					</ol>/breadcrumbs
 -->
				</div>
			</section>
			<!-- /PAGE HEADER -->

<center>
	<!-- 	<table>
		<tr>
		<th> -->
		<div  id="calendar" style="max-width: 1260px; margin: 0auto;"> </div>
	<!-- </th>
	</tr>
	<tr>
	<td>
	
	</td>
	</tr>
		
		</table> -->
		</center>
		
<section class="page-header">
				<div class="container">

				
<!-- 
					breadcrumbs
					<ol class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li><a href="#">Pages</a></li>
						<li class="active">Register</li>
					</ol>/breadcrumbs -->
	
	<!-- 			
					{
						id: 999,
						title: 'Repeating Event',
						start: '2019-06-09T16:00:00'
					},
					{
						id: 999,
						title: 'Repeating Event',
						start: '2019-06-16T16:00:00'
					},
					{
						title: 'Meeting',
						start: '2019-06-12T10:30:00',
						end: '2019-06-12T12:30:00'
					},
					{
						title: 'Lunch',
						start: '2019-06-12T12:00:00'
					},
					{
						title: 'HI',
						start: '2019-06-13T07:00:00'
					},
					{
						title: 'Click for Google',
						url: 'http://google.com/',
						start: '2019-06-28'
					} 
					  -->
			
				</div>
			</section>
			<!-- /PAGE HEADER -->
		<input type = "hidden" value = "${memberseq }" id = "memberseqVal"/>
		<input type = "hidden" value = "${trainerCheck }" id = "trainerCheck"/>
		<input type = "hidden" value = "${MyTraUserseq }" id = "MyTraUserseq"/>
		<input type = "hidden" value = "${reverseUserseq }" id = "reverseUserseq"/>
		<input type = "hidden" value = "${MycusUserseq }" id = "MycusUserseq"/>
		<input type = "hidden" value = "${reverseTrainerUserseq }" id = "reverseTrainerUserseq"/>
		
</body>
</html>