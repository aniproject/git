<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>배너삽입</title>
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- REVOLUTION SLIDER -->
		<link href="resources/assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css" />

		<!-- THEME CSS -->
		<link href="resources/assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="resources/assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script type="text/javascript" src="/resources/jquery.gdocsviewer.min.js"></script>
		<script src="https://sdk.amazonaws.com/js/aws-sdk-2.283.1.min.js"></script>
		<script src="resources/aws.js"></script>
		<script>
		var pageNumber=1;
		var gc=0;
		var url = "";
		function insertFiles(tagid,album){
			var date=new Date();
			var year=date.getFullYear();
			var month=date.getMonth();
			var day=date.getDay();
			var Time=date.getTime();
			var id =$("#seqname").val();
			var chid=$("#chseq").val();
			var datestr=year+"-"+month+"-"+day+"-"+Time+"-"+id;
			var fileChooser = document.getElementById(tagid);
	        var file = fileChooser.files[0];
			var oriname = file.name;
			var savname=datestr+oriname;
			var type="";
			type="배너";
			$.ajax({
				url:"insertFilesToChannel",
				data:{
					sav:savname,
					channelseq:chid,
					oriname:oriname,
					type:type
				},
				type:"POST",
				success:function(serverData){
					if(serverData=="fail"){
						alert("동영상 DB 등록 실패!");
						
					}else{
						FileUpload(savname,tagid,album);
						
					}
				}
				
				
			});
			
		};
		
		
		
	function FileUpload(savname,tagid,album)
	{		
	            var bucket = new AWS.S3({ params: { Bucket:'wodgym.tk'} });
	            var fileChooser = document.getElementById(tagid);
	            var file = fileChooser.files[0];
	            var fileName = savname;
	              var albumKey = encodeURIComponent(album) + '//';
	              var fileKey = albumKey + fileName;
	            if (file) 
	            {
	                var params = 
	                {
	                    Key: fileKey,
	                    ContentType: file.type,
	                    Body: file,
	                    ACL: 'public-read' // 접근 권한
	                };

	                bucket.putObject(params, function (err, data)
	                {
	                    alert("업로드성공");
	                    opener.parent.location.reload();
	                    window.close();                
	                });             
	            } 
	}	
			
	
	 $(function(){
		
			$("#UploadBtn").click(function(){
				insertFiles('image','banner');
			});	
			   $("#image").change(function() {
			        readURL(this);
			    });		
			});
		
			 function readURL(input) {
			        if (input.files && input.files[0]) {
			            var reader = new FileReader();
			            
			            reader.onload = function(e) {
			                $('#foo').attr('src', e.target.result);
			            }
			            reader.readAsDataURL(input.files[0]);
			        }
			    }
	
			
		
		</script>
	</head>

	<body class="smoothscroll enable-animation">	
			<section>
				<div class="container">				
					<div class="row">
					<!-- FORM -->
						<div class="col-md-9">
							<h3> 배너 <strong><em>등록</em></strong></h3>
						
							<form  method="POST" id ="TrainerApply" enctype = "multipart/form-data">
		
							<label for="contact:name">미리보기 1100x180 </label>
											<input type="file" name="imgFile" id="image" value="dataFile" required="">

							 <img id="foo"src="#" width='1100' height='180'/>

								<div class="row">
									<div class="col-md-12">
										<button type="button" class="btn btn-primary" id = "UploadBtn"><i class="fa fa-check"></i> 업로드</button>
									</div>
								</div>
							</form>					
						</div>			
					</div>
				</div>
				<input type ="hidden" value ="${channelseq}"id="chseq">		
				<input type ="hidden" value ="${sessionScope.loginSeq}" id="seqname">
			</section>
			<!-- / -->

		<!-- SCROLL TO TOP -->
		<a href="#" id="toTop"></a>


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->

			
		<!-- JAVASCRIPT FILES -->
		<script>var plugin_path = 'resources/assets/plugins/';</script>
		<script src="resources/assets/plugins/jquery/jquery-3.3.1.min.js"></script>

		<!-- REVOLUTION SLIDER -->
		<script src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
		<script src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
		<script src="resources/assets/js/view/demo.revolution_slider.js"></script>
		<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
		<!-- SCRIPTS -->
		<script src="resources/assets/js/scripts.js"></script>
		
	</body>
</html>