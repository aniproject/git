<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-US">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title> WOD'sGYM </title>

    </style> 
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

		<!-- WEB FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="resources/assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/layout.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/color_scheme/blue.css" rel="stylesheet" type="text/css" id="color_scheme" />
		<script src="https://sdk.amazonaws.com/js/aws-sdk-2.283.1.min.js"></script>
		<script src="resources/aws.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script>		

	
		$(function(){
			var exerciseseq = $("#dataValue").val();
			
	
//		printExercise(exerciseseq);
			printTraining(exerciseseq);
			
			$(document).on("click","#viewVideoBtn",function(){
				var trainingseq = $(this).attr("trainingseq");

				
				viewVideo(trainingseq);
				
			});
			
			
			
			
		});
		
	/* 	function printExercise(exerciseseq){
			
			$.ajax({
				url:"selectOneMealMenu",
				data:{
					mealmenuseq:mealmenuseq
				},
				success:function(serverData){
					
					
					
				}
				
				
				
			});
			
		}; */
		
		function printTraining(exerciseseq){
			$.ajax({
				url:"selectTrainingList",
				data:{
					exerciseseq:exerciseseq
				},
				success:function(serverData){
					var str="";
					
					str+="<div class='table-responsive'>";
					str+="<table class='table table-hover'>";
					str+="<thead>";
					str+="<tr>";
					str+="<th>운동명</th>";
					str+="<th>운동종류</th>";
					str+="<th>타겟부위</th>";
					str+="<th>횟수</th>";
					str+="<th>세트</th>";
					str+="<th>중량&시간</th>";
					str+="<th>동영상</th>";
					str+="</tr>";
					str+="</thead>";
					str+="<tbody>";
					for(var i = 0 ; i<serverData.length ; i++){
						str+="<tr>";
						str+="<td>"+serverData[i].trainingname+"</td>";
						str+="<td>"+serverData[i].trainingtype+"</td>";
						str+="<td>"+serverData[i].trainingtarget+"</td>";
						str+="<td>"+serverData[i].trainingcount+"</td>";
						str+="<td>"+serverData[i].trainingset+"</td>";
						str+="<td>"+serverData[i].weightandtime+"</td>";
						str+="<td>";
						str+="<a href='#' class='social-icon social-googleplay' id = 'viewVideoBtn' trainingseq='"+serverData[i].trainingseq+"' data-toggle='tooltip' data-placement='top' title='Video'>";
						str+="<i class='icon-googleplay'></i><i class='icon-googleplay'></i></a>";
						str+="</td>";
						str+="</tr>";
					};
					str+="</tbody>";
					str+="</table>";
					str+="</div>";
				
			
				
				
					printMemo(str, exerciseseq);
				//	$("#printArea").html(str);
				}
				
				
				
			});
			
			
		};
		
		function printMemo(str,exerciseseq){
			$.ajax({
				url:"selectOneExercise",
				data:{
					exerciseseq:exerciseseq
				},
				success:function(serverData){
					str+="<center>";
			/*  		str+="<div class='row'>";  */
		
					str+="<div class='col-md-6 col-sm-6'>";
				
					str+="<label>코멘트 *</label>";
				 	str+="</center>";
					str+="<label class='input mb-10'>";
					str+="<textarea rows='5' class='form-control word-count' data-maxlength='200' data-info='textarea-words-info' id = 'commentArea' readonly='readonly'></textarea>";
					str+="</label>";

				 	str+="</div>";
	
			/* 	 	str+="</div>"; */
				
					$("#printArea").html(str);
					$("#commentArea").val(serverData.memo);
				}
				
				
			});
			
		};
		
		 function getimgUrl2(albumName,buf)
		  {
		     var albumPhotosKey = encodeURIComponent(albumName) + '//';
		     var filebuf=[];
		     var cnt=0;
		     var photoKey;
		     var str="";
		     var comparator=albumName+'//';
		     s3.listObjects({Prefix: albumPhotosKey}, function(err, data) 
		     {
		       if (err) 
		         return alert('There was an error viewing your album: ' + err.message);
		   
		       var href = this.request.httpRequest.endpoint.href;
		       var bucketUrl = href + albumBucketName + '/';
		             
		       var photos = data.Contents.map(function(photo)
		       {
		         var photoKey = photo.Key;
		         var photoUrl = bucketUrl + encodeURIComponent(photoKey);
		        
		         imfiles[cnt]=photoUrl;
		         filebuf[cnt]=photoKey;
		         cnt++;
		        
		        }
		       );
		       var idx=0;
		       for(var i=0;i<imfiles.length;++i)
		       {
		          
		          for(var j=0;j<buf.length;++j)
		          {
		        	  
		              if(filebuf[i]==(comparator+buf[j])){
		          
		              
		             var str="";
		             str+='<video class="" preload="none" width="700" height="355" controls autoplay>';
                     str+='<source src="'+imfiles[i]+'" type="video/mp4" />';
                     str+='</video>';
		          //   alert(imfiles[i]);
		          
		             $("#videoArea").html(str);
		             /* 
		             alert(imfiles[i]); */
		              }
		          }

		       }   
		     })
		     
		     return result;
		  }
		 
		 
		 function viewVideo(trainingseq){
	
			 $.ajax({
				 url:"selectVideoFile",
				 data:{
					 trainingseq:trainingseq
				 },
				 success:function(serverData){
					 
					  var buf=[];
					  var albumName = "wod";
					  buf.push(serverData.savename);
					 getimgUrl2(albumName,buf);
					 
					 
					 
				 }
				 
			 });
			
			
		 		
		 };
		

		

		
		
		
		
		</script>
			
		
	</head>
	<!--
		.boxed = boxed version
	-->
	<body>


		<div class="padding-15">

			<div class="login-box">

				<!-- login form -->
				<form action="index.html" method="post" class="sky-form boxed">
					<header><i class="fa fa-users"></i> WOD'sGYM Menu</header>
					
					
					<fieldset style=width:1200px;>
				
				<!-- 	<div class='callout alert alert-border mt-20'> -->
					<div class='row countTo-sm text-center'>
					<div class='col-md-8 col-sm-7'>
			 		<div class='box-static box-transparent box-bordered p-30'> 
			 		
					<div class='box-title mb-30'>
					
		<!-- 			<h2 class='fs-20'>입력할 항목을 선택하세요!</h2> -->
			<!-- radio -->
				
	<!-- 				<div class='callout alert alert-border mt-100'>
					<div class='row countTo-sm text-center'>
					<div class='col-md-8 col-sm-7'> -->

					<div class='box-title mb-30'>
					<h2 class='fs-20'>${sessionScope.loginId}님 운동스케쥴</h2>
					</div>
					
					<div id = "printArea"></div>
					
				
					
		 	<!-- 		<div class='row'>
					<div class='col-md-6 col-sm-6'>
		 			<a href='#' class='btn btn-3d btn-lg btn-reveal btn-black' id = 'updateCustomers'>
		 			<i class='et-user'></i>
		 			<span>회원 정보 수정</span>
		 			</a>
		 			</div>
		 			<div class='col-md-6 col-sm-6'>
		 			<a href='#' class='btn btn-3d btn-lg btn-reveal btn-black' id = 'deleteCustomers'>
		 			<i class='et-user'></i>
		 			<span>회원 탈퇴</span>
		 			</a>
		 			</div>
		 			</div>
			
			
			
			
			
			
			
			
			
			
			
			
			
<h4>
	<input type="radio" name="radio-btn" id = "trainingRadio">
	<i> </i> 트레이닝
	<input type="radio" name="radio-btn" id = "foodRadio">
	<i> </i> 식단
</h4> -->
					
					
					</div>
					
					
				
					
					
					
					
					</div>
					
					<div id = "addMealArea"></div>
					
					<div id = "submitMealArea"></div>
					<fieldset>
					<div id = "videoArea">
		
		</div>
		</fieldset>
				<!-- 
					<form class='m-0 sky-form' action='#' method='post'>
					
				
		
					<div class='row'>
					
					<div class='col-md-6 col-sm-6'>
					<label>이메일 인증 코드를 입력하세요 *</label>
					<label class='input mb-10'>
					<i class='ico-append fa fa-user'></i>
					<input type='text' id = 'code' placeholder="Code Here">
					<b class='tooltip tooltip-bottom-right'>Code Here</b>
					</label>
					</div>
 				
				
		 			</div>
		 			
		 			<div class='row'>
		 				<div class='col-md-6 col-sm-6'>
		 			<a href='#' class='btn btn-3d btn-lg btn-reveal btn-black' id = 'emailCheckBtn'>
		 			<i class='et-user'></i>
		 			<span>인증하기</span>
		 			</a>
		 			</div>
		 			
		 			<div class='col-md-6 col-sm-6'>
		 			<a href='home' class='btn btn-3d btn-lg btn-reveal btn-black' id = 'returnHome'>
		 			<i class='et-user'></i>
		 			<span>홈으로</span>
		 			</a>
		 			</div>
		 			</div>
		 			</form> -->
		</div>
		</div>
		</div>
		</div>
<!-- 		</div> -->
	
		 			</fieldset>
					
					
					

		<!-- 	</div>

		</div> -->
	<!-- 	<div id = "exampleArea">
		
		</div>
	<fieldset style=width:1200px;>	 -->

	<!-- 	
		</fieldset>
 -->

	
		<input type = "hidden" value = "${exerciseseq }" id = "dataValue">
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'resources/assets/plugins/';</script>
		<script type="text/javascript" src="resources/assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="resources/assets/js/app.js"></script>
	</body>
</html>