<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Smarty - Multipurpose + Admin</title>
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="resources/assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL STYLE -->
		<link href="resources/assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />

		<!-- JQGRID TABLE -->
		<link href="resources/assets/plugins/jqgrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/layout-jqgrid.css" rel="stylesheet" type="text/css" />
	</head>

	<body class="smoothscroll enable-animation">




		<!-- wrapper -->
		<div id="wrapper">

			<div id="header" class="navbar-toggleable-md clearfix sticky">
			</div>

			
			<section>
				<div class="container">

				<table id="jqgrid">
				</table>
				<div id="pager_jqgrid"></div>
				<br />
				<div class="divider divider-dotted"></div>

				</div>
			</section>
			<!-- / -->

			<!-- FOOTER -->
			<footer id="footer">
				<div class="container">

					<div class="row">
						
						<div class="col-md-3">
							<!-- Footer Logo -->
							<img class="footer-logo" src="resources/assets/images/_smarty/logo-footer.png" alt="" />

							<!-- Small Description -->
							<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>

							<!-- Contact Address -->
							<address>
								<ul class="list-unstyled">
									<li class="footer-sprite address">
										PO Box 21132<br>
										Here Weare St, Melbourne<br>
										Vivas 2355 Australia<br>
									</li>
									<li class="footer-sprite phone">
										Phone: 1-800-565-2390
									</li>
									<li class="footer-sprite email">
										<a href="mailto:support@yourname.com">support@yourname.com</a>
									</li>
								</ul>
							</address>
							<!-- /Contact Address -->

						</div>

						<div class="col-md-3">

							<!-- Latest Blog Post -->
							<h4 class="letter-spacing-1">LATEST NEWS</h4>
							<ul class="footer-posts list-unstyled">
								<li>
									<a href="#">Donec sed odio dui. Nulla vitae elit libero, a pharetra augue</a>
									<small>29 June 2017</small>
								</li>
								<li>
									<a href="#">Nullam id dolor id nibh ultricies</a>
									<small>29 June 2017</small>
								</li>
								<li>
									<a href="#">Duis mollis, est non commodo luctus</a>
									<small>29 June 2017</small>
								</li>
							</ul>
							<!-- /Latest Blog Post -->

						</div>

						<div class="col-md-2">

							<!-- Links -->
							<h4 class="letter-spacing-1">EXPLORE SMARTY</h4>
							<ul class="footer-links list-unstyled">
								<li><a href="#">Home</a></li>
								<li><a href="#">About Us</a></li>
								<li><a href="#">Our Services</a></li>
								<li><a href="#">Our Clients</a></li>
								<li><a href="#">Our Pricing</a></li>
								<li><a href="#">Smarty Tour</a></li>
								<li><a href="#">Contact Us</a></li>
							</ul>
							<!-- /Links -->

						</div>

						<div class="col-md-4">

							<!-- Newsletter Form -->
							<h4 class="letter-spacing-1">KEEP IN TOUCH</h4>
							<p>Subscribe to Our Newsletter to get Important News &amp; Offers</p>

							<form class="validate" action="php/newsletter.php" method="post" data-success="Subscribed! Thank you!" data-toastr-position="bottom-right">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
									<input type="email" id="email" name="email" class="form-control required" placeholder="Enter your Email">
									<span class="input-group-btn">
										<button class="btn btn-success" type="submit">Subscribe</button>
									</span>
								</div>
							</form>
							<!-- /Newsletter Form -->

							<!-- Social Icons -->
							<div class="mt-20">
								<a href="#" class="social-icon social-icon-border social-facebook float-left" data-toggle="tooltip" data-placement="top" title="Facebook">

									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
								</a>

								<a href="#" class="social-icon social-icon-border social-twitter float-left" data-toggle="tooltip" data-placement="top" title="Twitter">
									<i class="icon-twitter"></i>
									<i class="icon-twitter"></i>
								</a>

								<a href="#" class="social-icon social-icon-border social-gplus float-left" data-toggle="tooltip" data-placement="top" title="Google plus">
									<i class="icon-gplus"></i>
									<i class="icon-gplus"></i>
								</a>

								<a href="#" class="social-icon social-icon-border social-linkedin float-left" data-toggle="tooltip" data-placement="top" title="Linkedin">
									<i class="icon-linkedin"></i>
									<i class="icon-linkedin"></i>
								</a>

								<a href="#" class="social-icon social-icon-border social-rss float-left" data-toggle="tooltip" data-placement="top" title="Rss">
									<i class="icon-rss"></i>
									<i class="icon-rss"></i>
								</a>
					
							</div>
							<!-- /Social Icons -->

						</div>

					</div>

				</div>

				<div class="copyright">
					<div class="container">
						<ul class="float-right m-0 list-inline mobile-block">
							<li><a href="#">Terms &amp; Conditions</a></li>
							<li>&bull;</li>
							<li><a href="#">Privacy</a></li>
						</ul>
						&copy; All Rights Reserved, Company LTD
					</div>
				</div>
			</footer>
			<!-- /FOOTER -->

		</div>
		<!-- /wrapper -->


		<!-- SCROLL TO TOP -->
		<a href="#" id="toTop"></a>


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->


		<!-- JAVASCRIPT FILES -->
		<script>var plugin_path = 'resources/assets/plugins/';</script>
		<script src="resources/assets/plugins/jquery/jquery-3.3.1.min.js"></script>

		<script src="resources/assets/js/scripts.js"></script>
		
		<!-- STYLESWITCHER - REMOVE -->
		<script async src="demo_files/styleswitcher/styleswitcher.js"></script>

		<!-- PAGE LEVEL SCRIPTS -->
		<script src="resources/assets/plugins/jqgrid/js/jquery.jqGrid.js"></script>
		<script src="resources/assets/plugins/jqgrid/js/i18n/grid.locale-en.js"></script>
		<script src="resources/assets/plugins/bootstrap.datepicker/js/bootstrap-datepicker.min.js"></script>
		<script>
			var jqgrid_data = [{
				id : "1",
				date : "2014-10-01",
				name : "Test 1",
				note : "Note 1",
				amount : "150.00",
				tax : "15.00",
				total : "210.00"
			}, {
				id : "2",
				date : "2014-10-02",
				name : "Test 2",
				note : "Note 2",
				amount : "220.00",
				tax : "22.00",
				total : "320.00"
			}];

			// ----------------------------------------------------------------------------------------------------
			jQuery("#jqgrid").jqGrid({
				data : jqgrid_data,
				datatype : "local",
				height : '250',
				colNames : ['Actions', 'Inv No', 'Date', 'Client', 'Amount', 'Tax', 'Total', 'Notes'],
				colModel : [
					{ name : 'act', index:'act', sortable:false }, 
					{ name : 'id', index : 'id' }, 
					{ name : 'date', index : 'sdate', editable : true, sorttype:"date",unformat: pickDate }, 
					{ name : 'name', index : 'name', editable : true }, 
					{ name : 'amount', index : 'amount', align : "right", editable : true }, 
					{ name : 'tax', index : 'tax', align : "right", editable : true }, 
					{ name : 'total', index : 'total', align : "right", editable : true }, 
					{ name : 'note', index : 'note', sortable : false, editable : true }],
				rowNum : 10,
				rowList : [10, 20, 30],
				pager : '#pager_jqgrid',
				sortname : 'id',
				toolbarfilter: true,
				viewrecords : true,
				sortorder : "asc",
				gridComplete: function(){
					var ids = jQuery("#jqgrid").jqGrid('getDataIDs');
					for(var i=0;i < ids.length;i++){
						var cl = ids[i];
						be = "<button class='btn btn-sm btn-default btn-quick' title='Edit Row' onclick=\"jQuery('#jqgrid').editRow('"+cl+"');\"><i class='fa fa-pencil'></i></button>"; 
						se = "<button class='btn btn-sm btn-default btn-quick' title='Save Row' onclick=\"jQuery('#jqgrid').saveRow('"+cl+"');\"><i class='fa fa-save'></i></button>";
						ca = "<button class='btn btn-sm btn-default btn-quick' title='Cancel' onclick=\"jQuery('#jqgrid').restoreRow('"+cl+"');\"><i class='fa fa-times'></i></button>";  
						jQuery("#jqgrid").jqGrid('setRowData',ids[i],{act:be+se+ca});
					}	
				},
				editurl : "ajax/dummy-jqtable.html",
				caption : "jqGrid with inline editing",
				multiselect : true,
				autowidth : true,
			});			
			// ----------------------------------------------------------------------------------------------------

			//enable datepicker
			function pickDate( cellvalue, options, cell ) {
				setTimeout(function(){
					jQuery(cell) .find('input[type=text]')
							.datepicker({format:'yyyy-mm-dd' , autoclose:true}); 
				}, 0);
			}

			jQuery("#jqgrid").jqGrid('navGrid', "#pager_jqgrid", {
				edit : false,
				add : false,
				del : true
			});

			jQuery("#jqgrid").jqGrid('inlineNav', "#pager_jqgrid");

			// Get Selected ID's
			jQuery("a.get_selected_ids").bind("click", function() {
				s = jQuery("#jqgrid").jqGrid('getGridParam', 'selarrrow');
				alert(s);
			});

			// Select/Unselect specific Row by id
			jQuery("a.select_unselect_row").bind("click", function() {
				jQuery("#jqgrid").jqGrid('setSelection', "13");
			});

			// Select/Unselect specific Row by id
			jQuery("a.delete_row").bind("click", function() {
				var su=jQuery("#jqgrid").jqGrid('delRowData',1);
				if(su) alert("Succes. Write custom code to delete row from server"); else alert("Already deleted or not in list");
			});


			// On Resize
			jQuery(window).resize(function() {

				if(window.afterResize) {
					clearTimeout(window.afterResize);
				}

				window.afterResize = setTimeout(function() {
					jQuery("#jqgrid").jqGrid('setGridWidth', jQuery("#middle").width() - 32);

				}, 500);

			});

			// ----------------------------------------------------------------------------------------------------

			/**
				@STYLING
			**/
			
		</script>

	</body>
</html>