<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" href="./css/common.css">
<style>
{
  margin: 0;
  padding: 0;
}
body {
  font-size: 12px;
}
.chat_list_wrap {
  list-style: none;
}
.chat_list_wrap .header {
  font-size: 14px;
  padding: 15px 0;
  background: #F18C7E;
  color: white;
  text-align: center;
  font-family: "Josefin Sans", sans-serif;
}
.chat_list_wrap .search {
  background: #eee;
  padding: 5px;
}
.chat_list_wrap .search input[type="text"] {
  width: 100%;
  border-radius: 4px;
  padding: 5px 0;
  border: 0;
  text-align: center;
}
.chat_list_wrap .list {
  padding: 0 16px;
}
.chat_list_wrap .list ul {
  width: 95%;
  list-style: none;
  margin-top: 3px;
}
.chat_list_wrap .list ul li {
  padding-top: 10px;
  padding-bottom: 10px;
  border-bottom: 1px solid #e5e5e5;
}
.chat_list_wrap .list ul li table {
  width: 100%;
}
.chat_list_wrap .list ul li table td.profile_td {
  width: 50px;
  padding-right: 11px;
}
.chat_list_wrap .list ul li table td.profile_td img {
  width: 50px;
  height: auto;
}
.chat_list_wrap .list ul li table td.chat_td .email {
  font-size: 12px;
  font-weight: bold;
}
.chat_list_wrap .list ul li table td.time_td {
  width: 90px;
  text-align: center;
}
.chat_list_wrap .list ul li table td.time_td .time {
  padding-bottom: 4px;
}
.chat_list_wrap .list ul li table td.time_td .check p {
  width: 5px;
  height: 5px;
  margin: 0 auto;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  background: #e51c23;
}


</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>

$(function(){
	var sendSeq = ${sendSeq};
	var receiveSeq = ${receiveSeq};
//	alert(sendSeq);
//	alert(receiveSeq);
	
	printChatList(sendSeq,receiveSeq);
	
	$("#insertChat").keydown(function(e){
		if(e.keyCode==13){
			
			insetChat(sendSeq,receiveSeq);
			
		};
		
	});
	
	
});


function insetChat(sendSeq,receiveSeq){
	
	$.ajax({
		url:"insertChat",
		data:{
			sendseq:sendSeq,
			receiveseq:receiveSeq,
			content:$("#insertChat").val()
		},
		type:"POST",
		success:function(serverData){
			
			if(serverData=="suc"){
				//alert("ok");
			//	alert(sendSeq);
				$("#insertChat").val("");
				$("#insertChat").focus();
				printChatList(sendSeq,receiveSeq);
	
				$('#printArea').animate( { scrollTop : $("[name=list]").eq($("[name=list]").length-1).offset().top }, 400 );
				


			}
			
		}
		
		
	});
	
	
};

function printChatList(sendSeq,receiveSeq){
	$.ajax({
		url:"selectChatList",
		data:{
			sendseq:sendSeq,
			receiveseq:receiveSeq
		},
		success:function(serverData){
			var name = serverData[0].name;
			var name2;
			for(var x = 0 ; x < serverData.length ; x++){
				if(serverData[x].name!=name){
					name2 = serverData[x].name;
				}
			};
			var str="";
			str+="<ul>";
		
			for(var i = 0 ; i < serverData.length ; i++){
				
				str+="<li>";
				str+="<table cellpadding='0' cellspacing='0'>";
				str+="<tr>";
				str+="<td class='profile_td'>";
			//	str+="<td class='profile_td'>";
				//str+="<img src='./images/profile_preview.png' />";
				str+="</td>";
				str+="<td class='chat_td'>";
				if(serverData[i].name==name){
					str+="<div class='email' style='color:red;padding:5px;'>"+serverData[i].name+"</div>";
					
				}else if(serverData[i].name==name2){
					str+="<div class='email' style='color:blue;padding:5px;'>"+serverData[i].name+"</div>";
				}
		//		str+="<div class='email'>"+serverData[i].name+"</div>";
				str+="<div class='chat_preview'><h2>"+serverData[i].content+"</h2></div>";
				str+="</td>";
				str+="<td class='time_td'>";
				str+="<div class='time'>"+serverData[i].chatdate+"</div>";
			//	str+="<div class='check'><p></p></div>";
				str+="</td>";
				str+="</tr>";
				str+="</table>";
				str+="</li>";
				
				
				
			}
			
			/* str+="<li>";
			str+="<table cellpadding='0' cellspacing='0'>";
			str+="<tr>";
			str+="<td class='profile_td'>";
			str+="<img src='./images/profile_preview.png' />";
			str+="</td>";
			str+="<td class='chat_td'>";
			str+="<div class='email'>kkotkkio@gmail.com</div>";
			str+="<div class='chat_preview'>�ȳ��ϼ���~</div>";
			str+="</td>";
			str+="<td class='time_td'>";
			str+="<div class='time'>2016.09.29 17:54</div>";
			str+="<div class='check'>";
			str+="</div>";
			str+="</td>";
			str+="</tr>";
			str+="</table>";
			str+="</li>"; */
			str+="</ul>";
			
			$("#printArea").html(str);
			
			
			
			
			
		}
		
	})
	
	
	
}


	
	 
	 
</script>


</head>

<body><div class="chat_list_wrap">
<div class="header">
WOD's GYM Message
</div>
<div class="list" id = "printArea">
<!-- <ul>
<li>
<table cellpadding="0" cellspacing="0">
<tr>
<td class="profile_td">
Img
<img src="./images/profile_preview.png" />
</td>
<td class="chat_td">
Email & Preview
<div class="email">
kkotkkio@gmail.com
</div>
<div class="chat_preview">
�ȳ��ϼ���~
</div>
</td>
<td class="time_td">
Time & Check
<div class="time">
2016.09.29 17:54
</div>
<div class="check">
<p> </p>
</div>
</td>
</tr>
</table>
</li>
<li>
<table cellpadding="0" cellspacing="0">
<tr>
<td class="profile_td">
Img
<img src="./images/profile_preview.png" />
</td>
<td class="chat_td">
Email & Preview
<div class="email">
kkotkkio@gmail.com
</div>
<div class="chat_preview">
�ȳ��ϼ���~
</div>
</td>
<td class="time_td">
Time & Check
<div class="time">
2016.09.29 17:54
</div>
<div class="check">

</div>
</td>
</tr>
</table>
</li>
</ul> -->
</div>
<div class="search">
<input type="text" id = "insertChat" placeholder="Send Message" />
</div>
</div>

</body>
</html>