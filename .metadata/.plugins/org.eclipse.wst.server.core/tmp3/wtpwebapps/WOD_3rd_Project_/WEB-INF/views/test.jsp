<!-- USER LIST -->
						<table class='table table-striped table-bordered table-hover m-top30 table-vertical-middle'>
							<thead>
								<tr>
									<th>강좌명</th>
									<th>L.Name</th>
									<th>Stars</th>
									<th>Email / Username</th>
									<th>City</th>
									<th>Progress</th>
									<th>Share</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><img src='assets/images/_smarty/female.png' alt='' width='20'></td>
									<td>John</td>
									<td>Doe</td>
									<td><div class='rating rating-1 fs-13 fw-100'><!-- rating-0 ... rating-5 --></div></td>
									<td>john.doe@yourdomain.com</td>
									<td>New York</td>
									<td>
										<div class='progress progress-xxs mb-0'>
											<div class='progress-bar progress-bar-info' role='progressbar' aria-valuenow='66' aria-valuemin='0' aria-valuemax='100' style='width: 66%; min-width: 2em;'>
												<span class='sr-only'>66% Complete</span>
											</div>
										</div>
									</td>
									<td>
										<ul class='list-inline m-0 fs-12'>
											<li><a href='#' class='icon-facebook' data-toggle='tooltip' data-placement='top' title='Facebook'></a></li>
											<li><a href='#' class='icon-twitter' data-toggle='tooltip' data-placement='top' title='Twitter'></a></li>
											<li><a href='#' class='icon-gplus' data-toggle='tooltip' data-placement='top' title='Google Plus'></a></li>
											<li><a href='#' class='icon-linkedin' data-toggle='tooltip' data-placement='top' title='Linkedin'></a></li>
											<li><a href='#' class='icon-pinterest' data-toggle='tooltip' data-placement='top' title='Pinterest'></a></li>
										</ul>
									</td>
									<td><span class='badge badge-success'>Active</span></td>
								</tr>

							</tbody>
						</table>
						<!-- /USER LIST -->

					</div>