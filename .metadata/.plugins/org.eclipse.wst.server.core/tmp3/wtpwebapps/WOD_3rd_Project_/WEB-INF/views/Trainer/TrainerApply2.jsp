<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Smarty - Multipurpose + Admin</title>
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- REVOLUTION SLIDER -->
		<link href="resources/assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css" />

		<!-- THEME CSS -->
		<link href="resources/assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="resources/assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
	</head>

	<body class="smoothscroll enable-animation">

	
	
		<!-- wrapper -->
		<div id="wrapper">

			<!-- Top Bar -->
			<div id="topBar">
				<div class="container">

					<!-- right -->
					<ul class="top-links list-inline float-right">
						<li class="text-welcome">Welcome to Smarty, <strong>John Doe</strong></li>
						<li>
							<a class="dropdown-toggle no-text-underline" data-toggle="dropdown" href="#"><i class="fa fa-user hidden-xs-down"></i> MY ACCOUNT</a>
							<ul class="dropdown-menu">
								<li><a tabindex="-1" href="moveUserPage"><i class="fa fa-history"></i> ORDER HISTORY</a></li>
								<li class="divider"></li>
								<li><a tabindex="-1" href="moveUserPage"><i class="fa fa-bookmark"></i> MY WISHLIST</a></li>
								<li><a tabindex="-1" href="moveUserPage"><i class="fa fa-edit"></i> MY REVIEWS</a></li>
								<li><a tabindex="-1" href="moveUserPage"><i class="fa fa-cog"></i> MY SETTINGS</a></li>
								<li class="divider"></li>
								<li><a tabindex="-1" href="moveUserPage"><i class="glyphicon glyphicon-off"></i> LOGOUT</a></li>
							</ul>
						</li>
						<li><a href="moveLogin">LOGIN</a></li>
						<li><a href="moveSignUp">REGISTER</a></li>
					</ul>

					<!-- left -->
					<ul class="top-links list-inline">
						<div class="social-icons float-right hidden-xs-down">
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook">
							<i class="icon-facebook"></i>
							<i class="icon-facebook"></i>
						</a>
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter">
							<i class="icon-twitter"></i>
							<i class="icon-twitter"></i>
						</a>
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-gplus" data-toggle="tooltip" data-placement="bottom" title="Google Plus">
							<i class="icon-gplus"></i>
							<i class="icon-gplus"></i>
						</a>
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-linkedin" data-toggle="tooltip" data-placement="bottom" title="Linkedin">
							<i class="icon-linkedin"></i>
							<i class="icon-linkedin"></i>
						</a>
						<a href="#" class="social-icon social-icon-sm social-icon-transparent social-flickr" data-toggle="tooltip" data-placement="bottom" title="Flickr">
							<i class="icon-flickr"></i>
							<i class="icon-flickr"></i>
						</a>
					</div>
					</ul>

				</div>
			</div>
			<div id="header" class="navbar-toggleable-md sticky clearfix">

				<!-- TOP NAV -->
				<header id="topNav">
					<div class="container">

						<!-- Mobile Menu Button -->
						<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
							<i class="fa fa-bars"></i>
						</button>

						<!-- BUTTONS -->
						<ul class="float-right nav nav-pills nav-second-main">

							<!-- SEARCH -->
							<li class="search">
								<a href="javascript:;">
									<i class="fa fa-search"></i>
								</a>
								<div class="search-box">
									<form action="page-search-result-1.html" method="get">
										<div class="input-group">
											<input type="text" name="src" placeholder="Search" class="form-control" />
											<span class="input-group-btn">
												<button class="btn btn-primary" type="submit">Search</button>
											</span>
										</div>
									</form>
								</div> 
							</li>
							<!-- /SEARCH -->

						</ul>
						<!-- /BUTTONS -->


						<!-- Logo -->
						<a class="logo float-left" href="home">
							<img src="resources/assets/images/_smarty/logo_dark.png" alt="" />
						</a>


						<div class="navbar-collapse collapse float-right nav-main-collapse">
							<nav class="nav-main">


								<ul id="topMain" class="nav nav-pills nav-main">
									<li class="dropdown"><!-- HOME -->
										<a class="dropdown-toggle" href="#">
											HOME
										</a>
										
									</li>
									<li class="dropdown"><!-- PAGES -->
										<a class="dropdown-toggle" href="#">
											PAGES
										</a>
										
									</li>
									<li class="dropdown active"><!-- FEATURES -->
										<a class="dropdown-toggle" href="#">
											FEATURES
										</a>										
									</li>
									<li class="dropdown mega-menu"><!-- PORTFOLIO -->
										<a class="dropdown-toggle" href="#">
											PORTFOLIO
										</a>
										
									</li>
									<li class="dropdown mega-menu"><!-- SHORTCODES -->
										<a class="dropdown-toggle" href="#">
											SHORTCODES
										</a>
									</li>

								</ul>
							</nav>
						</div>

					</div>
				</header>
				<!-- /Top Nav -->
			</div>

<section class="page-header page-header-xs">
				<div class="container text-right">

					<!-- breadcrumbs -->
					<ol class="breadcrumb  breadcrumb-inverse">
						<li><a href="#">Home</a></li>
						<li><a href="#">Pages</a></li>
						<li class="active">Apply</li>
					</ol><!-- /breadcrumbs -->

				</div>
			</section>
			<!-- /PAGE HEADER -->



			<!-- -->
			<section>
				<div class="container">
					
					<div class="row">

						<!-- FORM -->
						<div class="col-md-9">

							<h3>트레이너 <strong><em>신청</em></strong></h3>
							<div id="alert_success" class="alert alert-success mb-30">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<strong>Thank You!</strong> Your message successfully sent!
							</div><!-- /Alert Success -->


							<!-- Alert Failed -->
							<div id="alert_failed" class="alert alert-danger mb-30">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<strong>[SMTP] Error!</strong> Internal server error!
							</div><!-- /Alert Failed -->


							<!-- Alert Mandatory -->
							<div id="alert_mandatory" class="alert alert-danger mb-30">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<strong>Sorry!</strong> You need to complete all mandatory (*) fields!
							</div><!-- /Alert Mandatory -->


							<form action="#" method="post" enctype="multipart/form-data">
								<fieldset>
									<input type="hidden" name="action" value="contact_send" />
									<div class="row">
										<div class="col-md-4">
											<label for="contact:name">이름 *</label>
											<input required type="text" value="" class="form-control" name="contact[name][required]" id="contact:name">
										</div>
										<div class="col-md-4">
											<label for="contact:email">이메일 *</label>
											<input required type="email" value="" class="form-control" name="contact[email][required]" id="contact:email">
										</div>
										<div class="col-md-4">
											<label for="contact:phone">전화번호</label>
											<input type="text" value="" class="form-control" name="contact[phone]" id="contact:phone">
										</div>
									</div>
									<div class="row">
										<div class="col-md-8">
											<label for="contact:subject">주제 *</label>
											<input required type="text" value="" class="form-control" name="contact[subject][required]" id="contact:subject">
										</div>
										<div class="col-md-4">
											<label for="contact_department">근무지</label>
											<select class="form-control pointer" name="contact[department]">
												<option value="">--- Select ---</option>
												<option value="Marketing">Marketing</option>
												<option value="Webdesign">Webdesign</option>
												<option value="Architecture">Architecture</option>
											</select>
										</div>
									</div>
									<div class="clearfix">
										<label for="contact:message">메세지 *</label>
										<textarea required maxlength="10000" rows="8" class="form-control" name="contact[message]" id="contact:message"></textarea>
									</div>
									<div class="clearfix mt-30">
										<label for="contact:attachment">파일 첨부</label>

										<!-- custom file upload -->
										<input class="custom-file-upload" type="file" id="file" name="contact[attachment]" id="contact:attachment" data-btn-text="파일 선택" />
										<small class="text-muted block">Max file size: 10Mb (zip/pdf/jpg/png)</small>
									</div>

								</fieldset>

								<div class="row">
									<div class="col-md-12">
										<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> 트레이너 신청</button>
									</div>
								</div>
							</form>

						</div>
						<!-- /FORM -->


						<!-- INFO -->
						<div class="col-md-3">

							<h2>WOD 연락처</h2>

							<p>
								Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.
							</p>

							<hr />

							<p>
								<span class="block"><strong><i class="fa fa-map-marker"></i> Address:</strong> Street Name, City Name, Country</span>
								<span class="block"><strong><i class="fa fa-phone"></i> Phone:</strong> <a href="tel:1800-555-1234">1800-555-1234</a></span>
								<span class="block"><strong><i class="fa fa-envelope"></i> Email:</strong> <a href="mailto:mail@yourdomain.com">mail@yourdomain.com</a></span>
							</p>

							<hr />

							<h4 class="font300">Business Hours</h4>
							<p>
								<span class="block"><strong>Monday - Friday:</strong> 10am to 6pm</span>
								<span class="block"><strong>Saturday:</strong> 10am to 2pm</span>
								<span class="block"><strong>Sunday:</strong> Closed</span>
							</p>

						</div>
						<!-- /INFO -->

					</div>

				</div>
			</section>
			<!-- / -->
	<footer id="footer" class="footer-light">
	<div class="container">

		<div class="row">
			
			<div class="col-md-8">

				<!-- Footer Logo -->
				<img class="footer-logo footer-2" src="assets/images/_smarty/logo-footer.png" alt="" />

				<!-- Small Description -->
				<p>푸터라인 </p>

				<hr />

				<div class="row">
					<div class="col-md-6 col-sm-6">

						<!-- Newsletter Form -->
						<p class="mb-10">Subscribe to Our <strong>Newsletter</strong></p>

						<form action="php/newsletter.php".box-shadow- method="post">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								<input type="email" id="email" name="email" class="form-control" placeholder="Enter your Email" required="required">
								<span class="input-group-btn">
									<button class="btn btn-success" type="submit">Subscribe</button>
								</span>
							</div>
						</form>
						<!-- /Newsletter Form -->
					</div>

					<div class="col-md-6 col-sm-6 hidden-xs-down">

						<!-- Social Icons -->
						<div class="ml-50 clearfix">

							<p class="mb-10">Follow Us</p>
							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-facebook float-left" data-toggle="tooltip" data-placement="top" title="Facebook">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-twitter float-left" data-toggle="tooltip" data-placement="top" title="Twitter">
								<i class="icon-twitter"></i>
								<i class="icon-twitter"></i>
							</a>

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-gplus float-left" data-toggle="tooltip" data-placement="top" title="Google plus">
								<i class="icon-gplus"></i>
								<i class="icon-gplus"></i>
							</a>

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-linkedin float-left" data-toggle="tooltip" data-placement="top" title="Linkedin">
								<i class="icon-linkedin"></i>
								<i class="icon-linkedin"></i>
							</a>

							<a href="#" class="social-icon social-icon-sm social-icon-transparent social-rss float-left" data-toggle="tooltip" data-placement="top" title="Rss">
								<i class="icon-rss"></i>
								<i class="icon-rss"></i>
							</a>

						</div>
						<!-- /Social Icons -->

					</div>
					
				</div>

			</div>

			<div class="col-md-4">
				<h4 class="letter-spacing-1">PHOTO GALLERY</h4>
				
				<div class="footer-gallery lightbox" data-plugin-options='{"delegate": "a", "gallery": {"enabled": true}}'>
					<a href="resources/demo_files/images/1200x800/10-min.jpg">
						<img src="resources/demo_files/images/150x99/10-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="demo_files/images/1200x800/19-min.jpg">
						<img src="resources/demo_files/images/150x99/19-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="demo_files/images/1200x800/12-min.jpg">
						<img src="resources/demo_files/images/150x99/12-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="demo_files/images/1200x800/13-min.jpg">
						<img src="resources/demo_files/images/150x99/13-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="demo_files/images/1200x800/18-min.jpg">
						<img src="resources/demo_files/images/150x99/18-min.jpg" width="106" height="70" alt="" />
					</a>
					<a href="demo_files/images/1200x800/15-min.jpg">
						<img src="resources/demo_files/images/150x99/15-min.jpg" width="106" height="70" alt="" />
					</a>
				</div>

			</div>

		</div>

	</div>

	<div class="copyright">
		<div class="container">
			<ul class="float-right m-0 list-inline mobile-block">
				<li><a href="#">Terms & Conditions</a></li>
				<li>&bull;</li>
				<li><a href="#">Privacy</a></li>
			</ul>
			&copy; All Rights Reserved, Company LTD
		</div>
	</div>
	</footer>
		<!-- SCROLL TO TOP -->
		<a href="#" id="toTop"></a>


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->

			
		<!-- JAVASCRIPT FILES -->
		<script>var plugin_path = 'resources/assets/plugins/';</script>
		<script src="resources/assets/plugins/jquery/jquery-3.3.1.min.js"></script>

		<!-- REVOLUTION SLIDER -->
		<script src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
		<script src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
		<script src="resources/assets/js/view/demo.revolution_slider.js"></script>

		<!-- SCRIPTS -->
		<script src="resources/assets/js/scripts.js"></script>
		
	</body>
</html>