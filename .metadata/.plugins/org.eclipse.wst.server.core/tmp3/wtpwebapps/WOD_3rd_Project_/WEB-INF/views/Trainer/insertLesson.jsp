<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>강의등록</title>
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

		<!-- WEB FONTS : use %7C instead of | (pipe) -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

		<!-- REVOLUTION SLIDER -->
		<link href="resources/assets/plugins/slider.revolution/css/extralayers.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/plugins/slider.revolution/css/settings.css" rel="stylesheet" type="text/css" />

		<!-- THEME CSS -->
		<link href="resources/assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/layout.css" rel="stylesheet" type="text/css" />

		<!-- PAGE LEVEL SCRIPTS -->
		<link href="resources/assets/css/header-1.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script type="text/javascript" src="/resources/jquery.gdocsviewer.min.js"></script>
		<script src="https://sdk.amazonaws.com/js/aws-sdk-2.283.1.min.js"></script>
		<script src="resources/aws.js"></script>
		<script>
		var pageNumber=1;
		var gc=0;
		var url = "";
		var imgseq=0;
		var vedioseq=0;
		function insertFiles(tagid,album){
			var date=new Date();
			var year=date.getFullYear();
			var month=date.getMonth();
			var day=date.getDay();
			var Time=date.getTime();
			var id =$("#seqname").val();
			var chid=$("#chseq").val();
			
			var datestr=year+"-"+month+"-"+day+"-"+Time+"-"+id;
			var fileChooser = document.getElementById(tagid);
	        var file = fileChooser.files[0];
			var oriname = file.name;
			var savname=datestr+oriname;
			var type="";
			if(album=="wod")
				type="최신강의";
			if(album=="wodimg")
				type="섬네일";
			
			$.ajax({
				url:"insertFilesToChannel",
				data:{
					sav:savname,
					channelseq:chid,
					oriname:oriname,
					type:type
				},
				type:"POST",
				success:function(serverData){
					
					if(serverData=="fail"){
						alert("동영상 DB 등록 실패!");
						
					}else{
					if(album=='wodimg')
					{
						selectCur(0);
				
					}
					if(album=='wod')
						selectCur(1);
					FileUpload(savname,tagid,album);
					
					}
				}
				
				
			});
			return new Promise(function (resolve) 
			{
						setTimeout(function()			
						{
							resolve(111)
						 }, 300)
			})
		};
		
	async function work() 
	{		
		await insertFiles('image','wodimg');
		await insertFiles('video','wod');
	}	
	function selectCur(num)
	{
		
		$.ajax({
			url:"selectCurval",
			type:"POST",
			success:function(serverData){
		
				if(num==0)
				{				
					imgseq=serverData;
					var temp='<input type ="hidden" name="imgfile" value="'+imgseq +'">';
			
					
					document.getElementById('lesson').innerHTML+=temp;
				}
				if(num==1)
				{
					videoseq=serverData;
					
					var temp='<input type ="hidden" name="videofile" value="'+videoseq +'">';
	
					document.getElementById('lesson').innerHTML+=temp;
				}
				
			}
			
			
		});
	}
	function FileUpload(savname,tagid,album)
	{		
	            var bucket = new AWS.S3({ params: { Bucket:'wodgym.tk'} });
	            var fileChooser = document.getElementById(tagid);
	            var file = fileChooser.files[0];
	            var fileName = savname;
	              var albumKey = encodeURIComponent(album) + '//';
	              var fileKey = albumKey + fileName;
	              
	            if (file) 
	            {
	                var params = 
	                {
	                    Key: fileKey,
	                    ContentType: file.type,
	                    Body: file,
	                    ACL: 'public-read' // 접근 권한
	                };
	               
	                var request = s3.putObject(params);
	                request.on('httpUploadProgress', function (evt) {
	                	 
	                	  var a=parseInt((evt.loaded * 100))/	 parseInt(evt.total);
	                	  var number=parseInt(a);
	                	  var temp=number;
	                	  number+='%';
	                	  var str="";
	                	  str+='<div class="progress-bar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"  style="width:'+number+';">';
	                	  str+=number;
	                	  str+='</div>';
						  if(temp>=100)
							  str+="<br><p>업로드 완료!</p>";
	                	  $("#progressing").html(str);
	 
	                });
	                request.send();
	                
	                
	                       
	            } 
	}	
			
	
	 $(function(){
			
			$("#UploadBtn").click(function(){
				
				if($("#image").val()==""){
					alert("이미지를 업로드 해주세요.");
				}else if($("#video").val()==""){
					alert("비디오를 업로드해주세요.");
				}
				else
				{
					
					work();
					$("#UploadBtn").prop("disabled",true);
			//		insertFiles('image','wodimg');
			//		insertFiles('video','wod');	
				}
			});
			   $("#image").change(function() {
			        readURL(this);
			    });	
			   $("#video").change(function() {
				   readURL2(this);
			    });	
			   $("#insertform").click(function(){
				   $("#lessonform").submit();
				   if($("#image").val()!="" && $("#video").val()!="" &&  $("#lessonname").val()!="" && $("#lessontype").val()!="" &&  $("#lessoncontent").val()!=""&& $("#lessoncontent").val()!="")
				   {
				   		opener.parent.location.reload();
                   		window.close();  
				   }
			   });
			});
			
			function readURL(input) {
			        if (input.files && input.files[0]) {
			            var reader = new FileReader();
			            
			            reader.onload = function(e) {
			                $('#foo').attr('src', e.target.result);
			            }
			            reader.readAsDataURL(input.files[0]);
			        }
			  }
	
			 function readURL2(input) {
			        if (input.files && input.files[0]) {
			            var reader = new FileReader();            
			            reader.onload = function(e) {;
			                $('#foo2').attr('src', e.target.result);
			            }
			            reader.readAsDataURL(input.files[0]);
			        }
			    }
		
		</script>
	</head>

		<body>	
			<section class="page-header page-header-xs">
			<div class="container text-right">
				</div>
				<div class="container">				
					<div class="row">
					<!-- FORM -->
						
						<div class="col-md-12">
							<div class="heading-title heading-border mb-60">
							<h2>
								<span><span data-speed="2000"> 
									강의 등록
								</h2>
								<p class="font-lato fs-17">강의 등록하기</p>
							</div>				
						</div>
						
						<div class="col-md-12 col-sm-9">

						<!-- Useful Elements -->
						<div class="card card-default">
							<div class="card-heading card-heading-transparent">
								<h2 class="card-title">강의 등록</h2>
							</div>
 
							<div class="card-block">
								<form method="POST" id ="FileUpload" enctype = "multipart/form-data">
								<div class="row">
								
											<div class="col-md-6 col-sm-6">
												<h3> 섬네일 <strong><em>등록</em></strong></h3>
												<label for="contact:name">미리보기 300x200 </label>
												<input type="file"  id="image" value="dataFile" required="">
												<img id="foo"src="#" width='300' height='200'/>											
												<!-- custom file upload -->		
												<small class="text-muted block">Max file size: 10Mb (zip/pdf/jpg/png)</small>
												
											</div>
											<div class="col-md-6 col-sm-6">
												<h3> 동영상 <strong><em>등록</em></strong></h3>
													<input type="file" id="video"  value="dataFile" required="">	
												<!-- custom file upload -->		
												<small class="text-muted block">Max file size: 10Mb (mp4)</small>
												<!-- progress , bar, percent를 표시할 div 생성한다. -->
													
												<div class="progress" id="progressing">
    													<div class="progress-bar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"  style="width: 0%;">
    													<div class="percent">0%</div>
												</div>
												<div id="status"></div>
											</div>
											<button type="button" class="btn btn-primary" id = "UploadBtn"><i class="fa fa-check"></i> 업로드</button>
										</div>
									
										
								</form>
								<form class="validate"  id="lessonform" action="insertLessonlistAndFile" method="post" >
									<fieldset id="lesson">
										
										<input type="hidden" name="action" value="contact_send" />
										<div class="row">
											<div class="col-md-12 col-sm-12">
												<label>강의명</label>
												<input type="text" name="lessonname" id="lessonname" value="" class="form-control required">
											</div>
											
										</div>
										<div class="row">
											<div class="col-md-6 col-sm-6">
												<label for="contact:name">강의유형 </label>
												<select  id ="lessontype" name="lessontype" class="form-control pointer required"> <option value="">...</option> <option value="다이어트">다이어트</option> <option value="보디빌딩">보디빌딩</option> </select>			
											</div>
											<div class="col-md-6 col-sm-6">
													<label for="contact:name">강의타입 </label>
													<select  id="categoryname" class="form-control pointer required" name="categoryname"> <option value="">...</option> <option value="다이어트">대표운동</option> <option value="보디빌딩">선호운동</option> </select>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6 col-sm-6">
												<label>가격</label>
												<input class="form-control required" type="text" name="lessonprice" id="lessonprice" >
											</div>
											<div class="col-md-6 col-sm-6">
												<label>강의기간</label><input class="form-control required" type="number" name="lessoncoursedate" id="lessoncoursedate" >
											</div>
										</div>

										<div class="row">
											<div class="col-md-12 col-sm-12">		 	
												<label>강의내용</label>
												<textarea name="lessoncontent" id="lessoncontent" rows="4" class="form-control required"></textarea>
											</div>
										</div>
										
										
										
										
										
										<!-- <button type="button" class="btn btn-primary" id = "UploadBtn"><i class="fa fa-check"></i> 업로드</button> -->
										
										
									</fieldset>
									<button  id="insertform" type="button" class="btn btn-primary" ><i class="fa fa-check"></i> 등록</button>
									</form>	
									</div>
				
								</div>

							</div>
						</div>
						<!-- /Useful Elements -->


				</div>
			</section>			
	
				<input type ="hidden" value ="${channelseq}"id="chseq">		
				<input type ="hidden" value ="${sessionScope.loginSeq}" id="seqname">

			<!-- / -->

		<!-- SCROLL TO TOP -->
		<a href="#" id="toTop"></a>


		<!-- PRELOADER -->
		<div id="preloader">
			<div class="inner">
				<span class="loader"></span>
			</div>
		</div><!-- /PRELOADER -->

			
		<!-- JAVASCRIPT FILES -->
		<script>var plugin_path = 'resources/assets/plugins/';</script>
		<script src="resources/assets/plugins/jquery/jquery-3.3.1.min.js"></script>

		<!-- REVOLUTION SLIDER -->
		<script src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js"></script>
		<script src="resources/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js"></script>
		<script src="resources/assets/js/view/demo.revolution_slider.js"></script>
		<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
		<!-- SCRIPTS -->
		<script src="resources/assets/js/scripts.js"></script>
		
	</body>
</html>