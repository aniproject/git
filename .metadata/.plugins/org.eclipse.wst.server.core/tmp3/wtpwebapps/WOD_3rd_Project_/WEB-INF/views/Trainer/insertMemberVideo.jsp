<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-US">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title> WOD'sGYM </title>

    </style> 
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

		<!-- WEB FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="resources/assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/layout.css" rel="stylesheet" type="text/css" />
		<link href="resources/assets/css/color_scheme/blue.css" rel="stylesheet" type="text/css" id="color_scheme" />
		<script src="https://sdk.amazonaws.com/js/aws-sdk-2.283.1.min.js"></script>
		<script src="resources/aws.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script>		

	
		$(function(){
			
			var trainerseq = $("#trainerseqVal").val();
			var date = $("#dateValue").val();
			var memberseq = $("#memberseqVal").val();
			
		
			
			$("#fileButton").click(function(){
				
				
				var comment = $("#memberComment").val();
				insertMemberComment(date,trainerseq,comment,memberseq);
			
				
				
				
			});
			
			
			
			
			
		});
		
		function insertMemberVideo(date,trainerseq,memberseq){
			
			 var fileChooser = document.getElementById('fileOne');
		        var file = fileChooser.files[0];
				var oriname = file.name;
			
				$.ajax({
					url:"insertMemberVideo",
					data:{
						membercommentdate:date,
						trainerseq:trainerseq,
						oriname:oriname,
						memberseq:memberseq
					},
					type:"POST",
					success:function(serverData){
						if(serverData=="fail"){
							alert("동영상 DB 등록 실패!");
							
						}else{
					
							videoUpload(serverData);
							
							
						}
					}
					
					
				});
				
			
			
			
		};
		
		function videoUpload(serverData){
		
			
            var bucket = new AWS.S3({ params: { Bucket:'wodgym.tk'} });
            var fileChooser = document.getElementById('fileOne');
            var file = fileChooser.files[0];
            var fileName = serverData;
              var albumKey = encodeURIComponent('wod') + '//';
              var fileKey = albumKey + fileName;
          
            if (file) {
                var params = {
                    Key: fileKey,
                    ContentType: file.type,
                    Body: file,
                    ACL: 'public-read' // 접근 권한
                };

                bucket.putObject(params, function (err, data) {
                    alert("비디오 등록에 성공하셨습니다!");
                
                    opener.parent.location.reload();
        			window.close();
                    
                });             
            } return false;
		};
		
		
		
		function insertMemberComment(date,trainerseq,comment,memberseq){
			
			$.ajax({
				url:"insertMemberComment",
				data:{
					membercommentdate:date,
					trainerseq:trainerseq,
					comments:comment,
					memberseq:memberseq
					
				},
				type:"POST",
				success:function(serverData){
					if(serverData=="suc"){
						insertMemberVideo(date,trainerseq,memberseq);
						
						
						
					};
					
					
				}
				
				
			});
			
			
			
			
		};
		
		
		
		
		</script>
			
		
	</head>
	<!--
		.boxed = boxed version
	-->
	<body>


		<div class="padding-15">

			<div class="login-box">

				<!-- login form -->
				<form action="index.html" method="post" class="sky-form boxed">
					<header><i class="fa fa-users"></i> WOD'sGYM Customer Menu</header>
					
					
					<fieldset style=width:1200px;>
				
				<!-- 	<div class='callout alert alert-border mt-20'> -->
					<div class='row countTo-sm text-center'>
					<div class='col-md-8 col-sm-7'>
			 		<div class='box-static box-transparent box-bordered p-30'> 
			 		
					<div class='box-title mb-30'>
					
		<!-- 			<h2 class='fs-20'>입력할 항목을 선택하세요!</h2> -->
			<!-- radio -->
				
	<!-- 				<div class='callout alert alert-border mt-100'>
					<div class='row countTo-sm text-center'>
					<div class='col-md-8 col-sm-7'> -->

					<div class='box-title mb-30'>
					<h2 class='fs-20'>${sessionScope.loginId}님 운동 동영상 업로드</h2>
					</div>
					<div class="fancy-form">
					<textarea rows="5" id = "memberComment" class="form-control word-count" data-maxlength="200" data-info="textarea-words-info" placeholder="Text Here"></textarea>
	
				
					<span class="fancy-hint fs-11 text-muted">
					<strong>Notice!</strong> 더욱 자세한 상담을 원하시면 캘린더 상단의 1:1 상담 기능을 이용하세요!
					<span class="float-right">
					<span id="textarea-words-info">200</span> Words
					</span>
					</span>
					</div>
					
					<form id='fileUploadForm' method='post' enctype='multipart/form-data'>
					<input type='file' name='file' id='fileOne' value='dataFile' required=''>
					<input type='button' class='btn btn-success' id='fileButton' value='등록' />
					</form>
				
					<div id = "printArea"></div>
					
				
					
					</div>
					
					
				
					
					
					
					
					</div>
					
					<div id = "addMealArea"></div>
					
					<div id = "submitMealArea"></div>
					<fieldset>
					<div id = "videoArea">
		
		</div>
		</fieldset>
				
		</div>
		</div>
		</div>
		</div>

	
		 			</fieldset>
					
					
				

		<input type = "hidden" value = "${date }" id = "dateValue">
		<input type = "hidden" value = "${trainerseq }" id = "trainerseqVal">
		<input type = "hidden" value = "${memberseq }" id = "memberseqVal">
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'resources/assets/plugins/';</script>
		<script type="text/javascript" src="resources/assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="resources/assets/js/app.js"></script>
	</body>
</html>